@echo: System information
C:\mingw-w64\mingw64\bin\g++ --version

@echo: RUN THE INDIVIDUAL BATCH FILES INSTEAD: run-winres; run-make
pause
goto exit

@echo: Press enter to compile the version resource
pause

C:\mingw-w64\mingw64\bin\windres -i resource.rc -o resource.o

@echo: Press enter to run minGW32 Make
pause

C:\mingw-w64\mingw64\bin\mingw32-make

@echo: Done
pause
:exit
