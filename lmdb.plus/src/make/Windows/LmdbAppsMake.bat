@echo: System information
C:\mingw-w64\mingw64\bin\g++ --version

@echo: Copy folder liblmdb to liblmdbmake where files are to be compiled
pause

@echo: Press enter to run minGW32 Make
pause

:: C:\mingw-w64\mingw64\bin\mingw32-make -help

C:\mingw-w64\mingw64\bin\mingw32-make --directory=..\..\liblmdbmake

@echo: Press enter to get list of EXE files
pause
dir ..\..\liblmdbmake\*.exe

@echo: Done
pause

