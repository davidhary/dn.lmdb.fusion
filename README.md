# ISR LMDB Fusion Libraries

.NET libraries for Open LDAP Lightning Memory-mapped Database (LMDB).
ISR's LMDB Fusion libraries are amalgams of the work of a few [Authors](#Authors) as described 
in [Source Code Adaptations](#Adaptations).

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Source Code Adaptations](#Adaptations)
* [Facilitated By](#FacilitatedBy)
* [Repository Owner](#Repository-Owner)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Resources](#Resources)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
* [LMDB Fusion] - LMDB Fusion
```
git clone git@bitbucket.org:davidhary/dn.lmdb.fusion.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%vslib%\data\lmdb
```

where %vslib% is the root folder of the .NET libraries, e.g., %my%\lib\vs 
and %my% is the root folder of the .NET solutions


#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

<a name="Adaptations"></a>
## Source code adaptations

### LmdbPlus

This folder includes code for building the modified [LMDB Library] DLL from the C
source code:

* The [LMDB Library] files are in the liblmdb sub-folder.
* The [Spreads LMDB] files are in the Spreads sub-folder.
* The combined library was renamed form libLmdb and linspreads_lmdb to libLmdbPlus. 
* A resource version file was added.
* Make batch files were added tot eh WIndows sub-folder.

#### Notes
* Building the DLL with minGW32-make reports warnings. 

### [LMDB Fusion]

This project fuses the works of the [Authors](#Authors).

* Using [Framework Design Guidelines] with adherence to [Roslyn Code Analysis] rules as defined in the editor configuration file in the [IDE Repository]. 
* Transition to .NET5
* Making all native calls internal and declared as Safe Native Method classes. 
* Using Safe Handles.
* Using Integer type return codes.
* Augmenting the LMDB Extension.
* Using event handles as in [Lightning.NET] for managing the relationships between the LMDB objects. 
* Added Transaction states to clarify the state before and after the transition.
* Confine Opening Database to writable transactions.
* Adds 'Freed' handle state to address the release of handles by native calls.
* Distinguish between the freeing of LMDB handles and the disposal of Fusion LMDB objects.
* Adds Safe functions for checking the validity of the safe handles prior to the execution of the native code.
* Adds Throw methods for handling native code failure rather than returning the native result code.  

### [Lightning.NET]
* The [Lightning.NET] methodology was used and modified for managing the relationships between the cursor, database, and transaction and the environment classes.
* All test methods were merged using modified test collections.
* Test collections were set to not run in parallel.

### [Spreads LMDB]
* This project is an adaptation of the [Lightning.NET] project.
* Spreads added lookup functions for fix-size multi-value records. 
* libspreads-lmdb.dll was renamed to libLmdbPlus.
* Version information was added to the libLmdbPlus.
* Tests where merged.
* Async test operations were replaced with Sync operations.
* Multiple environments open with the same path are not reference equal. 
* Async methods were deprecated because these hang. 
* The LmdbBuffer structure was added to the Native methods.
* Methods from the Spreads pacakges were replace with generic .Net methods.

Still on the to do list:
* fix handing Environment Async tasks.

### [KdSoft.Lmdb]
* The [Lightning.NET] methodology was adapted to manage the relationship between the transactions, cursors and databases and the environment.
* The LmdbValue structure was added to the Native methods.

### Tests

#### LMDB Fusion Tests

This project includes tests from the [KdSoft.Lmdb] (Fixtures folder), 
[Lightning.NET] (lightning folder) and [Spreads LMDB] test projects. 

* The database fixture was documented. Functions were added 
to build the test data, open the database and save the test data.
* Environment test scenario was added for read-only transactions.  

#### Proto Tests

This project was forked from the [KdSoft.Lmdb] LMDB test project. 

* The obsolete Protocol Buffers package were replaced by the gRPC packages.
* Tests using Protocol Buffers Types from [Protocol buffers] were added under the Protocol Buffers namespace.

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio]
* [Jarte RTF Editor]
* [Wix Toolset]
* [Atomineer Code Documentation]
* [EW Software Spell Checker]
* [Code Converter]
* [Funduc Search and Replace]

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]
* [Corey Kaylor]
* [Howard Chu, Symas Corp]
* [Karl Waclawek]
* [Victor Baybecov]

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants
* [John Simmons] - outlaw programmer
* [Stack overflow] - Joel Spolsky
* [CAP'N PROTO] - Kenton Varda 

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[KdSoft.Lmdb]  
[Lightning.NET]  
[LMDB Library]  
[Protocol Buffers]  
[Spreads LMDB]  

<a name="Closed-software"></a>
### Closed software
None

<a name="Resources"></a>
### Resources
Additional resources and documentation can be found in following sites:  
[A short Guid to LMDB]  
[Technical Documents]  
[Technical Documents for Python]  
[Utilities: mdb_copy] -- copy and compress the LMDB database.  

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[A short Guid to LMDB]: https://blogs.kolabnow.com/2018/06/07/a-short-guide-to-lmdb
[CAP'N PROTO]: https://capnproto.org/news/2014-06-17-capnproto-flatbuffers-sbe.html  
[Corey Kaylor]: https://github.com/CoreyKaylor  
[Framework Design Guidelines]: https://www.informit.com/store/framework-design-guidelines-conventions-idioms-and-9780321545619  
[Howard Chu, Symas Corp]: https://symas.com/lmdb/  
[LMDB Fusion]: https://www.bitbucket.org/davidhary/vs.lmdb.fusion  
[Karl Waclawek]: https://github.com/kwaclaw  
[KdSoft.Lmdb]: https://github.com/kwaclaw/KdSoft.Lmdb   
[Lightning.NET]: https://github.com/CoreyKaylor/Lightning.NET  
[LMDB Library]: https://github.com/LMDB/lmdb  
[Protocol buffers]: https://github.com/protocolbuffers/protobuf  
[Roslyn Code Analysis]: https://docs.microsoft.com/en-us/visualstudio/code-quality/roslyn-analyzers-overview?view=vs-2019  
[Spreads LMDB]: https://github.com/Spreads/Spreads.LMDB  
[Technical Documents]: http://www.lmdb.tech/doc/  
[Technical Documents for Python]: https://lmdb.readthedocs.io/en/latest/  
[Utilities: mdb_copy]: http://www.lmdb.tech/doc/man1/mdb_copy_1.html
[Victor Baybecov]: https://github.com/buybackoff  

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
