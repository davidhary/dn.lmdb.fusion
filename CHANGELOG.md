# Change log
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [1.0.8070] - 2022-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references.
* Update build version.
* Display version file when updating build version.

## [1.0.7890] - 2021-08-08
* add Net Standard 2.1 target.

## [1.0.7884] - 2021-08-02
* compiles for .NET Standard 2.0 and .NET 5.0.

## [1.0.7753] - 2021-03-23
* moved to the dot net framework repositories.

## [1.0.7696] - 2020-12-08
* released.

## [0.0.7647] - 2020-12-08
* Started.

\(C\) 2020 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```

[1.0.8070]: (https://www.bitbucket.org/davidhary/dn.lmdb.fusion)
[1.0.7696]: (https://www.bitbucket.org/davidhary/vs.lmdb.fusion)
[0.0.7647]: (https://www.bitbucket.org/davidhary/vs.lmdb)
