using System;
using System.Runtime.CompilerServices;

namespace isr.Lmdb.Fusion
{

    /// <summary>
    /// LMDB Transaction that allows creating or opening databases. Only one of these can be active
    /// in the environment at a time.
    /// </summary>
    /// <remarks>   Remarked by David, 2020-12-11. </remarks>
    public partial class Transaction
    {

        #region " DATABASE MANAGEMENET "

        /// <summary>   The database lock. </summary>
        private readonly object _DatabaseLock = new();

        /// <summary>
        /// Open a <see cref="Database"/> in the environment.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="name">     Database name. Can be <c>null</c> for the default database. </param>
        /// <param name="config">   (optional) Database configuration instance. </param>
        /// <returns>   Database instance open for use. </returns>
        public Database OpenDatabase( string name = null, DatabaseConfiguration config = null )
        {
            config ??= new DatabaseConfiguration();
            lock ( this._DatabaseLock )
            {
                var result = Database.OpenDatabase( name, this, config );
                return result;
            }
        }

        /// <summary>   Open a multi-value database in the environment. </summary>
        /// <remarks>   David, 2021-01-02. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="name">     Database name. Can be <c>null</c> for the default database. </param>
        /// <param name="config">   (optional) Database configuration instance. </param>
        /// <returns>   <c>MultiValueDatabase</c> instance. </returns>
        public unsafe Database OpenMultiValueDatabase( string name = null, DatabaseConfiguration config = null )
        {
            config ??= new DatabaseConfiguration();
            lock ( this._DatabaseLock )
            {
                var result = Database.OpenMultiValueDatabase( name, this, config );
                return result;
            }
        }

        /// <summary>   Open a fixed multi-value database in the environment. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        /// <param name="name">     (Optional) Database name. Can be <c>null</c> for the default
        ///                         database. </param>
        /// <param name="config">   (optional) Database configuration instance. </param>
        /// <returns>   <c>FixedMultiValueDatabase</c> instance. </returns>
        public unsafe Database OpenFixedMultiValueDatabase( string name = null, DatabaseConfiguration config = null )
        {
            config ??= new DatabaseConfiguration();
            lock ( this._DatabaseLock )
            {
                var result = Database.OpenFixedMultiValueDatabase( name, this, config );
                return result;
            }
        }

        #endregion

        #region " DATABASE I/O  "

        /// <summary>   Tries to get a value by its key. </summary>
        /// <remarks>   David, 2021-06-16. </remarks>
        /// <param name="db">       The database to query. </param>
        /// <param name="key">      A span containing the key to look up. </param>
        /// <param name="value">    [out] A byte array containing the value found in the database, if it
        ///                         exists. </param>
        /// <returns>   True if key exists, false if not. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryGet( Database db, byte[] key, out byte[] value )
        {
            return this.TryGet( db, key.AsSpan(), out value );
        }

        /// <summary>   Tries to get a value by its key. </summary>
        /// <remarks>   David, 2021-06-16. </remarks>
        /// <param name="db">       The database to query. </param>
        /// <param name="key">      A span containing the key to look up. </param>
        /// <param name="value">    [out] A byte array containing the value found in the database, if it
        ///                         exists. </param>
        /// <returns>   True if key exists, false if not. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryGet( Database db, ReadOnlySpan<byte> key, out byte[] value )
        {
            var (resultCode, _, mdbValue) = this.Get( db, key );
            if ( resultCode == ( int ) NativeResultCode.Success )
            {
                value = mdbValue.CopyToNewArray();
                return true;
            }
            value = default;
            return false;
        }

        #endregion

        #region " CONTAINS "

        /// <summary>   Check whether data exists in database. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="db">   The database to query. </param>
        /// <param name="key">  A span containing the key to look up. </param>
        /// <returns>   True if key exists, false if not. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool ContainsKey( Database db, ReadOnlySpan<byte> key )
        {
            return this.Get( db, key ).resultCode == ( int ) NativeResultCode.Success;
        }

        /// <summary>   Check whether data exists in database. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="db">   The database to query. </param>
        /// <param name="key">  A span containing the key to look up. </param>
        /// <returns>   True if key exists, false if not. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool ContainsKey( Database db, byte[] key )
        {
            return this.ContainsKey( db, key.AsSpan() );
        }

        #endregion
    }
}
