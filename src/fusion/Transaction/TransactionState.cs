namespace isr.Lmdb
{

    /// <summary>   Transaction state. </summary>
    /// <remarks> Remark added by David, 2020-12-16. </remarks>
    public enum TransactionState
    {

        /// <summary>
        /// Transaction is disposing.
        /// </summary>
        Disposing,

        /// <summary>
        /// Transaction is disposed.
        /// </summary>
        Disposed,

        /// <summary>
        /// Transaction is currently active.
        /// </summary>
        Active,

        /// <summary>
        /// Transaction is aborting.
        /// </summary>
        Aborting,

        /// <summary>
        /// Transaction is aborted.
        /// </summary>
        Aborted,

        /// <summary>
        /// Transaction is committing.
        /// </summary>
        Committing,

        /// <summary>
        /// Transaction is committed.
        /// </summary>
        Committed,

        /// <summary>
        /// Transaction is renewing.
        /// </summary>
        Renewing,

        /// <summary>
        /// Transaction is resetting.
        /// </summary>
        Resetting,

        /// <summary>
        /// Transaction is reset.
        /// </summary>
        Reset
    }

}
