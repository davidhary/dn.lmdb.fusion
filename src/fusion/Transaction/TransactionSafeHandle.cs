using System;
using isr.Lmdb.Fusion.Interop;
using System.Runtime.CompilerServices;

namespace isr.Lmdb.Fusion
{
    /// <summary>   An Transaction safe handle. </summary>
    /// <remarks>   Remarked by David, 2020-12-16. </remarks>
    internal class TransactionSafeHandle : LmdbSafeHandle
    {

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with an invalid handle
        /// and a <see cref="LmdbSafeHandle.ReleaseHandle"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        public TransactionSafeHandle() : base() { this.State = TransactionState.Disposed; }

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with a
        /// handle and a <see cref="LmdbSafeHandle.ReleaseHandle"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="handle">   The handle. </param>
        public TransactionSafeHandle( IntPtr handle ) : base( handle ) { this.State = TransactionState.Disposed; }

        /// <summary>   Returns a valid and allocated handle; otherwise, throws an exception. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. A transaction handle is freed upon commit or abort. While
        /// freed, the safe handle is not yet disposed as its value is not invalid.
        /// </remarks>
        /// <exception cref="ObjectDisposedException">  Thrown when a supplied object has been disposed. </exception>
        /// <returns>   A valid allocated handle (IntPtr). </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal override IntPtr AssertAllocatedHandle()
        {
            // avoid multiple volatile memory access
            System.Threading.Interlocked.MemoryBarrier();
            IntPtr result = this.Handle;
            System.Threading.Interlocked.MemoryBarrier();
            return this.IsAllocated
                ? result
                : this.IsInvalid
                    ? throw new ObjectDisposedException( this.GetType().Name,
                                                         $"Attempted to use an invalid {this.handle} handle" )
                    : throw new ObjectDisposedException( this.GetType().Name,
                                                         $"Attempted to use a deallocated handle--handle freed at the {nameof( TransactionState )}.{this.FreedState } state" );
        }

        /// <summary>   Gets or sets the state that freed the handle. </summary>
        /// <value> The freed state. </value>
        private TransactionState FreedState { get; set; }

        private TransactionState _State;
        /// <summary>   Current transaction state. </summary>
        /// <value> The state. </value>
        internal TransactionState State
        {
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => this._State;
            set {
                this._State = value;
                if ( TransactionState.Committed == value || TransactionState.Aborted == value )
                {
                    this.Freed = true;
                    this.FreedState = value;
                }
            }
        }

        /// <summary>   Frees the allocated handle. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        protected override void Free()
        {
            // tag the handle as free.
            base.Free();
            // aborts the transaction if it was not already freed when disposed. 
            SafeNativeMethods.mdb_txn_abort( this.Handle );
        }

    }

}
