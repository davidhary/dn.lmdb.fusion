using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    /// <summary>
    /// LMDB Transaction. A thread can only use one transaction at a time, plus any child
    /// transactions. Each transaction belongs to one thread.
    /// </summary>
    /// <remarks>
    /// There can be multiple simultaneously active read-only transactions but only one that can
    /// write. Once a single read-write transaction is opened, all further attempts to begin one will
    /// block until the first one is committed or aborted. This has no effect on read-only
    /// transactions, however, and they may continue to be opened at any time.
    /// </remarks>
    public partial class Transaction : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="txn">          A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="parent">       Parent transaction. Can be <c>null</c>. </param>
        /// <param name="beginOptions"> Special options for this transaction. This parameter must be set
        ///                             to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        private Transaction( IntPtr txn, Transaction parent, TransactionBeginOptions beginOptions )
        {
            this.SafeHandle = new TransactionSafeHandle( txn );
            this.BeginOptions = beginOptions;
            this.Parent = parent;
            this.State = this.SafeHandle.IsInvalid ? TransactionState.Disposed : TransactionState.Active;
            this.Environment.Disposing += this.Dispose;
            if ( parent != null )
            {
                parent.Disposing += this.Dispose;
                parent.StateChanging += this.OnParentStateChanging;
            }
        }

        /// <summary>   Begins a transaction. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="safeHandle">   Safe handle holding the address where the #MDB_cursor handle is
        ///                             stored. </param>
        /// <param name="beginOptions"> Special options for this transaction. This parameter must be set
        ///                             to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        /// <param name="parent">       If this parameter is non-NULL, the new transaction will be a
        ///                             nested transaction, with the transaction indicated by parent as
        ///                             its parent. Transactions may be nested to any level. A parent
        ///                             transaction and its cursors may not issue any other operations
        ///                             than mdb_txn_commit and mdb_txn_abort while it has active child
        ///                             transactions. </param>
        /// <returns>   A Tuple of transaction handle and transaction id. </returns>
        internal static (IntPtr txn, IntPtr txnId) BeginTransactionInternal( EnvironmentSafeHandle safeHandle, TransactionBeginOptions beginOptions, Transaction parent )
        {
            var parentTxn = parent?.Handle ?? IntPtr.Zero;
            var txn = safeHandle.AssertExecute( ( IntPtr handle, out IntPtr value ) => SafeNativeMethods.mdb_txn_begin( handle, parentTxn, beginOptions, out value ) );
            var txnId = SafeNativeMethods.mdb_txn_id( txn );
            return (txn, txnId);
        }

        /// <summary>   Begins a transaction. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="environmentHandle">    Handle of the environment. </param>
        /// <param name="beginOptions">         Special options for this transaction. This parameter must
        ///                                     be set to 0 or by bitwise OR'ing together the
        ///                                     <see cref="TransactionBeginOptions"/>. </param>
        /// <param name="parent">               If this parameter is non-NULL, the new transaction will
        ///                                     be a nested transaction, with the transaction indicated by
        ///                                     parent as its parent. Transactions may be nested to any
        ///                                     level. A parent transaction and its cursors may not issue any
        ///                                     other operations than mdb_txn_commit and mdb_txn_abort while
        ///                                     it has active child transactions. </param>
        /// <returns>   A Tuple of transaction handle and transaction id. </returns>
        internal static (IntPtr txn, IntPtr txnId) BeginTransactionInternal( IntPtr environmentHandle, TransactionBeginOptions beginOptions, Transaction parent )
        {
            var parentTxn = parent?.Handle ?? IntPtr.Zero;
            int ret = SafeNativeMethods.mdb_txn_begin( environmentHandle, parentTxn, beginOptions, out IntPtr txn );
            _ = LmdbException.AssertExecute( ret );
            var txnId = SafeNativeMethods.mdb_txn_id( txn );
            return (txn, txnId);
        }

        /// <summary>   Created new instance of LightningTransaction. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="environment">  Environment. </param>
        /// <param name="parent">       Parent transaction or null. </param>
        /// <param name="beginOptions"> Transaction begin options. </param>
        internal Transaction( LmdbEnvironment environment, Transaction parent, TransactionBeginOptions beginOptions )
            : this( BeginTransactionInternal( (environment ?? throw new ArgumentNullException( nameof( environment ) )).Handle, beginOptions, parent ).txn,
                                              parent, beginOptions )
        {
            this.Environment = environment;
        }

        /// <summary>   Begins a child transaction. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="beginOptions"> Special options for this transaction. This parameter must be set
        ///                             to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        /// <returns>   New child transaction. </returns>
        public Transaction BeginChildTransaction( TransactionBeginOptions beginOptions = TransactionBeginOptions.None )
        {
            return new Transaction( this.Environment, this, beginOptions );
        }

        #region " IDISPOSABLE SUPPORT "

        /// <summary>
        /// An event queue for all cursors and child transactions the need to be disposed when this
        /// transaction is disposed.
        /// </summary>
        public event Action Disposing;

        /// <summary>   Dispose internal. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        private void DisposeInternal()
        {
            // this uses the validity of the handle rather than its being allocated 
            // because the handle may no longer be allocated due to Commit or Abort but is still valid and need to
            // be disposed.
            if ( this.SafeHandle is not Object || this.SafeHandle.IsInvalid )
                return;

            // unregister the dispose action with the Environment
            if ( this.Environment is object )
                this.Environment.Disposing -= this.Dispose;

            if ( this.Parent is object )
            {
                this.Parent.Disposing -= this.Dispose;
                this.Parent.StateChanging -= this.OnParentStateChanging;
            }

            // release any non-disposed cursors
            this.Disposing?.Invoke();

            // dispose the safe handle.
            this.SafeHandle?.Dispose();

        }


        /// <summary>   Implementation of Dispose() pattern. See <see cref="Dispose()"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// No finalizer is needed. The finalizer on <see cref="SafeHandle"/> will clean up the
        /// <see cref="TransactionSafeHandle"/> instance, if it hasn't already been disposed.
        /// However, because there may be a need for a subclass to introduce a finalizer, 
        /// so the <see cref="IDisposable"/> is properly implemented in this class.
        /// </para></remarks>
        /// <param name="disposing">    <c>true</c> if explicitly disposing (finalizer not run),
        ///                             <c>false</c> if disposed from finalizer. </param>
        protected virtual void Dispose( bool disposing )
        {

            this.OnStateChanging( TransactionState.Disposing );

            this.DisposeInternal();

            this.OnStateChanging( TransactionState.Disposed );

        }

        /// <summary>
        /// Abandon all the operations of the transaction instead of saving them. Same as
        /// <see cref="Abort"/>. The transaction handle is freed.It and its cursors must not be used
        /// again after this call, except with mdb_cursor_renew(). Note: Earlier documentation
        /// incorrectly said all cursors would be freed. Only write-transactions free cursors.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " UNMANAGED RESOURCES "

        /// <summary>   Safe handle holding the address where the #MDB_cursor handle is stored. </summary>
        internal TransactionSafeHandle SafeHandle { get; private set; }

        /// <summary>   Gets the handle. </summary>
        /// <value> The handle. </value>
        internal IntPtr Handle => this.SafeHandle.Handle;

        #endregion

        #region " TRANSACTION PROPERTIES "

        /// <summary>   Parent transaction. </summary>
        /// <value> The parent. </value>
        public Transaction Parent { get; private set; }

        private LmdbEnvironment _Environment;
        /// <summary>
        /// Environment that owns the transaction from the
        /// <see cref="SafeNativeMethods.mdb_env_get_userctx"/> unmanaged native context.
        /// </summary>
        /// <value> The environment. </value>
        public LmdbEnvironment Environment
        {
            get {
                if ( this._Environment is object )
                    return this._Environment;

                var handle = this.SafeHandle.AssertAllocatedHandle();
                var env = SafeNativeMethods.mdb_txn_env( handle );
                var gcHandle = ( GCHandle ) SafeNativeMethods.mdb_env_get_userctx( env );
                this._Environment = ( LmdbEnvironment ) gcHandle.Target;
                return this._Environment;
            }
            private set => this._Environment = value;
        }

        /// <summary>
        /// Gets the identifier associated with this transaction. For a read-only transaction, this
        /// corresponds to the snapshot being read;
        /// concurrent readers will frequently have the same transaction ID.
        /// </summary>
        /// <value> The identifier associated with this transaction. </value>
        public IntPtr Id
        {
            get {
                var handle = this.SafeHandle.AssertAllocatedHandle();
                return SafeNativeMethods.mdb_txn_id( handle );
            }
        }

        /// <summary>   Executes the 'parent state changing' action. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="state">    Current transaction state. </param>
        private void OnParentStateChanging( TransactionState state )
        {
            switch ( state )
            {
                case TransactionState.Aborting:
                case TransactionState.Committing:
                    // the child transaction should be aborted before the parent 
                    // transaction is committed. 
                    this.Abort();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Event queue for all listeners interested in <see cref="StateChanging"/> events.
        /// </summary>
        /// <remarks>
        /// A child transaction need to be aborted before the parent transaction is committed. Note that
        /// the new state is set before the native call is made.
        /// </remarks>
        private event Action<TransactionState> StateChanging;

        /// <summary>   Current transaction state. </summary>
        /// <value> The state. </value>
        internal TransactionState State
        {
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => this.SafeHandle.State;
            set => this.SafeHandle.State = value;
        }

        /// <summary>
        /// Gets Special options for this transaction. This parameter must be set to 0 or by bitwise
        /// OR'ing together the <see cref="TransactionBeginOptions"/>.
        /// </summary>
        /// <value> Options that control the transaction. </value>
        internal TransactionBeginOptions BeginOptions { get; }

        #endregion

        #region " TRANSACTION ACTIONS and METHODS "

        /// <summary>   Executes the 'state changing' action. </summary>
        /// <remarks>   David, 2021-01-06. </remarks>
        /// <param name="newState"> State of the new. </param>
        protected void OnStateChanging( TransactionState newState )
        {
            this.State = newState;
            StateChanging?.Invoke( this.State );
        }

        /// <summary>   Abandon all the operations of the transaction instead of saving them. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// The transaction handle is freed and the transaction can no longer be used.
        /// 
        /// Cursors: The handles of all non-read-only cursors spawned by this transactions are also freed
        /// and can no longer be used. Read-only cursors can be used upon
        /// <see cref="Cursor.Renew()">mdb_cursor_renew()</see>.
        /// 
        /// Note: Earlier documentation incorrectly said all cursors would be freed. Only write-
        /// transactions free cursors.
        /// 
        /// Database: databases spawned by this transactions also close and can no longer be used.  
        /// 
        /// Note: Presently, the aborting the transactions does not tag the
        /// <see cref="DatabaseSafeHandle"/> and <see cref="CursorSafeHandle"/>
        /// as <see cref="LmdbSafeHandle.Freed"/>. It is assumed that upon Abortion, the relevant Cursors
        /// and Databases associated with the transaction will by disposed in code. Possibly an Aborting event 
        /// can be added to the transactions with all databases and cursors registering event handlers to 
        /// tag their respective safe handles as Freed and thus no longer usable and prevent calling 
        /// <see cref="SafeNativeMethods"/> with invalid handles.
        /// </para>
        /// </remarks>
        public void Abort()
        {
            var currentHandle = this.SafeHandle.AssertAllocatedHandle();
            this.OnStateChanging( TransactionState.Aborting );
            SafeNativeMethods.mdb_txn_abort( currentHandle );
            this.OnStateChanging( TransactionState.Aborted );
        }

        /// <summary>
        /// Commit all the operations of a transaction into the database. The native code frees the transaction handle.
        /// Thus, the transaction handle, while not yet disposed, is in-fact invalid. 
        /// 
        /// The <see cref="TransactionSafeHandle"/> is disposed upon disposing the transaction. at which time the non-read-only cursors are disposed allowing
        /// to renew read-only cursors. Note: Earlier documentation incorrectly said all cursors would be
        /// freed. Only write-transactions free cursors.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public void Commit()
        {
            // lightning implementation
            // we check here, as we do not want to throw an exception later, but won't use this handle.
            var currentHandle = this.SafeHandle.AssertAllocatedHandle();

            this.OnStateChanging( TransactionState.Committing );
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_txn_commit( currentHandle ) );
            this.OnStateChanging( TransactionState.Committed );
        }

        #endregion

        #region " READ ONLY "

        /// <summary>   Assert read only valid handle. </summary>
        /// <remarks>   David, 2021-01-01. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   An IntPtr. </returns>
        internal IntPtr AssertReadOnlyHandle()
        {
            return this.AssertReadOnlyHandle( "Transaction is not read-only" );
        }

        /// <summary>   Assert read only valid handle. </summary>
        /// <remarks>   David, 2021-01-19. </remarks>
        /// <exception cref="LmdbException">    Thrown when a Lmdb error condition occurs. </exception>
        /// <param name="message">  The message. </param>
        /// <returns>   An IntPtr. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal IntPtr AssertReadOnlyHandle( string message )
        {
            return this.IsReadOnly ? this.SafeHandle.AssertAllocatedHandle() : throw new LmdbException( ( int ) LmdbFusionErrorCode.NotReadOnly, message );
        }

        /// <summary>   Ensures that the cursor has a handle and is writable (not read-only). </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   An valid cursor handle. </returns>
        protected IntPtr AssertWritableHandle()
        {
            return this.AssertWritableHandle( "Transaction is read-only" );
        }

        /// <summary>   Ensures that the cursor has a handle and is writable (not read-only). </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="LmdbException">    Thrown when a Lmdb error condition occurs. </exception>
        /// <param name="message">  The message. </param>
        /// <returns>   An valid cursor handle. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        protected IntPtr AssertWritableHandle( string message )
        {
            return this.IsReadOnly ? throw new LmdbException( ( int ) LmdbFusionErrorCode.NotWritable, message ) : this.SafeHandle.AssertAllocatedHandle();
        }

        /// <summary>   Indicates if cursor is read-only. </summary>
        /// <value> True if this object is read only; otherwise, false. </value>
        public bool IsReadOnly => TransactionBeginOptions.ReadOnly == (this.BeginOptions & TransactionBeginOptions.ReadOnly);

        /// <summary>   Reset a read-only transaction. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11.
        /// 
        /// This method aborts the transaction like mdb_txn_abort(), but keeps the transaction handle.
        /// mdb_txn_renew() may reuse the handle.
        /// 
        /// This saves allocation overhead if the process will start a new read-only transaction soon,
        /// and also locking overhead if MDB_NOTLS is in use.The reader table lock is released, but the
        /// table slot stays tied to its thread or MDB_txn.
        /// 
        /// Use mdb_txn_abort() to discard a reset handle, and to free its lock table slot if MDB_NOTLS
        /// is in use. Cursors opened within the transaction must not be used again after this call,
        /// except with mdb_cursor_renew().
        /// 
        /// Reader locks generally don't interfere with writers, but they keep old versions of database
        /// pages allocated. Thus they prevent the old pages from being reused when writers commit new
        /// data, and so under heavy load the database size may grow much more rapidly than otherwise.
        /// </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        public void Reset()
        {
            _ = this.AssertReadOnlyHandle();
            if ( this.State != TransactionState.Active )
            {
                throw new LmdbException( ( int ) LmdbFusionErrorCode.InvalidTransactionState,
                                         $"Transaction state {this.State} should be {TransactionState.Active} on {nameof( Transaction.Reset )}" );
            }
            this.OnStateChanging( TransactionState.Resetting );
            var handle = this.SafeHandle.AssertAllocatedHandle();
            SafeNativeMethods.mdb_txn_reset( handle );
            this.OnStateChanging( TransactionState.Reset );
        }

        /// <summary>   Renew a read-only transaction. </summary>
        /// <remarks>
        /// This acquires a new reader lock for a transaction handle that had been released by
        /// mdb_txn_reset(). It must be called before a reset transaction may be used again.
        /// </remarks>
        /// <exception cref="LmdbException">    Thrown when a Lmdb error condition occurs. </exception>
        /// <value> . </value>
        public void Renew()
        {
            _ = this.AssertReadOnlyHandle();
            if ( this.State != TransactionState.Reset )
            {
                throw new LmdbException( ( int ) LmdbFusionErrorCode.InvalidTransactionState,
                                         $"Transaction state {this.State} should be {TransactionState.Reset} on {nameof( Transaction.Renew )}" );
            }

            var handle = this.SafeHandle.AssertAllocatedHandle();
            this.OnStateChanging( TransactionState.Renewing );
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_txn_renew( handle ) );
            this.OnStateChanging( TransactionState.Active );
        }

        #endregion

    }
}
