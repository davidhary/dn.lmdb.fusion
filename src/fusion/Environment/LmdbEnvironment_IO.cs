
using System;
using System.Runtime.CompilerServices;

namespace isr.Lmdb.Fusion
{
    public partial class LmdbEnvironment
    {

        /// <summary>   Performs a write transaction on an open database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="writeFunction">    The write function. </param>
        /// <returns>   An object. </returns>
        public object Write( Func<Transaction, object> writeFunction )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Write )}." );
            using var txn = this.BeginTransaction( TransactionBeginOptions.None );
            var result = writeFunction( txn );
            return result;
        }

        /// <summary>   Performs a write transaction on an open database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="writeAction">      The write action. </param>
        public void Write( Action<Transaction> writeAction )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Write )}." );
            using var txn = this.BeginTransaction( TransactionBeginOptions.None );
            writeAction( txn );
        }

        /// <summary>   Performs a read transaction on an open database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="readFunc"> The read function. </param>
        /// <returns>   Typically true if succeeded; otherwise, false. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public T Read<T>( Func<Transaction, T> readFunc )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Read )}." );
            using var txn = this.BeginReadOnlyTransaction( TransactionBeginOptions.None );
            return readFunc( txn );
        }

        /// <summary>   Performs a read transaction on an open database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="readFunc"> The read function. </param>
        /// <param name="state">    The state. </param>
        /// <returns>   The transaction state. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public T Read<T>( Func<Transaction, object, T> readFunc, object state )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Read )}." );
            using var txn = this.BeginReadOnlyTransaction( TransactionBeginOptions.None );
            return readFunc( txn, state );
        }

        /// <summary>   Performs a read transaction on an open database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="readAction">   The read action. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void Read( Action<Transaction> readAction )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Read )}." );
            using var txn = this.BeginReadOnlyTransaction( TransactionBeginOptions.None );
            readAction( txn );
        }

        /// <summary>   Performs a read transaction on an open database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="readAction">   The read action. </param>
        /// <param name="state">        The state. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void Read( Action<Transaction, object> readAction, object state )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Read )}." );
            using var txn = this.BeginReadOnlyTransaction( TransactionBeginOptions.None );
            readAction( txn, state );
        }

    }
}
