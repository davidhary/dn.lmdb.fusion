using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   Assert function. </summary>
    /// <remarks>   Remark added by David, 2020-12-10. </remarks>
    /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
    /// <param name="msg">  The message. </param>
    public delegate void AssertFunction( LmdbEnvironment env, string msg );

    /// <summary>   LMDB environment. </summary>
    /// <remarks>
    /// We make Environment the only <see cref="CriticalFinalizerObject"/> because it can clean up
    /// all resources it owns (databases, transactions, cursors) in its finalizer.
    /// </remarks>
    public partial class LmdbEnvironment : CriticalFinalizerObject, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Creates and open a new instance of Environment. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="path">         Path of the relative directory for storing database files. </param>
        /// <param name="openFlags">    (Optional) Database open options. </param>
        /// <param name="accessMode">   Unix file access privileges (optional). Only makes sense on UNIX
        ///                             operating systems. </param>
        /// <returns>   A LMDBEnvironment. </returns>
        public static LmdbEnvironment OpenNew( string path,
                                               EnvironmentOpenOptions openFlags = EnvironmentOpenOptions.None,
                                               UnixAccessModes accessMode = UnixAccessModes.Default )
        {
            // This option is forced in this implementation of LMDB
            openFlags |= EnvironmentOpenOptions.NoThreadLocalStorage;

            // this is machine-local storage for each user.
            if ( string.IsNullOrWhiteSpace( path ) )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            var env = new LmdbEnvironment();
            env.Open( path, openFlags, accessMode );
            return env;
        }

        /// <summary>   Creates and open a new instance of Environment. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="path">         Path of the relative directory for storing database files. </param>
        /// <param name="mapSize">      The size of the map. </param>
        /// <param name="openFlags">    (Optional) Database open options. </param>
        /// <param name="accessMode">   Unix file access privileges (optional). Only makes sense on UNIX
        ///                             operating systems. </param>
        /// <returns>   A LMDBEnvironment. </returns>
        public static LmdbEnvironment OpenNew( string path, long mapSize,
                                               EnvironmentOpenOptions openFlags = EnvironmentOpenOptions.None,
                                               UnixAccessModes accessMode = UnixAccessModes.Default )
        {
            // This option is forced in this implementation of LMDB
            openFlags |= EnvironmentOpenOptions.NoThreadLocalStorage;

            // this is machine-local storage for each user.
            if ( string.IsNullOrWhiteSpace( path ) )
            {
                throw new ArgumentNullException( nameof( path ) );
            }

            var env = new LmdbEnvironment() { MapSize = mapSize };
            env.Open( path, openFlags, accessMode );
            return env;
        }


        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public LmdbEnvironment()
        {
            // so that we can refer back to the Environment instance using the user context pointer.
            this._InstanceHandle = GCHandle.Alloc( this, GCHandleType.WeakTrackResurrection );

            // Write Async assume this was created.
            this._WriteQueue = new BlockingCollection<Delegates>();

            int ret = SafeNativeMethods.mdb_env_create( out IntPtr envHandle );
            if ( ret == ( int ) NativeResultCode.Success )
            {
                // set the instance handle associated with the native environment
                var ret2 = SafeNativeMethods.mdb_env_set_userctx( envHandle, ( IntPtr ) this._InstanceHandle );
                if ( ret2 == ( int ) NativeResultCode.Success )
                    // instantiate the safe handle
                    this.SafeHandle = new EnvironmentSafeHandle( envHandle );
                else
                {
                    ret = ret2;
                    SafeNativeMethods.mdb_env_close( envHandle );
                }
            }
            _ = LmdbException.AssertExecute( ret );

            this.MaxDatabases = LmdbEnvironment.DefaultMaxDatabases;

        }

        /// <summary>   Default MapSize for new environments. </summary>
        public const long DefaultMapSize = 1024 * 1024;

        /// <summary>   Default MaxReaders for new environments. </summary>
        public const int DefaultMaxReaders = 126;

        /// <summary>   Default MaxDatabases for new environments. </summary>
        public const int DefaultMaxDatabases = 1024;

        /// <summary>   The default maximum key size. </summary>
        public const int DefaultMaxKeySize = 1982;

        /// <summary>   The default page size. </summary>
        public const int DefaultPageSize = 4096;

        /// <summary>   The default overflow page header size. </summary>
        public const int DefaultOverflowPageHeaderSize = 16;

        /// <summary>
        /// Gets a value indicating whether the automatic reduce map size in 32 bit process.
        /// Defaults to false.
        /// </summary>
        /// <value> True if automatic reduce map size in 32 bit process; otherwise, false. </value>
        public bool AutoReduceMapSizeIn32BitProcess { get; }

        #region " IDISPOSABLE SUPPORT "

        /// <summary>
        /// Event queue of all transaction and database dispose actions ensuring that transactions and
        /// databases are disposed when the environment is disposed. Called before the environment 
        /// is disposed.
        /// </summary>
        public event Action Disposing;

        /// <summary>   Gets a value indicating whether this object has disposing events. </summary>
        /// <value> True if this object has disposing events, false if not. </value>
        internal bool HasDisposingEvents
        {
            get {
                var disposingAction = this.Disposing;
                return disposingAction is object;
            }
        }

        /// <summary>   Returns if Environment handle is closed/disposed. </summary>
        /// <value> True if this object is disposed; otherwise, false. </value>
        public bool IsDisposed => this.SafeHandle.IsInvalid;

        /// <summary>   Implementation of Dispose() pattern. See <see cref="Dispose()"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="disposing">    <c>true</c> if explicitly disposing (finalizer not run),
        ///                             <c>false</c> if disposed from finalizer. </param>
        protected virtual void Dispose( bool disposing )
        {

            // mark the environment is no longer open.
            this.IsOpen = false;

            // if already disposed, make sure the instance handle is also disposed. 
            if ( this._InstanceHandle.IsAllocated )
                this._InstanceHandle.Free();

            // the handle becomes invalid only on dispose.
            if ( this.SafeHandle is not Object || this.SafeHandle.IsInvalid )
                return;

            // dispose any non-disposed databases or transactions.
            // this code might be called from the finalizer to make sure the 
            // database handle, which is not save, is disposed.
            this.Disposing?.Invoke();

            // now close the environment. 
            this.SafeHandle?.Dispose();

        }

        /// <summary>   Closes the environment and release the memory map. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Only a single thread may call this function. The environment handle will be freed and must
        /// not be used again after this call. For good order, all transactions, databases, and cursors
        /// must already be disposed before calling this function. However, the Environment
        /// <see cref="LmdbEnvironment.Disposing"/> event will be invoked to dispose of the Environment
        /// objects. Attempts to use any such handles after calling this function will cause a SIGSEGV
        /// (invalid memory access (segmentation fault)). The environment handle will be freed and must
        /// not be used again after this call.
        /// </para>
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>   Finalizer. Releases unmanaged resources. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. 
        /// The finalizer is needed because the database does not have a safe handle. </remarks>
        ~LmdbEnvironment()
        {
            this.Dispose( false );
        }

        #endregion " NATIVE FUNCTIONS "

        /// <summary>   Gets environment from user context. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        /// <param name="environmentHandle">    The address of the environment handle. </param>
        /// <returns>   The environment from user context. </returns>
        public static LmdbEnvironment GetEnvironmentFromUserContext( IntPtr environmentHandle )
        {
            var gcHandle = ( GCHandle ) SafeNativeMethods.mdb_env_get_userctx( environmentHandle );
            return ( LmdbEnvironment ) gcHandle.Target;
        }

        #endregion

        #region " UNMANAGED RESOURCES "

        /// <summary>   Handle of the instance. Provides a way to access a managed object from unmanaged memory. </summary>
        private readonly GCHandle _InstanceHandle;

        /// <summary>   A safe handle providing access to properly aligned types of size "native int" is atomic! </summary>
        internal EnvironmentSafeHandle SafeHandle { get; }

        /// <summary>   Gets the handle. </summary>
        /// <value> The handle. </value>
        internal IntPtr Handle => this.SafeHandle.Handle;

        #endregion

        #region " MANAGE ENVIRONMENT "

        /// <summary>   The access mode. </summary>
        private UnixAccessModes _AccessMode;

        /// <summary>   Directory path to store database files. </summary>
        /// <value> The pathname of the directory. </value>
        public string DirectoryPath { get; set; }

        /// <summary>   Whether the environment is opened. </summary>
        /// <value> True if this object is opened, false if not. </value>
        public bool IsOpen { get; private set; }

        private EnvironmentOpenOptions _OpenOptions;
        /// <summary>
        /// Open an environment handle. Do not open multiple times in the same process. If this function
        /// fails, mdb_env_close() must be called to discard the MDB_env handle.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="directoryPath">    The directory in which the database files reside. This
        ///                                 directory must already exist and be writable. </param>
        /// <param name="openOptions">          (Optional) Special options for this environment. See
        ///                                 <see cref="EnvironmentOpenOptions"/>. This parameter must be
        ///                                 set to 0
        ///                                 or by bitwise OR'ing together one or more of the values. Flags
        ///                                 set by mdb_env_set_flags() are also used. </param>
        /// <param name="accessMode">         (Optional) The UNIX permissions to set on created files and
        ///                                 semaphores. This parameter is ignored on Windows. </param>
        public void Open( string directoryPath, EnvironmentOpenOptions openOptions = EnvironmentOpenOptions.None, UnixAccessModes accessMode = UnixAccessModes.Default )
        {
            if ( !System.IO.Directory.Exists( directoryPath ) )
            {
                _ = System.IO.Directory.CreateDirectory( directoryPath );
            }

            if ( !this.IsOpen )
            {
                this.DirectoryPath = directoryPath;
                this._OpenOptions = openOptions;
                this._AccessMode = accessMode;
                var ptr = LmdbEnvironment.StringToHGlobalUTF8( directoryPath );
                _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_env_open( handle, ptr, openOptions, accessMode ) );
                if ( ptr != IntPtr.Zero )
                {
                    Marshal.FreeHGlobal( ptr );
                }
            }
            this.IsOpen = true;
        }

        /// <summary>
        /// Open an environment handle. Do not open multiple times in the same process. If this function
        /// fails, mdb_env_close() must be called to discard the MDB_env handle.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="openOptions">  (Optional) Special options for this environment. See
        ///                             <see cref="EnvironmentOpenOptions"/>. This parameter must be
        ///                             set to 0 or by bitwise OR'ing together one or more of the values.
        ///                             Flags set by mdb_env_set_flags() are also used. </param>
        /// <param name="accessMode">   (Optional) The UNIX permissions to set on created files and
        ///                             semaphores. This parameter is ignored on Windows. </param>
        public void Open( EnvironmentOpenOptions openOptions = EnvironmentOpenOptions.None, UnixAccessModes accessMode = UnixAccessModes.Default )
        {
            this.Open( this.DirectoryPath, openOptions, accessMode );
        }

        /// <summary>
        /// Flush the data buffers to disk. Data is always written to disk when mdb_txn_commit() is
        /// called, but the operating system may keep it buffered.LMDB always flushes the OS buffers upon
        /// commit as well, unless the environment was opened with MDB_NOSYNC or in part MDB_NOMETASYNC.
        /// This call is not valid if the environment was opened with MDB_RDONLY.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="force">    If <c>true</c>, force a synchronous flush. Otherwise if the
        ///                         environment has the MDB_NOSYNC flag set the flushes will be omitted, and
        ///                         with MDB_MAPASYNC they will be asynchronous. </param>
        public void Sync( bool force )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.Sync )}." );
            _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_env_sync( handle, force ) );
        }

        /// <summary>   Flush the data buffers to disk. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-16. <para>
        /// Data is always written to disk when
        /// <see cref="Transaction.Commit"/> is called, but the operating system may keep it buffered. MDB
        /// always flushes the OS buffers upon commit as well, unless the environment was opened with
        /// <see cref="EnvironmentOpenOptions.NoSync"/> or
        /// <see cref="EnvironmentOpenOptions.NoMetaSync"/>.
        /// </para>
        /// </remarks>
        /// <param name="force">    If true, force a synchronous flush. Otherwise if the environment has
        ///                         the <see cref="EnvironmentOpenOptions.NoSync"/> flag set the flushes will
        ///                         be omitted, and with <see cref="EnvironmentOpenOptions.MapAsync"/>
        ///                         (MDB_MAPASYNC) they will be asynchronous. </param>
        public void Flush( bool force )
        {
            this.Sync( force );
        }

        /// <summary>
        /// Copy an MDB environment to the specified path. This function may be used to make a backup of
        /// an existing environment.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="path">     The directory in which the copy will reside. This directory must
        ///                         already exist and be writable but must otherwise be empty. </param>
        /// <param name="compact">  (Optional) Omit empty pages when copying. </param>
        public void CopyTo( string path, bool compact = false )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.CopyTo )}." );

            var flags = compact ? EnvironmentCopyOptions.Compact : EnvironmentCopyOptions.None;
            var ptr = LmdbEnvironment.StringToHGlobalUTF8( path );
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_env_copy2( this.SafeHandle.AssertAllocatedHandle(), ptr, flags ) );
            if ( ptr != EnvironmentSafeHandle.InvalidHandle )
                Marshal.FreeHGlobal( ptr );
        }

        /// <summary>   Touch space. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="megabytes">    (Optional) The megabytes. </param>
        /// <returns>   The updated used size. </returns>
        public unsafe long TouchSpace( int megabytes = 0 )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.TouchSpace )}." );
            _ = this.SafeHandle.AssertAllocatedHandle();
            int size;
            var used = this.UsedSize;
            if ( megabytes == 0 )
            {
                size = ( int ) ((this.MapSize - used) / 2);
                if ( size == 0 )
                {
                    return used;
                }
            }
            else
            {
                size = megabytes * 1024 * 1024;
            }
            if ( size > this.MapSize - used )
            {
                size = ( int ) (Math.Min( this.MapSize - used, int.MaxValue ) * 8 / 10);
            }

            if ( size <= 0 )
            {
                return used;
            }

            using ( var txn = this.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                using Database db = txn.OpenDatabase( "__touch_space___", new DatabaseConfiguration( DatabaseOpenOptions.Create ) );
                var key = 0;
                var keyPtr = Unsafe.AsPointer( ref key );
                var key1 = new LmdbBuffer( System.Runtime.CompilerServices.Unsafe.SizeOf<int>(), ( byte* ) keyPtr );
                LmdbBuffer value = new( size, ( byte* ) IntPtr.Zero );
                _ = db.Put( txn, ref key1, ref value, TransactionPutOptions.ReserveSpace );
                Unsafe.InitBlockUnaligned( value.Data, 0, ( uint ) value.Length );
                txn.Commit();
            }
            this.Sync( true );
            return this.UsedSize;
        }

        #endregion

        #region " CONFIGURATION AND STATS "

        /// <summary>
        /// Set the size of the memory map to use for this environment. The size should be a multiple of
        /// the OS page size. The default is 10485760 bytes. The size of the memory map is also the
        /// maximum size of the database. The value should be chosen as large as possible, to accommodate
        /// future growth of the database. This function should be called after mdb_env_create() and
        /// before mdb_env_open(). It may be called at later times if no transactions are active in this
        /// process. Note that the library does not check for this condition, the caller must ensure it
        /// explicitly.
        /// </summary>
        /// <remarks>
        /// The new size takes effect immediately for the current process but will not be persisted to
        /// any others until a write transaction has been committed by the current process. Also, only
        /// <see cref="MapSize"/> increases are persisted into the environment. If the
        /// <see cref="MapSize"/> is increased by another process, and data has grown beyond the range of
        /// the current <see cref="MapSize"/>, mdb_txn_begin() will return MDB_MAP_RESIZED. This function
        /// may be called with a size of zero to adopt the new size. Any attempt to set a size smaller
        /// than the space already consumed by the environment will be silently changed to the current
        /// size of the used space. Note: the current MapSize can be obtained by calling GetEnvInfo().
        /// </remarks>
        /// <param name="newValue"> . </param>
        public void SetMapSize( long newValue )
        {
            if ( this.AutoReduceMapSizeIn32BitProcess && (IntPtr.Size == 4) && (newValue > int.MaxValue) )
                newValue = int.MaxValue;
            _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_env_set_mapsize( handle, ( IntPtr ) newValue ) );
            this._MapSizeConfigurated = newValue;
        }


        private long _MapSizeConfigurated;
        private long _MapSize;
        /// <summary>
        /// Set the size of the memory map to use for this environment. The size should be a multiple of
        /// the OS page size. The default is 10485760 bytes. The size of the memory map is also the
        /// maximum size of the database. The value should be chosen as large as possible, to accommodate
        /// future growth of the database. This function may only be called before the environment is
        /// opened. The size may be changed by closing and reopening the environment. Any attempt to set
        /// a size smaller than the space already consumed by the environment will be silently changed to
        /// the current size of the used space.
        /// </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The size of the map. </value>
        public long MapSize
        {
            get {
                if ( this.IsOpen && this._MapSize == 0 )
                    this._MapSize = this.GetInfo().MapSize;
                return this.IsOpen ? this._MapSize : this._MapSizeConfigurated;
            }
            set {
                if ( this.IsOpen )
                {
                    // during tests we return same environment instance, ignore setting the same size to it
                    if ( value == this.MapSize ) { return; }
                    throw new InvalidOperationException( $"Can't change {nameof( LmdbEnvironment.MapSize )} of an opened environment" );
                }
                this.SetMapSize( value );
            }
        }

        /// <summary>   Gets the maximum size of the key. </summary>
        /// <value> The maximum size of the key. </value>
        public int MaxKeySize => SafeNativeMethods.mdb_env_get_maxkeysize( this.SafeHandle.AssertAllocatedHandle() );

        private uint _MaxDatabases;
        /// <summary>
        /// Set/get the maximum number of named databases for the environment. This function is only
        /// needed if multiple databases will be used in the environment. Simpler applications that use
        /// the environment as a single unnamed database can ignore this option. This function may only
        /// be called after mdb_env_create() and before mdb_env_open(). Currently a moderate number of
        /// slots are cheap but a huge number gets expensive: 7-120 words per transaction, and every
        /// mdb_dbi_open() does a linear search of the opened slots.
        /// </summary>
        /// <value> The maximum databases. </value>
        public int MaxDatabases
        {
            // suppress overflow-checking
            get => unchecked(( int ) this._MaxDatabases);
            set {
                if ( this.IsOpen )
                {
                    if ( value == this._MaxDatabases ) return;
                    throw new InvalidOperationException( $"Can't change {nameof( LmdbEnvironment.MaxDatabases )} of opened environment" );
                }
                _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_env_set_maxdbs( handle, unchecked(( uint ) value) ) );
                this._MaxDatabases = unchecked(( uint ) value);
            }
        }

        /// <summary>   The maximum readers. </summary>
        private uint _MaxReaders;

        /// <summary>   Get the maximum number of threads for the environment. 
        /// This defines the number of slots in the lock table that is used to track readers in the
        /// environment. The default is 126. Starting a read-only transaction normally ties a lock table
        /// slot to the current thread until the environment closes or the thread exits. If MDB_NOTLS is
        /// in use, mdb_txn_begin() instead ties the slot to the MDB_txn object until it or the MDB_env
        /// object is destroyed. This function may only be called after mdb_env_create() and before
        /// mdb_env_open().
        /// </summary>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <value> The maximum readers. </value>
        public int MaxReaders
        {
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get {
                if ( this.IsOpen && this._MaxReaders != 0 )
                {
                    return ( int ) this._MaxReaders;
                }
                this._MaxReaders = this.SafeHandle.AssertExecute( ( IntPtr handle, out uint value ) => SafeNativeMethods.mdb_env_get_maxreaders( handle, out value ) );
                return unchecked(( int ) this._MaxReaders);
            }
            set {
                if ( this.IsOpen )
                {
                    if ( value == this.MaxReaders ) { return; }
                    throw new InvalidOperationException( $"Can't change {nameof( LmdbEnvironment.MaxReaders )} of opened environment" );
                }
                _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_env_set_maxreaders( handle, unchecked(( uint ) value) ) );
            }
        }

        /// <summary>
        /// Set environment flags. This may be used to set some flags in addition to those from
        /// mdb_env_open(), or to unset these flags. If several threads change the flags at the same time,
        /// the result is undefined.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">  Option flags to set, bitwise OR'ed together. </param>
        /// <param name="onoff">    A <c>true</c> value sets the flags, <c>false</c> clears them. </param>
        public void SetOptions( EnvironmentOpenOptions options, bool onoff )
        {
            _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_env_set_flags( handle, options, onoff ) );
        }

        /// <summary>   Return information about the LMDB environment. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   The options. </returns>
        public EnvironmentOpenOptions GetOptions()
        {
            return this.SafeHandle.AssertExecute( ( IntPtr handle, out EnvironmentOpenOptions value ) => SafeNativeMethods.mdb_env_get_flags( handle, out value ) );
        }

        /// <summary>   Return information about the LMDB environment. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   The information. </returns>
        public LmdbEnvironmentInfo GetInfo()
        {
            return this.IsOpen
                ? new LmdbEnvironmentInfo( this.SafeHandle.AssertExecute( ( IntPtr handle, out LmdbEnvInfo value ) => SafeNativeMethods.mdb_env_info( handle, out value ) ) )
                : throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.GetInfo )}" );
        }

        /// <summary>   Last used page of the environment multiplied by its page size. </summary>
        /// <value> The size of the used. </value>
        public long UsedSize
        {
            get {
                var info = this.GetInfo();
                return info.UsedSize( this.PageSize );
            }
        }

        /// <summary>   Check for stale entries in the reader lock table. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   An int. </returns>
        public int ReaderCheck()
        {
            return this.SafeHandle.AssertExecute( ( IntPtr handle, out int value ) => SafeNativeMethods.mdb_reader_check( handle, out value ) );
        }

        /// <summary>   Return statistics about the LMDB environment. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   The statistics. </returns>
        public Statistics GetStats()
        {
            return this.IsOpen
                ? new Statistics( this.SafeHandle.AssertExecute( ( IntPtr handle, out LmdbStat value ) => SafeNativeMethods.mdb_env_stat( handle, out value ) ) )
                : throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.GetStats )}" );
        }

        /// <summary>   Number of entires in the main database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   The entries count. </returns>
        public long GetEntriesCount()
        {
            var stat = this.GetStats();
            return stat.Entries;
        }

        /// <summary>   Gets the number of entries. </summary>
        /// <value> The number of entries. </value>
        public long EntriesCount => this.GetStats().Entries;

        /// <summary>   Gets used size. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   The used size. </returns>
        public long GetUsedSize()
        {
            var stat = this.GetStats();
            return stat.UsedSize;
        }

        /// <summary>
        /// Get the maximum size of keys and MDB_DUPSORT data we can write. Depends on the compile-time
        /// constant MDB_MAXKEYSIZE.Default 511. See MDB_val.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   The maximum key size. </returns>
        public int GetMaxKeySize()
        {
            var handle = this.SafeHandle.AssertAllocatedHandle();
            return SafeNativeMethods.mdb_env_get_maxkeysize( handle );
        }

        private long _PageSize;
        /// <summary>   Gets the page size. </summary>
        /// <value> The size of the page. </value>
        public long PageSize
        {
            get {
                if ( this._PageSize == 0 )
                {
                    var stat = this.GetStats();
                    this._PageSize = stat.PageSize;
                }
                return this._PageSize;
            }
        }

        private string _Path;
        /// <summary>   Return the path that was used in mdb_env_open(). </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   The path. </returns>
        public string GetPath()
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.GetPath )}." );
            if ( string.IsNullOrEmpty( this._Path ) )
            {
                int ret = SafeNativeMethods.mdb_env_get_path( this.SafeHandle.Handle, out IntPtr pathPointer );
                _ = LmdbException.AssertExecute( ret );
                this._Path = Marshal.PtrToStringAnsi( pathPointer );
            }
            return this._Path;
        }

        /// <summary>   Size of the overflow page header. </summary>
        private int _OverflowPageHeaderSize;

        /// <summary>   Gets the size of the overflow page header. </summary>
        /// <value> The size of the overflow page header. </value>
        public int OverflowPageHeaderSize
        {
            get {
                if ( this._OverflowPageHeaderSize == 0 )
                {
                    this._OverflowPageHeaderSize = (( int ) this._OpenOptions & ( int ) EnvironmentOpenOptions.WriteMap) != 0 ? this.GetOverflowPageHeaderSize() : -1;
                }
                return this._OverflowPageHeaderSize;
            }
        }

        /// <summary>   Gets overflow page header size. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   The overflow page header size. </returns>
        private unsafe int GetOverflowPageHeaderSize()
        {

            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.GetOverflowPageHeaderSize )}." );

            if ( (( int ) this._OpenOptions & ( int ) EnvironmentOpenOptions.WriteMap) == 0 )
            {
                throw new InvalidOperationException( $"{nameof( GetOverflowPageHeaderSize )} requires {nameof( EnvironmentOpenOptions )}.{nameof( EnvironmentOpenOptions.WriteMap )} flag" );
            }

            using var txn = this.BeginOpenDatabaseTransaction( TransactionBeginOptions.None );
            try
            {
                using Database db = txn.OpenDatabase( "temp", new DatabaseConfiguration( DatabaseOpenOptions.Create ) );
                var bufferRef = 0L;
                var keyPtr = Unsafe.AsPointer( ref bufferRef );
                var key1 = new LmdbBuffer( 8, ( byte* ) keyPtr );
                var value = new LmdbBuffer( this.PageSize * 10, ( byte* ) IntPtr.Zero );
                _ = db.Put( txn, ref key1, ref value, TransactionPutOptions.ReserveSpace );
                return checked(( int ) ((( IntPtr ) value.Data).ToInt64() % this.PageSize));
            }
            finally
            {
                txn.Abort();
            }
        }

        #endregion

        #region " READER LIST "

        //TODO expose mdb_reader_list()

        #endregion

        #region " BEGIN TRANSACTION "

        /// <summary>
        /// Create a transaction for use with the environment. The transaction handle may be discarded
        /// using mdb_txn_abort() or mdb_txn_commit(). Note: A transaction and its cursors must only be
        /// used by a single thread, and a thread may only have a single transaction at a time. If
        /// MDB_NOTLS is in use, this does not apply to read-only transactions. Cursors may not span
        /// transactions.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="beginOptions"> (Optional) Special options for this transaction. This parameter
        ///                             must be set to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        /// <param name="parent">       (Optional)
        ///                             If this parameter is non-NULL, the new transaction will be a
        ///                             nested transaction, with the transaction indicated by parent as
        ///                             its parent. Transactions may be nested to any level. A parent
        ///                             transaction and its cursors may not issue any other operations
        ///                             than mdb_txn_commit and mdb_txn_abort while it has active child
        ///                             transactions. </param>
        /// <returns>   New transaction instance. </returns>
        public Transaction BeginTransaction( TransactionBeginOptions beginOptions = TransactionBeginOptions.None, Transaction parent = null )
        {
            return !this.IsOpen
                ? throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.BeginTransaction )}" )
                : new Transaction( this, parent, beginOptions );
        }

        /// <summary>
        /// Creates a read only <see cref="Transaction"/>. The
        /// <see cref="TransactionBeginOptions.ReadOnly"/> flag will be set automatically. For details,
        /// see
        /// <see cref="BeginTransaction(TransactionBeginOptions, Transaction)"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="beginOptions"> (Optional) Special options for this transaction. This parameter
        ///                             must be set to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        /// <param name="parent">       (Optional)
        ///                             If this parameter is non-NULL, the new transaction will be a
        ///                             nested transaction, with the transaction indicated by parent as
        ///                             its parent. Transactions may be nested to any level. A parent
        ///                             transaction and its cursors may not issue any other operations
        ///                             than mdb_txn_commit and mdb_txn_abort while it has active child
        ///                             transactions. </param>
        /// <returns>   A ReadOnlyTransaction. </returns>
        public Transaction BeginReadOnlyTransaction( TransactionBeginOptions beginOptions = TransactionBeginOptions.None, Transaction parent = null )
        {
            beginOptions |= TransactionBeginOptions.ReadOnly;
            Transaction txn = this.BeginTransaction( beginOptions, parent );
            return txn;
        }

        #endregion

        #region " BEGIN DATABASE TRANSACTION and OPEN DATABASE "

        /// <summary>   The open database transaction lock. </summary>
        private readonly object _OpenDatabaseTransactionLock = new();

        /// <summary>
        /// Creates a transaction for opening a database in the environment. 
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. 
        /// A newly opened database handle will be private to the transaction until the transaction is successfully committed. 
        /// 
        /// If the transaction is aborted, the handle will be closed automatically. 
        /// 
        /// After a successful commit the handle will reside in the shared environment, and may be used by other transactions.
        /// 
        /// The OpenDatabase function must not be called from multiple concurrent transactions in the same process. 
        /// 
        /// A transaction that uses that function must finish (either commit or abort) before any other transaction in the
        /// process may use the OpenDatabase function, therefore only one such transaction is allowed to
        /// be active in the environment at a time.
        /// </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="beginOptions"> (Optional) Special options for this transaction. This parameter
        ///                             must be set to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        /// <param name="parent">       (Optional) If this parameter is non-NULL, the new transaction
        ///                             will be a nested transaction. </param>
        /// <returns>   New transaction instance. </returns>
        public Transaction BeginOpenDatabaseTransaction( TransactionBeginOptions beginOptions = TransactionBeginOptions.None, Transaction parent = null )
        {
            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.BeginOpenDatabaseTransaction )}." );
            if ( (beginOptions & TransactionBeginOptions.ReadOnly) != 0 )
                throw new LmdbException( $"{nameof( TransactionBeginOptions )}.{nameof( TransactionBeginOptions.ReadOnly )} must not be set when beginning a writable database transaction." );
            lock ( this._OpenDatabaseTransactionLock )
            {
                return new Transaction( this, parent, beginOptions );
            }
        }

        /// <summary>   Opens a database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="name">     The database name. </param>
        /// <param name="config">   The database configuration. </param>
        /// <returns>   A Database. </returns>
        public Database OpenDatabase( string name, DatabaseConfiguration config )
        {
            using var txn = this.BeginOpenDatabaseTransaction( TransactionBeginOptions.None );
            try
            {
                var db = txn.OpenDatabase( name, config );
                txn.Commit();
                return db;
            }
            catch
            {
                txn.Abort();
                throw;
            }
        }

        /// <summary>   Opens a database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="beginOptions"> Special options for this transaction. This parameter must be set
        ///                             to 0 or by bitwise OR'ing together the
        ///                             <see cref="TransactionBeginOptions"/>. </param>
        /// <param name="config">       The database configuration. </param>
        /// <returns>   A Database. </returns>
        public Database OpenDatabase( string name, TransactionBeginOptions beginOptions, DatabaseConfiguration config )
        {
            using var txn = this.BeginOpenDatabaseTransaction( beginOptions );
            try
            {
                var db = txn.OpenDatabase( name, config );
                txn.Commit();
                return db;
            }
            catch
            {
                txn.Abort();
                throw;
            }
        }

        #endregion

        #region " UTF8 SUPPORT "

        /// <summary>   String to H-Global UTF 8 Pointer. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="s">        The string. </param>
        /// <param name="length">   [out] The length. </param>
        /// <returns>   An IntPtr. </returns>
        private static IntPtr StringToHGlobalUTF8( string s, out int length )
        {
            if ( s == null )
            {
                length = 0;
                return IntPtr.Zero;
            }

            var bytes = System.Text.Encoding.UTF8.GetBytes( s );
            var ptr = Marshal.AllocHGlobal( bytes.Length + 1 );
            Marshal.Copy( bytes, 0, ptr, bytes.Length );
            Marshal.WriteByte( ptr, bytes.Length, 0 );
            length = bytes.Length;

            return ptr;
        }

        /// <summary>   String to H-Global UTF 8 Pointer. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="s">    The string. </param>
        /// <returns>   An IntPtr. </returns>
        private static IntPtr StringToHGlobalUTF8( string s )
        {
            return StringToHGlobalUTF8( s, out _ );
        }

        #endregion

    }
}
