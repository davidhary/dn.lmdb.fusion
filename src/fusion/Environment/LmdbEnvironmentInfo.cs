namespace isr.Lmdb.Fusion
{

    /// <summary>
    /// Information about the environment. 
    /// </summary>
    public class LmdbEnvironmentInfo
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-12-26. </remarks>
        /// <param name="nativeEnvInfo">    Information describing the native environment. </param>
        internal LmdbEnvironmentInfo( Interop.LmdbEnvInfo nativeEnvInfo )
        {
            unsafe
            {
                this.MapAddress = new System.IntPtr( nativeEnvInfo.MapAddress ).ToInt32();
                this.MapSize = nativeEnvInfo.MapSize.ToInt64();
                this.LastUsedPageId = nativeEnvInfo.LastUsedPageId.ToInt32();
                this.LastCommittedTransactionId = nativeEnvInfo.LastCommittedTransactionId.ToInt32();
                this.MaxReaders = nativeEnvInfo.MaxReaders;
                this.UsedReaders = nativeEnvInfo.UsedReaders;
            }
        }

        /// <summary>
        /// Address of map, if fixed
        /// </summary>
        public int MapAddress { get; }

        /// <summary>
        /// Size of the data memory map
        /// </summary>
        public long MapSize { get; }

        /// <summary>
        /// ID of the last used page
        /// </summary>
        public int LastUsedPageId { get; }

        /// <summary>
        /// ID of the last committed transaction  
        /// </summary>
        public int LastCommittedTransactionId { get; set; }

        /// <summary>
        /// max reader slots in the environment
        /// </summary>
        public long MaxReaders { get; }

        /// <summary>
        /// max reader slots used in the environment
        /// </summary>
        public long UsedReaders { get; }

        /// <summary>   Used size. </summary>
        /// <remarks>   David, 2020-12-26. </remarks>
        /// <param name="pageSize"> Size of the page from the <see cref="Statistics"/> <see cref="Statistics.PageSize"/>. </param>
        /// <returns>   A long. </returns>
        public long UsedSize( long pageSize )
        {
            return this.LastUsedPageId * pageSize;
        }
    }
}
