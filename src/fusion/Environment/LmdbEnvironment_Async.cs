// 
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace isr.Lmdb.Fusion
{
    public partial class LmdbEnvironment
    {

        /// <summary>   Queue of writes. </summary>
        private readonly BlockingCollection<Delegates> _WriteQueue;

        /// <summary>   The write task completion. </summary>
        private readonly TaskCompletionSource<object> _WriteTaskCompletion = new();

        /// <summary>
        /// Queues a write action and spin until it is completed unless fireAndForget is true. If
        /// fireAndForget is true then return immediately.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="OperationCanceledException">   Thrown when an Operation Canceled error
        ///                                                 condition occurs. </exception>
        /// <param name="writeFunction">    The write function. </param>
        /// <param name="fireAndForget">    (Optional) True to fire and forget. </param>
        /// <returns>   An asynchronous result. </returns>
        [Obsolete( "TODO: Hangs" )]
        internal Task<object> WriteAsync( Func<Transaction, object> writeFunction, bool fireAndForget = false )
        {

            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.WriteAsync )}." );
            TaskCompletionSource<object> tcs;
            if ( !this._WriteQueue.IsAddingCompleted )
            {
                tcs = fireAndForget ? null : new TaskCompletionSource<object>( TaskCreationOptions.RunContinuationsAsynchronously );
                var act = new Delegates { WriteFunction = writeFunction, TaskCompletionSource = tcs };

                this._WriteQueue.Add( act );
            }
            else
            {
                throw new OperationCanceledException();
            }

            return fireAndForget ? Task.FromResult<object>( null ) : tcs.Task;
        }

        /// <summary>
        /// Queues a write action and spin until it is completed unless fireAndForget is true. If
        /// fireAndForget is true then return immediately.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="OperationCanceledException">   Thrown when an Operation Canceled error
        ///                                                 condition occurs. </exception>
        /// <param name="writeAction">      The write action. </param>
        /// <param name="fireAndForget">    (Optional) True to fire and forget. </param>
        /// <returns>   An asynchronous result. </returns>
        [Obsolete( "TODO: Hangs" )]
        public Task WriteAsync( Action<Transaction> writeAction, bool fireAndForget = false )
        {

            if ( !this.IsOpen )
                throw new InvalidOperationException( $"Environment must be open before {nameof( LmdbEnvironment.WriteAsync )}." );
            TaskCompletionSource<object> tcs;
            if ( !this._WriteQueue.IsAddingCompleted )
            {
                tcs = fireAndForget ? null : new TaskCompletionSource<object>( TaskCreationOptions.RunContinuationsAsynchronously );
                var act = new Delegates { WriteAction = writeAction, TaskCompletionSource = tcs };

                this._WriteQueue.Add( act );
            }
            else
            {
                throw new OperationCanceledException();
            }

            return fireAndForget ? Task.CompletedTask : tcs.Task;
        }

        #region " DELEGATES "

        /// <summary>   A delegates. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        private struct Delegates
        {
            /// <summary>   The write function. Must open a database. </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            public Func<Transaction, object> WriteFunction;

            /// <summary>   The write action. Must open a database. </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            public Action<Transaction> WriteAction;

            /// <summary>   The task completion source. </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
            public TaskCompletionSource<object> TaskCompletionSource;
        }

        #endregion

    }
}

