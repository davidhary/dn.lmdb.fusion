using System;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    /// <summary>   An environment safe handle. </summary>
    /// <remarks>   Remarked by David, 2020-12-16. </remarks>
    internal class EnvironmentSafeHandle : LmdbSafeHandle
    {

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with an invalid handle
        /// and a <see cref="LmdbSafeHandle.ReleaseHandle"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        public EnvironmentSafeHandle() : base() { }

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with a
        /// handle and a <see cref="LmdbSafeHandle.ReleaseHandle"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="handle">   The handle. </param>
        public EnvironmentSafeHandle( IntPtr handle ) : base( handle ) { }

        /// <summary>   Frees the allocated handle. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        protected override void Free()
        {
            // tag the handle as free.
            base.Free();
            // free the handle
            SafeNativeMethods.mdb_env_close( this.handle );
        }

    }

}
