using System;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    /// <summary>   A cursor. </summary>
    /// <remarks>   David, 2021-01-05. </remarks>
    public partial class Cursor
    {

        #region " PRIVATE GET IMPLEMENTATIONS "

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b>
        /// refers. See <see cref="Interop.SafeNativeMethods.mdb_cursor_get(IntPtr, in LmdbValue, LmdbValue*, CursorOperation)"/> for restrictions on using the output
        /// values.
        /// </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        private (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( CursorOperation operation )
        {
            var mdbKey = new LmdbBuffer();
            var mdbValue = new LmdbBuffer();
            return (SafeNativeMethods.mdb_cursor_get( this.Handle, ref mdbKey, ref mdbValue, operation ), mdbKey, mdbValue);
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b>
        /// refers. See <see cref="Interop.SafeNativeMethods.mdb_get(void*, uint, void*, void*)"/> for restrictions on using the output
        /// values.
        /// </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        private (LmdbBuffer key, LmdbBuffer value) GetThrow( CursorOperation operation )
        {
            (int resultCode, LmdbBuffer key, LmdbBuffer value) = this.Get( operation );
            _ = LmdbException.AssertRead( resultCode );
            return (key, value);
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>   David, 2021-01-16. </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <returns>   The safe. </returns>
        private (LmdbBuffer key, LmdbBuffer value) GetSafe( CursorOperation operation )
        {
            var mdbKey = new LmdbBuffer();
            var mdbValue = new LmdbBuffer();
            (int resultCode, LmdbBuffer key, LmdbBuffer value) = (SafeNativeMethods.mdb_cursor_get( this.SafeHandle.AssertAllocatedHandle(),
                                                                                                    ref mdbKey, ref mdbValue, operation ), mdbKey, mdbValue);
            _ = LmdbException.AssertRead( resultCode );
            return (key, value);

        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b>
        /// refers. See <see cref="Interop.SafeNativeMethods.mdb_cursor_get(IntPtr, in LmdbValue, LmdbValue*, CursorOperation)"/> for restrictions on using the output
        /// values.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        private (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( CursorOperation operation, byte[] key )
        {
            return key is null ? throw new ArgumentNullException( nameof( key ) ) : this.Get( operation, key.AsSpan() );
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b> refers. See
        /// <see cref="Interop.SafeNativeMethods.mdb_cursor_get(IntPtr, in LmdbValue, LmdbValue*, CursorOperation)"/>
        /// for restrictions on using the output values.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <returns>
        /// Returns an <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to
        /// the key.
        /// </returns>
        private (LmdbBuffer key, LmdbBuffer value) GetThrow( CursorOperation operation, byte[] key )
        {
            return key is null ? throw new ArgumentNullException( nameof( key ) ) : this.GetThrow( operation, key.AsSpan() );
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b> refers. See
        /// <see cref="Interop.SafeNativeMethods.mdb_cursor_get(IntPtr, in LmdbValue, LmdbValue*, CursorOperation)"/>
        /// for restrictions on using the output values.
        /// </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        private unsafe (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( CursorOperation operation, ReadOnlySpan<byte> key )
        {
            fixed ( byte* keyPtr = key )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                var mdbValue = new LmdbBuffer();
                return (SafeNativeMethods.mdb_cursor_get( this.Handle, ref mdbKey, ref mdbValue, operation ), mdbKey, mdbValue);
            }
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b> refers. See
        /// <see cref="Interop.SafeNativeMethods.mdb_cursor_get(IntPtr, in LmdbValue, LmdbValue*, CursorOperation)"/>
        /// for restrictions on using the output values.
        /// </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <returns>
        /// Returns an <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to
        /// the key.
        /// </returns>
        private (LmdbBuffer key, LmdbBuffer value) GetThrow( CursorOperation operation, ReadOnlySpan<byte> key )
        {
            (int resultCode, LmdbBuffer mdbKey, LmdbBuffer mdbValue) = this.Get( operation, key );
            _ = LmdbException.AssertRead( resultCode );
            return (mdbKey, mdbValue);
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b>
        /// refers. See <see cref="Interop.SafeNativeMethods.mdb_get(void*, uint, void*, void*)"/> for
        /// restrictions on using the output values.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <param name="value">        [out] The data corresponding to the key. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        private (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( CursorOperation operation, byte[] key, byte[] value )
        {
            return key is null
                ? throw new ArgumentNullException( nameof( key ) )
                : value is null
                    ? throw new ArgumentNullException( nameof( value ) )
                    : this.Get( operation, key.AsSpan(), value.AsSpan() );
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b>
        /// refers. See <see cref="Interop.SafeNativeMethods.mdb_get(void*, uint, void*, void*)"/> for
        /// restrictions on using the output values.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <param name="value">        [out] The data corresponding to the key. </param>
        /// <returns>
        /// Returns an <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to
        /// the key.
        /// </returns>
        private (LmdbBuffer key, LmdbBuffer value) GetThrow( CursorOperation operation, byte[] key, byte[] value )
        {
            return key is null
                ? throw new ArgumentNullException( nameof( key ) )
                : value is null
                    ? throw new ArgumentNullException( nameof( value ) )
                    : this.GetThrow( operation, key.AsSpan(), value.AsSpan() );
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b> refers. See
        /// <see cref="Interop.SafeNativeMethods.mdb_get(void*, uint, void*, void*)"/> for restrictions
        /// on using the output values.
        /// </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <param name="value">        [out] The data corresponding to the key. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        private unsafe (int resultCode, LmdbBuffer key, LmdbBuffer value) Get( CursorOperation operation, ReadOnlySpan<byte> key, ReadOnlySpan<byte> value )
        {
            fixed ( byte* keyPtr = key )
            fixed ( byte* valPtr = value )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                var mdbValue = new LmdbBuffer( value.Length, valPtr );
                return (SafeNativeMethods.mdb_cursor_get( this.Handle, ref mdbKey, ref mdbValue, operation ), mdbKey, mdbValue);
            }
        }

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b> refers. See
        /// <see cref="Interop.SafeNativeMethods.mdb_get(void*, uint, void*, void*)"/> for restrictions
        /// on using the output values.
        /// </remarks>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <param name="key">          The key to search for in the database. </param>
        /// <param name="value">        [out] The data corresponding to the key. </param>
        /// <returns>
        /// Returns an <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to
        /// the key.
        /// </returns>
        private unsafe (LmdbBuffer key, LmdbBuffer value) GetThrow( CursorOperation operation, ReadOnlySpan<byte> key, ReadOnlySpan<byte> value )
        {
            fixed ( byte* keyPtr = key )
            fixed ( byte* valPtr = value )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                var mdbValue = new LmdbBuffer( value.Length, valPtr );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_cursor_get( this.Handle, ref mdbKey, ref mdbValue, operation ) );
                return (mdbKey, mdbValue);
            }
        }

        #endregion

        #region " MOVE "

        /// <summary>
        /// Position at specified key, if key is not found index will be positioned to closest match.
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">  Key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Set( byte[] key )
        {
            return this.Get( CursorOperation.Set, key ).resultCode;
        }

        /// <summary>
        /// Position at specified key, if key is not found index will be positioned to closest match.
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">  Key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Set( ReadOnlySpan<byte> key )
        {
            return this.Get( CursorOperation.Set, key ).resultCode;
        }

        /// <summary>
        /// Position at key/data pair. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">      Key. </param>
        /// <param name="value">    Value. </param>
        /// <returns>   Returns true if the key/value pair was found. </returns>
        public int GetBoth( byte[] key, byte[] value )
        {
            return this.Get( CursorOperation.GetBoth, key, value ).resultCode;
        }

        /// <summary>
        /// Position at key/data pair. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">      Key. </param>
        /// <param name="value">    Value. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int GetBoth( ReadOnlySpan<byte> key, ReadOnlySpan<byte> value )
        {
            return this.Get( CursorOperation.GetBoth, key, value ).resultCode;
        }

        /// <summary>
        /// position at key, nearest data. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">      Key. </param>
        /// <param name="value">    Value. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int GetBothRange( byte[] key, byte[] value )
        {
            return this.Get( CursorOperation.GetBothRange, key, value ).resultCode;
        }

        /// <summary>
        /// position at key, nearest data. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">      Key. </param>
        /// <param name="value">    Value. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int GetBothRange( ReadOnlySpan<byte> key, ReadOnlySpan<byte> value )
        {
            return this.Get( CursorOperation.GetBothRange, key, value ).resultCode;
        }

        /// <summary>   Position at first key greater than or equal to specified key. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">  Key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int SetRange( byte[] key )
        {
            return this.Get( CursorOperation.SetRange, key ).resultCode;
        }

        /// <summary>   Position at first key greater than or equal to specified key. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">  Key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int SetRange( ReadOnlySpan<byte> key )
        {
            return this.Get( CursorOperation.SetRange, key ).resultCode;
        }

        /// <summary>   Position at first key/data item. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int First()
        {
            return this.Get( CursorOperation.First ).resultCode;
        }

        /// <summary>
        /// Position at first data item of current key. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int FirstDuplicate()
        {
            return this.Get( CursorOperation.FirstDuplicate ).resultCode;
        }

        /// <summary>   Position at last key/data item. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Last()
        {
            return this.Get( CursorOperation.Last ).resultCode;
        }

        /// <summary>
        /// Position at last data item of current key. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int LastDuplicate()
        {
            return this.Get( CursorOperation.LastDuplicate ).resultCode;
        }

        /// <summary>   Position at previous data item. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Previous()
        {
            return this.Get( CursorOperation.Previous ).resultCode;
        }

        /// <summary>
        /// Position at previous data item of current key. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>.
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int PreviousDuplicate()
        {
            return this.Get( CursorOperation.PreviousDuplicate ).resultCode;
        }

        /// <summary>
        /// Position at last data item of previous key. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>.
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int PreviousNoDuplicate()
        {
            return this.Get( CursorOperation.PreviousNoDuplicate ).resultCode;
        }

        /// <summary>
        /// Position at next data item
        /// </summary>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public int Next()
        {
            return this.Get( CursorOperation.Next ).resultCode;
        }

        /// <summary>
        /// Position at next data item of current key. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>.
        /// </summary>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public int NextDuplicate()
        {
            return this.Get( CursorOperation.NextDuplicate ).resultCode;
        }

        /// <summary>
        /// Position at first data item of next key. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>.
        /// </summary>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public int NextNoDuplicate()
        {
            return this.Get( CursorOperation.NextNoDuplicate ).resultCode;
        }

        #endregion

        #region " MOVE AND READ "

        /// <summary>   Moves to the key and populates Current with the values stored. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">  Key. </param>
        /// <returns>
        /// Returns <see cref="NativeResultCode"/>, and <see cref="LmdbBuffer"/> key/value.
        /// </returns>
        public (int resultCode, LmdbBuffer key, LmdbBuffer value) SetKey( byte[] key )
        {
            return this.Get( CursorOperation.SetKey, key );
        }

        /// <summary>   Moves to the key and populates Current with the values stored. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <param name="key">  Key. </param>
        /// <returns>
        /// Returns <see cref="NativeResultCode"/>, and <see cref="LmdbBuffer"/> key/value.
        /// </returns>
        public (int resultCode, LmdbBuffer key, LmdbBuffer value) SetKey( ReadOnlySpan<byte> key )
        {
            return this.Get( CursorOperation.SetKey, key );
        }

        /// <summary>   Return key/data at current cursor position. </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>   Key/data at current cursor position. </returns>
        public (int resultCode, LmdbBuffer key, LmdbBuffer value) GetCurrent()
        {
            return this.Get( CursorOperation.GetCurrent );
        }

        /// <summary>
        /// Return up to a page of duplicate data items at the next cursor position. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>. It is assumed you know the array size to
        /// break up a single byte[] into byte[][].
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, and
        /// <see cref="LmdbBuffer"/> key will be empty here, values are 2D array.
        /// </returns>
        public (int resultCode, LmdbBuffer key, LmdbBuffer value) NextMultiple()
        {
            return this.Get( CursorOperation.NextMultiple );
        }

        /// <summary>
        /// Return up to a page of duplicate data items at the next cursor position. Only for MDB_DUPSORT
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>. It is assumed you know the array size to
        /// break up a single byte[] into byte[][].
        /// </summary>
        /// <remarks>   David, 2021-01-05. </remarks>
        /// <returns>
        /// Returns A non-zero error value on failure and 0 on success, and <see cref="LmdbBuffer"/> key
        /// will be empty here, values are 2D array.
        /// </returns>
        public (int resultCode, LmdbBuffer key, LmdbBuffer value) GetMultiple()
        {
            return this.Get( CursorOperation.GetMultiple );
        }

        #endregion

        #region " GET BUFFER BY REFERENCE "

        /// <summary>   Uses a cursor to read a key/data pair from the database. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-05. This function retrieves key/data pairs from the database. The
        /// address and length of the key are returned in the object to which <b>key</b> refers (except
        /// for the case of the #MDB_SET <see cref="CursorGetOption.Set"/> option, in which the
        /// <b>key</b> object is unchanged), and the address and length of the data are returned in the
        /// object to which <b>data</b>
        /// refers. See <see cref="Interop.SafeNativeMethods.mdb_get(void*, uint, void*, void*)"/> for restrictions on using the output
        /// values.
        /// </remarks>
        /// <param name="key">          [in,out] The key to search for in the database. </param>
        /// <param name="value">        [out] The data corresponding to the key. </param>
        /// <param name="operation">    A cursor <see cref="CursorOperation"/> operation. </param>
        /// <returns>
        /// Returns a Result Code: A non-zero error value on failure and 0 on success, a
        /// <see cref="LmdbBuffer"/> key and a <see cref="LmdbBuffer"/> value corresponding to the key.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Get( ref LmdbBuffer key, ref LmdbBuffer value, CursorGetOption operation )
        {
            return SafeNativeMethods.mdb_cursor_get( this.Handle, ref key, ref value, operation );
        }

        #endregion

    }
}
