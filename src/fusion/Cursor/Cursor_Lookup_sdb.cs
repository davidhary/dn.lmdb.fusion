
using System;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        /// <summary>   Attempts to find the specified key and fetch the associated value. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public unsafe bool TryFind<TKey, TValue>( Lookup direction, ref TKey key, out TValue value )
            where TKey : struct where TValue : struct
        {
            _ = this.AssertFixedDataHandle();
            var keyPtr = System.Runtime.CompilerServices.Unsafe.AsPointer( ref key );
            long size = System.Runtime.CompilerServices.Unsafe.SizeOf<TKey>();

            var key1 = new LmdbBuffer( size, ( byte* ) keyPtr );
            // _ = TypeHelper<TValue>.EnsureFixedSize();
            var res = this.TryFind( direction, ref key1, out LmdbBuffer value1 );
            if ( res )
            {
                key = System.Runtime.CompilerServices.Unsafe.ReadUnaligned<TKey>( key1.Data );
                value = System.Runtime.CompilerServices.Unsafe.ReadUnaligned<TValue>( value1.Data );
                return true;
            }
            value = default;
            return false;
        }

        /// <summary>   Attempts to find the specified key and fetch the associated value. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [out] The value. </param>
        /// <returns>   True if it succeeds; otherwise, false. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryFind( Lookup direction, ref LmdbBuffer key, out LmdbBuffer value )
        {
            int res = 0;
            value = default;

            if ( direction == Lookup.LE )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_le( this.Handle, ref key, out value )
                );
            }
            else if ( direction == Lookup.GE )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_ge( this.Handle, ref key, out value )
                );
            }
            else if ( direction == Lookup.EQ )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_eq( this.Handle, ref key, out value )
                );
            }
            else if ( direction == Lookup.LT )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_lt( this.Handle, ref key, out value )
                );
            }
            else if ( direction == Lookup.GT )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_gt( this.Handle, ref key, out value )
                );
            }

            return res != ( int ) NativeResultCode.NotFound;
        }

        /// <summary>   Attempts to find duplicate value for the specified key. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public unsafe bool TryFindDup<TKey, TValue>( Lookup direction, ref TKey key, ref TValue value )
            where TKey : struct where TValue : struct
        {

            _ = this.AssertMultiValueHandle();
            _ = this.AssertFixedDataHandle();

            var keyPtr = Unsafe.AsPointer( ref key );
            long size = System.Runtime.CompilerServices.Unsafe.SizeOf<TKey>();
            var key1 = new LmdbBuffer( size, ( byte* ) keyPtr );

            var valuePtr = Unsafe.AsPointer( ref value );
            size = System.Runtime.CompilerServices.Unsafe.SizeOf<TValue>();
            var value1 = new LmdbBuffer( size, ( byte* ) valuePtr );

            var res = this.TryFindDup( direction, ref key1, ref value1 );
            if ( res )
            {
                key = Unsafe.ReadUnaligned<TKey>( key1.Data );
                value = Unsafe.ReadUnaligned<TValue>( value1.Data );
                return true;
            }
            return false;
        }

        /// <summary>   Attempts to find duplicate. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryFindDup( Lookup direction, ref LmdbBuffer key, ref LmdbBuffer value )
        {
            int res = 0;

            if ( direction == Lookup.LE )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_le_dup( this.Handle, ref key, ref value )
                );
            }
            else if ( direction == Lookup.GE )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_ge_dup( this.Handle, ref key, ref value )
                );
            }
            else if ( direction == Lookup.EQ )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_eq_dup( this.Handle, ref key, ref value )
                );
            }
            else if ( direction == Lookup.LT )
            {
                res = LmdbException.AssertRead(
                     SafeNativeMethods.sdb_cursor_get_lt_dup( this.Handle, ref key, ref value )
                );
            }
            else if ( direction == Lookup.GT )
            {
                res = LmdbException.AssertRead(
                    SafeNativeMethods.sdb_cursor_get_gt_dup( this.Handle, ref key, ref value )
                );
            }

            return res != ( int ) NativeResultCode.NotFound;
        }

    }


    /// <summary>
    /// A bit-field of flags for specifying directions to lookup data or move from a starting point.
    /// </summary>
    /// <remarks>   David, 2021-01-05. </remarks>
    [Flags]
    public enum Lookup : byte
    {
        /// <summary>   A binary constant representing the less than flag. </summary>
        LT = 0x4,

        /// <summary>   A binary constant representing the less or equal flag. </summary>
        LE = 0x6,

        /// <summary>   A binary constant representing the equal flag. </summary>
        EQ = 0x2,

        /// <summary>   A binary constant representing the greater or equal flag. </summary>
        GE = 0x3,

        /// <summary>   A binary constant representing the greater than flag. </summary>
        GT = 0x1
    }

}


