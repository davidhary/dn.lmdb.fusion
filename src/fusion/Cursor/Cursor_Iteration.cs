using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        #region " ENUMERABLE " 

        /// <summary>
        /// Enumerates the key/value pairs of the <see cref="Cursor"/> starting at the current position.
        /// </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        /// <returns>   <see cref="ValueTuple"/> key/value pairs of <see cref="LmdbBuffer"/> </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public IEnumerable<ValueTuple<LmdbBuffer, LmdbBuffer>> AsEnumerable()
        {
            do
            {
                var (resultCode, key, value) = this.GetCurrent();
                if ( resultCode == ( int ) NativeResultCode.Success )
                {
                    yield return (key, value);
                }
            } while ( this.Next() == ( int ) NativeResultCode.Success );
        }

        #endregion

        #region " ITERATORS "


        /// <summary>   Iterates over all records in sort order. For use in foreach loop. </summary>
        /// <value> The forward. </value>
        public ItemsIterator Forward => new( this, CursorOperation.First, CursorOperation.Next );

        /// <summary>
        /// Iterates over records in sort order, from the next position on. For use in foreach loops. If
        /// duplicates are allowed, then the next position may be on the same key, in duplicate sort
        /// order.
        /// </summary>
        /// <value> The forward from next. </value>
        public NextItemsIterator ForwardFromNext => new( this, CursorOperation.Next );

        /// <summary>
        /// Iterates over records in sort order, from the current position on. For use in foreach loops.
        /// If duplicates are allowed, then the next position may be on the same key, in duplicate sort
        /// order.
        /// </summary>
        /// <value> The forward from current. </value>
        public ItemsIterator ForwardFromCurrent => new( this, CursorOperation.GetCurrent, CursorOperation.Next );

        /// <summary>   Iterates over all records in reverse sort order. For use in foreach loops. </summary>
        /// <value> The reverse. </value>
        public ItemsIterator Reverse => new( this, CursorOperation.Last, CursorOperation.Previous );

        /// <summary>
        /// Iterates over records in reverse sort order, from the previous position on. For use in
        /// foreach loops. If duplicates are allowed, then the previous position may be on the same key,
        /// in reverse duplicate sort order.
        /// </summary>
        /// <value> The reverse from previous. </value>
        public NextItemsIterator ReverseFromPrevious => new( this, CursorOperation.Previous );

        /// <summary>
        /// Iterates over records in reverse sort order, from the current position on. For use in foreach
        /// loops. If duplicates are allowed, then the previous position may be on the same key, in
        /// reverse duplicate sort order.
        /// </summary>
        /// <value> The reverse from current. </value>
        public ItemsIterator ReverseFromCurrent => new( this, CursorOperation.GetCurrent, CursorOperation.Previous );

        #endregion

        #region " IENUMERABLE SUPPORT "

#pragma warning disable CA1815 // Override equals and operator equals on value types
#pragma warning disable CA1034 // Nested types should not be visible

        /// <summary>   Gets an item. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="item"> [out] The item. </param>
        /// <returns>   A bool. </returns>
        internal delegate bool GetItem( out KeyDataPair item );

        /// <summary>
        /// Implements the Enumerable pattern for use with foreach loops when the items to be iterated
        /// over can only live on the stack. This works because the foreach loop does not require an
        /// explicit implementation of the <see cref="IEnumerable"/> interface.
        /// </summary>
        /// <remarks>
        /// <a href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerable#remarks">
        /// See Remarks in the <c>IEnumerable</c> documentation.
        /// </a>
        /// </remarks>
        public struct ItemsIterator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The operation first. </summary>
            private readonly CursorOperation _OpFirst;

            /// <summary>   The operation next. </summary>
            private readonly CursorOperation _OpNext;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="opFirst">  The operation first. </param>
            /// <param name="opNext">   The operation next. </param>
            internal ItemsIterator( Cursor cursor, CursorOperation opFirst, CursorOperation opNext )
            {
                this._Cursor = cursor;
                this._OpFirst = opFirst;
                this._OpNext = opNext;
            }

            /// <summary>   Gets the first item. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="item"> [out] The item. </param>
            /// <returns>   True if it succeeds, false if it fails. </returns>
            private bool GetFirstItem( out KeyDataPair item ) => this._Cursor.TryGet( out item, this._OpFirst );

            /// <summary>   Gets the next item. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="item"> [out] The item. </param>
            /// <returns>   True if it succeeds, false if it fails. </returns>
            private bool GetNextItem( out KeyDataPair item ) => this._Cursor.TryGet( out item, this._OpNext );

            /// <summary>   Equivalent to <see cref="IEnumerable.GetEnumerator()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   The enumerator. </returns>
            public ItemsEnumerator GetEnumerator() => new( this.GetFirstItem, this.GetNextItem );
        }

        /// <summary>
        /// Implements the Enumerable pattern for use with foreach loops when the items to be iterated
        /// over can only live on the stack. This works because the foreach loop does not require an
        /// explicit implementation of the <see cref="System.Collections.IEnumerable"/> interface.
        /// </summary>
        /// <remarks>
        /// <a href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerable#remarks">
        /// See Remarks in the <c>IEnumerable</c> documentation.
        /// </a>
        /// </remarks>
        public ref struct NextItemsIterator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The next operation. </summary>
            private readonly CursorOperation _NextOp;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="nextOp">   The next operation. </param>
            internal NextItemsIterator( Cursor cursor, CursorOperation nextOp )
            {
                this._Cursor = cursor;
                this._NextOp = nextOp;
            }

            /// <summary>   Equivalent to <see cref="IEnumerable.GetEnumerator()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   The enumerator. </returns>
            public NextItemsEnumerator GetEnumerator() => new( this._Cursor, this._NextOp );
        }

        /// <summary>
        /// Implements Enumerator pattern for use with foreach loops when the items to be iterated over
        /// can only live on the stack. This works because the foreach loop does not require an explicit
        /// implementation of the <see cref="System.Collections.IEnumerator"/> interface.
        /// </summary>
        /// <remarks>
        /// <a href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerable#remarks">
        /// See Remarks in the <c>IEnumerable</c> documentation.
        /// </a>
        /// </remarks>
        public ref struct ItemsEnumerator
        {
            /// <summary>   The get first. </summary>
            private readonly GetItem _GetFirst;

            /// <summary>   The get next. </summary>
            private readonly GetItem _GetNext;

            /// <summary>   The current. </summary>
            private KeyDataPair _Current;

            /// <summary>   True if is current; otherwise, false. </summary>
            private bool _IsCurrent;

            /// <summary>   True if is initialized; otherwise, false. </summary>
            private bool _IsInitialized;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="getFirst"> The get first. </param>
            /// <param name="getNext">  The get next. </param>
            internal ItemsEnumerator( GetItem getFirst, GetItem getNext )
            {
                this._GetFirst = getFirst;
                this._GetNext = getNext;
                this._Current = default;
                this._IsCurrent = false;
                this._IsInitialized = false;
            }

            /// <summary>   Equivalent to <see cref="IEnumerator.Current"/>. </summary>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <value> The current. </value>
            public KeyDataPair Current
            {
                [MethodImpl( MethodImplOptions.AggressiveInlining )]
                get => this._IsCurrent ? this._Current : throw new InvalidOperationException( "Invalid cursor position." );
            }

            /// <summary>   Equivalent to <see cref="IEnumerator.MoveNext()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   True if it succeeds, false if it fails. </returns>
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            public bool MoveNext()
            {
                if ( this._IsInitialized )
                    return this._IsCurrent = this._GetNext( out this._Current );
                else
                {
                    this._IsInitialized = true;
                    return this._IsCurrent = this._GetFirst( out this._Current );
                }
            }
        }

        /// <summary>
        /// Implements Enumerator pattern for use with foreach loops when the items to be iterated over
        /// can only live on the stack. This works because the foreach loop does not require an explicit
        /// implementation of the <see cref="System.Collections.IEnumerator"/> interface.
        /// </summary>
        /// <remarks>
        /// <a href="https://docs.microsoft.com/en-us/dotnet/api/system.collections.ienumerable#remarks">
        /// See Remarks in the <c>IEnumerable</c> documentation.
        /// </a>
        /// </remarks>
        public ref struct NextItemsEnumerator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The next operation. </summary>
            private readonly CursorOperation _NextOp;

            /// <summary>   True if is current; otherwise, false. </summary>
            private bool _IsCurrent;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="nextOp">   The next operation. </param>
            internal NextItemsEnumerator( Cursor cursor, CursorOperation nextOp )
            {
                this._Cursor = cursor;
                this._NextOp = nextOp;
                this._Current = default;
                this._IsCurrent = false;
            }

            /// <summary>   The current. </summary>
            private KeyDataPair _Current;

            /// <summary>   Equivalent to <see cref="IEnumerator.Current"/>. </summary>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <value> The current. </value>
            public KeyDataPair Current
            {
                [MethodImpl( MethodImplOptions.AggressiveInlining )]
                get => this._IsCurrent ? this._Current : throw new InvalidOperationException( "Invalid cursor position." );
            }

            /// <summary>   Equivalent to <see cref="IEnumerator.MoveNext()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   True if it succeeds, false if it fails. </returns>
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            public bool MoveNext()
            {
                return this._IsCurrent = this._Cursor.TryGet( out this._Current, this._NextOp );
            }
        }

#pragma warning restore CA1034 // Nested types should not be visible
#pragma warning restore CA1815 // Override equals and operator equals on value types
        #endregion


    }
}
