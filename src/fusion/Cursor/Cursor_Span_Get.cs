using System;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        #region " TRY GET KEY DATA PROTECTED "

        /// <summary>
        /// Internal GET KEY operation. Valid for <see cref="LmdbCursorOperation.MDB_GET_CURRENT"/>,
        /// <see cref="LmdbCursorOperation.MDB_FIRST"/>, <see cref="LmdbCursorOperation.MDB_NEXT"/>,
        /// <see cref="LmdbCursorOperation.MDB_PREV"/>
        /// and <see cref="LmdbCursorOperation.MDB_LAST"/>. Returns key, but not data.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">  [out] . </param>
        /// <param name="op">   The operation. </param>
        /// <returns>   <c>true</c>if record was found, <c>false</c> otherwise. </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        protected bool TryGetKey( out ReadOnlySpan<byte> key, CursorOperation op )
        {
            int ret;
            unsafe
            {
                var dbKey = default( LmdbValue );
                ret = SafeNativeMethods.mdb_cursor_get( this.Handle, in dbKey, null, op );
                key = dbKey.ToReadOnlySpan();
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>
        /// Internal GET DATA operation. Valid for <para>
        /// <see cref="CursorOperation.FirstDuplicate"/> <see cref="LmdbCursorOperation.MDB_FIRST_DUP"/>, </para><para>
        /// <see cref="CursorOperation.NextDuplicate"/> <see cref="LmdbCursorOperation.MDB_NEXT_DUP"/>,            </para><para>
        /// <see cref="CursorOperation.NextNoDuplicate"/> <see cref="LmdbCursorOperation.MDB_NEXT_NODUP"/>,        </para><para>
        /// <see cref="CursorOperation.PreviousDuplicate"/> <see cref="LmdbCursorOperation.MDB_PREV_DUP"/>,        </para><para>
        /// <see cref="CursorOperation.PreviousNoDuplicate"/> <see cref="LmdbCursorOperation.MDB_PREV_NODUP"/>     </para><para>
        /// <see cref="CursorOperation.LastDuplicate"/> <see cref="LmdbCursorOperation.MDB_LAST_DUP"/>,            </para><para>
        /// <see cref="CursorOperation.GetMultiple"/> <see cref="LmdbCursorOperation.MDB_GET_MULTIPLE"/>,          </para><para>
        /// <see cref="CursorOperation.NextMultiple"/> <see cref="LmdbCursorOperation.MDB_NEXT_MULTIPLE"/>         </para><para>
        /// <see cref="CursorOperation.PreviousNoDuplicate"/> <see cref="LmdbCursorOperation.MDB_PREV_MULTIPLE"/>. </para><para>
        /// Returns [out] data, but not key. </para>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="data"> [out] The data. </param>
        /// <param name="op">   The operation. </param>
        /// <returns>   <c>true</c>if record was found, <c>false</c> otherwise. </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        protected bool TryGetData( out ReadOnlySpan<byte> data, CursorOperation op )
        {
            int ret;
            unsafe
            {
                var dbKey = default( LmdbValue );
                var dbData = default( LmdbValue );
                ret = SafeNativeMethods.mdb_cursor_get( this.Handle, in dbKey, &dbData, op );
                data = dbData.ToReadOnlySpan();
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        #endregion

        #region " TRY GET PROTECTED "

        /// <summary>
        /// Internal GET operation. Valid for <see cref="LmdbCursorOperation.MDB_SET_KEY"/> and
        /// <see cref="LmdbCursorOperation.MDB_SET_RANGE"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">      . </param>
        /// <param name="entry">    [out]. </param>
        /// <param name="op">       The operation. </param>
        /// <returns>   <c>true</c>if record was found, <c>false</c> otherwise. </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        protected bool TryGet( in ReadOnlySpan<byte> key, out KeyDataPair entry, CursorOperation op )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                var dbData = default( LmdbValue );
                ret = SafeNativeMethods.mdb_cursor_get( this.Handle, in dbKey, &dbData, op );
                entry = new KeyDataPair( dbKey.ToReadOnlySpan(), dbData.ToReadOnlySpan() );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>
        /// Internal GET operation. Valid for <see cref="LmdbCursorOperation.MDB_GET_BOTH"/> and
        /// <see cref="LmdbCursorOperation.MDB_GET_BOTH_RANGE"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="keyData">  Information describing the key. </param>
        /// <param name="entry">    [out]. </param>
        /// <param name="op">       The operation. </param>
        /// <returns>   <c>true</c>if record was found, <c>false</c> otherwise. </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        protected bool TryGet( in KeyDataPair keyData, out KeyDataPair entry, CursorOperation op )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( keyData.Key );
                var dbData = LmdbValue.From( keyData.Data );
                ret = SafeNativeMethods.mdb_cursor_get( this.Handle, in dbKey, &dbData, op );
                entry = new KeyDataPair( dbKey.ToReadOnlySpan(), dbData.ToReadOnlySpan() );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>
        /// Internal GET operation. Valid for <see cref="LmdbCursorOperation.MDB_GET_CURRENT"/>,
        /// <see cref="LmdbCursorOperation.MDB_FIRST"/>, <see cref="LmdbCursorOperation.MDB_NEXT"/>,
        /// <see cref="LmdbCursorOperation.MDB_PREV"/>
        /// and <see cref="LmdbCursorOperation.MDB_LAST"/>. Returns both, key and data.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="entry">    [out]. </param>
        /// <param name="op">       The operation. </param>
        /// <returns>   <c>true</c>if record was found, <c>false</c> otherwise. </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        protected bool TryGet( out KeyDataPair entry, CursorOperation op )
        {
            int ret;
            unsafe
            {
                var dbKey = default( LmdbValue );
                var dbData = default( LmdbValue );
                ret = SafeNativeMethods.mdb_cursor_get( this.Handle, in dbKey, &dbData, op );
                entry = new KeyDataPair( dbKey.ToReadOnlySpan(), dbData.ToReadOnlySpan() );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        #endregion

        #region " MOVE and READ "

        /// <summary>
        /// Internal MOVE TO KEY operation. Valid for <see cref="LmdbCursorOperation.MDB_SET"/>. Does not
        /// return key or data.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">  . </param>
        /// <param name="op">   The operation. </param>
        /// <returns>   <c>true</c>if record was found, <c>false</c> otherwise. </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        protected bool TryMoveToKey( in ReadOnlySpan<byte> key, CursorOperation op )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                ret = SafeNativeMethods.mdb_cursor_get( this.Handle, in dbKey, null, op );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>   Moves cursor to key position. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">  . </param>
        /// <returns>   <c>true</c> if successful (key exists), false otherwise. </returns>
        public bool TryMoveToKey( in ReadOnlySpan<byte> key )
        {
            return this.TryMoveToKey( in key, CursorOperation.Set );
        }

        /// <summary>   Move cursor to key position and get the record (key and data). </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">      . </param>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if successful (key exists), false otherwise. </returns>
        public bool TryGetAt( in ReadOnlySpan<byte> key, out KeyDataPair entry )
        {
            return this.TryGet( in key, out entry, CursorOperation.SetKey );
        }

        /// <summary>
        /// Move cursor to first position greater than or equal to specified key and get the record (key
        /// and data).
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">      . </param>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if successful (nearest key exists), false otherwise. </returns>
        public bool TryGetNearest( in ReadOnlySpan<byte> key, out KeyDataPair entry )
        {
            return this.TryGet( in key, out entry, CursorOperation.SetRange );
        }

        /// <summary>   Get key at current position. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">  [out]. </param>
        /// <returns>   <c>true</c> if successful (key exists), false otherwise. </returns>
        public bool TryGetCurrent( out ReadOnlySpan<byte> key )
        {
            return this.TryGetKey( out key, CursorOperation.GetCurrent );
        }

        /// <summary>   Get record (key and data) at current position. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if successful (key exists), false otherwise. </returns>
        public bool TryGetCurrent( out KeyDataPair entry )
        {
            return this.TryGet( out entry, CursorOperation.GetCurrent );
        }

        /// <summary>   Move cursor to next position and get the record (key and data). </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if successful (next key exists), false otherwise. </returns>
        public bool TryGetNext( out KeyDataPair entry )
        {
            return this.TryGet( out entry, CursorOperation.Next );
        }

        /// <summary>   Move cursor to previous position and get the record (key and data). </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if successful (previous key exists), false otherwise. </returns>
        public bool TryGetPrevious( out KeyDataPair entry )
        {
            return this.TryGet( out entry, CursorOperation.Previous );
        }

        #endregion

        #region " MULTI VALUE "

        /// <summary>
        /// Move cursor to first data position of next key and get the key. Only for
        /// <see cref="DatabaseOpenOptions"/>.<see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/></para> </remarks>
        /// <param name="key">  [out]. </param>
        /// <returns>   <c>true</c> if next key exists, false otherwise. </returns>
        public bool TryGetNextKey( out ReadOnlySpan<byte> key )
        {
            return this.TryGetKey( out key, CursorOperation.NextNoDuplicate );
        }

        /// <summary>
        /// Move cursor to last data position of previous key and get the key. Only for
        /// <see cref="DatabaseOpenOptions"/>.<see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/></para> </remarks>
        /// <param name="key">  [out]. </param>
        /// <returns>   <c>true</c> if previous key exists, false otherwise. </returns>
        public bool TryGetPreviousKey( out ReadOnlySpan<byte> key )
        {
            return this.TryGetKey( out key, CursorOperation.PreviousNoDuplicate );
        }

        /// <summary>
        /// Move cursor to first data position of next key and get the record (key and data). Only for
        /// <see cref="DatabaseOpenOptions"/>.<see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/></para> </remarks>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if next key exists, false otherwise. </returns>
        public bool TryGetNextByKey( out KeyDataPair entry )
        {
            return this.TryGet( out entry, CursorOperation.NextNoDuplicate );
        }

        /// <summary>
        /// Move cursor to last data position of previous key and get the record (key and data). Only for
        /// <see cref="DatabaseOpenOptions"/>.<see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/></para> </remarks>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if previous key exists, false otherwise. </returns>
        public bool TryGetPreviousByKey( out KeyDataPair entry )
        {
            return this.TryGet( out entry, CursorOperation.PreviousNoDuplicate );
        }

        /// <summary>
        /// Move cursor to position of key/data pair and get the record (key and data). Only for
        /// <see cref="DatabaseOpenOptions"/>.<see cref="DatabaseOpenOptions.DuplicatesSort"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/></para> </remarks>
        /// <param name="keyData">  . </param>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if key/data pair exists, false otherwise. </returns>
        public bool TryGetAt( in KeyDataPair keyData, out KeyDataPair entry )
        {
            return this.TryGet( in keyData, out entry, CursorOperation.GetBoth );
        }

        /// <summary>
        /// Move cursor to key and nearest data position. key must match, data position must be greater
        /// than or equal to specified data. Then get the record (key and data). Only for
        /// <see cref="DatabaseOpenOptions"/>.<see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/></para> </remarks>
        /// <param name="keyData">  . </param>
        /// <param name="entry">    [out]. </param>
        /// <returns>   <c>true</c> if nearest key/data pair exists, false otherwise. </returns>
        public bool TryGetNearest( in KeyDataPair keyData, out KeyDataPair entry )
        {
            return this.TryGet( in keyData, out entry, CursorOperation.GetBothRange );
        }

        #endregion

        #region " FIXED MULTI VALUE "

        /// <summary>
        /// Return up to a page of duplicate data items from current cursor position. Move cursor to
        /// prepare for <see cref="LmdbCursorOperation.MDB_NEXT_MULTIPLE"/>. Only for
        /// <see cref="LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/> and <see cref="AssertFixedDataHandle"/></para> </remarks>
        /// <param name="data"> [out]. </param>
        /// <returns>   <c>true</c> if key/data pair exists, false otherwise. </returns>
        public bool TryGetMultiple( out ReadOnlySpan<byte> data )
        {
            return this.TryGetData( out data, CursorOperation.GetMultiple );
        }

        /// <summary>
        /// Return up to a page of duplicate data items from next cursor position. Move cursor to prepare
        /// for <see cref="LmdbCursorOperation.MDB_NEXT_MULTIPLE"/>. Only for
        /// <see cref="LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/> and <see cref="AssertFixedDataHandle"/></para> </remarks>
        /// <param name="data"> [out]. </param>
        /// <returns>   <c>true</c> if key/data pair exists, false otherwise. </returns>
        public bool TryGetNextMultiple( out ReadOnlySpan<byte> data )
        {
            return this.TryGetData( out data, CursorOperation.NextMultiple );
        }

        /// <summary>
        /// Position at previous page and return up to a page of duplicate data items. Only for
        /// <see cref="LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate <see cref="AssertMultiValueHandle"/> and <see cref="AssertFixedDataHandle"/></para> </remarks>
        /// <param name="data"> [out]. </param>
        /// <returns>   <c>true</c> if key/data pair exists, false otherwise. </returns>
        public bool TryGetPreviousMultiple( out ReadOnlySpan<byte> data )
        {
            return this.TryGetData( out data, CursorOperation.PreviousMultiple );
        }

        #endregion

    }

}
