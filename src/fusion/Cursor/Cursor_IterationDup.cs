using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        #region " ENUMERABLE " 

        /// <summary>
        /// Enumerates the key/value pairs of the <see cref="Cursor"/> starting at the current position.
        /// </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        /// <returns>   <see cref="ValueTuple"/> key/value pairs of <see cref="LmdbBuffer"/> </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public IEnumerable<ValueTuple<LmdbBuffer, LmdbBuffer>> AsEnumerableMultiple()
        {
            do
            {
                var (resultCode, key, value) = this.GetMultiple();
                if ( resultCode == ( int ) NativeResultCode.Success )
                {
                    yield return (key, value);
                }
            } while ( this.Next() == ( int ) NativeResultCode.Success );
        }

        #endregion


        #region " MULTI VALUE ITERATORS "

        /// <summary>
        /// Iterates over all keys in sort order. Retrieves each key's first duplicate record in
        /// duplicate sort order. One can iterate over the duplicate records separately in a nested loop
        /// with <see cref="ValuesForward"/>
        /// or <see cref="ValuesReverse"/>, or one can iterate including duplicate records with
        /// <see cref="Cursor.Forward"/>.
        /// </summary>
        /// <value> The forward by key. </value>
        public ItemsIterator ForwardByKey => new( this, CursorOperation.First, CursorOperation.NextNoDuplicate );

        /// <summary>   Iterates over keys in sort order, from the next position on. </summary>
        /// <value> The forward from next by key. </value>
        public NextItemsIterator ForwardFromNextByKey => new( this, CursorOperation.NextNoDuplicate );

        /// <summary>   Iterates over keys in sort order, from the current position on. </summary>
        /// <value> The forward from current by key. </value>
        public ItemsIterator ForwardFromCurrentByKey => new( this, CursorOperation.GetCurrent, CursorOperation.NextNoDuplicate );

        /// <summary>
        /// Iterates over all keys in reverse sort order. Retrieves each key's first duplicate record in
        /// duplicate sort order. One can iterate over the duplicate records separately in a nested loop
        /// with <see cref="ValuesForward"/>
        /// or <see cref="ValuesReverse"/>, or one can iterate including duplicate records with
        /// <see cref="Cursor.Reverse"/>.
        /// </summary>
        /// <value> The reverse by key. </value>
        public ItemsIterator ReverseByKey => new( this, CursorOperation.Last, CursorOperation.PreviousNoDuplicate );

        /// <summary>   Iterates over keys in reverse sort order, from the previous position on. </summary>
        /// <value> The reverse from next by key. </value>
        public NextItemsIterator ReverseFromNextByKey => new( this, CursorOperation.PreviousNoDuplicate );

        /// <summary>   Iterates over keys in reverse sort order, from the current position on. </summary>
        /// <value> The reverse from current by key. </value>
        public ItemsIterator ReverseFromCurrentByKey => new( this, CursorOperation.GetCurrent, CursorOperation.PreviousNoDuplicate );

        /// <summary>   Iterates over all duplicate records for the current key. </summary>
        /// <value> The values forward. </value>
        public ValuesIterator ValuesForward => new( this, CursorOperation.FirstDuplicate, CursorOperation.NextDuplicate );

        /// <summary>
        /// Iterates over duplicate records for the current key, in duplicate sort order from the next
        /// position on.
        /// </summary>
        /// <value> The values forward from next. </value>
        public ValuesNextIterator ValuesForwardFromNext => new( this, CursorOperation.NextDuplicate );

        /// <summary>
        /// Iterates over duplicate records for the current key, in duplicate sort order from the current
        /// position on.
        /// </summary>
        /// <value> The values forward from current. </value>
        public ValuesIterator ValuesForwardFromCurrent => new( this, CursorOperation.GetCurrent, CursorOperation.NextDuplicate );

        /// <summary>
        /// Iterates over all duplicate records for the current key, in reverse duplicate sort order.
        /// </summary>
        /// <value> The values reverse. </value>
        public ValuesIterator ValuesReverse => new( this, CursorOperation.LastDuplicate, CursorOperation.PreviousDuplicate );

        /// <summary>
        /// Iterates over duplicate records for the current key, in reverse duplicate sort order from the
        /// previous position on.
        /// </summary>
        /// <value> The values reverse from previous. </value>
        public ValuesNextIterator ValuesReverseFromPrevious => new( this, CursorOperation.PreviousDuplicate );

        /// <summary>
        /// Iterates over duplicate records for the current key, in reverse duplicate sort order from the
        /// current position on.
        /// </summary>
        /// <value> The values reverse from current. </value>
        public ValuesIterator ValuesReverseFromCurrent => new( this, CursorOperation.GetCurrent, CursorOperation.PreviousDuplicate );

        #endregion

        #region " FIXED ITERATORS "

        /// <summary>
        /// Iterates over multiple duplicate records in sort order, from the current position on.
        /// Retrieves at most one page of records. For use in foreach loops.
        /// </summary>
        /// <value> The forward multiple. </value>
        public ValuesIterator ForwardMultiple => new( this, CursorOperation.GetMultiple, CursorOperation.NextMultiple );

        /// <summary>
        /// Iterates over multiple duplicate records in reverse sort order, from the current position on.
        /// Retrieves at most one page of records. For use in foreach loops.
        /// </summary>
        /// <value> The reverse multiple. </value>
        public ValuesIterator ReverseMultiple => new( this, CursorOperation.GetMultiple, CursorOperation.PreviousMultiple );

        #endregion

        #region " MULTI VALUE  IENUMERABLE SUPPORT "

#pragma warning disable CA1815 // Override equals and operator equals on value types
#pragma warning disable CA1034 // Nested types should not be visible

        /// <summary>
        /// Iterator class that iterates over all values in a key. Equivalent to, but not an
        /// implementation of, <see cref="IEnumerable"/>. Can be used in foreach statements as if it was
        /// an implementation of <see cref="IEnumerable"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public struct ValuesIterator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The operation first. </summary>
            private readonly CursorOperation _OpFirst;

            /// <summary>   The operation next. </summary>
            private readonly CursorOperation _OpNext;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="opFirst">  The operation first. </param>
            /// <param name="opNext">   The operation next. </param>
            internal ValuesIterator( Cursor cursor, CursorOperation opFirst, CursorOperation opNext )
            {
                this._Cursor = cursor;
                this._OpFirst = opFirst;
                this._OpNext = opNext;
            }

            /// <summary>   Equivalent to <see cref="IEnumerable.GetEnumerator()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   The enumerator. </returns>
            public ValuesEnumerator GetEnumerator() => new( this._Cursor, this._OpFirst, this._OpNext );
        }

        /// <summary>
        /// Iterator class that iterates over the values forward from the current duplicate position.
        /// Equivalent to, but not an implementation of, <see cref="IEnumerable"/>. Can be used in
        /// foreach statements as if it was an implementation of <see cref="IEnumerable"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public struct ValuesNextIterator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The operation next. </summary>
            private readonly CursorOperation _OpNext;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="opNext">   The operation next. </param>
            internal ValuesNextIterator( Cursor cursor, CursorOperation opNext )
            {
                this._Cursor = cursor;
                this._OpNext = opNext;
            }

            /// <summary>   Equivalent to <see cref="IEnumerable.GetEnumerator()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   The enumerator. </returns>
            public ValuesNextEnumerator GetEnumerator() => new( this._Cursor, this._OpNext );
        }

        /// <summary>
        /// Enumerator class that iterates over all values in a key. See <see cref="ValuesIterator"/>.
        /// Equivalent to, but not an implementation of, <see cref="IEnumerator"/>. Will be be used in
        /// foreach statements as if it was an implementation of <see cref="IEnumerator"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public ref struct ValuesEnumerator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The operation first. </summary>
            private readonly CursorOperation _OpFirst;

            /// <summary>   The operation next. </summary>
            private readonly CursorOperation _OpNext;

            /// <summary>   True if is current; otherwise, false. </summary>
            private bool _IsCurrent;

            /// <summary>   True if is initialized; otherwise, false. </summary>
            private bool _IsInitialized;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="opFirst">  The operation first. </param>
            /// <param name="opNext">   The operation next. </param>
            internal ValuesEnumerator( Cursor cursor, CursorOperation opFirst, CursorOperation opNext )
            {
                this._Cursor = cursor;
                this._OpFirst = opFirst;
                this._OpNext = opNext;
                this._Current = default;
                this._IsCurrent = false;
                this._IsInitialized = false;
            }

            /// <summary>   The current. </summary>
            private ReadOnlySpan<byte> _Current;

            /// <summary>   Equivalent to <see cref="IEnumerator.Current"/>. </summary>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <value> The current. </value>
            public ReadOnlySpan<byte> Current
            {
                [MethodImpl( MethodImplOptions.AggressiveInlining )]
                get => this._IsCurrent ? this._Current : throw new InvalidOperationException( "Invalid cursor position." );
            }

            /// <summary>   Equivalent to <see cref="IEnumerator.MoveNext()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   True if it succeeds, false if it fails. </returns>
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            public bool MoveNext()
            {
                if ( this._IsInitialized )
                    return this._IsCurrent = this._Cursor.TryGetData( out this._Current, this._OpNext );
                else
                {
                    this._IsInitialized = true;
                    return this._IsCurrent = this._Cursor.TryGetData( out this._Current, this._OpFirst );
                }
            }
        }

        /// <summary>
        /// Enumerator class that iterates over the values forward from the current duplicate position.
        /// See <see cref="ValuesNextIterator"/>. Equivalent to, but not an implementation of,
        /// <see cref="IEnumerator"/>. Will be used in foreach statements as if it was an
        /// implementation of <see cref="IEnumerator"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public ref struct ValuesNextEnumerator
        {
            /// <summary>   The cursor. </summary>
            private readonly Cursor _Cursor;

            /// <summary>   The operation next. </summary>
            private readonly CursorOperation _OpNext;

            /// <summary>   True if is current; otherwise, false. </summary>
            private bool _IsCurrent;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
            /// <param name="opNext">   The operation next. </param>
            internal ValuesNextEnumerator( Cursor cursor, CursorOperation opNext )
            {
                this._Cursor = cursor;
                this._OpNext = opNext;
                this._Current = default;
                this._IsCurrent = false;
            }

            /// <summary>   The current. </summary>
            private ReadOnlySpan<byte> _Current;

            /// <summary>   Equivalent to <see cref="IEnumerator.Current"/>. </summary>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <value> The current. </value>
            public ReadOnlySpan<byte> Current
            {
                [MethodImpl( MethodImplOptions.AggressiveInlining )]
                get => this._IsCurrent ? this._Current : throw new InvalidOperationException( "Invalid cursor position." );
            }

            /// <summary>   Equivalent to <see cref="IEnumerator.MoveNext()"/>. </summary>
            /// <remarks>   Remarked by David, 2020-12-11. </remarks>
            /// <returns>   True if it succeeds, false if it fails. </returns>
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            public bool MoveNext()
            {
                return this._IsCurrent = this._Cursor.TryGetData( out this._Current, this._OpNext );
            }
        }

#pragma warning restore CA1034 // Nested types should not be visible
#pragma warning restore CA1815 // Override equals and operator equals on value types
        #endregion

    }


}
