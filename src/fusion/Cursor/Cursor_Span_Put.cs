using System;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    public partial class Cursor
    {

        #region " PUT OPERATIONS: LIGHTNING "

        /// <summary>
        /// Store by cursor.
        /// This function stores key/data pairs into the database. The cursor is positioned at the new item, or on failure usually near it.
        /// Note: Earlier documentation incorrectly said errors would leave the state of the cursor unchanged.
        /// If the function fails for any reason, the state of the cursor will be unchanged. 
        /// If the function succeeds and an item is inserted into the database, the cursor is always positioned to refer to the newly inserted item.
        /// </summary>
        /// <param name="key">The key operated on.</param>
        /// <param name="value">The data operated on.</param>
        /// <param name="options">
        /// Options for this operation. This parameter must be set to 0 or one of the values described here.
        ///     CursorPutOptions.Current - overwrite the data of the key/data pair to which the cursor refers with the specified data item. The key parameter is ignored.
        ///     CursorPutOptions.NoDuplicateData - enter the new key/data pair only if it does not already appear in the database. This flag may only be specified if the database was opened with MDB_DUPSORT. The function will return MDB_KEYEXIST if the key/data pair already appears in the database.
        ///     CursorPutOptions.NoOverwrite - enter the new key/data pair only if the key does not already appear in the database. The function will return MDB_KEYEXIST if the key already appears in the database, even if the database supports duplicates (MDB_DUPSORT).
        ///     CursorPutOptions.ReserveSpace - reserve space for data of the given size, but don't copy the given data. Instead, return a pointer to the reserved space, which the caller can fill in later. This saves an extra memcpy if the data is being generated later.
        ///     CursorPutOptions.AppendData - append the given key/data pair to the end of the database. No key comparisons are performed. This option allows fast bulk loading when keys are already known to be in the correct order. Loading unsorted keys with this flag will cause data corruption.
        ///     CursorPutOptions.AppendDuplicateData - as above, but for sorted dup data.
        /// </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public int Put( byte[] key, byte[] value, CursorPutOptions options )
        {
            return this.Put( key.AsSpan(), value.AsSpan(), options );
        }


        /// <summary>
        /// Store by cursor.
        /// This function stores key/data pairs into the database. The cursor is positioned at the new item, or on failure usually near it.
        /// Note: Earlier documentation incorrectly said errors would leave the state of the cursor unchanged.
        /// If the function fails for any reason, the state of the cursor will be unchanged. 
        /// If the function succeeds and an item is inserted into the database, the cursor is always positioned to refer to the newly inserted item.
        /// </summary>
        /// <param name="key">The key operated on.</param>
        /// <param name="value">The data operated on.</param>
        /// <param name="options">
        /// Options for this operation. This parameter must be set to 0 or one of the values described here.
        ///     CursorPutOptions.Current - overwrite the data of the key/data pair to which the cursor refers with the specified data item. The key parameter is ignored.
        ///     CursorPutOptions.NoDuplicateData - enter the new key/data pair only if it does not already appear in the database. This flag may only be specified if the database was opened with MDB_DUPSORT. The function will return MDB_KEYEXIST if the key/data pair already appears in the database.
        ///     CursorPutOptions.NoOverwrite - enter the new key/data pair only if the key does not already appear in the database. The function will return MDB_KEYEXIST if the key already appears in the database, even if the database supports duplicates (MDB_DUPSORT).
        ///     CursorPutOptions.ReserveSpace - reserve space for data of the given size, but don't copy the given data. Instead, return a pointer to the reserved space, which the caller can fill in later. This saves an extra memcpy if the data is being generated later.
        ///     CursorPutOptions.AppendData - append the given key/data pair to the end of the database. No key comparisons are performed. This option allows fast bulk loading when keys are already known to be in the correct order. Loading unsorted keys with this flag will cause data corruption.
        ///     CursorPutOptions.AppendDuplicateData - as above, but for sorted dup data.
        /// </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public unsafe int Put( ReadOnlySpan<byte> key, ReadOnlySpan<byte> value, CursorPutOptions options )
        {
            fixed ( byte* keyPtr = key )
            fixed ( byte* valPtr = value )
            {
                var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                var mdbValue = new LmdbBuffer( value.Length, valPtr );
                // was: return SafeNativeMethods.mdb_cursor_put( this.Handle, mdbKey, mdbValue, options );
                return SafeNativeMethods.mdb_cursor_put( this.Handle, ref mdbKey, ref mdbValue, options );
            }
        }

        /// <summary>
        /// Store by cursor.
        /// This function stores key/data pairs into the database. 
        /// If the function fails for any reason, the state of the cursor will be unchanged. 
        /// If the function succeeds and an item is inserted into the database, the cursor is always positioned to refer to the newly inserted item.
        /// </summary>
        /// <param name="key">The key operated on.</param>
        /// <param name="values">The data items operated on.</param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public unsafe int Put( byte[] key, byte[][] values )
        {
            const int StackAllocateLimit = 256;//I just made up a number, this can be much more aggressive -arc

            int overallLength = values.Sum( arr => arr.Length );//probably allocates but boy is it handy...


            //the idea here is to gain some performance by stack-allocating the buffer to 
            //hold the contiguous keys
            if ( overallLength < StackAllocateLimit )
            {
                Span<byte> contiguousValues = stackalloc byte[overallLength];

                return InnerPutMultiple( contiguousValues );
            }
            else
            {
                fixed ( byte* contiguousValuesPtr = new byte[overallLength] )
                {
                    Span<byte> contiguousValues = new( contiguousValuesPtr, overallLength );
                    return InnerPutMultiple( contiguousValues );
                }
            }

            // these local methods could be made static, but the compiler will emit these closures
            // as structs with very little overhead. Also static local functions isn't available 
            // until C# 8 so I can't use it anyway...
            int InnerPutMultiple( Span<byte> contiguousValuesBuffer )
            {
                FlattenInfo( contiguousValuesBuffer );
                var contiguousValuesPtr = ( byte* ) Unsafe.AsPointer( ref contiguousValuesBuffer.GetPinnableReference() );

                var mdbValue = new LmdbBuffer( GetSize(), contiguousValuesPtr );
                var mdbCount = new LmdbBuffer( values.Length, ( byte* ) null );

                Span<LmdbBuffer> dataBuffer = stackalloc LmdbBuffer[2] { mdbValue, mdbCount };

                fixed ( byte* keyPtr = key )
                {
                    var mdbKey = new LmdbBuffer( key.Length, keyPtr );
                    return SafeNativeMethods.mdb_cursor_put( this.Handle, ref mdbKey, ref dataBuffer, CursorPutOptions.MultipleData );
                }
            }

            void FlattenInfo( Span<byte> targetBuffer )
            {
                var cursor = targetBuffer;

                foreach ( var buffer in values )
                {
                    buffer.CopyTo( cursor );
                    cursor = cursor[buffer.Length..];
                }
            }

            int GetSize()
            {
                return values.Length == 0 || values[0] == null || values[0].Length == 0 ? 0 : values[0].Length;
            }
        }

        #endregion

        #region " PUT OPERATIONS "

        /// <summary>   Internal PUT operation. Inserts or updates data at key position. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not <see cref="AssertWritableHandle"/></para></remarks>
        /// <param name="entry">    [in] <see cref="KeyDataPair"/>. </param>
        /// <param name="option">   Cursor put option. </param>
        /// <returns>
        /// <c>true</c> if inserted or updated without error, <c>false</c> if
        /// <see cref="Fusion.CursorPutOptions.NoOverwrite"/> was specified and the key already exists.
        /// </returns>
        ///
        /// <exception cref="LmdbException">    Exception based on native return code. </exception>
        [CLSCompliant( false )]
        protected bool TryPutInternal( in KeyDataPair entry, uint option )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( entry.Key );
                var dbData = LmdbValue.From( entry.Data );
                ret = SafeNativeMethods.mdb_cursor_put( this.Handle, in dbKey, &dbData, option );
            }
            if ( ret == ( int ) NativeResultCode.KeyExist )
                return false;
            _ = LmdbException.AssertExecute( ret );
            return true;
        }

        /// <summary>
        /// Store by cursor. This function stores key/data pairs into the database. The cursor is
        /// positioned at the new item, or on failure usually near it. Note: Earlier documentation
        /// incorrectly said errors would leave the state of the cursor unchanged.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not <see cref="AssertWritableHandle"/></para></remarks>
        /// <param name="entry">    [in] <see cref="KeyDataPair"/>. </param>
        /// <param name="option">   Cursor put option. </param>
        /// <returns>
        /// <c>true</c> if inserted or updated without error, <c>false</c> if
        /// <see cref="Fusion.CursorPutOptions.NoOverwrite"/>
        /// was specified and the key already exists.
        /// </returns>
        public bool TryPut( in KeyDataPair entry, CursorPutOptions option )
        {
            return this.TryPutInternal( in entry, unchecked(( uint ) option) );
        }

        #endregion

        #region " FIX MULTI VALUE "

        /// <summary>
        /// Store multiple contiguous data elements by cursor. This is only valid for
        /// Fixed value multi-database instances (they are opened with
        /// <see cref="DatabaseOpenOptions.DuplicatesFixed"/> flag). The cursor is positioned at
        /// the new item, or on failure usually near it. Note: Earlier documentation incorrectly said
        /// errors would leave the state of the cursor unchanged.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not <see cref="AssertWritableHandle"/> and <see cref="AssertMultiValueHandle"/> and <see cref="AssertFixedDataHandle"/></para></remarks>
        /// <param name="key">          . </param>
        /// <param name="data">         Fixed size records laid out in contiguous memory. </param>
        /// <param name="itemCount">    [in,out] On input, # of items to store, on output, number of
        ///                             items actually stored. </param>
        /// <returns>
        /// <c>true</c> if inserted without error, <c>false</c> if
        /// <see cref="Fusion.CursorPutOptions.NoOverwrite"/>
        /// was specified and the key already exists.
        /// </returns>
        public bool TryPutMultiple( in ReadOnlySpan<byte> key, in ReadOnlySpan<byte> data, ref int itemCount )
        {
            if ( this.DataSize * itemCount > data.Length )
                _ = LmdbException.AssertExecute( ( int ) LmdbFusionErrorCode.TooManyFixedItems );

            int ret;
            unsafe
            {
                var dbMultiData = stackalloc LmdbValue[2];
                fixed ( void* firstDataPtr = data )
                {
                    var dbKey = LmdbValue.From( key );
                    dbMultiData[0] = new LmdbValue( firstDataPtr, this.DataSize );
                    dbMultiData[1] = new LmdbValue( null, itemCount );
                    ret = SafeNativeMethods.mdb_cursor_put( this.Handle, in dbKey, dbMultiData, LmdbConstants.MDB_MULTIPLE );
                    itemCount = ( int ) dbMultiData[1].Size;
                }
            }
            if ( ret == ( int ) NativeResultCode.KeyExist )
                return false;
            _ = LmdbException.AssertExecute( ret );
            return true;
        }

        #endregion
    }

}
