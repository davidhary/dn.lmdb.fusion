using System;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    /// <summary>   A Cursor for iterating an LMDB database. </summary>
    /// <remarks>
    /// Remarked by David, 2020-12-11.
    /// 
    /// Cursors are associated with a specific transaction and database and may not span threads.
    /// 
    /// A cursor cannot be used when its database handle is closed. Nor when its transaction has
    /// ended, except with mdb_cursor_renew(). It can be discarded with mdb_cursor_close().
    /// 
    /// A cursor in a write-transaction can be closed before its transaction ends, and will otherwise
    /// be closed when its transaction ends.
    /// 
    /// A cursor in a read-only transaction must be closed explicitly, before or after its
    /// transaction ends. It can be reused with mdb_cursor_renew()
    /// before finally closing it.
    /// 
    /// <see cref="Fusion.Cursor.Renew()"/>: Cursors that are only used in read-only transactions may be re-
    /// used, to avoid unnecessary malloc/free overhead. Thus, the renewed cursor may be associated
    /// with a new read-only transaction, and referencing the same database handle as it was created
    /// with.
    /// 
    /// Read-only cursors are therefore not disposed when their transaction is disposed. However
    /// because cursors use safe handles, the managed environment is capable of handling the 
    /// orphan handles.
    /// 
    /// Using a Safe Handle cursors should be treated as a managed resource. In fact, using a finalizer
    /// could cause race relations in case the reference tot he cursor is not kept in the managed objects
    /// which use the cursor handle. 
    ///  
    /// Note: Earlier documentation said that cursors in every transaction were closed when the
    /// transaction committed or aborted.
    /// 
    /// Note: If one does not close database handles (leaving that to the environment), then one does
    /// not have to worry about closing a cursor before closing its database.
    /// </remarks>
    public partial class Cursor : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Create a cursor. A cursor is associated with a specific transaction and database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="database">         The database. </param>
        /// <param name="transaction">      Transaction under which the cursor performs. </param>
        /// <param name="isReadOnly">       (Optional) True if this object is read only; otherwise,
        ///                                 false. </param>
        /// <param name="isMultiValue">     (Optional) True if this object is multi-value; otherwise,
        ///                                 false. </param>
        /// <param name="dataSize">         (Optional) The size of the data. Greater than zero for fixed
        ///                                 data size. </param>
        /// <param name="validateHandles">  (Optional) True to validate handles. </param>
        internal Cursor( Database database, Transaction transaction, bool isReadOnly = false, bool isMultiValue = false, int dataSize = 0, bool validateHandles = false )
        {
            if ( database == null )
                throw new ArgumentNullException( nameof( database ) );

            if ( transaction == null )
                throw new ArgumentNullException( nameof( transaction ) );

            this.SafeHandle = new CursorSafeHandle( Cursor.OpenCursorHandle( database, transaction, validateHandles ) );
            this.IsReadOnly = isReadOnly || transaction.IsReadOnly;
            this.IsMultiValue = isMultiValue || (0 != (DatabaseOpenOptions.DuplicatesSort & database.Config.OpenOptions));
            this.DataSize = dataSize;
            this.Transaction = transaction;
            if ( !this.IsReadOnly )
                this.Transaction.Disposing += this.Dispose;
        }

        /// <summary>   Obtains a cursor handle. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="database">         The database. </param>
        /// <param name="transaction">      Transaction under which the cursor performs. </param>
        /// <param name="validateHandles">  True to validate handles. </param>
        /// <returns>   An IntPtr. </returns>
        private static IntPtr OpenCursorHandle( Database database, Transaction transaction, bool validateHandles )
        {
            int ret = validateHandles
                        ? SafeNativeMethods.mdb_cursor_open( transaction.SafeHandle.AssertAllocatedHandle(), database.AssertAllocatedHandle(), out IntPtr cursorHandle )
                        : SafeNativeMethods.mdb_cursor_open( transaction.SafeHandle.AssertAllocatedHandle(), database.AssertAllocatedHandle(), out cursorHandle );
            _ = LmdbException.AssertExecute( ret );
            return cursorHandle;
        }

        /// <summary>
        /// Create a cursor for a specific transaction and database. The cursor
        /// <see cref="Cursor.IsReadOnly"/> if the transaction <see cref="Transaction.IsReadOnly"/>,
        /// <see cref="Cursor.IsMultiValue"/>  if database
        /// <see cref="DatabaseConfiguration.IsMultiValue"/>, and <see cref="Cursor.IsFixedData"/> if
        /// database <see cref="DatabaseConfiguration.FixedDataSize"/> is non-zero.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="database">     The database. </param>
        /// <param name="transaction">  Transaction under which the cursor performs. </param>
        /// <returns>   A Cursor. </returns>
        internal static Cursor OpenCursor( Database database, Transaction transaction )
        {
            return new Cursor( database, transaction, transaction.IsReadOnly, database.Config.IsMultiValue, database.Config.FixedDataSize );
        }

        #region " UNMANAGED RESOURCES "

        /// <summary>   Safe handle holding the address where the #MDB_cursor handle is stored. </summary>
        internal CursorSafeHandle SafeHandle { get; private set; }

        /// <summary>   Gets the handle. </summary>
        /// <value> The handle. </value>
        internal IntPtr Handle => this.SafeHandle.Handle;

        #endregion

        #region " IDISPOSABLE SUPPORT "

        /// <summary>   Implementation of Dispose() pattern. See <see cref="Dispose()"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// No finalizer is needed. The finalizer on <see cref="SafeHandle"/> will clean up the
        /// <see cref="Cursor.SafeHandle"/> instance, if it hasn't already been disposed.
        /// However, because there may be a need for a subclass to introduce a finalizer, 
        /// so the <see cref="IDisposable"/> is properly implemented in this class.
        /// </para></remarks>
        /// <param name="disposing">    <c>true</c> if explicitly disposing (finalizer not run),
        ///                             <c>false</c> if disposed from finalizer. </param>
        protected virtual void Dispose( bool disposing )
        {
            // the handle becomes invalid only on dispose.
            if ( this.SafeHandle is not Object || this.SafeHandle.IsInvalid )
                return;

            this.SafeHandle?.Dispose();

            if ( !this.IsReadOnly && this.Transaction is object )
                this.Transaction.Disposing -= this.Dispose;
        }

        /// <summary>
        /// Closes a cursor handle. The cursor handle will be freed and must
        /// not be used again after this call. Its transaction must still be live if it is a write-
        /// transaction.
        /// </summary>
        /// <remarks>
        /// Cursors will be automatically closed when they are owned by a write transaction. However, in
        /// a read-only transaction, cursors must be closed explicitly.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        #region " NATIVE FUNCTIONS "

        /// <summary>   Return count of duplicates for current key. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not <see cref="AssertMultiValueHandle"/> </para></remarks>
        /// <returns>   The number of duplicates for the current key. </returns>
        public long ValuesCount()
        {
            var ret = SafeNativeMethods.mdb_cursor_count( this.Handle, out UIntPtr result );
            _ = LmdbException.AssertExecute( ret );
            return result.ToUInt32();
        }

        /// <summary>   Return count of duplicates for current key. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Validates <see cref="AssertMultiValueHandle"/> cursor handle. </para></remarks>
        /// <returns>   The number of duplicates for the current key. </returns>
        public long ValuesCountSafe()
        {
            var handle = this.AssertMultiValueHandle();
            var ret = SafeNativeMethods.mdb_cursor_count( handle, out UIntPtr result );
            _ = LmdbException.AssertExecute( ret );
            return result.ToUInt32();
        }

        #endregion

        #region " TRANSACTION AND DATABASE HANDLES "

        /// <summary>
        /// Cursor's transaction.
        /// </summary>
        private Transaction Transaction { get; }

        /// <summary>   Native transaction handle. </summary>
        /// <value> The transaction handle. </value>
        public IntPtr TransactionHandle => this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_cursor_txn( handle ) );

        /// <summary>   Native database handle. </summary>
        /// <value> The database handle. </value>
        public IntPtr DatabaseHandle => this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_cursor_dbi( handle ) );

        #endregion

        #region " DELETE OPERATIONS "

        /// <summary>
        /// Delete current key/data pair. This function deletes the key/data pair to which the cursor
        /// refers.
        /// </summary>
        /// <remarks>   David, 2021-01-16. <para>
        /// Does not validate cursor handle or return result. </para></remarks>
        /// <param name="option">   (Optional) Cursor put option. This parameter must be set to 0 or one
        ///                         of the values described here: <para>
        ///                         <see cref="CursorDeleteOption.NoDuplicateData"/> - delete all of the
        ///                         data items for the current key. This option may only be specified if
        ///                         the database was opened with
        ///                         <see cref="DatabaseOpenOptions.DuplicatesSort"/>.</para> </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Delete( CursorDeleteOption option = CursorDeleteOption.None )
        {
            return SafeNativeMethods.mdb_cursor_del( this.Handle, unchecked(( int ) option) );
        }

        /// <summary>
        /// Delete current key/data pair. This function deletes the key/data pair to which the cursor
        /// refers.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="option">   (Optional) Cursor put option. This parameter must be set to 0 or one
        ///                         of the values described here: <para>
        ///                         <see cref="CursorDeleteOption.NoDuplicateData"/> - delete all of the
        ///                         data items for the current key. This option may only be specified if
        ///                         the database was opened with
        ///                         <see cref="DatabaseOpenOptions.DuplicatesSort"/>.</para> </param>
        public void DeleteSafe( CursorDeleteOption option = CursorDeleteOption.None )
        {
            if ( CursorDeleteOption.NoDuplicateData == (option & CursorDeleteOption.NoDuplicateData) )
                _ = this.AssertMultiValueHandle();
            var handle = this.AssertWritableHandle();
            int ret = SafeNativeMethods.mdb_cursor_del( handle, unchecked(( int ) option) );
            _ = LmdbException.AssertExecute( ret );
        }

        /// <summary>
        /// Delete current key/data pair. This function deletes the key/data pair to which the cursor
        /// refers.
        /// </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-16. <para>
        /// Does not validate cursor handle or return result. </para>
        /// </remarks>
        /// <param name="removeAllDuplicateData">   If true, delete all of the data items for the current
        ///                                         key. This flag may only be specified if the database
        ///                                         was opened with MDB_DUPSORT. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Delete( bool removeAllDuplicateData )
        {
            return this.Delete( removeAllDuplicateData ? CursorDeleteOption.NoDuplicateData : CursorDeleteOption.None );
        }

        /// <summary>
        /// Delete current key/data pair. This function deletes the key/data pair to which the cursor
        /// refers.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="removeAllDuplicateData">   (Optional) if true, delete all of the data items for
        ///                                         the current key. This flag may only be specified if
        ///                                         the database was opened with MDB_DUPSORT. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void DeleteSafe( bool removeAllDuplicateData )
        {
            _ = this.Delete( removeAllDuplicateData ? CursorDeleteOption.NoDuplicateData : CursorDeleteOption.None );
        }

        /// <summary>   Deletes the duplicate data. </summary>
        /// <remarks>
        /// Remarked by David, 2021-01-16. <para>
        /// Does not validate cursor handle or return result. </para>
        /// </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int DeleteDuplicateData()
        {
            return this.Delete( CursorDeleteOption.NoDuplicateData );
        }

        /// <summary>
        /// Delete current key/data pair.
        /// This function deletes the key/data range for which duplicates are found.
        /// </summary>
        public void DeleteDuplicateDataSafe()
        {
            this.DeleteSafe( CursorDeleteOption.NoDuplicateData );
        }

        #endregion

        #region " READ ONLY CURSOR "

        /// <summary>   Ensures that the cursor has a handle and is writable (not read-only). </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   An valid cursor handle. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        protected IntPtr AssertWritableHandle()
        {
            return this.IsReadOnly ? throw new LmdbException( ( int ) LmdbFusionErrorCode.NotWritable, "Cursor is read-only" ) : this.SafeHandle.AssertAllocatedHandle();
        }

        /// <summary>   Indicates if cursor is read-only. </summary>
        /// <value> True if this object is read only; otherwise, false. </value>
        public bool IsReadOnly { get; }

        /// <summary>
        /// Renews cursor, allows re-use without re-allocation. Only valid for read-only cursors.
        /// </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Cursors are associated with a specific transaction and database and may not span threads.
        /// Cursors that are only used in read-only transactions may be re-used, to avoid unnecessary
        /// malloc/free overhead. Thus, the renewed cursor may be associated with a new read-only
        /// transaction, and referencing the same database handle as it was created with.</para> <para>
        /// Does not validate <see cref="IsReadOnly"/> handle</para>
        /// </remarks>
        /// <param name="transaction">  Transaction under which the cursor performs. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Renew( Transaction transaction )
        {
            return this.Renew( transaction.Handle );
        }

        /// <summary>
        /// Renews cursor, allows re-use without re-allocation. Only valid for read-only cursors.
        /// </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Cursors are associated with a specific transaction and database and may not span threads.
        /// Cursors that are only used in read-only transactions may be re-used, to avoid unnecessary
        /// malloc/free overhead. Thus, the renewed cursor may be associated with a new read-only
        /// transaction, and referencing the same database handle as it was created with.</para>
        /// </remarks>
        /// <param name="transaction">  The transaction. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public void RenewSafe( Transaction transaction )
        {
            if ( transaction is not object )
                throw new ArgumentNullException( nameof( transaction ) );
            this.RenewSafe( transaction.Handle );
        }

        /// <summary>
        /// Renews cursor, allows re-use without re-allocation. Only valid for read-only cursors.
        /// </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Cursors are associated with a specific transaction and database and may not span threads.
        /// Cursors that are only used in read-only transactions may be re-used, to avoid unnecessary
        /// malloc/free overhead. Thus, the renewed cursor may be associated with a new read-only
        /// transaction, and referencing the same database handle as it was created with.</para> <para>
        /// Does not validate <see cref="IsReadOnly"/> handle</para>
        /// </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Renew()
        {
            return this.Renew( this.TransactionHandle );
        }

        /// <summary>
        /// Renews cursor, allows re-use without re-allocation. Only valid for read-only cursors.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public void RenewSafe()
        {
            this.RenewSafe( this.TransactionHandle );
        }

        /// <summary>
        /// Renews cursor, allows re-use without re-allocation. Only valid for read-only cursors.
        /// </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Cursors are associated with a specific transaction and database and may not span threads.
        /// Cursors that are only used in read-only transactions may be re-used, to avoid unnecessary
        /// malloc/free overhead. Thus, the renewed cursor may be associated with a new read-only
        /// transaction, and referencing the same database handle as it was created with.</para> <para>
        /// Does not validate <see cref="IsReadOnly"/> handle</para>
        /// </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        private int Renew( IntPtr txn )
        {
            return SafeNativeMethods.mdb_cursor_renew( txn, this.Handle );
        }

        /// <summary>
        /// Renews cursor, allows re-use without re-allocation. Only valid for read-only cursors.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        private void RenewSafe( IntPtr txn )
        {
            // Only read-only cursors can be renewed.
            if ( !this.IsReadOnly )
                throw new LmdbException( ( int ) LmdbFusionErrorCode.NotRenewable, "Only read-only cursors can be renewed." );
            _ = this.SafeHandle.AssertExecute( ( handle ) => SafeNativeMethods.mdb_cursor_renew( txn, handle ) );
        }

        #endregion

        #region " SINGLE VALUE CURSOR "


        /// <summary>
        /// Ensures that the cursor has a handle and allows only single-value operations.
        /// </summary>
        /// <remarks>   David, 2020-12-31. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   An IntPtr. </returns>
        ///
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        protected IntPtr AssertSingleValue()
        {
            return !this.IsMultiValue
                   ? this.SafeHandle.AssertAllocatedHandle()
                   : throw new LmdbException( ( int ) LmdbFusionErrorCode.NotSingleValue, "Cursor must not be multi-valued" );
        }

        /// <summary>   Reserves spaces for the key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Not supported for database open options <see cref="Fusion.DatabasePutOptions.AppendDuplicateData"/></para><para>
        /// Does not validate <see cref="AssertWritableHandle"/> writable cursor handle </para> </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        public int Reserve( ref LmdbBuffer key, ref LmdbBuffer value )
        {
            return SafeNativeMethods.mdb_cursor_put( this.Handle, ref key, ref value, CursorPutOptions.ReserveSpace );
        }

        /// <summary>   Reserves spaces for the key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Not supported for database open options <see cref="DatabasePutOptions.AppendDuplicateData"/></para> </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void ReserveSafe( ref LmdbBuffer key, ref LmdbBuffer value )
        {
            // if ( this.Database.OpenFlags.HasFlag( DatabaseOpenOptions.DuplicatesSort ) ) throw new NotSupportedException( "Reserve is not supported for DupSort" );
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_cursor_put( this.AssertWritableHandle(), ref key, ref value, CursorPutOptions.ReserveSpace ) );
        }

        #endregion

        #region " MULTI VALUE CURSOR "

        /// <summary>   Indicates if cursor is multi-value. </summary>
        /// <value> True if this object is multi-value; otherwise, false. </value>
        public bool IsMultiValue { get; }

        /// <summary>
        /// Ensures that the cursor has a handle and allows multi-value search and sort.
        /// </summary>
        /// <remarks>   David, 2020-12-31. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   An IntPtr. </returns>
        ///
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        protected IntPtr AssertMultiValueHandle()
        {
            return this.IsMultiValue
                   ? this.SafeHandle.AssertAllocatedHandle()
                   : throw new LmdbException( ( int ) LmdbFusionErrorCode.NotMultiValue, "Cursor is not multi-valued" );
        }

        #endregion

        #region " FIXED MULTI-VALUE CURSOR "

        /// <summary>   Size of fixed size data items (records). </summary>
        /// <value> The size of the data. </value>
        public int DataSize { get; private set; }

        /// <summary>   Gets a value indicating whether this object is fixed data. </summary>
        /// <value> True if this object is fixed data, false if not. </value>
        public bool IsFixedData => this.DataSize != 0;

        /// <summary>   Ensures that the cursor has a handle and is fixed-size values. </summary>
        /// <remarks>   David, 2020-12-31. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   An IntPtr. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        protected IntPtr AssertFixedDataHandle()
        {
            return this.IsFixedData
                   ? this.SafeHandle.AssertAllocatedHandle()
                   : throw new LmdbException( ( int ) LmdbFusionErrorCode.NotFixed, "Cursor is not fixed data size" );
        }

        #endregion

    }

}


