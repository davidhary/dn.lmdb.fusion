namespace isr.Lmdb.Fusion
{

    /// <summary>   Cursor operation types. </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-10. <para>
    /// (c) 2020 Corey Kaylor. All rights reserved. </para>
    /// </remarks>
    public enum CursorOperation
    {
        /// <summary>
        /// Position at first key/data item (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_FIRST"/>).
        /// </summary>
        First,

        /// <summary>
        /// Position at first data item of current key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_FIRST_DUP"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        FirstDuplicate,

        /// <summary>
        /// Position at key/data pair (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_GET_BOTH"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        GetBoth,

        /// <summary>
        /// position at key, nearest data (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_GET_BOTH_RANGE"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        GetBothRange,

        /// <summary>
        /// Return key/data at current cursor position (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_GET_CURRENT"/>).
        /// </summary>
        GetCurrent,

        /// <summary>
        /// Return all the duplicate data items at the current cursor position (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_GET_MULTIPLE"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesFixed"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        GetMultiple,

        /// <summary>
        /// Position at last key/data item (MDB_LAST).
        /// </summary>
        Last,

        /// <summary>
        /// Position at last data item of current key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_LAST_DUP"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        LastDuplicate,

        /// <summary>
        /// Position at next data item (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_NEXT"/>).
        /// </summary>
        Next,

        /// <summary>
        /// Position at next data item of current key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_NEXT_DUP"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        NextDuplicate,

        /// <summary>
        /// Return all duplicate data items at the next cursor position (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_NEXT_MULTIPLE"/>).
        /// Only for <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        NextMultiple,

        /// <summary>
        /// Position at first data item of next key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_NEXT_NODUP"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        NextNoDuplicate,

        /// <summary>
        /// Position at previous data item (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_PREV"/>).
        /// </summary>
        Previous,

        /// <summary>
        /// Position at previous data item of current key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_PREV_DUP"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        PreviousDuplicate,

        /// <summary>
        /// Position at last data item of previous key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_PREV_NODUP"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        PreviousNoDuplicate,

        /// <summary>
        /// Position at specified key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_SET"/>).
        /// </summary>
        Set,

        /// <summary>
        /// Position at specified key, return key + data (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_SET_KEY"/>).
        /// </summary>
        SetKey,

        /// <summary>
        /// Position at first key greater than or equal to specified key (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_SET_RANGE"/>).
        /// </summary>
        SetRange,

        /// <summary>
        /// Position at previous page and return up to a page of duplicate data items (<see cref="isr.Lmdb.Fusion.Interop.LmdbCursorOperation.MDB_PREV_MULTIPLE"/>).
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesFixed"/> <see cref="isr.Lmdb.Fusion.Interop.LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        PreviousMultiple

    }

}
