using System;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   A Cursor safe handle. </summary>
    /// <remarks>   Remarked by David, 2020-12-16. </remarks>
    internal class CursorSafeHandle : LmdbSafeHandle
    {

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with an invalid handle
        /// and a <see cref="LmdbSafeHandle.ReleaseHandle"/> action
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        public CursorSafeHandle() : base() { }

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with a
        /// handle and a <see cref="LmdbSafeHandle.ReleaseHandle"/> action.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="handle">   The handle. </param>
        public CursorSafeHandle( IntPtr handle ) : base( handle ) { }

        /// <summary>   Frees the allocated handle. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        protected override void Free()
        {
            // tag the handle as free.
            base.Free();
            // free than handle.
            SafeNativeMethods.mdb_cursor_close( this.Handle );
        }
    }
}
