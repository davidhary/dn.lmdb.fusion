using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   Special options for cursor put operations. Includes both singe- and multi-value key cursors. </summary>
    /// <remarks> Remark added by David, 2020-12-16. </remarks>
    [System.Flags]
    public enum CursorPutOptions
    {
        /// <summary>
        /// No special behavior.
        /// </summary>
        None = 0,

        /// <summary>
        /// For put: Don't write if the key already exists <see cref="LmdbConstants.MDB_NOOVERWRITE"/>.
        /// </summary>
        NoOverwrite = 0x10,

        /// <summary>
        /// For put: don't write if the key and data pair already exist.
        /// For mdb_cursor_del: remove all duplicate data items.
        /// <see cref="LmdbConstants.MDB_NODUPDATA"/>
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        NoDuplicateData = 0x20,

        /// <summary>
        /// Overwrite the current key/data pair <see cref="LmdbConstants.MDB_CURRENT"/> 
        /// </summary>
        Current = 0x40,

        /// <summary>
        /// For put: Just reserve space for data, don't copy it. Return a pointer to the reserved space <see cref="LmdbConstants.MDB_RESERVE"/>.
        /// </summary>
        ReserveSpace = 0x10000,

        /// <summary>
        /// Data is being appended, don't split full pages <see cref="LmdbConstants.MDB_APPEND"/>.
        /// </summary>
        AppendData = 0x20000,

        /// <summary>
        /// Duplicate data is being appended, don't split full pages <see cref="LmdbConstants.MDB_APPENDDUP"/>.
        /// Only for <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        AppendDuplicateData = 0x40000,

        /// <summary>   Store multiple data items in one call (MDB_MULTIPLE) <see cref="LmdbConstants.MDB_MULTIPLE"/>. 
        /// Only for <see cref="LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        MultipleData = 0x80000
    }

}
