using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    public partial class Database
    {

        /// <summary>   Puts <see cref="LmdbBuffer"/> key-value pair using Spreads database methods. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        internal int Put( ref LmdbBuffer key, ref LmdbBuffer value, TransactionPutOptions flags = TransactionPutOptions.None )
        {
            return SafeNativeMethods.sdb_put( this.Environment.Handle, this.Handle, ref key, ref value, flags );
        }

        /// <summary>   Puts <see cref="LmdbBuffer"/> key-value pair using Spreads database methods. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        internal void PutThrow( ref LmdbBuffer key, ref LmdbBuffer value, TransactionPutOptions flags = TransactionPutOptions.None )
        {
            _ = LmdbException.AssertExecute( SafeNativeMethods.sdb_put( this.Environment.Handle, this.Handle, ref key, ref value, flags ) );
        }

        /// <summary>   Puts generic key-value pair using Spreads database methods. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Put<TKey, TValue>( TKey key, TValue value, TransactionPutOptions flags = TransactionPutOptions.None )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var valuePtr = Unsafe.AsPointer( ref value );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                return SafeNativeMethods.sdb_put( this.Environment.Handle, this.Handle, ref keyBuffer, ref valueBuffer, flags );
            }
        }

        /// <summary>   Puts generic key-value pair using Spreads database methods. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void PutThrow<TKey, TValue>( TKey key, TValue value, TransactionPutOptions flags = TransactionPutOptions.None )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var valuePtr = Unsafe.AsPointer( ref value );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                _ = LmdbException.AssertExecute( SafeNativeMethods.sdb_put( this.Environment.Handle, this.Handle, ref keyBuffer, ref valueBuffer, flags ) );
            }
        }
    }
}
