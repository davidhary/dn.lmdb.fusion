using System;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    public partial class Database
    {

        /// <summary>   Get items from a database. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Does not validate the database or transaction handles.
        /// 
        /// This function retrieves key/data pairs from the database.
        /// 
        /// The address and length of the data associated with the specified key are returned in the
        /// structure to which data refers.
        /// 
        /// If the database supports duplicate keys (MDB_DUPSORT) then the first data item for the key
        /// will be returned. Retrieval of other items requires the use of mdb_cursor_get().
        /// 
        /// Note: The memory pointed to by the returned values is owned by the database. The caller need
        /// not dispose of the memory, and may not modify it in any way. For values returned in a read-
        /// only transaction any modification attempts will cause a SIGSEGV (invalid memory access
        /// (segmentation fault)). Values returned from the database are valid only until a subsequent
        /// update operation, or the end of the transaction.
        /// </para>
        /// </remarks>
        /// <param name="transaction">  Transaction under which the Get operation is performed. </param>
        /// <param name="key">          Key to specify which data item / record to retrieve. </param>
        /// <param name="data">         [out] Data / record to be returned. </param>
        /// <returns>
        /// <c>true</c> if data for key retrieved without error, <c>false</c> if key does not exist.
        /// </returns>
        public bool Get( Transaction transaction, in ReadOnlySpan<byte> key, out ReadOnlySpan<byte> data )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                // var dbData = default( LmdbValue );
                // was ret = SafeNativeMethods.mdb_get( transaction.Handle, handle, in dbKey, in dbData );
                ret = SafeNativeMethods.mdb_get( transaction.Handle, this.Handle, in dbKey, out LmdbValue dbData );
                data = dbData.ToReadOnlySpan();
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>   Get items from a database. </summary>
        /// <remarks>   David, 2021-01-18. <para>
        /// Validates the database handle. </para> </remarks>
        /// <param name="transaction">  Transaction under which the Get operation is performed. </param>
        /// <param name="key">          Key to specify which data item / record to retrieve. </param>
        /// <param name="data">         [out] Data / record to be returned. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public bool GetSafe( Transaction transaction, in ReadOnlySpan<byte> key, out ReadOnlySpan<byte> data )
        {
            int ret;
            var handle = this.AssertAllocatedHandle();
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                ret = SafeNativeMethods.mdb_get( transaction.Handle, handle, in dbKey, out LmdbValue dbData );
                data = dbData.ToReadOnlySpan();
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>   Internal implementation of the Put operation for use by public operations. </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Does not validate the database or transaction handles. </para></remarks>
        /// <param name="transaction">  Transaction under which the Put operation is performed. </param>
        /// <param name="key">          Key to identify the Data item to be stored. </param>
        /// <param name="data">         Data / record to be stored. </param>
        /// <param name="options">      Options to specify how the Put operation should be performed. </param>
        /// <returns>
        /// <c>true</c> if data for key could be stored without error, <c>false</c> if key already exists
        /// (depending on options).
        /// </returns>
        [CLSCompliant( false )]
        protected bool PutInternal( Transaction transaction, in ReadOnlySpan<byte> key, in ReadOnlySpan<byte> data, uint options )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                var dbData = LmdbValue.From( data );
                ret = SafeNativeMethods.mdb_put( transaction.Handle, this.Handle, in dbKey, in dbData, options );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.KeyExist );
        }

        /// <summary>
        /// Store items into a database. This function stores key/data pairs in the database. The default
        /// behavior is to enter the new key/data pair, replacing any previously existing key.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Does not validate the database or transaction handles. </para></remarks>
        /// <param name="transaction">  Transaction under which the Put operation is performed. </param>
        /// <param name="key">          Key to identify the Data item to be stored. </param>
        /// <param name="data">         Data / record to be stored. </param>
        /// <param name="options">      Options to specify how the Put operation should be performed. </param>
        /// <returns>
        /// <c>true</c> if inserted without error, <c>false</c> if <see cref="DatabasePutOptions.NoOverwrite"/>
        /// was specified and the key already exists.
        /// </returns>
        public bool Put( Transaction transaction, in ReadOnlySpan<byte> key, in ReadOnlySpan<byte> data, DatabasePutOptions options )
        {
            return this.PutInternal( transaction, in key, in data, unchecked(( uint ) options) );
        }

        /// <summary>
        /// Store items into a database. This function stores key/data pairs in the database. The default
        /// behavior is to enter the new key/data pair, replacing any previously existing key.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Does not validate the database or transaction handles. </para></remarks>
        /// <param name="transaction">  Transaction under which the Put operation is performed. </param>
        /// <param name="key">          Key to identify the Data item to be stored. </param>
        /// <param name="value">        The value. </param>
        /// <param name="options">      (Optional) Options to specify how the Put operation should be
        ///                             performed. </param>
        /// <returns>
        /// <c>true</c> if inserted without error, <c>false</c> if <see cref="DatabasePutOptions.NoOverwrite"/>
        /// was specified and the key already exists.
        /// </returns>
        public bool Put( Transaction transaction, byte[] key, byte[] value, DatabasePutOptions options = DatabasePutOptions.None )
        {
            return this.PutInternal( transaction, key.AsSpan(), value.AsSpan(), unchecked(( uint ) options) );
        }

        /// <summary>   Internal implementation of the Put operation for use by public operations. </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Validates the database handle. </para></remarks>
        /// <param name="transaction">  Transaction under which the Put operation is performed. </param>
        /// <param name="key">          Key to identify the Data item to be stored. </param>
        /// <param name="data">         Data / record to be stored. </param>
        /// <param name="options">      Options to specify how the Put operation should be performed. </param>
        /// <returns>
        /// <c>true</c> if data for key could be stored without error, <c>false</c> if key already exists
        /// (depending on options).
        /// </returns>
        [CLSCompliant( false )]
        protected bool PutInternalSafe( Transaction transaction, in ReadOnlySpan<byte> key, in ReadOnlySpan<byte> data, uint options )
        {
            int ret;
            var handle = this.AssertAllocatedHandle();
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                var dbData = LmdbValue.From( data );
                ret = SafeNativeMethods.mdb_put( transaction.Handle, handle, in dbKey, in dbData, options );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.KeyExist );
        }

        /// <summary>
        /// Store items into a database. This function stores key/data pairs in the database. The default
        /// behavior is to enter the new key/data pair, replacing any previously existing key.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Validates the database handle. </para></remarks>
        /// <param name="transaction">  Transaction under which the Put operation is performed. </param>
        /// <param name="key">          Key to identify the Data item to be stored. </param>
        /// <param name="data">         Data / record to be stored. </param>
        /// <param name="options">      Options to specify how the Put operation should be performed. </param>
        /// <returns>
        /// <c>true</c> if inserted without error, <c>false</c> if <see cref="DatabasePutOptions.NoOverwrite"/>
        /// was specified and the key already exists.
        /// </returns>
        public bool PutSafe( Transaction transaction, in ReadOnlySpan<byte> key, in ReadOnlySpan<byte> data, DatabasePutOptions options )
        {
            return this.PutInternalSafe( transaction, in key, in data, unchecked(( uint ) options) );
        }

        /// <summary>
        /// Store items into a database. This function stores key/data pairs in the database. The default
        /// behavior is to enter the new key/data pair, replacing any previously existing key.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Validates the database handle. </para></remarks>
        /// <param name="transaction">  Transaction under which the Put operation is performed. </param>
        /// <param name="key">          Key to identify the Data item to be stored. </param>
        /// <param name="value">        The value. </param>
        /// <param name="options">      (Optional) Options to specify how the Put operation should be
        ///                             performed. </param>
        /// <returns>
        /// <c>true</c> if inserted without error, <c>false</c> if <see cref="DatabasePutOptions.NoOverwrite"/>
        /// was specified and the key already exists.
        /// </returns>
        public bool PutSafe( Transaction transaction, byte[] key, byte[] value, DatabasePutOptions options = DatabasePutOptions.None )
        {
            return this.PutInternalSafe( transaction, key.AsSpan(), value.AsSpan(), unchecked(( uint ) options) );
        }

        /// <summary>
        /// Delete items from a database. This function removes key/data pairs from the database. If this
        /// instance is a multi-database then all of the duplicate data items for the
        /// key will be deleted. This function will return MDB_NOTFOUND if the specified key/data pair is
        /// not in the database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Does not validate the database handle. </para></remarks>
        /// <param name="transaction">  . </param>
        /// <param name="key">          . </param>
        /// <returns>   True if it succeeds (key was found), false if not deleted (key not found). </returns>
        public bool Delete( Transaction transaction, in ReadOnlySpan<byte> key )
        {
            int ret;
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                ret = SafeNativeMethods.mdb_del( transaction.Handle, this.Handle, in dbKey, IntPtr.Zero );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>
        /// Delete items from a database. This function removes key/data pairs from the database. If this
        /// instance is a multi-database then all of the duplicate data items for the
        /// key will be deleted. This function will return MDB_NOTFOUND if the specified key/data pair is
        /// not in the database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Validates the database handle. </para></remarks>
        /// <param name="transaction">  . </param>
        /// <param name="key">          . </param>
        /// <returns>   True if it succeeds (key was found), false if not deleted (key not found). </returns>
        public bool DeleteSafe( Transaction transaction, in ReadOnlySpan<byte> key )
        {
            int ret;
            var handle = this.AssertAllocatedHandle();
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                ret = SafeNativeMethods.mdb_del( transaction.Handle, handle, in dbKey, IntPtr.Zero );
            }
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>
        /// Compare two data items according to a particular database. This returns a comparison as if
        /// the two data items were keys in the specified database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Does not validate the database handle. </para></remarks>
        /// <param name="transaction">  . </param>
        /// <param name="x">            . </param>
        /// <param name="y">            . </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        public int Compare( Transaction transaction, in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            int result;
            unsafe
            {
                var dbx = LmdbValue.From( x );
                var dby = LmdbValue.From( y );
                result = SafeNativeMethods.mdb_cmp( transaction.Handle, this.Handle, in dbx, in dby );
            }
            return result;
        }

        /// <summary>
        /// Compare two data items according to a particular database. This returns a comparison as if
        /// the two data items were keys in the specified database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11 <para>
        ///  Validates the database handle. </para></remarks>
        /// <param name="transaction">  . </param>
        /// <param name="x">            . </param>
        /// <param name="y">            . </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        public int CompareSafe( Transaction transaction, in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            int result;
            var handle = this.AssertAllocatedHandle();
            unsafe
            {
                var dbx = LmdbValue.From( x );
                var dby = LmdbValue.From( y );
                result = SafeNativeMethods.mdb_cmp( transaction.Handle, handle, in dbx, in dby );
            }
            return result;
        }

    }
}
