using System;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    public partial class Database
    {

        #region " PUT " 

        /// <summary>   Puts <see cref="LmdbBuffer"/> key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public int Put( Transaction txn, ref LmdbBuffer key, ref LmdbBuffer value, TransactionPutOptions flags = TransactionPutOptions.None )
        {
            return SafeNativeMethods.mdb_put( txn.Handle, this.Handle, ref key, ref value, flags );
        }

        /// <summary>   Puts <see cref="LmdbBuffer"/> key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <remarks>   David, 2021-01-18. </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        public void PutThrow( Transaction txn, ref LmdbBuffer key, ref LmdbBuffer value, TransactionPutOptions flags = TransactionPutOptions.None )
        {
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_put( txn.Handle, this.Handle, ref key, ref value, flags ) );
        }

        /// <summary>   Puts Generic key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Put<TKey, TValue>( Transaction txn, TKey key, TValue value, TransactionPutOptions flags = TransactionPutOptions.None )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var valuePtr = Unsafe.AsPointer( ref value );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                return SafeNativeMethods.mdb_put( txn.Handle, this.Handle, ref keyBuffer, ref valueBuffer, flags );
            }
        }

        /// <summary>   Puts Generic key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        /// <param name="flags">    (Optional) The flags. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void PutThrow<TKey, TValue>( Transaction txn, TKey key, TValue value, TransactionPutOptions flags = TransactionPutOptions.None )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var valuePtr = Unsafe.AsPointer( ref value );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_put( txn.Handle, this.Handle, ref keyBuffer, ref valueBuffer, flags ) );
            }
        }

        #endregion

        #region " DELETE "

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  [in,out] The key. </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        public int Delete( Transaction txn, ref LmdbBuffer key )
        {
            return SafeNativeMethods.mdb_del( txn.Handle, this.Handle, in key, IntPtr.Zero );
        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">    Generic type parameter. </typeparam>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  The key. </param>
        /// <returns> <see cref="NativeResultCode"/>: A non-zero error value on failure and 0 on success. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Delete<TKey>( Transaction txn, TKey key )
            where TKey : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                return this.Delete( txn, ref keyBuffer );
            }

        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not check on <see cref="LmdbBuffer.IsValid"/> or <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </para> </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Delete( Transaction txn, LmdbBuffer key, LmdbBuffer value )
        {
            return SafeNativeMethods.mdb_del( txn.Handle, this.Handle, in key, in value );
        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not check on <see cref="LmdbBuffer.IsValid"/> or <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </para> </remarks>
        /// <typeparam name="TValue">    Generic type parameter. </typeparam>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Delete<TValue>( Transaction txn, LmdbBuffer key, TValue value )
            where TValue : struct
        {
            unsafe
            {
                var valuePtr = Unsafe.AsPointer( ref value );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                return this.Delete( txn, key, valueBuffer );
            }
        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not check on <see cref="LmdbBuffer.IsValid"/> or <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </para> </remarks>
        /// <typeparam name="TKey">    Generic type parameter. </typeparam>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Delete<TKey>( Transaction txn, TKey key, LmdbBuffer value )
            where TKey : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                return this.Delete( txn, keyBuffer, value );
            }

        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not check on <see cref="LmdbBuffer.IsValid"/> or <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </para> </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public int Delete<TKey, TValue>( Transaction txn, TKey key, TValue value )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var valuePtr = Unsafe.AsPointer( ref value );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                return this.Delete( txn, keyBuffer, valueBuffer );
            }

        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  [in,out] The key. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        public bool DeleteOkay( Transaction txn, ref LmdbBuffer key )
        {
            int ret = SafeNativeMethods.mdb_del( txn.Handle, this.Handle, in key, IntPtr.Zero );
            return LmdbException.AssertFailed( ret, NativeResultCode.NotFound );
        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">    Generic type parameter. </typeparam>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  The key. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool DeleteOkay<TKey>( Transaction txn, TKey key )
            where TKey : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyBuffer = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                return this.DeleteOkay( txn, ref keyBuffer );
            }
        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Checks on <see cref="LmdbBuffer.IsValid"/> and <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </para> </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool DeleteSafe( Transaction txn, LmdbBuffer key, LmdbBuffer value )
        {
            if ( (( int ) this.Config.OpenOptions & ( int ) DatabaseOpenOptions.DuplicatesSort) == 0 )
            {
                throw new InvalidOperationException( $"{nameof( LmdbBuffer )} Value parameter should only be provided for multi-value sorted databases" );
            }

            if ( !value.IsValid )
            {
                throw new InvalidOperationException( $"{nameof( LmdbBuffer )} Value is invalid" );
            }
            int res = SafeNativeMethods.mdb_del( txn.Handle, this.Handle, in key, in value );
            return LmdbException.AssertFailed( res, NativeResultCode.NotFound );
        }

        /// <summary>   Delete key/value at key or all multi-values if db supports multi-values. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-16. <para>
        /// Checks on <see cref="LmdbBuffer.IsValid"/> and
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// </para>
        /// </remarks>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    The value. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool DeleteSafe<TValue>( Transaction txn, LmdbBuffer key, TValue value )
            where TValue : struct
        {
            unsafe
            {
                var valuePtr = Unsafe.AsPointer( ref value );
                var valueBuffer = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );
                return this.DeleteSafe( txn, key, valueBuffer );
            }
        }

        #endregion

        #region " GET "

        /// <summary>   Attempts to get key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not validate the transaction handle as <see cref="Transaction.AssertReadOnlyHandle()"/></para></remarks>
        /// <param name="txn">      A read-only transaction. </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryGet( Transaction txn, ref LmdbBuffer key, out LmdbBuffer value )
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                value = default;
                var valuePtr = Unsafe.AsPointer( ref value );
                var res = SafeNativeMethods.mdb_get( ( void* ) txn.Handle, this.Handle, keyPtr, valuePtr );
                return LmdbException.AssertFailed( res, NativeResultCode.NotFound );
            }
        }

        /// <summary>   Attempts to get key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not validate the transaction handle as <see cref="Transaction.AssertReadOnlyHandle()"/></para></remarks>
        /// <typeparam name="TValue">    Generic type parameter. </typeparam>
        /// <param name="txn">      A read-only transaction. </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryGet<TValue>( Transaction txn, ref LmdbBuffer key, out TValue value )
            where TValue : struct
        {
            unsafe
            {
                if ( this.TryGet( txn, ref key, out var valueDb ) )
                {
                    value = Unsafe.ReadUnaligned<TValue>( valueDb.Data );
                    return true;
                }
            }

            value = default;
            return false;
        }

        /// <summary>   Attempts to get key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not validate the transaction handle as <see cref="Transaction.AssertReadOnlyHandle()"/></para></remarks>
        /// <typeparam name="TKey">    Generic type parameter. </typeparam>
        /// <param name="txn">      A read-only transaction. </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryGet<TKey>( Transaction txn, ref TKey key, out LmdbBuffer value )
            where TKey : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyDb = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                return this.TryGet( txn, ref keyDb, out value );
            }
        }

        /// <summary>   Attempts to get key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. <para>
        /// Does not validate the transaction handle as <see cref="Transaction.AssertReadOnlyHandle()"/></para></remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">      A read-only transaction. </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="value">    [in,out] The value. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <see cref="NativeResultCode.NotFound"/>; otherwise, throws and exception.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryGet<TKey, TValue>( Transaction txn, ref TKey key, out TValue value )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyDb = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );
                return this.TryGet( txn, ref keyDb, out value );
            }
        }

        #endregion
    }
}
