// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;
using isr.Lmdb.Fusion.Span;

namespace isr.Lmdb.Fusion
{
    /// <summary>   A database configuration. </summary>
    /// <remarks>   Remarked by David, 2020-12-16. </remarks>
    /// <remarks>   Remarked by David, 2020-12-16. </remarks>
    public class DatabaseConfiguration
    {

        #region " CONSTRUCTION "

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        public DatabaseConfiguration()
        {
            this.OpenOptions = DatabaseOpenOptions.None;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="openOptions">  Options for opening the database. </param>
        public DatabaseConfiguration( DatabaseOpenOptions openOptions )
        {
            this.OpenOptions = openOptions;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="openOptions">      Options for opening the database. </param>
        /// <param name="dummy">            The dummy value for selecting <see cref="LmdbValue"/>
        ///                                 functions. </param>
        /// <param name="keyCompareFunc">   (Optional) Key comparison function. Can be <c>null</c>. </param>
        /// <param name="dataCompareFunc">  (Optional) Data comparison function. Can be <c>null</c>. </param>
        public DatabaseConfiguration( DatabaseOpenOptions openOptions,
                                      LmdbValue dummy,
                                      Span.SpanCompare<byte> keyCompareFunc = null,
                                      Span.SpanCompare<byte> dataCompareFunc = null )
        {
            this.OpenOptions = openOptions;
#if true
            this.LmdbValueKeyCompareWith( keyCompareFunc );
            this.LmdbValueDataCompareWith( dataCompareFunc );
#else
            this.KeySpanCompare = keyCompareFunc;
            if ( keyCompareFunc is object )
                this.LmdbValueKeyCompareFunction = this.LmdbValueKeySpanCompare;
            this.DataSpanCompare = dataCompareFunc;
            if ( dataCompareFunc is object )
                this.LmdbValueDataCompareFunction = this.LmdbValueDataSpanCompare;
#endif
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="openOptions">      Options for opening the database. </param>
        /// <param name="dummy">            The dummy value for selecting <see cref="LmdbBuffer"/>
        ///                                 functions. </param>
        /// <param name="keyCompareFunc">   (Optional) Key comparison function. Can be <c>null</c>. </param>
        /// <param name="dataCompareFunc">  (Optional) Data comparison function. Can be <c>null</c>. </param>
        public DatabaseConfiguration( DatabaseOpenOptions openOptions,
                                      LmdbBuffer dummy,
                                      Span.SpanCompare<byte> keyCompareFunc = null,
                                      Span.SpanCompare<byte> dataCompareFunc = null )
        {
            this.OpenOptions = openOptions;
#if true
            this.LmdbBufferKeyCompareWith( keyCompareFunc );
            this.LmdbBufferDataCompareWith( dataCompareFunc );
#else
            this.KeySpanCompare = keyCompareFunc;
            if ( keyCompareFunc is object )
                this.LmdbBufferKeyCompareFunction = this.LmdbBufferKeySpanCompare;
            this.DataSpanCompare = dataCompareFunc;
            if ( dataCompareFunc is object )
                this.LmdbBufferDataCompareFunction = this.LmdbBufferDataSpanCompare;
#endif
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="openOptions">      Options for opening the database. </param>
        /// <param name="keyCompareFunc">   Key comparison function. Can be <c>null</c>. </param>
        /// <param name="dataCompareFunc">  (Optional) Data comparison function. Can be <c>null</c>. </param>
        public DatabaseConfiguration( DatabaseOpenOptions openOptions, LmdbValueCompareFunction keyCompareFunc, LmdbValueCompareFunction dataCompareFunc = null )
        {
            this.OpenOptions = openOptions;
            this.LmdbValueKeyCompareFunction = keyCompareFunc;
            this.LmdbValueDataCompareFunction = dataCompareFunc;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="openOptions">  Options for opening the database. </param>
        /// <param name="keyCompareFunc">   (Optional) Key comparison function. Can be <c>null</c>. </param>
        /// <param name="dataCompareFunc">  (Optional) Data comparison function. Can be <c>null</c>. </param>
        public DatabaseConfiguration( DatabaseOpenOptions openOptions, LmdbBufferCompareFunction keyCompareFunc, LmdbBufferCompareFunction dataCompareFunc = null )
        {
            this.OpenOptions = openOptions;
            this.LmdbBufferKeyCompareFunction = keyCompareFunc;
            this.LmdbBufferDataCompareFunction = dataCompareFunc;
        }

#if NET5_0_OR_GREATER
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-12-27. </remarks>
        /// <param name="openOptions">      Options to configure database. </param>
        /// <param name="keyCompareFunc">   [in,out] Key comparison function. Mandatory. </param>
        /// <param name="dataCompareFunc">  [in,out] (Optional) If non-null, the data comparison function. </param>
        /// <returns>   An unsafe. </returns>
        internal unsafe DatabaseConfiguration( DatabaseOpenOptions openOptions,
                                               delegate* unmanaged< LmdbValue, LmdbValue, int > keyCompareFunc,
                                               delegate* unmanaged< LmdbValue, LmdbValue, int > dataCompareFunc = null )
        {
            this.OpenOptions = openOptions;
            this.UnsafeLmdbValueKeyCompare = keyCompareFunc;
            this.UnsafeLmdbValueDataCompare = dataCompareFunc;
        }
#endif

        #endregion

        #region " MULTI_VALUE CONFIGURATIONS "

        /// <summary>   Multi value database configuration for the <see cref="LmdbBuffer"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">      Options to configure database. </param>
        /// <param name="dummy">        A dummy argument for selecting the <see cref="LmdbBuffer"/>
        ///                             functions. </param>
        /// <param name="compareFunc">  (Optional) compare function. Can be <c>null</c>. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        public static DatabaseConfiguration MultiValueDatabaseConfiguration( DatabaseOpenOptions options, LmdbBuffer dummy, Span.SpanCompare<byte> compareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesSort, dummy, compareFunc );
        }

        /// <summary>   Multi value database configuration for the <see cref="LmdbValue"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">      Options to configure database. </param>
        /// <param name="dummy">        A dummy argument for selecting the <see cref="LmdbValue"/>
        ///                             functions. </param>
        /// <param name="compareFunc">  (Optional) compare function. Can be <c>null</c>. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        public static DatabaseConfiguration MultiValueDatabaseConfiguration( DatabaseOpenOptions options, LmdbValue dummy, Span.SpanCompare<byte> compareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesSort, dummy, compareFunc );
        }

        /// <summary>   Multi value database configuration for the <see cref="LmdbBuffer"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">          Options to configure database. </param>
        /// <param name="dummy">            A dummy argument for selecting the <see cref="LmdbBuffer"/>
        ///                                 functions. </param>
        /// <param name="keyCompareFunc">   (Optional) Key compare function. Can be <c>null</c>. </param>
        /// <param name="dataCompareFunc">  (Optional) Duplicate (by key) data compare function. Can be
        ///                                 <c>null</c>. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        ///
        public static DatabaseConfiguration MultiValueDatabaseConfiguration( DatabaseOpenOptions options, LmdbBuffer dummy, Span.SpanCompare<byte> keyCompareFunc = null,
                                                Span.SpanCompare<byte> dataCompareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesSort, dummy, keyCompareFunc, dataCompareFunc );
        }


        /// <summary>   Multi value database configuration for the <see cref="LmdbValue"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">          Options to configure database. </param>
        /// <param name="dummy">            A dummy argument for selecting the <see cref="LmdbValue"/>
        ///                                 functions. </param>
        /// <param name="keyCompareFunc">   (Optional) Key compare function. Can be <c>null</c>. </param>
        /// <param name="dataCompareFunc">  (Optional) Duplicate (by key) data compare function. Can be
        ///                                 <c>null</c>. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        ///
        public static DatabaseConfiguration MultiValueDatabaseConfiguration( DatabaseOpenOptions options, LmdbValue dummy, Span.SpanCompare<byte> keyCompareFunc = null,
                                                Span.SpanCompare<byte> dataCompareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesSort, dummy, keyCompareFunc, dataCompareFunc );
        }

#if NET5_0_OR_GREATER
        /// <summary>   Unsafe multi value database configuration for the <see cref="LmdbValue"/>. </summary>
        /// <param name="options">Options to configure database.</param>
        /// <param name="keyCompareFunc">Key compare function. Mandatory.</param>
        /// <param name="dataCompareFunc">Duplicate (by key) data compare function. Can be <c>null</c>.</param>
        internal static unsafe DatabaseConfiguration MultiValueDatabaseConfiguration( DatabaseOpenOptions options,
                                                                                      delegate* unmanaged< LmdbValue, LmdbValue, int > keyCompareFunc,
                                                                                      delegate* unmanaged< LmdbValue, LmdbValue, int > dataCompareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesSort, keyCompareFunc, dataCompareFunc );
        }
#endif

        #endregion

        #region " FIXED MULTI_VALUE CONFIGURATIONS "

        /// <summary>   Fixed multi value database configuration for the <see cref="LmdbBuffer"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">          Options to configure database. </param>
        /// <param name="fixedDataSize">    Size of records. Must be the same for all records. </param>
        /// <param name="dummy">            A dummy argument for selecting the <see cref="LmdbBuffer"/>
        ///                                 functions. </param>
        /// <param name="keyCompareFunc">   (Optional) Key compare function. </param>
        /// <param name="dataCompareFunc">  (Optional) Duplicate (by key) data compare function. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        public static DatabaseConfiguration FixedMultiValueDatabaseConfiguration( DatabaseOpenOptions options, int fixedDataSize,
                                                                                  LmdbBuffer dummy,
                                                                                  Span.SpanCompare<byte> keyCompareFunc = null,
                                                                                  Span.SpanCompare<byte> dataCompareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesFixed, dummy, keyCompareFunc, dataCompareFunc ) {
                FixedDataSize = fixedDataSize
            };
        }

        /// <summary>   Fixed multi value database configuration for the <see cref="LmdbValue"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="options">          Options to configure database. </param>
        /// <param name="fixedDataSize">    Size of records. Must be the same for all records. </param>
        /// <param name="dummy">            A dummy argument for selecting the <see cref="LmdbValue"/>
        ///                                 functions. </param>
        /// <param name="keyCompareFunc">   (Optional) Key compare function. </param>
        /// <param name="dataCompareFunc">  (Optional) By key data compare function. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        public static DatabaseConfiguration FixedMultiValueDatabaseConfiguration( DatabaseOpenOptions options, int fixedDataSize,
                                                                                  LmdbValue dummy,
                                                                                  Span.SpanCompare<byte> keyCompareFunc = null,
                                                                                  Span.SpanCompare<byte> dataCompareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesFixed, dummy, keyCompareFunc, dataCompareFunc ) {
                FixedDataSize = fixedDataSize
            };
        }

#if NET5_0_OR_GREATER
        /// <summary>   Unsafe fixed multi value database configuration for the <see cref="LmdbValue"/>. </summary>
        /// <remarks>   David, 2021-01-02. </remarks>
        /// <param name="options">          Options to configure database. </param>
        /// <param name="fixedDataSize">    Size of records. Must be the same for all records. </param>
        /// <param name="keyCompareFunc">   [in,out] Key compare function. Mandatory. </param>
        /// <param name="dataCompareFunc">  [in,out] (Optional) By key data compare function. Can be
        ///                                 <c>null</c>. </param>
        /// <returns>   A DatabaseConfiguration. </returns>
        internal static unsafe DatabaseConfiguration FixedMultiValueDatabaseConfiguration( DatabaseOpenOptions options, int fixedDataSize,
                                                              delegate* unmanaged< LmdbValue, LmdbValue, int > keyCompareFunc,
                                                              delegate* unmanaged< LmdbValue, LmdbValue, int > dataCompareFunc = null )
        {
            return new DatabaseConfiguration( options | DatabaseOpenOptions.DuplicatesFixed, keyCompareFunc, dataCompareFunc ) {
                FixedDataSize = fixedDataSize
            };
        }
#endif

        #endregion

        #region " CONFIGURATION PROPERTIES "

        /// <summary>   Gets or set the <see cref="DatabaseOpenOptions"/>. </summary>
        /// <value> The <see cref="DatabaseOpenOptions"/>. </value>
        public DatabaseOpenOptions OpenOptions { get; set; }

        /// <summary>
        /// Gets a value indicating whether this database configuration
        /// <see cref="DatabaseOpenOptions.Create"/> open option set.
        /// </summary>
        /// <value> True if the create database open option is set, false if not. </value>
        public bool IsCreate => 0 != (this.OpenOptions & DatabaseOpenOptions.Create);

        /// <summary>   Gets a value indicating whether this database configuration is multi value. </summary>
        /// <value> True if this database configuration is multi value, false if not. </value>
        public bool IsMultiValue => 0 != (this.OpenOptions & DatabaseOpenOptions.DuplicatesSort);

        /// <summary>   Gets a value indicating whether this object is fixed. </summary>
        /// <value> True if this object is fixed, false if not. </value>
        public bool IsFixed => (0 != (this.OpenOptions & DatabaseOpenOptions.DuplicatesFixed)) || (0 < this.FixedDataSize);

        /// <summary>   Gets or sets the duplicate sort prefix. </summary>
        /// <value> The duplicate sort prefix. </value>
        public int DupSortPrefix { get; set; } = -1;

        /// <summary>   Size of records. Must be the same for all records. </summary>
        /// <value> The size of the fixed data. </value>
        public int FixedDataSize { get; set; }

#endregion

#region " SPAN KEY AND DATA COMPARE FUNCTIONS "

        /// <summary>   Key span comparison function. </summary>
        /// <value> The Key span Comparison function. </value>
        private SpanCompare<byte> KeySpanCompare { get; set; }

        /// <summary>   The Data span comparison function. </summary>
        /// <value> The Data span comparison. </value>
        private SpanCompare<byte> DataSpanCompare { get; set; }

#endregion

#region " LMDB BUFFER "

        /// <summary>   Define the <see cref="LmdbBuffer"/> key span comparison function. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="compare">  Key comparison function. Can be <c>null</c>. </param>
        public void LmdbBufferKeyCompareWith( Span.SpanCompare<byte> compare )
        {
            this.KeySpanCompare = compare;
            if ( compare is object )
                this.LmdbBufferKeyCompareFunction = this.LmdbBufferKeySpanCompare;
        }

        /// <summary>   The <see cref="LmdbBuffer"/> Key span comparison function. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="x">    A LmdbBuffer to process. </param>
        /// <param name="y">    A LmdbBuffer to process. </param>
        /// <returns>   0 if equal, -1 if x less than y, +1 if x greater than y. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private int LmdbBufferKeySpanCompare( ref LmdbBuffer x, ref LmdbBuffer y )
        {
            return this.KeySpanCompare( x.AsSpan(), y.AsSpan() );
        }

        /// <summary>   Gets the <see cref="LmdbBuffer"/> key compare function. </summary>
        /// <value> The <see cref="LmdbBuffer"/> key compare function. </value>
        public LmdbBufferCompareFunction LmdbBufferKeyCompareFunction { get; set; }

        /// <summary>   Define the <see cref="LmdbBuffer"/> data span comparison function. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="compare">  Data comparison function. Can be <c>null</c>. </param>
        public void LmdbBufferDataCompareWith( Span.SpanCompare<byte> compare )
        {
            this.DataSpanCompare = compare;
            if ( compare is object )
                this.LmdbBufferDataCompareFunction = this.LmdbBufferDataSpanCompare;
        }

        /// <summary>   The <see cref="LmdbBuffer"/> data span comparison function. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="x">    A LmdbBuffer to process. </param>
        /// <param name="y">    A LmdbBuffer to process. </param>
        /// <returns>   0 if equal, -1 if x less than y, +1 if x greater than y. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private int LmdbBufferDataSpanCompare( ref LmdbBuffer x, ref LmdbBuffer y )
        {
            return this.DataSpanCompare( x.AsSpan(), y.AsSpan() );
        }

        /// <summary>   Gets the <see cref="LmdbBuffer"/> data compare function. </summary>
        /// <value> The <see cref="LmdbBuffer"/> data compare function. </value>
        public LmdbBufferCompareFunction LmdbBufferDataCompareFunction { get; set; }

#endregion

#region " COMPARE BUFFER: I COMPARER "

        private IComparer<LmdbBuffer> _KeyComparer;

        /// <summary>   Compare <see cref="LmdbBuffer"/> key. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="left">     [in,out] The left. </param>
        /// <param name="right">    [in,out] The right. </param>
        /// <returns>   0 if equal, -1 if x less than y, +1 if x greater than y. </returns>
        private int CompareKey( ref LmdbBuffer left, ref LmdbBuffer right )
        {
            return this._KeyComparer.Compare( left, right );
        }

        /// <summary>   Assigns an <see cref="LmdbBuffer"/> key comparer. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="comparer"> The comparer. </param>
        public void CompareKeyWith( IComparer<LmdbBuffer> comparer )
        {
            this._KeyComparer = comparer;
        }

        private IComparer<LmdbBuffer> _DataComparer;

        /// <summary>   Compare <see cref="LmdbBuffer"/> data. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="left">     [in,out] The left. </param>
        /// <param name="right">    [in,out] The right. </param>
        /// <returns>   0 if equal, -1 if x less than y, +1 if x greater than y. </returns>
        private int CompareData( ref LmdbBuffer left, ref LmdbBuffer right )
        {
            return this._DataComparer.Compare( left, right );
        }

        /// <summary>   Assigns an <see cref="LmdbBuffer"/> data comparer. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="comparer"> The comparer. </param>
        public void CompareDataWith( IComparer<LmdbBuffer> comparer )
        {
            this._DataComparer = comparer;
        }

#endregion

#region " LMDB VALUE "

        /// <summary>   Define the <see cref="LmdbValue"/> key span comparison function. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="compare">  Key comparison function. Can be <c>null</c>. </param>
        public void LmdbValueKeyCompareWith( Span.SpanCompare<byte> compare )
        {
            this.KeySpanCompare = compare;
            if ( compare is object )
                this.LmdbValueKeyCompareFunction = this.LmdbValueKeySpanCompare;
        }

        /// <summary>   The <see cref="LmdbValue"/> Key span comparison function. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="x">    A LmdbValue to process. </param>
        /// <param name="y">    A LmdbValue to process. </param>
        /// <returns>   0 if equal, -1 if x less than y, +1 if x greater than y. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private int LmdbValueKeySpanCompare( in LmdbValue x, in LmdbValue y )
        {
            return this.KeySpanCompare( x.ToReadOnlySpan(), y.ToReadOnlySpan() );
        }

        /// <summary>   Gets the <see cref="LmdbValue"/> key compare function. </summary>
        /// <value> The LMDB Value compare function. </value>
        public LmdbValueCompareFunction LmdbValueKeyCompareFunction { get; set; }

        /// <summary>   Define the <see cref="LmdbValue"/> data span comparison function. </summary>
        /// <remarks>   David, 2020-12-29. </remarks>
        /// <param name="compare">  Data comparison function. Can be <c>null</c>. </param>
        public void LmdbValueDataCompareWith( Span.SpanCompare<byte> compare )
        {
            this.DataSpanCompare = compare;
            if ( compare is object )
                this.LmdbValueDataCompareFunction = this.LmdbValueDataSpanCompare;
        }

        /// <summary>   The <see cref="LmdbValue"/> data span comparison function. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="x">    A LmdbValue to process. </param>
        /// <param name="y">    A LmdbValue to process. </param>
        /// <returns>   0 if equal, -1 if x less than y, +1 if x greater than y. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private int LmdbValueDataSpanCompare( in LmdbValue x, in LmdbValue y )
        {
            return this.DataSpanCompare( x.ToReadOnlySpan(), y.ToReadOnlySpan() );
        }

        /// <summary>   Gets the <see cref="LmdbValue"/> data compare function. </summary>
        /// <value> The <see cref="LmdbValue"/> data compare function. </value>
        public LmdbValueCompareFunction LmdbValueDataCompareFunction { get; set; }

        #endregion

        #region " UNSAFE KEY AND DATA COMPARE "

#if NET5_0_OR_GREATER
        /// <summary>
        /// An <see cref="LmdbValue"/> Key comparison function that is only callable from unmanaged code.
        /// </summary>
        internal unsafe delegate* unmanaged< LmdbValue, LmdbValue, int > UnsafeLmdbValueKeyCompare { get; }

        /// <summary>
        /// An <see cref="LmdbValue"/> Data comparison function that is only callable from unmanaged code.
        /// </summary>
        internal unsafe delegate* unmanaged< LmdbValue, LmdbValue, int > UnsafeLmdbValueDataCompare { get; }
#endif

        #endregion

        #region " CONFIGURATE COMPARE FUNCTIONS "

        /// <summary>   Configure the database compare functions. </summary>
        /// <remarks>   David, 2020-12-26. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   An IDisposable to be used for releasing the function pointers. </returns>
        internal IDisposable ConfigureCompareFunctions( IntPtr txn, uint dbi )
        {
            var pinnedComparer = new ComparerKeepAlive();

            if ( this.LmdbBufferKeyCompareFunction is object )
            {
                pinnedComparer.AddComparer( this.LmdbBufferKeyCompareFunction );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_compare( txn, dbi, this.LmdbBufferKeyCompareFunction ) );
            }
            if ( this.LmdbBufferDataCompareFunction is object )
            {
                pinnedComparer.AddComparer( this.LmdbBufferDataCompareFunction );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_dupsort( txn, dbi, this.LmdbBufferDataCompareFunction ) );
            }

            if ( this.LmdbValueKeyCompareFunction is object )
            {
                pinnedComparer.AddComparer( this.LmdbValueKeyCompareFunction );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_compare( txn, dbi, this.LmdbValueKeyCompareFunction ) );
            }
            if ( this.LmdbValueDataCompareFunction is object )
            {
                pinnedComparer.AddComparer( this.LmdbValueDataCompareFunction );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_dupsort( txn, dbi, this.LmdbValueDataCompareFunction ) );
            }

            if ( this._KeyComparer != null )
            {
                LmdbBufferCompareFunction compare = this.CompareKey;
                pinnedComparer.AddComparer( compare );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_compare( txn, dbi, compare ) );
            }
            if ( this._DataComparer != null )
            {
                LmdbBufferCompareFunction compare = this.CompareData;
                pinnedComparer.AddComparer( compare );
                _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_dupsort( txn, dbi, compare ) );
            }

#if NET5_0_OR_GREATER
            unsafe
            {
                if ( this.UnsafeLmdbValueKeyCompare != null )
                {
                    _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_compare( txn, dbi, this.UnsafeLmdbValueKeyCompare ) );
                }
                if ( this.UnsafeLmdbValueDataCompare != null )
                {
                    _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_set_dupsort( txn, dbi, this.UnsafeLmdbValueDataCompare ) );
                }
            }
#endif

            if ( this.DupSortPrefix > 0 )
            {
                _ = this.DupSortPrefix == 64 * 64
                    ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint64x64( txn, dbi ) )
                    : this.DupSortPrefix == 128
                        ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint128( txn, dbi ) )
                        : this.DupSortPrefix == 96
                            ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint96( txn, dbi ) )
                            : this.DupSortPrefix == 80
                                ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint80( txn, dbi ) )
                                : this.DupSortPrefix == 64
                                    ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint64( txn, dbi ) )
                                    : this.DupSortPrefix == 48
                                        ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint48( txn, dbi ) )
                                        : this.DupSortPrefix == 32
                                            ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint32( txn, dbi ) )
                                            : this.DupSortPrefix == 16
                                                ? LmdbException.AssertExecute( SafeNativeMethods.sdb_set_dupsort_as_uint16( txn, dbi ) )
                                                : throw new NotSupportedException( $"Rethink your design if you need {nameof( this.DupSortPrefix )}={this.DupSortPrefix}!" );
            }

            return pinnedComparer;
        }

        /// <summary>   A comparer keep alive class. </summary>
        /// <remarks>   David, 2020-12-27. </remarks>
        private class ComparerKeepAlive : IDisposable
        {
            private readonly List<GCHandle> _Comparisons = new();

            public void AddComparer( LmdbBufferCompareFunction compare )
            {
                var handle = GCHandle.Alloc( compare );
                this._Comparisons.Add( handle );
            }


            public void AddComparer( LmdbValueCompareFunction compare )
            {
                var handle = GCHandle.Alloc( compare );
                this._Comparisons.Add( handle );
            }

            public void Dispose()
            {
                for ( int i = 0; i < this._Comparisons.Count; ++i )
                {
                    this._Comparisons[i].Free();
                }
            }
        }

#endregion
    }
}
