using System;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    public partial class Database
    {

        /// <summary>
        /// Ensures that the cursor has a handle and allows multi-value search and sort.
        /// </summary>
        /// <remarks>   David, 2020-12-31. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   A valid database handle. </returns>
        ///
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal uint AssertFixedDataHandle()
        {
            return this.AssertFixedDataHandle( $"Database {this.Name} is not fixed-valued" );
        }

        /// <summary>
        /// Ensures that the cursor has a handle and allows multi-value search and sort.
        /// </summary>
        /// <remarks>   David, 2020-12-31. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="message">  The message. </param>
        /// <returns>   A valid database handle. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal uint AssertFixedDataHandle( string message )
        {
            return this.Config.IsFixed
                   ? this.AssertAllocatedHandle()
                   : throw new LmdbException( ( int ) LmdbFusionErrorCode.NotFixed, message );
        }


        /// <summary>
        /// Ensures that the cursor has a handle and allows multi-value search and sort.
        /// </summary>
        /// <remarks>   David, 2020-12-31. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <returns>   A valid database handle. </returns>
        internal uint AssertMultiValueHandle()
        {
            return this.AssertMultiValueHandle( $"Database {this.Name} is not multi-valued" );
        }

        /// <summary>
        /// Ensures that the cursor has a handle and allows multi-value search and sort.
        /// </summary>
        /// <remarks>   David, 2021-01-19. </remarks>
        /// <exception cref="LmdbException">    Thrown when a Lmdb error condition occurs. </exception>
        /// <param name="message">  The message. </param>
        /// <returns>   A valid database handle. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal uint AssertMultiValueHandle( string message )
        {
            return this.Config.IsMultiValue
                   ? this.AssertAllocatedHandle()
                   : throw new LmdbException( ( int ) LmdbFusionErrorCode.NotMultiValue, message );
        }

        /// <summary>
        /// Delete items from a database. This function removes key/data pairs from the database. Only
        /// the matching data item will be deleted. This function will return MDB_NOTFOUND if the
        /// specified key/data pair is not in the database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate database <see cref="Database.AssertAllocatedHandle"/>. </para> </remarks>
        /// <param name="transaction">  The <see cref="Transaction"/>. </param>
        /// <param name="key">          The key element of the key data pair. </param>
        /// <param name="data">         The data element of the key data pair. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals a failed result code; otherwise, throws and exception.
        /// </returns>
        public bool Delete( Transaction transaction, in ReadOnlySpan<byte> key, in ReadOnlySpan<byte> data )
        {
            int result;
            unsafe
            {
                var dbKey = LmdbValue.From( key );
                var dbData = LmdbValue.From( data );
                result = SafeNativeMethods.mdb_del( transaction.Handle, this.Handle, in dbKey, in dbData );
            }
            return LmdbException.AssertFailed( result, NativeResultCode.NotFound );
        }

        /// <summary>
        /// Compare two data items according to a particular database. This returns a comparison as if
        /// the two data items were keys in the specified database.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate database <see cref="AssertMultiValueHandle()"/>. </para> </remarks>
        /// <param name="transaction">  The <see cref="Transaction"/>. </param>
        /// <param name="x">            The item to compare. </param>
        /// <param name="y">            The other item to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        public int DupCompare( Transaction transaction, in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            int result;
            unsafe
            {
                var dbx = LmdbValue.From( x );
                var dby = LmdbValue.From( y );
                result = SafeNativeMethods.mdb_dcmp( transaction.Handle, this.Handle, in dbx, in dby );
            }
            return result;
        }

        /// <summary>   Attempts to find multi-value key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate transaction <see cref="Transaction.AssertReadOnlyHandle()"/>
        /// of database <see cref="AssertMultiValueHandle()"/>. </para> </remarks>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">          The transaction. </param>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [in,out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryFindDup<TKey, TValue>( Transaction txn, Lookup direction, ref TKey key, ref TValue value )
            where TKey : struct where TValue : struct
        {
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var key1 = new LmdbBuffer( Unsafe.SizeOf<TKey>(), ( byte* ) keyPtr );

                var valuePtr = Unsafe.AsPointer( ref value );
                var value1 = new LmdbBuffer( Unsafe.SizeOf<TValue>(), ( byte* ) valuePtr );

                var res = this.Config.IsFixed
                           ? this.TryFindFixedDup( txn, direction, ref key1, ref value1 )
                           : this.TryFindDup( txn, direction, ref key1, ref value1 );
                if ( res )
                {
                    key = Unsafe.ReadUnaligned<TKey>( key1.Data );
                    value = Unsafe.ReadUnaligned<TValue>( value1.Data );
                    return true;
                }
                return false;
            }
        }

        /// <summary>   Attempts to find multi-value key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate transaction <see cref="Transaction.AssertReadOnlyHandle()"/>. </para> </remarks>
        /// <param name="txn">          The transaction. </param>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [in,out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryFindDup( Transaction txn, Lookup direction, ref LmdbBuffer key, ref LmdbBuffer value )
        {
            using var cursor = new Cursor( this, txn, true, this.Config.IsMultiValue, this.Config.FixedDataSize );
            return cursor.TryFindDup( direction, ref key, ref value );
        }

        /// <summary>   Attempts to find multi-value key-value pair. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate transaction <see cref="Transaction.AssertReadOnlyHandle()"/>. </para> </remarks>
        /// <param name="txn">          The transaction. </param>
        /// <param name="direction">    The direction. </param>
        /// <param name="key">          [in,out] The key. </param>
        /// <param name="value">        [in,out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool TryFindFixedDup( Transaction txn, Lookup direction, ref LmdbBuffer key, ref LmdbBuffer value )
        {
            using var cursor = Cursor.OpenCursor( this, txn );
            return cursor.TryFindDup( direction, ref key, ref value );
        }

    }
}
