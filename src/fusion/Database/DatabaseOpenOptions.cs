using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    /// <summary>   Option for opening a database. </summary>
    /// <remarks>   Remarked by David, 2020-12-11. </remarks>
    [System.Flags]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2217:Do not mark enums with FlagsAttribute", Justification = "<Pending>" )]
    public enum DatabaseOpenOptions
    {

        /// <summary>
        /// No special options.
        /// </summary>
        None = 0,

        /// <summary>
        /// Keys are strings to be compared in reverse order, from the end of the strings to the beginning.
        /// By default, Keys are treated as strings and compared from beginning to end
        /// <see cref="LmdbConstants.MDB_REVERSEKEY"/>.
        /// </summary>
        ReverseKey = ( int ) LmdbConstants.MDB_REVERSEKEY,

        /// <summary>
        /// Duplicate keys may be used in the database.
        /// (Or, from another perspective, keys may have multiple data items, stored in sorted order.) 
        /// By default keys must be unique and may have only a single data item
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="LmdbConstants.MDB_DUPSORT"/>.
        /// </summary>
        DuplicatesSort = ( int ) LmdbConstants.MDB_DUPSORT,

        /// <summary>
        /// Keys are binary integers in native byte order.
        /// Setting this option requires all keys to be the same size, typically sizeof(int) or sizeof(size_t)
        /// <see cref="LmdbConstants.MDB_INTEGERKEY"/>. .
        /// </summary>
        IntegerKey = ( int ) LmdbConstants.MDB_INTEGERKEY,

        /// <summary>
        /// This option tells the library that the data items for this database 
        /// are all the same size, which allows further optimizations in storage and retrieval. 
        /// When all data items are the same size, the <see cref="CursorOperation.GetMultiple"/> <see cref="LmdbCursorOperation.MDB_GET_MULTIPLE"/> 
        /// and <see cref="CursorOperation.NextMultiple"/> <see cref="LmdbCursorOperation.MDB_NEXT_MULTIPLE"/> cursor operations may 
        /// be used to retrieve multiple items at once.
        /// This option may only be used in combination with <see cref="DatabaseOpenOptions.DuplicatesSort"/> 
        /// <see cref="DatabaseOpenOptions.DuplicatesSort"/> <see cref="LmdbConstants.MDB_DUPSORT"/>, which is ORed herein.
        /// <see cref="LmdbConstants.MDB_DUPFIXED"/>.
        /// </summary>
        DuplicatesFixed = ( int ) (LmdbConstants.MDB_DUPSORT | LmdbConstants.MDB_DUPFIXED),

        /// <summary>
        /// This option specifies that duplicate data items are also integers, and should be sorted as such.
        /// This option may only be used in combination with <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// <see cref="LmdbConstants.MDB_DUPSORT"/> and <see cref="LmdbConstants.MDB_DUPFIXED"/>, which are ORed herein.
        /// <see cref="LmdbConstants.MDB_INTEGERDUP"/>.
        /// </summary>
        IntegerDuplicates = ( int ) (LmdbConstants.MDB_DUPSORT | LmdbConstants.MDB_DUPFIXED | LmdbConstants.MDB_INTEGERDUP),

        /// <summary>
        /// This option specifies that duplicate data items should be compared as strings in reverse order.
        /// This option may only be used in combination with <see cref="DatabaseOpenOptions.DuplicatesSort"/>
        /// <see cref="LmdbConstants.MDB_DUPSORT"/>, which is ORed herein.
        /// <see cref="LmdbConstants.MDB_REVERSEDUP"/>.
        /// </summary>
        ReverseDuplicates = ( int ) (LmdbConstants.MDB_DUPSORT | LmdbConstants.MDB_REVERSEDUP),

        /// <summary>
        /// Create the named database if it doesn't exist.
        /// This option is not allowed in a read-only transaction or a read-only environment.
        /// <see cref="LmdbConstants.MDB_CREATE"/>.
        /// </summary>
        Create = ( int ) LmdbConstants.MDB_CREATE
    }

}
