using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    public partial class Database
    {

        /// <summary>   Iterate over db values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  The transaction. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as enumerable in this collection.
        /// </returns>
        public IEnumerable<KeyValuePair<LmdbBuffer, LmdbBuffer>> AsEnumerable( Transaction txn )
        {
            LmdbBuffer key = default;
            LmdbBuffer value = default;
            using var c = new Cursor( this, txn, true, this.Config.IsMultiValue, this.Config.FixedDataSize );
            if ( 0 == c.Get( ref key, ref value, CursorGetOption.First ) )
            {
                yield return new KeyValuePair<LmdbBuffer, LmdbBuffer>( key, value );
                value = default;

                while ( 0 == c.Get( ref key, ref value, CursorGetOption.NextNoDuplicate ) )
                {
                    yield return new KeyValuePair<LmdbBuffer, LmdbBuffer>( key, value );
                    value = default;
                }
            }
        }


        /// <summary>   Iterate over db values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">  The transaction. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as enumerable in this collection.
        /// </returns>
        public IEnumerable<KeyValuePair<TKey, TValue>> AsEnumerable<TKey, TValue>( Transaction txn )
        {
            var keySize = Unsafe.SizeOf<TKey>();
            var valueSize = Unsafe.SizeOf<TValue>();

            return this.AsEnumerable( txn ).Select( kvp => {
                return kvp.Key.Length != keySize
                    ? throw new InvalidOperationException( $"Key buffer {kvp.Key.Length} length does not equals to key size {keySize}" )
                    : kvp.Value.Length != valueSize
                    ? throw new InvalidOperationException( $"Value buffer length {kvp.Value.Length} does not equals to value size {valueSize}" )
                    : new KeyValuePair<TKey, TValue>( kvp.Key.Read<TKey>( 0 ), kvp.Value.Read<TValue>( 0 ) );
            } );
        }

        /// <summary>   Iterate over multi-values by key. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  The key. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as enumerable in this collection.
        /// </returns>
        private IEnumerable<LmdbBuffer> AsEnumerable( Transaction txn, Memory<byte> key )
        {
            _ = this.AssertMultiValueHandle( $"{nameof( AsEnumerable )} overload with key parameter should only be provided for multi-value database" );
            try
            {
                var key1 = new LmdbBuffer( key.Span );
                LmdbBuffer value = default;
                using var c = new Cursor( this, txn, true, this.Config.IsMultiValue, this.Config.FixedDataSize );
                if ( 0 == c.Get( ref key1, ref value, CursorGetOption.SetKey ) &&
                     0 == c.Get( ref key1, ref value, CursorGetOption.FirstDuplicate ) )
                {
                    yield return value;

                    while ( 0 == c.Get( ref key1, ref value, CursorGetOption.NextDuplicate ) )
                    {
                        yield return value;
                    }
                }
            }
            finally
            {
                key.Pin().Dispose();
            }
        }


        /// <summary>   Iterate over multiple values by key. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  The key. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as enumerable in this collection.
        /// </returns>
        public IEnumerable<LmdbBuffer> AsEnumerable1( Transaction txn, LmdbBuffer key )
        {
            _ = this.AssertMultiValueHandle( $"{nameof( AsEnumerable1 )} overload with key parameter should only be provided for multi-value database" );

            // must be a read-only transaction.
            _ = txn.AssertReadOnlyHandle( $"{nameof( AsEnumerable1 )} overload with key parameter should only be provided for read-only transaction" );

            Memory<byte> fixedMemory = new byte[key.Length];
            key.Span.CopyTo( fixedMemory.Span );
            return this.AsEnumerable( txn, fixedMemory );
        }

        /// <summary>   Iterate over multiple values by key. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <typeparam name="TKey">    Generic type parameter. </typeparam>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  The key. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as enumerable in this collection.
        /// </returns>
        public IEnumerable<LmdbBuffer> AsEnumerable<TKey>( Transaction txn, TKey key )
        {
            _ = txn.AssertReadOnlyHandle( $"{nameof( AsEnumerable )} overload with key parameter should only be provided for read-only transaction" );
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyLength = Unsafe.SizeOf<TKey>();
                var key1 = new LmdbBuffer( keyLength, ( byte* ) keyPtr );

                Memory<byte> fixedMemory = new byte[keyLength];
                key1.Span.CopyTo( fixedMemory.Span );

                return this.AsEnumerable( txn, fixedMemory );
            }
        }

        /// <summary>   Iterate over multiple values by key. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <typeparam name="TKey">     Type of the key. </typeparam>
        /// <typeparam name="TValue">   Type of the value. </typeparam>
        /// <param name="txn">  The transaction. </param>
        /// <param name="key">  The key. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process as enumerable in this collection.
        /// </returns>
        public IEnumerable<TValue> AsEnumerable<TKey, TValue>( Transaction txn, TKey key )
        {
            _ = txn.AssertReadOnlyHandle( $"{nameof( AsEnumerable )} overload with key parameter should only be provided for read-only transaction" );
            unsafe
            {
                var keyPtr = Unsafe.AsPointer( ref key );
                var keyLength = Unsafe.SizeOf<TKey>();
                var key1 = new LmdbBuffer( keyLength, ( byte* ) keyPtr );

                var valueSize = Unsafe.SizeOf<TValue>();
                Memory<byte> fixedMemory = new byte[valueSize];
                key1.Span.CopyTo( fixedMemory.Span );

                return this.AsEnumerable( txn, fixedMemory ).Select( buf => {
                    return buf.Length != valueSize
                        ? throw new InvalidOperationException( $"Buffer length {buf.Length} does not equals to data value size {valueSize}" )
                        : buf.Read<TValue>( 0 );
                } );
            }
        }
    }
}
