using System;
using System.Threading;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{

    /// <summary>   LMDB Database. </summary>
    /// <remarks>   Remarked by David, 2020-12-11. 
    ///             
    /// A database might allow unique (by key) records only, multi-value records by key (Dup), or 
    /// values of the same size per key. 
    /// 
    /// A database handle denotes the name and parameters of a database, independently of whether
    /// such a database exists.
    /// 
    /// The database handle may be discarded by calling mdb_dbi_close().
    /// 
    /// The old database handle is returned if the database was already open.
    /// 
    /// The handle may only be closed once.
    /// 
    /// The database handle will be private to the current transaction until the transaction is
    /// successfully committed.
    /// 
    /// If the transaction is aborted the handle will be closed automatically.
    /// 
    /// After a successful commit the handle will reside in the shared environment, and may be used
    /// by other transactions.
    /// 
    /// This function must not be called from multiple concurrent transactions in the same process.
    /// 
    /// A transaction that uses this function must finish (either commit or abort) before any other
    /// transaction in the process may use this function.
    /// 
    /// To use named databases (with name != NULL), mdb_env_set_maxdbs() must be called before
    /// opening the environment.
    /// 
    /// Database names are keys in the unnamed database, and may be read but not written.
    /// </remarks>
    public partial class Database : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Opens database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="name">         Database name. Can be <c>null</c> for the default database. </param>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        /// <param name="config">       Database configuration instance. </param>
        /// <returns>   A Tuple of handles: database, transaction and environment. </returns>
        private static (uint dbi, IntPtr txn, IntPtr env, IDisposable pinnedComparer) OpenDatabaseInternal( string name, Transaction transaction, DatabaseConfiguration config )
        {
            var txn = transaction.SafeHandle.AssertAllocatedHandle();
            var ret = SafeNativeMethods.mdb_dbi_open( txn, name, config.OpenOptions, out uint dbi );
            _ = LmdbException.AssertExecute( ret );

            // get the environment handle for this transaction
            var env = SafeNativeMethods.mdb_txn_env( txn );

            if ( EnvironmentSafeHandle.IsInvalidHandle( env ) )
                throw new LmdbException( ( int ) LmdbFusionErrorCode.InvalidPointer, $"Failed getting the environment handle {env} of this transaction handle {txn}" );

            IDisposable pinnedComparer;
            try
            {
                // configure database compare functions.
                pinnedComparer = config.ConfigureCompareFunctions( txn, dbi );
            }
            catch ( Exception )
            {
                SafeNativeMethods.mdb_dbi_close( env, dbi );
                throw;
            }

            return (dbi, txn, env, pinnedComparer);
        }



        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-01-04. The <paramref name="transaction"/> is not committed. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="LmdbException">            Thrown when attempting to create a database using
        ///                                             a Read-Only transaction. </exception>
        /// <param name="name">         Database name. Can be <c>null</c> for the default database. </param>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        /// <param name="config">       Database configuration. </param>
        private Database( string name, Transaction transaction, DatabaseConfiguration config )
        {
            if ( transaction is not object )
                throw new ArgumentNullException( nameof( transaction ) );
            this.Config = config ?? throw new ArgumentNullException( nameof( config ) );
            if ( this.Config.IsCreate && transaction.IsReadOnly )
                throw new LmdbException( ( int ) LmdbFusionErrorCode.NotWritable, "Cannot create a database with a read-only transaction" );
            this.Name = name;

            // open the database
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_dbi_open( transaction.Handle, name, this.Config.OpenOptions, out var dbi ) );

            try
            {
                // configure database compare functions.
                this._PinnedComparer = config.ConfigureCompareFunctions( transaction.Handle, dbi );
                this.Environment = transaction.Environment;
                this.Transaction = transaction;
                this.Environment.Disposing += this.Dispose;
                this.SafeHandle = new DatabaseSafeHandle( dbi, this.Environment );
            }
            catch ( Exception )
            {
                SafeNativeMethods.mdb_dbi_close( this.Environment.Handle, dbi );
                throw;
            }
        }

        /// <summary>   Opens a database. </summary>
        /// <remarks>   David, 2021-01-04. </remarks>
        /// <param name="name">                 Database name. </param>
        /// <param name="transaction">          A <see cref="Transaction"/> under which the operation is performed. </param>
        /// <param name="config">               Database configuration. </param>
        /// <returns>   A Database. </returns>
        public static Database OpenDatabase( string name, Transaction transaction, DatabaseConfiguration config )
        {
            return new Database( name, transaction, config );
        }

        /// <summary>   Opens multi value database. </summary>
        /// <remarks>   David, 2021-01-04. </remarks>
        /// <param name="name">                 Database name. </param>
        /// <param name="transaction">          A <see cref="Transaction"/> under which the operation is performed. </param>
        /// <param name="config">               Database configuration. </param>
        /// <returns>   A Database. </returns>
        public static Database OpenMultiValueDatabase( string name, Transaction transaction, DatabaseConfiguration config )
        {
            config.OpenOptions |= DatabaseOpenOptions.DuplicatesSort;
            return new Database( name, transaction, config );
        }

        /// <summary>   Opens fixed multi value database. </summary>
        /// <remarks>   David, 2021-01-04. </remarks>
        /// <param name="name">                 Database name. </param>
        /// <param name="transaction">          A <see cref="Transaction"/> under which the
        ///                                     operation is performed. </param>
        /// <param name="config">               Database configuration. </param>
        /// <returns>   A Database. </returns>
        public static Database OpenFixedMultiValueDatabase( string name, Transaction transaction, DatabaseConfiguration config )
        {
            config.OpenOptions |= DatabaseOpenOptions.DuplicatesFixed;
            return new Database( name, transaction, config );
        }

        #region " IDISPOSABLE SUPPORT "

        private readonly IDisposable _PinnedComparer;

        /// <summary>
        /// Returns Database handle. Throws if Database handle is already closed/disposed of.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   The native handle to the database. </returns>
        internal uint AssertAllocatedHandle()
        {
            return ( uint ) this.SafeHandle.AssertAllocatedHandle();
        }

        /// <summary>   Implementation of Dispose() pattern. See <see cref="Dispose()"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="disposing">    <c>true</c> if explicitly disposing (finalizer not run),
        ///                             <c>false</c> if disposed from finalizer. </param>
        protected virtual void Dispose( bool disposing )
        {
            // the handle becomes invalid only on dispose.
            if ( this.SafeHandle is not Object || this.SafeHandle.IsInvalid )
                return;

            // release the comparer.
            this._PinnedComparer?.Dispose();

            // unregister the dispose action with the Environment
            if ( this.Environment is object )
                this.Environment.Disposing -= this.Dispose;

            this.SafeHandle?.Dispose();
        }

        /// <summary>
        /// Close a database handle.
        /// </summary>
        /// <remarks>
        /// Normally unnecessary, as databases don't have unmanaged resources that can be cleaned up independently of the environment.
        /// 
        /// Database handles should only be released if one wants to reuse them so that the limit of database
        /// handles is not exceeded.
        /// 
        /// Use with care: This call is not mutex protected. 
        /// 
        /// Handles should only be closed by a single thread, and only if no other threads are going to reference the database handle or one of its
        /// cursors any further. 
        /// 
        /// Do not close a handle if an existing transaction has modified its database. Doing so can cause misbehavior from database corruption to errors like
        /// MDB_BAD_VALSIZE(since the DB name is gone). 
        /// 
        /// Closing a database handle is not necessary, but lets mdb_dbi_open() reuse the handle value. Usually it's better to set a bigger
        /// mdb_env_set_maxdbs(), unless that value would be large.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>   Finalizer. Releases unmanaged resources. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. 
        /// The finalizer is needed here because the database does not have a Safe Handle. </remarks>
        ~Database()
        {
            this.Dispose( false );
        }


        #endregion

        #endregion

        #region " FIELDS "


        /// <summary>   Returns true if the database handle is assigned and was not freed. </summary>
        /// <value> True if this object is opened (it's handle was not freed), false if not. </value>
        public bool IsOpen => !this.SafeHandle.Freed;

        /// <summary>   Database name. </summary>
        /// <value> The name. </value>
        public string Name { get; }

        /// <summary>   Database configuration. </summary>
        /// <value> The configuration. </value>
        public DatabaseConfiguration Config { get; }

        /// <summary>   The transaction. </summary>
        private Transaction Transaction { get; }

        /// <summary>   Gets the environment. </summary>
        /// <value> The environment. </value>
        public LmdbEnvironment Environment { get; }

        /// <summary>   Gets or sets the database safe handle. </summary>
        /// <value> The safe handle. </value>
        internal DatabaseSafeHandle SafeHandle { get; set; }

        /// <summary>   Gets the handle. </summary>
        /// <value> The handle. </value>
        internal uint Handle
        {
            get {
                Interlocked.MemoryBarrier();
                uint result = this.SafeHandle.DatabaseHandle;
                return result;
            }
        }

        #endregion

        #region " DATABASE MANAGEMENT "

        /// <summary>   Empty a database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Unlike <see cref="DropThrow()"/>, <see cref="Truncate"/> does not close the database.
        /// Does not validate the transaction or the Database handles.
        /// </para></remarks>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        public int Truncate( Transaction transaction )
        {
            return SafeNativeMethods.mdb_drop( transaction.Handle, this.Handle, false );
        }

        /// <summary>   Truncate or throw an exception on failure. </summary>
        /// <remarks>   David, 2021-01-18. </remarks>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        public void TruncateThrow( Transaction transaction )
        {
            _ = LmdbException.AssertExecute( SafeNativeMethods.mdb_drop( transaction.Handle, this.AssertAllocatedHandle(), false ) );
        }

        /// <summary>   Truncates all data from the database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        public void TruncateThrow()
        {
            using var txn = this.Environment.BeginTransaction();
            try
            {
                this.TruncateThrow( txn );
                txn.Commit();
            }
            catch
            {
                txn.Abort();
                throw;
            }
        }

        /// <summary>   Deletes and closes a database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Does not validate the transaction or the Database handles. </para><para>
        /// Marks the <see cref="Handle"/> as freed.
        /// </para></remarks>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        public int Drop( Transaction transaction )
        {
            var ret = SafeNativeMethods.mdb_drop( transaction.Handle, this.Handle, true );

            // mark the safe handle as freed. The handle will be disposed when the Database class is disposed.
            this.SafeHandle.Freed = true;

            return ret;
        }

        /// <summary>   Deletes and closes a database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para>
        /// Marks the <see cref="Handle"/> as freed.
        /// </para></remarks>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        public void DropThrow( Transaction transaction )
        {
            var handle = this.AssertAllocatedHandle();

            var ret = SafeNativeMethods.mdb_drop( transaction.Handle, handle, true );
            _ = LmdbException.AssertExecute( ret );

            // mark the safe handle as freed. The handle will be disposed when the Database class is disposed.
            this.SafeHandle.Freed = true;
        }

        /// <summary>   Deletes and closes a database. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. . <para>
        /// Marks the <see cref="Handle"/> as freed.
        /// </para></remarks>
        public void DropThrow()
        {
            using var txn = this.Environment.BeginTransaction();
            try
            {
                this.DropThrow( txn );
                txn.Commit();
            }
            catch
            {
                txn.Abort();
                throw;
            }
        }

        /// <summary>   Retrieve the <see cref="DatabaseOpenOptions"/> for the database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para> 
        /// Does not validate the transaction or the Database handles. </para><para>
        /// Throws an <see cref="LmdbException"/> on failure. </para> </remarks>
        /// <param name="transaction">  Transaction under which the operation is performed. </param>
        /// <returns>   The options. </returns>
        public DatabaseOpenOptions GetOpenOptions( Transaction transaction )
        {
            int ret = SafeNativeMethods.mdb_dbi_flags( transaction.Handle, this.Handle, out uint opts );
            _ = LmdbException.AssertExecute( ret );
            return unchecked(( DatabaseOpenOptions ) opts);
        }

        /// <summary>   Retrieve the <see cref="DatabaseOpenOptions"/> for the database. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. <para>
        /// Does not validate the transaction or the Database handles. </para><para>
        /// Throws an <see cref="LmdbException"/> on failure. </para>
        /// </remarks>
        /// <returns>   The options. </returns>
        public DatabaseOpenOptions GetOpenOptions()
        {
            using var txn = this.Environment.BeginTransaction();
            DatabaseOpenOptions result;
            try
            {
                result = this.GetOpenOptions( txn );
                txn.Commit();
            }
            catch
            {
                txn.Abort();
                throw;
            }
            return result;
        }

        #endregion

        #region " STATISTICS "

        /// <summary>   Retrieve statistics for the database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para> 
        /// Does not validate the transaction or the Database handles. </para><para>
        /// Throws an <see cref="LmdbException"/> on failure. </para> </remarks>
        /// <param name="transaction">  . </param>
        /// <returns>   The statistics. </returns>
        public Statistics GetStats( Transaction transaction )
        {
            int ret = SafeNativeMethods.mdb_stat( transaction.Handle, this.Handle, out LmdbStat value );
            _ = LmdbException.AssertExecute( ret );
            return new Statistics( value );
        }

        /// <summary>   Retrieve statistics for the database. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. <para> 
        /// Does not validate the transaction or the Database handles. </para><para>
        /// Throws an <see cref="LmdbException"/> on failure. </para> </remarks>
        /// <returns>   The statistics. </returns>
        public Statistics GetStats()
        {
            using var txn = this.Environment.BeginTransaction();
            Statistics result;
            try
            {
                result = this.GetStats( txn );
                txn.Commit();
            }
            catch
            {
                txn.Abort();
                throw;
            }
            return result;
        }

        /// <summary>   Gets entries count. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   The entries count. </returns>
        public long GetEntriesCount( Transaction transaction )
        {
            var stat = this.GetStats( transaction );
            return stat.Entries;
        }

        /// <summary>   Gets entries count. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   The entries count. </returns>
        public long GetEntriesCount()
        {
            return this.GetStats().Entries;
        }

        /// <summary>   Gets used size. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   The used size. </returns>
        public long GetUsedSize( Transaction transaction )
        {
            var stat = this.GetStats( transaction );
            return stat.UsedSize;
        }

        /// <summary>   Gets used size. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>   The used size. </returns>
        public long GetUsedSize()
        {
            return this.GetStats().UsedSize;
        }

        #endregion

    }
}
