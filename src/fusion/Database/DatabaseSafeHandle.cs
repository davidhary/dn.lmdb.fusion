using System;
using isr.Lmdb.Fusion.Interop;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace isr.Lmdb.Fusion
{
    /// <summary>   An Database safe handle. </summary>
    /// <remarks>   Remarked by David, 2020-12-16. <para>
    /// Although the database handle is non-pointer type and the database close function is said to be unnecessary, 
    /// using this <see cref="SafeHandle"/> provides a way to address the database handle Free status, close 
    /// the database, and encapsulate the Open status of the database. 
    /// </para></remarks>
    internal class DatabaseSafeHandle : LmdbSafeHandle
    {

        /// <summary>
        /// Constructor that creates a <see cref="System.Runtime.InteropServices.SafeHandle"/> with a
        /// handle and a <see cref="LmdbSafeHandle.ReleaseHandle"/>
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="databaseHandle">   The state. </param>
        /// <param name="environment">      The environment. </param>
        public DatabaseSafeHandle( uint databaseHandle, LmdbEnvironment environment ) : base( new IntPtr( databaseHandle ) )
        {
            this.EnvironmentHandle = environment.Handle;
            this.EnvironmentGcHandle = GCHandle.Alloc( environment, GCHandleType.Normal );
        }

        /// <summary>   Returns a valid and allocated handle; otherwise, throws an exception. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. A Database handle is freed upon commit or abort. While
        /// freed, the safe handle is not yet disposed as its value is not invalid.
        /// </remarks>
        /// <exception cref="ObjectDisposedException">  Thrown when a supplied object has been disposed. </exception>
        /// <returns>   A valid allocated handle (IntPtr). </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal override IntPtr AssertAllocatedHandle()
        {
            // avoid multiple volatile memory access
            System.Threading.Interlocked.MemoryBarrier();
            IntPtr result = this.Handle;
            System.Threading.Interlocked.MemoryBarrier();
            return this.IsAllocated
                ? result
                : throw new ObjectDisposedException( this.GetType().Name, $"Attempted to use an invalid {this.handle} handle" );
        }

        /// <summary>   Gets the handle of the database. </summary>
        /// <value> The database handle. </value>
        internal uint DatabaseHandle => ( uint ) this.handle;

        /// <summary>   Gets the handle of the environment. </summary>
        /// <value> The environment handle. </value>
        internal IntPtr EnvironmentHandle { get; }

        /// <summary>   Get a GC Handle of the <see cref="LmdbEnvironment"/>. </summary>
        internal GCHandle EnvironmentGcHandle { get; }

        /// <summary>   Frees the allocated handle. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        protected override void Free()
        {
            // tag the handle as free.
            base.Free();
            if ( this.EnvironmentGcHandle.IsAllocated )
                this.EnvironmentGcHandle.Free();
            if ( !EnvironmentSafeHandle.IsInvalidHandle( this.EnvironmentHandle ) )
                SafeNativeMethods.mdb_dbi_close( this.EnvironmentHandle, this.DatabaseHandle );
        }

    }

}
