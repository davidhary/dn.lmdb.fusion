using System;
using System.Collections.Generic;
using System.Text;

namespace isr.Lmdb.Fusion.Span
{

    /// <summary>   Delegate type for Value compare functions. </summary>
    /// <remarks>   Remarked by David, 2020-12-11. </remarks>
    /// <typeparam name="T">    Type of <see cref="Span{T}"/> items. </typeparam>
    /// <param name="x">    Left item to use for comparison. </param>
    /// <param name="y">    Right item to use for comparison. </param>
    /// <returns>
    /// <c>0</c> if items are equal, <c>&lt; 0</c> if left item is less, and <c>&gt; 0</c>
    /// if left item is greater then the right item.
    /// </returns>
    public delegate int SpanCompare<T>( in ReadOnlySpan<T> x, in ReadOnlySpan<T> y );

    /// <summary>   A compare functions. </summary>
    /// <remarks>   Remarked by David, 2020-12-14. </remarks>
    public static class CompareFunctions
    {

        /// <summary>   Compares values representing <see cref="T:Int32"/> types. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A ReadOnlySpan{byte} to compare. </param>
        /// <param name="y">    A ReadOnlySpan{byte} to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        public static int CompareInt32( in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            var xInt = BitConverter.ToInt32( x.ToArray(), 0 );
            var yInt = BitConverter.ToInt32( y.ToArray(), 0 );
            return Comparer<int>.Default.Compare( xInt, yInt );
        }

        /// <summary>   Compares values representing <see cref="T:string"/> types. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A ReadOnlySpan{byte} to compare. </param>
        /// <param name="y">    A ReadOnlySpan{byte} to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        public static int CompareStringOrdinal( in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            var xStr = Encoding.UTF8.GetString( x.ToArray() );
            var yStr = Encoding.UTF8.GetString( y.ToArray() );
            return StringComparer.Ordinal.Compare( xStr, yStr );
        }

        /// <summary>   Unique identifier data compare. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A ReadOnlySpan&lt;byte&gt; to process. </param>
        /// <param name="y">    A ReadOnlySpan&lt;byte&gt; to process. </param>
        /// <returns>   An int. </returns>
        public static int CompareGuid( in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            var xStr = Encoding.UTF8.GetString( x.ToArray() );
            var yStr = Encoding.UTF8.GetString( y.ToArray() );
            return StringComparer.Ordinal.Compare( xStr, yStr );
        }

    }
}
