using System;
using System.ComponentModel;

namespace isr.Lmdb.Fusion
{
    /// <summary>
    /// Represents a database entry/record as a stack-only data structure (ref struct).
    /// </summary>
    /// <remarks>   Remarked by David, 2020-12-11. </remarks>
    public readonly ref struct KeyDataPair
    {
        /// <summary>   Key bytes. </summary>
        /// <value> The key. </value>
        public ReadOnlySpan<byte> Key { get; }

        /// <summary>   Data bytes. </summary>
        /// <value> The data. </value>
        public ReadOnlySpan<byte> Data { get; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">  Key bytes. </param>
        /// <param name="data"> Data bytes. </param>
        public KeyDataPair( ReadOnlySpan<byte> key, ReadOnlySpan<byte> data )
        {
            this.Key = key;
            this.Data = data;
        }

        /// <summary>   Constructor with empty data. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="key">  Key bytes. </param>
        public KeyDataPair( ReadOnlySpan<byte> key )
        {
            this.Key = key;
            this.Data = default;
        }

        /// <summary>   Equality comparison. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="x">    Key data pair to be compared. </param>
        /// <param name="y">    Key data pair to be compared. </param>
        /// <returns>
        /// true if <paramref name="x">X</paramref> and <paramref name="y">Y</paramref> are the same type
        /// and represent the same value; otherwise, false.
        /// </returns>
        public static bool Equals( KeyDataPair x, KeyDataPair y )
        {
            return x.Key == y.Key && x.Data == y.Data;
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( KeyDataPair left, KeyDataPair right )
        {
            return Equals( left, right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( KeyDataPair left, KeyDataPair right )
        {
            return !(left == right);
        }

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// This method is not supported as DbValue cannot be boxed. To compare two DbValues, use
        /// operator==.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// true if <paramref name="obj">obj</paramref> and this instance are the same type and represent
        /// the same value; otherwise, false.
        /// </returns>
        ///
        /// <exception cref="System.NotSupportedException"> Always thrown by this method. </exception>
        [Obsolete( "Equals() on KeyDataPair will always throw an exception. Use == instead." )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        public override bool Equals( object obj ) => throw new NotSupportedException( "Not supported. Cannot call Equals() on KeyDataPair" );

        /// <summary>
        /// This method is not supported as DbValue cannot be boxed. To compare two DbValues, use
        /// operator==.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        ///
        /// <exception cref="System.NotSupportedException"> Always thrown by this method. </exception>
        [Obsolete( "GetHashCode() on KeyDataPair will always throw an exception." )]
        [EditorBrowsable( EditorBrowsableState.Never )]
        public override int GetHashCode() => throw new NotSupportedException( "Not supported. Cannot call GetHashCode() on KeyDataPair" );

#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member
    }
}
