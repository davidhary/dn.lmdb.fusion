using System;
using isr.Lmdb.Fusion.Interop;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace isr.Lmdb.Fusion
{
    /// <summary>   An LMDB safe handle. </summary>
    /// <remarks>   Remarked by David, 2020-12-16. </remarks>
    internal abstract class LmdbSafeHandle : SafeHandle
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-23. </remarks>
        public LmdbSafeHandle() : this( LmdbSafeHandle.InvalidHandle ) { }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-21. </remarks>
        /// <param name="handle">           The handle. </param>
        public LmdbSafeHandle( IntPtr handle ) : base( handle, ownsHandle: true )
        {
        }

        /// <summary>   Gets or sets a value indicating whether the handle was freed. </summary>
        /// <remarks> This field is used to tag a handle as free before the parent object holding the safe handle is disposed. 
        /// This is used with native operations such as <see cref="SafeNativeMethods.mdb_txn_abort(IntPtr)"/> or 
        /// <see cref="SafeNativeMethods.mdb_txn_commit(IntPtr)"/>, which free the handle. </remarks>
        /// <value> True if freed, false if not. </value>
        public virtual bool Freed { get; set; }

        /// <summary>   Checks if the handle of this Safe handle is allocated (not freed) and valid. </summary>
        /// <value> True if the handle of this safe handle is allocated. </value>
        public virtual bool IsAllocated => !(this.IsInvalid || this.Freed);

        /// <summary>  The Invalid Handle of this <see cref="SafeHandle"/>. </summary>
        public static IntPtr InvalidHandle = IntPtr.Zero;

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the handle value is
        /// invalid. 
        /// </summary>
        /// <value> true if the handle value is invalid; otherwise, false. </value>
        public override bool IsInvalid => LmdbSafeHandle.IsInvalidHandle( this.handle );

        /// <summary>   Query if 'handle' is an invalid handle. </summary>
        /// <remarks>   Remarked by David, 2020-12-22. </remarks>
        /// <param name="handle">   The handle. </param>
        /// <returns>   True if invalid handle, false if valid. </returns>
        public static bool IsInvalidHandle( IntPtr handle )
        {
            System.Threading.Interlocked.MemoryBarrier();
            return LmdbSafeHandle.InvalidHandle == handle;
        }

        /// <summary>   Clears the handle. </summary>
        /// <remarks>
        /// Remarked by David, 2020-12-11. This function is needed for cases were the handle was closed
        /// by the Native library, such as when a cursor was closed by the write transaction.
        /// </remarks>
        /// <returns>   The handle before it was set as invalid. </returns>
        internal IntPtr ClearHandle()
        {
            System.Threading.Interlocked.MemoryBarrier();
            IntPtr h = System.Threading.Interlocked.Exchange( ref this.handle, LmdbSafeHandle.InvalidHandle );
            System.Threading.Interlocked.MemoryBarrier();
            this.SetHandleAsInvalid();
            System.Threading.Interlocked.MemoryBarrier();
            return h;
        }

        /// <summary>   Gets the handle. </summary>
        /// <value> The handle. </value>
        public IntPtr Handle => base.handle;

        /// <summary>   Frees the allocated handle. </summary>
        /// <remarks>   David, 2021-01-09. </remarks>
        protected virtual void Free()
        { this.Freed = true; }

        /// <summary>
        /// When overridden in a derived class, executes the code required to free the handle.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <returns>
        /// true if the handle is released successfully; otherwise, in the event of a catastrophic
        /// failure, false. The releaseHandleFailed managed debugging assistant (MDA) is activated to
        /// notify developers when the ReleaseHandle method of a class derived from SafeHandle or
        /// CriticalHandle returns false. See
        /// Https://docs/framework/debug-trace-profile/releasehandlefailed-mda.md Managed Debugging
        /// Assistant.
        /// </returns>
        protected override bool ReleaseHandle()
        {
            if ( this.IsAllocated )
                // free the allocated handle
                this.Free();
            _ = this.ClearHandle();
            return this.IsInvalid;
        }

        #region " ENCAPSULATED NATIVE CALLS "

        /// <summary>   Returns a valid and allocated handle; otherwise, throws an exception. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="ObjectDisposedException">  Thrown when a supplied object has been disposed. </exception>
        /// <returns>   A valid allocated handle (IntPtr). </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal virtual IntPtr AssertAllocatedHandle()
        {
            // avoid multiple volatile memory access
            System.Threading.Interlocked.MemoryBarrier();
            return this.IsAllocated
                ? this.Handle
                : throw new ObjectDisposedException( this.GetType().Name, $"Attempted to use an invalid {this.handle} or deallocated handle" );
        }

        /// <summary>
        /// Executes a native library action <see cref="Action{IntPtr}"/> using a valid handle. Throws an exception on invalid handles.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="nativeAction">  A native action delegate that does not return a result code. </param>
        internal void AssertExecute( Action<IntPtr> nativeAction )
        {
            nativeAction( this.AssertAllocatedHandle() );
        }

        /// <summary>
        /// Executes a native library function <see cref="Func{IntPtr}"/> using a valid handle. Throws an exception on invalid handles or
        /// native function failure.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="nativeFunction">  A native function delegate that returns an <see cref="int"/> result code. </param>
        internal int AssertExecute( Func<IntPtr, int> nativeFunction )
        {
            var handle = this.AssertAllocatedHandle();
            var ret = nativeFunction( handle );
            return LmdbException.AssertExecute( ret );
        }

        /// <summary>
        /// Executes a native library function <see cref="Func{IntPtr, IntPtr}"/> using a valid handle. 
        /// Throws an exception on inputing or outputting an invalid handle.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="nativeFunction">  A native function delegate that returns an <see cref="IntPtr"/> pointer. </param>
        /// <returns>   A valid pointer. </returns>
        internal IntPtr AssertExecute( Func<IntPtr, IntPtr> nativeFunction )
        {
            var ret = nativeFunction( this.AssertAllocatedHandle() );
            return (LmdbSafeHandle.InvalidHandle == ret)
                ? throw new LmdbException( ( int ) LmdbFusionErrorCode.InvalidPointer )
                : ret;
        }

        /// <summary>
        /// A delegate for calling native library functions with a common signature using a
        /// <see cref="IntPtr"/> handle.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <typeparam name="T">    Generic type parameter defining the Result type. </typeparam>
        /// <typeparam name="TR">   Generic type parameter defining the Return code type. </typeparam>
        /// <param name="handle">   The handle to pass to the native library function. </param>
        /// <param name="result">   [out] Result code returned from the native library function call. </param>
        /// <returns>   Code returned by the native library function call. </returns>
        internal delegate TR NativeFunction<T, out TR>( IntPtr handle, out T result );

        /// <summary>
        /// Executes a native library function <see cref="NativeFunction{T, TR}"/> using a valid handle. Throws an exception on invalid handles or 
        /// native function failure.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <typeparam name="T">    Result type returned by native function. </typeparam>
        /// <param name="nativeFunction">  A native function delegate that returns an integer result code. </param>
        /// <returns>   Result returned from the native library function call. </returns>
        internal T AssertExecute<T>( NativeFunction<T, int> nativeFunction )
        {
            var ret = nativeFunction( this.AssertAllocatedHandle(), out T result );
            _ = LmdbException.AssertExecute( ret );
            return result;
        }

        #endregion

    }

}
