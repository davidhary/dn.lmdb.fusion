using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;


namespace isr.Lmdb.Fusion
{
    /// <summary>
    /// Exception class representing errors returned from MLDB API calls, or inappropriate use of the
    /// .NET bindings.
    /// </summary>
    /// <remarks>   Remarked by David, 2020-12-11. </remarks>
    /// <remarks>   Remark added by David, 2020-12-10. </remarks>
    public class LmdbException : Exception
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        public LmdbException() { this.ErrorCode = ( int ) LmdbFusionErrorCode.GeneralFailure; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="message">  The message. </param>
        public LmdbException( string message ) : base( message ) { this.ErrorCode = ( int ) LmdbFusionErrorCode.GeneralFailure; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="message">          The message. </param>
        /// <param name="innerException">   The inner exception. </param>
        public LmdbException( string message, Exception innerException ) : base( message, innerException ) { this.ErrorCode = ( int ) LmdbFusionErrorCode.GeneralFailure; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="errorCode">   The LMDB library or LMDB Fusion library error code. </param>
        public LmdbException( int errorCode ) : base( LmdbException.BuildErrorMessage( errorCode ) )
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="errorCode">    The LMDB library or LMDB Fusion library error code. </param>
        /// <param name="message">      The message. </param>
        public LmdbException( int errorCode, string message ) : base( message )
        {
            this.ErrorCode = errorCode;
        }

        /// <summary> Initializes a new instance of the class with serialized data. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        /// that holds the serialized object data about the exception being
        /// thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        /// that contains contextual information about the source or destination.
        /// </param>
        protected LmdbException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
            this.ErrorCode = info.GetInt32( nameof( LmdbException.ErrorCode ) );
        }

        /// <summary> Overrides the GetObjectData method to serialize custom values. </summary>
        /// <remarks> David, 2020-09-15. </remarks>
        /// <param name="info">    Represents the SerializationInfo of the exception. </param>
        /// <param name="context"> Represents the context information of the exception. </param>
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            if ( info is not object )
                throw new ArgumentNullException( nameof( info ) );
            info.AddValue( nameof( LmdbException.ErrorCode ), this.ErrorCode );
            base.GetObjectData( info, context );
        }

        /// <summary>   Gets or sets the LMDB library or LMDB Fusion library error code. </summary>
        /// <value> The error code. </value>
        public int ErrorCode { get; private set; }

        /// <summary>   Converts the <see cref="LmdbException.ErrorCode"/> to a valid LMDB error Code or 
        ///             <see cref="NativeResultCode.Unknown"/>. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <returns>   <see cref="LmdbException.ErrorCode"/> as a <see cref="NativeResultCode"/>. </returns>
        public NativeResultCode LmdbErrorCode => LmdbException.ToLmdbErrorCode( this.ErrorCode );

        /// <summary>   Gets the native result code. </summary>
        /// <value> The native result code. </value>
        public NativeResultCode NativeResultCode => LmdbException.ToNativeResultCode( this.ErrorCode );

        #region " LMDB EXCEPTION INFORMATION "

        /// <summary>   Print LMDB errors as Trace.Error. </summary>
        /// <value> True if trace errors; otherwise, false. </value>
        public static bool TraceErrors { get; set; } = false;

        /// <summary>   Query if <paramref name="resultCode"/> is an LMDB error code. </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   True if native result code; otherwise, false. </returns>
        private static bool IsLmdbErrorCode( int resultCode )
        {
            return resultCode >= ( int ) NativeResultCode.FirstLmdbErrorCode &&
                   resultCode <= ( int ) NativeResultCode.LastLmdbErrorCode &&
                   Enum.IsDefined( typeof( NativeResultCode ), resultCode );
        }

        /// <summary>   Converts a result code to an LMDB error Code or <see cref="NativeResultCode.Unknown"/>. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   The result code as <see cref="NativeResultCode"/> LMDB error code or <see cref="NativeResultCode.Unknown"/>. </returns>
        private static NativeResultCode ToLmdbErrorCode( int resultCode )
        {
            return LmdbException.IsLmdbErrorCode( resultCode ) ? ( NativeResultCode ) resultCode : NativeResultCode.Unknown;
        }

        /// <summary>   Query if 'resultCode' is a <see cref="NativeResultCode"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-21. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   True if native result code; otherwise, false. </returns>
        private static bool IsNativeResultCode( int resultCode )
        {
            return resultCode != ( int ) NativeResultCode.Unknown && Enum.IsDefined( typeof( NativeResultCode ), resultCode );
        }

        /// <summary>
        /// Converts a result code to a <see cref="NativeResultCode"/> or <see cref="NativeResultCode.Unknown"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   ResultCode as a NativeResultCode. </returns>
        public static NativeResultCode ToNativeResultCode( int resultCode )
        {
            return LmdbException.IsNativeResultCode( resultCode ) ? ( NativeResultCode ) resultCode : NativeResultCode.Unknown;
        }

        /// <summary>   Query if 'resultCode' is an <see cref="LmdbFusionErrorCode"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-21. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   True if LMDB Fusion result code; otherwise, false. </returns>
        private static bool IsLmdbFusionResultCode( int resultCode )
        {
            return Enum.IsDefined( typeof( LmdbFusionErrorCode ), resultCode );
        }

        /// <summary>   Converts a resultCode to a LMDB Fusion result code. </summary>
        /// <remarks>   Remarked by David, 2020-12-21. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   ResultCode as a LmdbFusionErrorCode. </returns>
        public static LmdbFusionErrorCode ToLmdbFusionResultCode( int resultCode )
        {
            return LmdbException.IsLmdbFusionResultCode( resultCode ) ? ( LmdbFusionErrorCode ) resultCode : LmdbFusionErrorCode.Unknown;
        }

        /// <summary>   Builds an unknown string. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   A string. </returns>
        private static string BuildUnknownMessage( int resultCode )
        {
            return $"Unknown LMDB error code #{resultCode}: { LmdbException.ToNativeResultCode( resultCode )}.";
        }

        private static Dictionary<int, string> _OtherErrorMessages;

        /// <summary>   The LMDB Fusion Library error message. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="errorCode">   The error code. </param>
        /// <returns>   A string. </returns>
        private static string LmdbFusionErrorMessage( int errorCode )
        {
            if ( _OtherErrorMessages is not object )
            {
                _OtherErrorMessages = new Dictionary<int, string> {
                    { ( int ) LmdbFusionErrorCode.NotReadOnly, "Attempt to use a read-only operation on a writable object." },
                    { ( int ) LmdbFusionErrorCode.InvalidTransactionState, "Transaction state is invalid for this operation." },
                    { ( int ) LmdbFusionErrorCode.NotFixed, "Attempt to issue a fixed data operation a variable size data object." },
                    { ( int ) LmdbFusionErrorCode.NotRenewable, "Attempt to renew a non-renewable object." },
                    { ( int ) LmdbFusionErrorCode.NotSingleValue, "Attempt to issue single-value operation a multi-value key object." },
                    { ( int ) LmdbFusionErrorCode.NotMultiValue, "Attempt to issue multi-value operation a single-value key object." },
                    { ( int ) LmdbFusionErrorCode.NotWritable, "Attempt to write using a read-only object." },
                    { ( int ) LmdbFusionErrorCode.InvalidPointer, "Invalid pointer value was returned by a native function." },
                    { ( int ) LmdbFusionErrorCode.TooManyFixedItems, "Item count for PutMultiple exceeds buffer size." },
                    { ( int ) LmdbFusionErrorCode.Unknown, BuildUnknownMessage( ( int ) LmdbFusionErrorCode.Unknown ) }
                };
            }
            return _OtherErrorMessages.TryGetValue( errorCode, out string message ) ? message : BuildUnknownMessage( errorCode );
        }

        /// <summary>   Fetches the LMDB Library error message. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="errorCode">    The error code. </param>
        /// <returns>   The error message. </returns>
        private static string FetchErrorMessage( int errorCode )
        {
            IntPtr errStr = isr.Lmdb.Fusion.Interop.SafeNativeMethods.mdb_strerror( errorCode );
            return System.Runtime.InteropServices.Marshal.PtrToStringAnsi( errStr );
        }

        /// <summary>   Build error message. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   A string. </returns>
        private static string BuildErrorMessage( int resultCode )
        {
            string msg = LmdbException.IsNativeResultCode( resultCode )
                            ? FetchErrorMessage( resultCode )
                            : LmdbException.LmdbFusionErrorMessage( resultCode );
            return $"({resultCode}) {msg}";
        }

#endregion " LMDB EXCEPTION INFORMATION "

#region " THROW METHODS "

        /// <summary>
        /// Check result code for error and throw if not <see cref="NativeResultCode.Success"/> code.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-11. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="resultCode">   The result code. </param>
        [Obsolete( " Use Assert Execute" )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static void CheckRetCode( int resultCode )
        {
            if ( resultCode != ( int ) NativeResultCode.Success )
            {
                string errorMessage = LmdbException.IsLmdbErrorCode( resultCode )
                    ? FetchErrorMessage( resultCode )
                    : LmdbException.LmdbFusionErrorMessage( resultCode );
                throw new LmdbException( resultCode, errorMessage );
            }
        }

        /// <summary>   Throws an <see cref="LmdbException"/> if <paramref name="resultCode"/> is not zero. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   The result code. </returns>
        public static int AssertExecute( int resultCode )
        {
            return AssertExecute( resultCode, true );
        }

        /// <summary>   Throws an <see cref="LmdbException"/> if <paramref name="resultCode"/> is not zero and
        /// the <paramref name="shouldThrow"/> is affirmative . </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <param name="shouldThrow">  True if should throw. </param>
        /// <returns>   The result code. </returns>
        public static int AssertExecute( int resultCode, bool shouldThrow )
        {
            return resultCode != 0 && shouldThrow ? throw new LmdbException( resultCode ) : resultCode;
        }

        /// <summary>
        /// Throws an <see cref="LmdbException"/> if <paramref name="resultCode"/> is not zero.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <exception cref="LmdbException">    Thrown when a LMDB error condition occurs. </exception>
        /// <param name="resultCode">   The result code. </param>
        /// <param name="message">      The message. </param>
        /// <returns>   The result code. </returns>
        public static int AssertExecute( int resultCode, string message )
        {
            return resultCode == 0 ? resultCode : throw new LmdbException( resultCode, message );
        }

        /// <summary> Asserts failure. </summary>
        /// <remarks>   David, 2021-01-18. </remarks>
        /// <param name="resultCode">       The result code. </param>
        /// <param name="failedResultCode"> The failed result code. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <paramref name="failedResultCode"/>; otherwise, throws and exception.
        /// </returns>
        public static bool AssertFailed( int resultCode, NativeResultCode failedResultCode )
        {
#pragma warning disable IDE0075 // Simplify conditional expression
            return ( int ) failedResultCode == resultCode
                ? false
                : 0 == AssertExecute( resultCode );
#pragma warning restore IDE0075 // Simplify conditional expression
        }

        /// <summary>   Asserts failure. </summary>
        /// <remarks>   David, 2021-01-18. </remarks>
        /// <param name="resultCode">       The result code. </param>
        /// <param name="failedResultCode"> The failed result code. </param>
        /// <param name="message">          The message. </param>
        /// <returns>
        /// <c>true</c> if native operation return code is <see cref="NativeResultCode.Success"/> or
        /// false if it equals <paramref name="failedResultCode"/>; otherwise, throws and exception.
        /// </returns>
        public static bool AssertFailed( int resultCode, NativeResultCode failedResultCode, string message )
        {
#pragma warning disable IDE0075 // Simplify conditional expression
            return ( int ) failedResultCode == resultCode
                ? false
                : 0 == AssertExecute( resultCode, message );
#pragma warning restore IDE0075 // Simplify conditional expression
        }

        /// <summary>   Throws an <see cref="LmdbException"/> unless <paramref name="resultCode"/> is zero 
        /// or <see cref="NativeResultCode.NotFound"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="resultCode">   The result code. </param>
        /// <returns>   The result code. </returns>
        public static int AssertRead( int resultCode )
        {
            return (0 == resultCode || ( int ) NativeResultCode.NotFound == resultCode)
                ? resultCode
               : AssertExecute( resultCode, true );
        }

#endregion " THROW METHODS "

    }

    /// <summary>   Values that represent LMDB Fusion library error codes. </summary>
    /// <remarks>   Remarked by David, 2020-12-21. </remarks>
    public enum LmdbFusionErrorCode
    {

        /// <summary>   An error code representing the unknown option. </summary>
        Unknown = isr.Lmdb.Fusion.NativeResultCode.Unknown,

        /// <summary>   An error code indicating a general failure. </summary>
        GeneralFailure = -41011,

        /// <summary>   An error code indicating an attempt to use a database with a non-database transaction. </summary>
        NotDatabaseTransaction = -41010,

        /// <summary>   An error code indicating an attempt to use a read-only operation on a writable object. </summary>
        NotReadOnly = -41009,

        /// <summary>   An error code indicating an attempt execute a transaction operation during an invalid state. </summary>
        InvalidTransactionState = -41008,

        /// <summary>   An error code indicating an attempt to use fixed data on a non-fixed data object. </summary>
        NotFixed = -41007,

        /// <summary>   An error code indicating an attempt to renew a non-renewable object. </summary>
        NotRenewable = -41006,

        /// <summary>   An error code indicating an attempt to execute a single-value
        ///             operation on a multi-valued object. </summary>
        NotSingleValue = -41005,

        /// <summary>   An error code indicating an attempt to execute a multi-value
        ///             operation on a non-multi-valued object. </summary>
        NotMultiValue = -41004,

        /// <summary>   An error code indicating an attempt to write using a read only object. </summary>
        NotWritable = -41003,

        /// <summary>   An error code indicating an invalid pointer value
        ///             was returned by a native function. </summary>
        InvalidPointer = -41002,

        /// <summary>   An error code representing too many fixed items. </summary>
        TooManyFixedItems = -41001

    }

}

