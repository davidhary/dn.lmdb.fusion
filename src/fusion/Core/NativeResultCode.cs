namespace isr.Lmdb.Fusion
{
    /// <summary>   Values that represent the LMDB Result Codes Extended with Native C codes. </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-10. 
    /// </remarks>
    public enum NativeResultCode : int
    {

        /// <summary> key/data pair already exists: MDB_KEYEXIST </summary>
        KeyExist = -30799,

        /// <summary> key/data pair not found (EOF): MDB_NOTFOUND </summary>
        NotFound = -30798,

        /// <summary> Requested page not found - this usually indicates corruption: MDB_PAGE_NOTFOUND </summary>
        PageNotFound = -30797,

        /// <summary> Located page was wrong type: MDB_CORRUPTED. </summary>
        Corrupted = -30796,

        /// <summary> Update of meta page failed or environment had fatal error: MDB_PANIC. </summary>
        Panic = -30795,

        /// <summary> Environment version mismatch: MDB_VERSION_MISMATCH. </summary>
        VersionMismatch = -30794,

        /// <summary> File is not a valid LMDB file: MDB_INVALID. </summary>
        Invalid = -30793,

        /// <summary>  Environment map size reached: MDB_MAP_FULL. </summary>
        MapFull = -30792,

        /// <summary> Environment max databases reached: MDB_DBS_FULL. </summary>
        DbsFull = -30791,

        /// <summary> Environment max readers reached: MDB_READERS_FULL. </summary>
        ReadersFull = -30790,

        /// <summary> Too many TLS keys in use - Windows only: MDB_TLS_FULL. </summary>
        TLSFull = -30789,

        /// <summary> Transaction has too many dirty pages: MDB_TXN_FULL. </summary>
        TransactionFull = -30788,

        /// <summary> Cursor stack too deep - internal error: MDB_CURSOR_FULL. </summary>
        CursorFull = -30787,

        /// <summary> Page has not enough space - internal error: MDB_PAGE_FULL. </summary>
        PageFull = -30786,

        /// <summary> Database contents grew beyond environment map size: MDB_MAP_RESIZED. </summary>
        MapResized = -30785,

        /// <summary>
        ///  <list type="bullet"><listheader>Operation and DB incompatible, or DB type changed:
        /// MDB_INCOMPATIBLE. This can mean:</listheader> <item>
        /// The operation expects an #MDB_DUPSORT / #MDB_DUPFIXED database.</item><item>
        /// Opening a named DB when the unnamed DB has #MDB_DUPSORT / #MDB_INTEGERKEY.</item><item>
        /// Accessing a data record as a database, or vice versa.</item><item>
        /// The database was dropped and recreated with different flags.</item><item>
        /// </item>
        /// </list>
        /// </summary>
        Incompatible = -30784,

        /// <summary> Invalid reuse of reader lock table slot: MDB_BAD_RSLOT.
        /// </summary>
        BadReaderLockTableSlot = -30783,

        /// <summary> Transaction must abort, has a child, or is invalid: MDB_BAD_TXN. </summary>
        BadTransaction = -30782,

        /// <summary> Unsupported size of key/DB name/data, or wrong DUPFIXED size: MDB_BAD_VALSIZE. </summary>
        BadValueSize = -30781,

        /// <summary>  The specified database handle (DBI) was changed unexpectedly: MDB_BAD_DBI.  </summary>
        BadDatabaseHandle = -30780,

        /// <summary>  Unexpected problem - transaction should abort: MDB_PROBLEM.  </summary>
        Problem = -30779,

        /// <summary> The last defined LMDB error code </summary>
        FirstLmdbErrorCode = KeyExist,

        /// <summary> The last defined LMDB error code </summary>
        LastLmdbErrorCode = Problem,

        /// <summary> Unknown; Return code not matching <see cref="NativeResultCode"/>. </summary>
        Unknown = LastLmdbErrorCode + 1,

        // Error Codes defined in C runtime (errno.h) 
        // -----------------------------------------

        /// <summary> Successful result: SUCCESS. </summary>
        Success = 0,

        /// <summary> Operation not permitted: EPERM. </summary>
        EPERM = 1,

        /// <summary> No such file or directory: ENOENT. </summary>
        FileNotFound = 2,

        /// <summary>  No such process: ESRCH, </summary>
        ESRCH = 3,

        /// <summary>  Interrupted system call: EINTR. </summary>
        EINTR = 4,

        /// <summary>  I/O error: EIO </summary>
        AccessDenied = 5,

        /// <summary>  No such device or address: ENXIO </summary>
        ENXIO = 6,

        /// <summary>  Argument list too long: E2BIG </summary>
        E2BIG = 7,

        /// <summary>  Exec format error: ENOEXEC   </summary>
        ENOEXEC = 8,

        /// <summary>  Bad file number: EBADF   </summary>
        EBADF = 9,

        /// <summary>  No child processes: ECHILD  </summary>
        ECHILD = 10,

        /// <summary>  Try again: EAGAIN  </summary>
        EAGAIN = 11,

        /// <summary>  Out of memory: ENOMEM. </summary>
        InvalidAccess = 12,

        /// <summary>  Permission denied: EACCES </summary>
        InvalidData = 13,

        /// <summary>  Bad address: EFAULT </summary>
        EFAULT = 14,

        /// <summary>  Block device required: ENOTBLK </summary>
        ENOTBLK = 15,

        /// <summary>  Device or resource busy: EBUSY  </summary>
        CurrentDirectory = 16,

        /// <summary>  File exists: EEXIST </summary>
        EEXIST = 17,

        /// <summary>  Cross-device link: EXDEV  </summary>
        EXDEV = 18,

        /// <summary>  No such device: ENODEV </summary>
        ENODEV = 19,

        /// <summary>  Not a directory: ENOTDIR </summary>
        ENOTDIR = 20,

        /// <summary>  Is a directory: EISDIR </summary>
        EISDIR = 21,

        /// <summary>  Invalid argument: EINVAL   </summary>
        BadCommand = 22,

        /// <summary>  File table overflow: ENFILE   </summary>
        ENFILE = 23,

        /// <summary>  Too many open files: EMFILE   </summary>
        EMFILE = 24,

        /// <summary>  Not a typewriter: ENOTTY   </summary>
        ENOTTY = 25,

        /// <summary>  Text file busy: ETXTBSY   </summary>
        ETXTBSY = 26,

        /// <summary>  File too large: EFBIG  </summary>
        EFBIG = 27,

        /// <summary>  No space left on device: ENOSPC </summary>
        NoSpaceOnDevice = 28,

        /// <summary>  Illegal seek: ESPIPE  </summary>
        IllegalSeek = 29,

        /// <summary>  Read-only file system: EROFS   </summary>
        EROFS = 30,

        /// <summary>  Too many links: EMLINK </summary>
        EMLINK = 31,

        /// <summary>  Broken pipe: EPIPE  </summary>
        EPIPE = 32,

        /// <summary>  Math argument out of domain of func: EDOM   </summary>
        EDOM = 33,

        /// <summary>  Math result not representable: ERANGE </summary>
        ERANGE = 34,

        /// <summary>  Resource deadlock would occur  : EDEADLK   </summary>
        EDEADLK = 35,

        /// <summary>  File name too long : ENAMETOOLONG  </summary>
        ENAMETOOLONG = 36,

        /// <summary>  No record locks available : ENOLCK   </summary>
        ENOLCK = 37,

        /// <summary>  Function not implemented : ENOSYS  </summary>
        ENOSYS = 38,

        /// <summary>  Directory not empty : ENOTEMPTY   </summary>
        ENOTEMPTY = 39,

        /// <summary>  Too many symbolic links encountered  : ELOOP  </summary>
        ELOOP = 40,

        /// <summary>  Operation would block : EWOULDBLOCK </summary>
        EWOULDBLOCK = EAGAIN,

        /// <summary>  No message of desired type  : ENOMSG </summary>
        ENOMSG = 42,

        /// <summary>  Accessing a corrupted shared library : ELIBBAD </summary>
        ELIBBAD = 80

    }

}
