using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion
{
    /// <summary>
    /// Represents LMDB version information.
    /// </summary>
    public static class LmdbVersionInfo
    {

        /// <summary>   Static constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        static LmdbVersionInfo()
        {
            LmdbVersionMessage = Marshal.PtrToStringAnsi( SafeNativeMethods.mdb_version( out int major, out int minor, out int patch ) );
            LmdbVersionInfo.Major = major;
            LmdbVersionInfo.Minor = minor;
            LmdbVersionInfo.Patch = patch;
            LmdbVersionInfo.Version = new System.Version( major, Minor, patch );
        }

        /// <summary>
        /// Major version number.
        /// </summary>
        public static int Major { get; }

        /// <summary>
        /// Minor version number.
        /// </summary>
        public static int Minor { get; }

        /// <summary>
        /// Patch version number.
        /// </summary>
        public static int Patch { get; }

        /// <summary>   Gets or sets a message describing the LMDB version. </summary>
        /// <value> A message describing the LMDB version. </value>
        public static string LmdbVersionMessage { get; }

        /// <summary>   Gets or sets the version. </summary>
        /// <value> The version. </value>
        public static System.Version Version { get; }
    }
}
