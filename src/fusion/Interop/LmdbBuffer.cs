using System;
using System.Buffers;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

using static System.Runtime.CompilerServices.Unsafe;

namespace isr.Lmdb.Fusion.Interop
{

    /// <summary>   Compare function for <see cref="LmdbBuffer"/>. </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-10. <para>
    /// (c) 2018 Victor Baybecov. All rights reserved. </para>
    /// </remarks>
    /// <param name="left">     [in,out] The left. </param>
    /// <param name="right">    [in,out] The right. </param>
    /// <returns>   An int. </returns>
    [UnmanagedFunctionPointer( CallingConvention.Cdecl )]
    public delegate int LmdbBufferCompareFunction( ref LmdbBuffer left, ref LmdbBuffer right );

    /// <summary>   Provides unsafe read/write operations on a memory pointer. </summary>
    /// <summary>
    /// Fundamental data-exchange structure for native interop with the MDB_val LMDB API structure.
    /// MDB_Val is a Generic structure used for passing keys and data in and out of the database.
    /// 
    /// Values returned from the database are valid only until a subsequent update operation, or the
    /// end of the transaction. Do not modify or free them, they commonly point into the database
    /// itself.
    /// 
    /// Key sizes must be between 1 and <see cref="Interop.SafeNativeMethods.mdb_env_get_maxkeysize(IntPtr)"/>
    /// inclusive. The same applies to data sizes in databases with the
    /// <see cref="DatabaseOpenOptions.DuplicatesSort"/> 
    /// <see cref="LmdbConstants.MDB_DUPSORT"/> option. Other data items can in theory be from 0 to
    /// 0xffffffff bytes long.
    /// </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-10. <para>
    /// (c) 2018 Victor Baybecov. All rights reserved. </para><para>
    /// For Performance and Correctness, the layout of this structure must not be changed. This
    /// structure is blittable and is Marshalled directly to Native code via P/Invoke. In other words,
    /// this structure has a common representation in both managed and unmanaged memory and do not
    /// require special handling by the interop marshaler. </para> </remarks>
    [DebuggerTypeProxy( typeof( LmdbBufferDebugView ) )]
    [DebuggerDisplay( "Length={" + nameof( Length ) + ("}") )]
    [StructLayout( LayoutKind.Sequential )]
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>" )]
    public readonly unsafe struct LmdbBuffer : IEquatable<LmdbBuffer>
    {

        /// <summary>   Instance of an invalid <see cref="LmdbBuffer"/>. </summary>
        internal static LmdbBuffer Invalid = new( 0, ( byte* ) IntPtr.Zero );

        /// <summary>   The size. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        internal readonly IntPtr Size;

        /// <summary>   The pointer. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        internal readonly byte* Pointer;

        /// <summary>   Attach a view to an unmanaged buffer owned by external code. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <param name="length">   Length of the buffer. </param>
        /// <param name="data">     Unmanaged byte buffer. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbBuffer( long length, IntPtr data )
        {
            if ( data == IntPtr.Zero )
            {
                throw new ArgumentNullException( nameof( data ) );
            }
            if ( length <= 0 )
            {
                throw new ArgumentException( "Memory size must be > 0", nameof( length ) );
            }
            this.Size = ( IntPtr ) length;
            this.Pointer = ( byte* ) data;
        }

        /// <summary>   Attach a view to an unmanaged buffer owned by external code. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="length">   The length. </param>
        /// <param name="handle">   The handle. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbBuffer( long length, MemoryHandle handle ) : this( length, ( byte* ) handle.Pointer )
        {
        }

        /// <summary>   Unsafe constructors performs no input checks. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="length">   The length. </param>
        /// <param name="pointer">  [in,out] If non-null, the pointer. </param>
        [CLSCompliant( false )]
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbBuffer( long length, byte* pointer )
        {
            this.Size = ( IntPtr ) length;
            this.Pointer = pointer;
        }

        /// <remarks>
        /// We only expose this shape constructor to basically force you to use
        /// a fixed statement to obtain the pointer. If we accepted a Span or 
        /// ReadOnlySpan here, we would have to do scarier things to pin/unpin
        /// the buffer. Since this library is geared towards safe and easy usage,
        /// this way somewhat forces you onto the correct path.
        /// 
        /// </remarks>
        /// <param name="bufferSize">The length of the buffer</param>
        /// <param name="pinnedOrStackAllocBuffer">A pointer to a buffer. 
        /// The underlying memory may be managed(an array), unmanaged or stack-allocated.
        /// If it is managed, it **MUST** be pinned via either GCHandle.Alloc or a fixed statement
        /// </param>
        internal LmdbBuffer( int bufferSize, byte* pinnedOrStackAllocBuffer )
        {
            this.Size = ( IntPtr ) bufferSize;
            this.Pointer = pinnedOrStackAllocBuffer;
        }

        /// <summary>   Throw retained memory not pinned. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        [MethodImpl( MethodImplOptions.NoInlining )]
        private static void ThrowRetainedMemoryNotPinned()
        {
            throw new InvalidOperationException( "RetainedMemory must be pinned for using as LmdbBuffer." );
        }

        /// <summary>   Attach a view to an unmanaged buffer owned by external code. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="span"> The span. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbBuffer( Span<byte> span )
        {
            this.Size = ( IntPtr ) span.Length;
            this.Pointer = ( byte* ) AsPointer( ref span.GetPinnableReference() );
        }

#if ( NETSTANDARD2_1_OR_GREATER || NET5_0_OR_GREATER )
        /// <summary>   Attach a view to an unmanaged buffer owned by external code. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="value">    value to be written. </param>
        public LmdbBuffer( string value )
        {
            fixed ( char* keyPtr = value )
            {
                var keyUtf8Length = Encoding.UTF8.GetByteCount( value );
                var keyBytes = stackalloc byte[keyUtf8Length];
                _ = Encoding.UTF8.GetBytes( keyPtr, value.Length, keyBytes, keyUtf8Length );
                this.Size = ( IntPtr ) keyUtf8Length;
                this.Pointer = keyBytes;
            }
        }
#endif

        /// <summary>   Gets a value indicating whether this object is valid. </summary>
        /// <value> True if this object is valid; otherwise, false. </value>
        public bool IsValid
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => this.Pointer != null;
        }

        /// <summary>
        /// TODO review: empty could be a result from Slice and perfectly valid, so it is not the same as
        /// IsValid which checks pointer.
        /// </summary>
        /// <value> True if this object is empty; otherwise, false. </value>
        public bool IsEmpty
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => this.Size == IntPtr.Zero;
        }

        /// <summary>   Query if 'first' is filled with value. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="value">    The value. </param>
        /// <returns>   True if filled with value; otherwise, false. </returns>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool IsFilledWithValue( byte value )
        {
            return IsFilledWithValue( ref *this.Data, ( ulong ) this.Size, value );
        }

        /// <summary>   Gets a span representation of the buffer. </summary>
        /// <remarks>   David, 2020-12-26. </remarks>
        /// <returns>   A ReadOnlySpan&lt;byte&gt; </returns>
        public ReadOnlySpan<byte> AsSpan() => new( this.Pointer, checked(( int ) this.Size) );

        /// <summary>   Copies the data of the buffer to a new array. </summary>
        /// <remarks>   Equivalent to AsSpan().ToArray() but makes intent a little more clear. </remarks>
        /// <returns>
        /// A newly allocated array containing data copied from the dereferenced data pointer.
        /// </returns>
        public byte[] CopyToNewArray() => this.AsSpan().ToArray();

        /// <summary>   Gets the span. </summary>
        /// <value> The span. </value>
        public Span<byte> Span
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => new( this.Pointer, ( int ) this.Size );
        }

        /// <summary>   Gets the length. </summary>
        /// <value> The length. </value>
        public int Length
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => checked(( int ) ( long ) this.Size);
        }

        /// <summary>   Gets the long length. </summary>
        /// <value> The length of the long. </value>
        public long LongLength
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => ( long ) this.Size;
        }

        /// <summary>   Gets the data. </summary>
        /// <value> The data. </value>
        [CLSCompliant( false )]
        public byte* Data
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => this.Pointer;
        }

        /// <summary>   for cases when unsafe is not allowed, e.g. async. </summary>
        /// <value> The int pointer. </value>
        public IntPtr IntPtr
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => ( IntPtr ) this.Pointer;
        }

#region " READ / WRITE "

        /// <summary>
        /// Gets a value indicating whether the additional correctness checks is enabled.
        /// Used for debugging.
        /// </summary>
        /// <value> True if additional correctness checks enabled; otherwise, false. </value>
        public static bool AdditionalCorrectnessChecksEnabled => false;

        /// <summary>   Slices. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="start">    The start. </param>
        /// <returns>   A LmdbBuffer. </returns>
        [Pure]
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbBuffer Slice( long start )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( 0, start ); }

            return new LmdbBuffer( ( long ) this.Size - start, this.Pointer + start );
        }

        /// <summary>   Slices. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="start">    The start. </param>
        /// <param name="length">   The length. </param>
        /// <returns>   A LmdbBuffer. </returns>
        [Pure]
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbBuffer Slice( long start, long length )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( start, length ); }

            return new LmdbBuffer( length, this.Pointer + start );
        }

        /// <summary>   Asserts. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="length">   The length. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal void Assert( long index, long length )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            {
                if ( !this.IsValid )
                {
                    throw new InvalidOperationException( "LmdbBuffer is invalid." );
                }

                unchecked
                {
                    if ( ( ulong ) index + ( ulong ) length > ( ulong ) this.Size )
                    {
                        ArgumentException argumentException = new(
                            $"Not enough space in LmdbBuffer; [{nameof( index )}({index}) + {nameof( length )}({length})] > {nameof( LmdbBuffer.Size )}({this.Size}).",
                                                                                nameof( index ) );
                        throw argumentException;
                    }
                }
            }
        }

        /// <summary>   Gets the <see cref="byte"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [Pure]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public char ReadChar( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 2 ); }
            return ReadUnaligned<char>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="byte"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteChar( long index, char value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 2 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="sbyte"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public sbyte ReadSByte( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 1 ); }
            return ReadUnaligned<sbyte>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="sbyte"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteSByte( long index, sbyte value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 1 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="byte"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [Pure]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public byte ReadByte( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 1 ); }
            return ReadUnaligned<byte>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="byte"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteByte( long index, byte value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 1 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>
        /// Indexer to get or set items within this collection using array index syntax.
        /// </summary>
        /// <param name="index">    Zero-based index of the entry to access. </param>
        /// <returns>   The indexed item. </returns>
        [Pure]
        public byte this[long index]
        {
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get {
                if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
                {
                    this.Assert( index, 1 );
                }

                return *(this.Pointer + index);
            }
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            set {
                if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
                { this.Assert( index, 1 ); }

                *(this.Pointer + index) = value;
            }
        }

        /// <summary>   Gets the <see cref="short"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public short ReadInt16( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 2 ); }
            return ReadUnaligned<short>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="short"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteInt16( long index, short value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 2 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="int"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int ReadInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return ReadUnaligned<int>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="int"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteInt32( long index, int value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Volatile read int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   An int. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int VolatileReadInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Volatile.Read( ref *( int* ) (this.Pointer + index) );
        }

        /// <summary>   Volatile write int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void VolatileWriteInt32( long index, int value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            Volatile.Write( ref *( int* ) (this.Pointer + index), value );
        }

        /// <summary>   Volatile read u int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   An uint. </returns>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public uint VolatileReadUInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Volatile.Read( ref *( uint* ) (this.Pointer + index) );
        }

        /// <summary>   Volatile write u int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void VolatileWriteUInt32( long index, uint value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            Volatile.Write( ref *( uint* ) (this.Pointer + index), value );
        }

        /// <summary>   Volatile read int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   A long. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long VolatileReadInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Volatile.Read( ref *( long* ) (this.Pointer + index) );
        }

        /// <summary>   Volatile write u int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void VolatileWriteUInt64( long index, ulong value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            Volatile.Write( ref *( ulong* ) (this.Pointer + index), value );
        }

        /// <summary>   Volatile read u int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   An ulong. </returns>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public ulong VolatileReadUInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Volatile.Read( ref *( ulong* ) (this.Pointer + index) );
        }

        /// <summary>   Volatile write int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void VolatileWriteInt64( long index, long value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            Volatile.Write( ref *( long* ) (this.Pointer + index), value );
        }

        /// <summary>   Interlocked increment int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   An int. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int InterlockedIncrementInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Interlocked.Increment( ref *( int* ) (this.Pointer + index) );
        }

        /// <summary>   Interlocked decrement int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   An int. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int InterlockedDecrementInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Interlocked.Decrement( ref *( int* ) (this.Pointer + index) );
        }

        /// <summary>   Interlocked add int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        /// <returns>   An int. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int InterlockedAddInt32( long index, int value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Interlocked.Add( ref *( int* ) (this.Pointer + index), value );
        }

        /// <summary>   Interlocked read int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   An int. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int InterlockedReadInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Interlocked.Add( ref *( int* ) (this.Pointer + index), 0 );
        }

        /// <summary>   Interlocked compare exchange int 32. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">        Zero-based index of the. </param>
        /// <param name="value">        The value. </param>
        /// <param name="comparand">    The comparand. </param>
        /// <returns>   An int. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int InterlockedCompareExchangeInt32( long index, int value, int comparand )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return Interlocked.CompareExchange( ref *( int* ) (this.Pointer + index), value, comparand );
        }

        /// <summary>   Interlocked increment int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   A long. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long InterlockedIncrementInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Interlocked.Increment( ref *( long* ) (this.Pointer + index) );
        }

        /// <summary>   Interlocked decrement int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   A long. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long InterlockedDecrementInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Interlocked.Decrement( ref *( long* ) (this.Pointer + index) );
        }

        /// <summary>   Interlocked add int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        /// <returns>   A long. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long InterlockedAddInt64( long index, long value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Interlocked.Add( ref *( long* ) (this.Pointer + index), value );
        }

        /// <summary>   Interlocked read int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   A long. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long InterlockedReadInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Interlocked.Add( ref *( long* ) (this.Pointer + index), 0 );
        }

        /// <summary>   Interlocked compare exchange int 64. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">        Zero-based index of the. </param>
        /// <param name="value">        The value. </param>
        /// <param name="comparand">    The comparand. </param>
        /// <returns>   A long. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long InterlockedCompareExchangeInt64( long index, long value, long comparand )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return Interlocked.CompareExchange( ref *( long* ) (this.Pointer + index), value, comparand );
        }

        /// <summary>   Gets the <see cref="long"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public long ReadInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return ReadUnaligned<long>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="long"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteInt64( long index, long value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="ushort"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public ushort ReadUInt16( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 2 ); }
            return ReadUnaligned<ushort>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="ushort"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteUInt16( long index, ushort value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 2 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="uint"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public uint ReadUInt32( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return ReadUnaligned<uint>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="uint"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteUInt32( long index, uint value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="ulong"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public ulong ReadUInt64( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return ReadUnaligned<ulong>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="ulong"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [CLSCompliant( false )]
        public void WriteUInt64( long index, ulong value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            {
                this.Assert( index, 8 );
            }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="float"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public float ReadFloat( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            return ReadUnaligned<float>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="float"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteFloat( long index, float value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 4 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Gets the <see cref="double"/> value at a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes from which to get. </param>
        /// <returns>   the value at a given index. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public double ReadDouble( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            return ReadUnaligned<double>( this.Pointer + index );
        }

        /// <summary>   Writes a <see cref="double"/> value to a given index. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    index in bytes for where to put. </param>
        /// <param name="value">    value to be written. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteDouble( long index, double value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 8 ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>
        /// Unaligned read starting from index. A shortcut to
        /// <see cref="Unsafe.ReadUnaligned{T}(void*)"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   A T. </returns>
        [Pure]
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public T Read<T>( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            {
                var size = SizeOf<T>();
                this.Assert( index, size );
            }
            return ReadUnaligned<T>( this.Pointer + index );
        }

        /// <summary>   Writes. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void Write<T>( long index, T value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, SizeOf<T>() ); }
            WriteUnaligned( this.Pointer + index, value );
        }

        /// <summary>   Writes the s. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        /// <returns>   An int. </returns>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        internal int WriteS<T>( long index, T value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, SizeOf<T>() ); }
            WriteUnaligned( this.Pointer + index, value );
            return SizeOf<T>();
        }

        /// <summary>   Clears this object to its blank/initial state. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="length">   The length. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void Clear( long index, int length )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, length ); }
            var destination = this.Pointer + index;
            InitBlockUnaligned( destination, 0, ( uint ) length );
        }

        /// <summary>   Fills. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="length">   The length. </param>
        /// <param name="value">    The value. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void Fill( long index, int length, byte value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, length ); }
            var destination = this.Pointer + index;
            InitBlockUnaligned( destination, value, ( uint ) length );
        }

        /// <summary>   Reads ASCII digit. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <returns>   The ASCII digit. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [Pure]
        public int ReadAsciiDigit( long index )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 1 ); }
            return ReadUnaligned<byte>( this.Pointer + index ) - '0';
        }

        /// <summary>   Writes an ASCII digit. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="value">    The value. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void WriteAsciiDigit( long index, byte value )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, 1 ); }
            WriteUnaligned( this.Pointer + index, ( byte ) (value + '0') );
        }

        /// <summary>   Verify alignment. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="alignment">    The alignment. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void VerifyAlignment( int alignment )
        {
            if ( 0 != (( long ) this.Pointer & (alignment - 1)) )
            {
                throw new InvalidOperationException(
                    $"LmdbBuffer is not correctly aligned: addressOffset={( long ) this.Pointer:D} in not divisible by {alignment:D}" );
            }
        }

        /// <summary>   Copies to. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="destination">  [in,out] Destination for the. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CopyTo( ref Memory<byte> destination )
        {
            this.Span.CopyTo( destination.Span );
        }

        /// <summary>   Copies to. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="destination">  Destination for the. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CopyTo( in LmdbBuffer destination )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            {
                destination.Assert( 0, ( long ) this.Size );
            }
            this.CopyTo( 0, destination.Data, ( int ) this.Size );
        }

        /// <summary>   Copies to. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="index">        Zero-based index of the. </param>
        /// <param name="destination">  [in,out] If non-null, destination for the. </param>
        /// <param name="length">       The length. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CopyTo( long index, void* destination, int length )
        {
            if ( LmdbBuffer.AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, length ); }
            CopyBlockUnaligned( destination, this.Pointer + index, checked(( uint ) length) );
        }

#region " DEBUGGER PROXY CLASS "

        /// <summary>   The LMDB Buffer debug view. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        internal class LmdbBufferDebugView
        {
            /// <summary>   The buffer. </summary>
            private readonly LmdbBuffer _Buffer;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-20. </remarks>
            /// <param name="buffer">   The buffer. </param>
            public LmdbBufferDebugView( LmdbBuffer buffer )
            {
                this._Buffer = buffer;
            }

            /// <summary>   Gets the data. </summary>
            /// <value> The data. </value>
            public void* Data => this._Buffer.Data;

            /// <summary>   Gets the length. </summary>
            /// <value> The length. </value>
            public long Length => this._Buffer.Length;

            /// <summary>   Gets a value indicating whether this object is valid. </summary>
            /// <value> True if this object is valid; otherwise, false. </value>
            public bool IsValid => this._Buffer.IsValid;

            /// <summary>   Gets the span. </summary>
            /// <value> The span. </value>
            public Span<byte> Span => this._Buffer.IsValid ? this._Buffer.Span : default;
        }

#endregion " DEBUGGER PROXY CLASS "

        /// <summary>   Returns the hash code for this instance. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <exception cref="NotSupportedException">    Thrown when the requested operation is not
        ///                                             supported. </exception>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        public override int GetHashCode()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="other">    An object to compare with this object. </param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other">other</paramref> parameter;
        /// otherwise, false.
        /// </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public bool Equals( LmdbBuffer other )
        {
            return this.Size == other.Size && SequenceEqual( ref *this.Pointer, ref *other.Pointer, checked(( uint ) ( long ) this.Size) );
        }

        /// <summary>
        /// Methods from
        /// https://source.dot.net/#System.Private.CoreLib/shared/System/SpanHelpers.Byte.cs,ae8b63bad07668b3.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="start">    [in,out] The start. </param>
        /// <param name="offset">   The offset. </param>
        /// <returns>   The u int pointer. </returns>

        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private static UIntPtr LoadUIntPtr( ref byte start, IntPtr offset )
            => ReadUnaligned<UIntPtr>( ref AddByteOffset( ref start, offset ) );

        /// <summary>   Loads a vector. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="start">    [in,out] The start. </param>
        /// <param name="offset">   The offset. </param>
        /// <returns>   The vector. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        private static Vector<byte> LoadVector( ref byte start, IntPtr offset )
            => ReadUnaligned<Vector<byte>>( ref AddByteOffset( ref start, offset ) );

        /// <summary>   Sequence equal. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="first">    [in,out] The first. </param>
        /// <param name="second">   [in,out] The second. </param>
        /// <param name="length">   The length. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        [CLSCompliant( false )]
#if HAS_AGGR_OPT
        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
#endif
        public static bool SequenceEqual( ref byte first, ref byte second, uint length )
        {
            if ( AreSame( ref first, ref second ) )
            {
                goto Equal;
            }

            IntPtr offset = ( IntPtr ) 0; // Use IntPtr for arithmetic to avoid unnecessary 64->32->64 truncations
            IntPtr lengthToExamine = ( IntPtr ) ( void* ) length;

            if ( Vector.IsHardwareAccelerated && ( byte* ) lengthToExamine >= ( byte* ) Vector<byte>.Count )
            {
                lengthToExamine -= Vector<byte>.Count;
                while ( ( byte* ) lengthToExamine > ( byte* ) offset )
                {
                    if ( LoadVector( ref first, offset ) != LoadVector( ref second, offset ) )
                    {
                        goto NotEqual;
                    }
                    offset += Vector<byte>.Count;
                }
                return LoadVector( ref first, lengthToExamine ) == LoadVector( ref second, lengthToExamine );
            }

            if ( ( byte* ) lengthToExamine >= ( byte* ) sizeof( UIntPtr ) )
            {
                lengthToExamine -= sizeof( UIntPtr );
                while ( ( byte* ) lengthToExamine > ( byte* ) offset )
                {
                    if ( LoadUIntPtr( ref first, offset ) != LoadUIntPtr( ref second, offset ) )
                    {
                        goto NotEqual;
                    }
                    offset += sizeof( UIntPtr );
                }
                return LoadUIntPtr( ref first, lengthToExamine ) == LoadUIntPtr( ref second, lengthToExamine );
            }

            while ( ( byte* ) lengthToExamine > ( byte* ) offset )
            {
                if ( AddByteOffset( ref first, offset ) != AddByteOffset( ref second, offset ) )
                {
                    goto NotEqual;
                }
                offset += 1;
            }

            Equal:
            return true;
            NotEqual: // Workaround for https://github.com/dotnet/coreclr/issues/13549
            return false;
        }

        /// <summary>   Query if 'first' is filled with value. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="first">    [in,out] The first. </param>
        /// <param name="length">   The length. </param>
        /// <param name="value">    The value. </param>
        /// <returns>   True if filled with value; otherwise, false. </returns>
        [CLSCompliant( false )]
#if HAS_AGGR_OPT
        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
#endif
        public static bool IsFilledWithValue( ref byte first, ulong length, byte value )
        {
            var valueVector = new Vector<byte>( value );
            IntPtr offset = ( IntPtr ) 0; // Use IntPtr for arithmetic to avoid unnecessary 64->32->64 truncations
            IntPtr lengthToExamine = ( IntPtr ) ( void* ) length;

            if ( Vector.IsHardwareAccelerated && ( byte* ) lengthToExamine >= ( byte* ) Vector<byte>.Count )
            {
                lengthToExamine -= Vector<byte>.Count;
                while ( ( byte* ) lengthToExamine > ( byte* ) offset )
                {
                    if ( LoadVector( ref first, offset ) != valueVector )
                    {
                        goto NotEqual;
                    }
                    offset += Vector<byte>.Count;
                }
                return LoadVector( ref first, lengthToExamine ) == valueVector;
            }

            // TODO UIntPtr or at least uint/ulong from value
            if ( ( byte* ) lengthToExamine >= ( byte* ) sizeof( UIntPtr ) )
            {
                lengthToExamine -= sizeof( UIntPtr );
                UIntPtr uintPtrValue = UIntPtr.Size == 8 ? ( UIntPtr ) Vector.AsVectorUInt64( valueVector )[0] : ( UIntPtr ) Vector.AsVectorUInt32( valueVector )[0];
                while ( ( byte* ) lengthToExamine > ( byte* ) offset )
                {
                    if ( LoadUIntPtr( ref first, offset ) != uintPtrValue )
                    {
                        goto NotEqual;
                    }
                    offset += sizeof( UIntPtr );
                }
                return LoadUIntPtr( ref first, lengthToExamine ) == uintPtrValue;
            }

            while ( ( byte* ) lengthToExamine > ( byte* ) offset )
            {
                if ( AddByteOffset( ref first, offset ) != value )
                {
                    goto NotEqual;
                }
                offset += 1;
            }

            return true;
            NotEqual: // Workaround for https://github.com/dotnet/coreclr/issues/13549
            return false;
        }

#endregion
    }

    /// <summary>   LMDB buffer extensions. </summary>
    /// <remarks>   Remarked by David, 2020-12-20. </remarks>
    internal static unsafe class LmdbBufferExtensions
    {
        /// <summary>   An Encoding extension method that gets a string. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="encoding"> The encoding to act on. </param>
        /// <param name="buffer">   The buffer. </param>
        /// <returns>   The string. </returns>
        public static string GetString( this Encoding encoding, in LmdbBuffer buffer )
        {
            return encoding.GetString( buffer.Pointer, buffer.Length );
        }

        /// <summary>   A Span&lt;byte&gt; extension method that copies to. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="source">       The source to act on. </param>
        /// <param name="destination">  Destination for the. </param>
        public static void CopyTo( this ReadOnlySpan<byte> source, LmdbBuffer destination )
        {
            fixed ( byte* ptr = source )
            {
                var sourceDb = new LmdbBuffer( source.Length, ptr );
                sourceDb.CopyTo( destination );
            }
        }

        /// <summary>   A Span&lt;byte&gt; extension method that copies to. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="source">       The source to act on. </param>
        /// <param name="destination">  Destination for the. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static void CopyTo( this Span<byte> source, LmdbBuffer destination )
        {
            fixed ( byte* ptr = source )
            {
                var sourceDb = new LmdbBuffer( source.Length, ptr );
                sourceDb.CopyTo( destination );
            }
        }
    }
}
