using System;
using System.Runtime.InteropServices;

namespace isr.Lmdb.Fusion.Interop
{
    /// <summary>
    /// Information about the environment. 
    /// </summary>
    [StructLayout( LayoutKind.Sequential, Pack = 4 )]
    internal readonly unsafe struct LmdbEnvInfo
    {

#pragma warning disable IDE1006 // Naming Styles
        /// <summary>
        /// Address of map, if fixed
        /// </summary>
        public readonly void* MapAddress;

        /// <summary>
        /// Size of the data memory map
        /// </summary>
        public readonly IntPtr MapSize;

        /// <summary>
        /// ID of the last used page
        /// </summary>
        public readonly IntPtr LastUsedPageId;

        /// <summary>
        /// ID of the last committed transaction
        /// </summary>
        public readonly IntPtr LastCommittedTransactionId;

        /// <summary>
        /// max reader slots in the environment
        /// </summary>
        public readonly uint MaxReaders;

        /// <summary>
        /// max reader slots used in the environment
        /// </summary>
        public readonly uint UsedReaders;

#pragma warning restore IDE1006 // Naming Styles
    }
}
