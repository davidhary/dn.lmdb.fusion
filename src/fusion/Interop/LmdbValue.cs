using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace isr.Lmdb.Fusion.Interop
{

    /// <summary>
    /// Defines wrapper signature for managed comparison functions that can be called from native
    /// code.
    /// </summary>
    /// <remarks> 
    /// Defines a callback function used to compare two keys in a database: MDB_com_func.
    /// </remarks>
    /// <param name="x">    Left <see cref="LmdbValue"/> to use in comparison. </param>
    /// <param name="y">    Right <see cref="LmdbValue"/> to use in comparison. </param>
    /// <returns>   An int. </returns>
    [UnmanagedFunctionPointer( CallingConvention.Cdecl ), SuppressUnmanagedCodeSecurity]
    public delegate int LmdbValueCompareFunction( in LmdbValue x, in LmdbValue y );

    /// <summary>
    /// Fundamental data-exchange structure for native interop with the MDB_val LMDB API structure.
    /// MDB_Val is a Generic structure used for passing keys and data in and out of the database.
    /// 
    /// Values returned from the database are valid only until a subsequent update operation, or the
    /// end of the transaction. Do not modify or free them, they commonly point into the database
    /// itself.
    /// 
    /// Key sizes must be between 1 and <see cref="Interop.SafeNativeMethods.mdb_env_get_maxkeysize(IntPtr)"/>
    /// inclusive. The same applies to data sizes in databases with the
    /// <see cref="DatabaseOpenOptions.DuplicatesSort"/>
    /// <see cref="LmdbConstants.MDB_DUPSORT"/> option. Other data items can in theory be from 0 to
    /// 0xffffffff bytes long.
    /// </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-10. <para>
    /// (c) 2020 Karl Waclawek. All rights reserved. </para><para>
    /// For Performance and Correctness, the layout of this structure must not be changed. This
    /// structure is blittable and is Marshalled directly to Native code via P/Invoke. In other words,
    /// this structure has a common representation in both managed and unmanaged memory and do not
    /// require special handling by the interop marshaler. </para>
    /// </remarks>
    [DebuggerTypeProxy( typeof( LmdbValueDebugView ) )]
    [DebuggerDisplay( "Length={" + nameof( Length ) + ("}") )]
    [StructLayout( LayoutKind.Sequential )]
    public readonly unsafe ref struct LmdbValue
    {

        /// <summary>   Data size in bytes. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        internal readonly IntPtr Size;

        /// <summary>   Native data pointer. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE1006:Naming Styles", Justification = "<Pending>" )]
        internal readonly void* Data;

        /// <summary>   Constructor. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="data"> [in,out] Pointer to value. </param>
        /// <param name="size"> Size of value in bytes. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbValue( void* data, long size )
        {
            this.Data = data;
            this.Size = unchecked(( IntPtr ) size);
        }

        /// <summary>   Constructor. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="data"> [in,out] Pointer to value. </param>
        /// <param name="size"> Size of value in bytes. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbValue( void* data, int size )
        {
            this.Data = data;
            this.Size = unchecked(( IntPtr ) size);
        }

        /// <summary>   Constructor. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="data"> [in,out] Pointer to value. </param>
        /// <param name="size"> Size of value in bytes. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbValue( void* data, ulong size )
        {
            this.Data = data;
            this.Size = unchecked(( IntPtr ) size);
        }

        /// <summary>   Constructor. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="data"> [in,out] Pointer to value. </param>
        /// <param name="size"> Size of value in bytes. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbValue( void* data, uint size )
        {
            this.Data = data;
            this.Size = unchecked(( IntPtr ) size);
        }


        /// <summary>   Constructor. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <param name="span"> <c>Span</c> instance to create the <c>DbValue</c> from. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public LmdbValue( Span<byte> span )
        {
            this.Size = ( IntPtr ) span.Length;
            this.Data = Unsafe.AsPointer( ref MemoryMarshal.GetReference( span ) );
        }

        /// <summary>   Creates <see cref="LmdbValue"/> from <see cref="ReadOnlySpan{T}"/>. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <typeparam name="T">    Type of <c>Span</c> element. </typeparam>
        /// <param name="span"> <c>Span</c> instance to create the <c>DbValue</c> from. </param>
        /// <returns>   New <c>DbValue</c> instance. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static LmdbValue From<T>( ReadOnlySpan<T> span ) where T : struct
        {
            return new LmdbValue(
                Unsafe.AsPointer( ref MemoryMarshal.GetReference( span ) ),
                span.Length * Unsafe.SizeOf<T>()
            );
        }

        /// <summary>   Creates <see cref="LmdbValue"/> from <see cref="Span{T}"/>. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <typeparam name="T">    Type of <c>Span</c> element. </typeparam>
        /// <param name="span"> <c>Span</c> instance to create the <c>DbValue</c> from. </param>
        /// <returns>   New <c>DbValue</c> instance. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static LmdbValue From<T>( Span<T> span ) where T : struct
        {
            return new LmdbValue(
                Unsafe.AsPointer( ref MemoryMarshal.GetReference( span ) ),
                span.Length * Unsafe.SizeOf<T>()
            );
        }

        /// <summary>
        /// Creates <see cref="LmdbValue"/> from <see cref="ReadOnlySpan{T}">ReadOnlySpan&lt;byte&gt;</see>.
        /// </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="span"> <c>Span</c> instance to create the <c>DbValue</c> from. </param>
        /// <returns>   New <c>DbValue</c> instance. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static LmdbValue From( ReadOnlySpan<byte> span )
        {
            return new LmdbValue( Unsafe.AsPointer( ref MemoryMarshal.GetReference( span ) ), span.Length );
        }

        /// <summary>
        /// Creates <see cref="LmdbValue"/> from <see cref="Span{T}">Span&lt;byte&gt;</see>.
        /// </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="span"> <c>Span</c> instance to create the <c>DbValue</c> from. </param>
        /// <returns>   New <c>DbValue</c> instance. </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static LmdbValue From( Span<byte> span )
        {
            return new LmdbValue( Unsafe.AsPointer( ref MemoryMarshal.GetReference( span ) ), span.Length );
        }

        /// <summary>   Gets a value indicating whether this object is valid. </summary>
        /// <value> True if this object is valid; otherwise, false. </value>
        public bool IsValid
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => (this.Data != null);
        }

        /// <summary>   Converts to <see cref="ReadOnlySpan{T}">ReadOnlySpan&lt;byte&gt;</see>. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <returns>   This object as a ReadOnlySpan&lt;byte&gt; </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public ReadOnlySpan<byte> ToReadOnlySpan()
        {
            return new ReadOnlySpan<byte>( this.Data, unchecked(( int ) this.Size) );
        }

        /// <summary>   Converts to <see cref="Span{T}">Span&lt;byte&gt;</see>. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <returns>   This object as a Span&lt;byte&gt; </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public Span<byte> ToSpan()
        {
            return new Span<byte>( this.Data, unchecked(( int ) this.Size) );
        }

        /// <summary>   Gets the span. </summary>
        /// <value> The span. </value>
        public Span<byte> Span
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => new( this.Data, ( int ) this.Size );
        }

        /// <summary>   Gets the length. </summary>
        /// <value> The length. </value>
        public int Length
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => checked(( int ) ( long ) this.Size);
        }

        /// <summary>   Gets the long length. </summary>
        /// <value> The length of the long. </value>
        public long LongLength
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => ( long ) this.Size;
        }

        /// <summary>   Gets the int pointer. </summary>
        /// <remarks>   for cases when unsafe is not allowed, e.g. async. </remarks>
        /// <value> The int pointer. </value>
        public IntPtr IntPtr
        {
            [DebuggerStepThrough]
            [MethodImpl( MethodImplOptions.AggressiveInlining )]
            get => ( IntPtr ) this.Data;
        }

        #region " READ / WRITE "

        /// <summary>
        /// Gets a value indicating whether the additional correctness checks is enabled.
        /// </summary>
        /// <value> True if additional correctness checks enabled; otherwise, false. </value>
        public static bool AdditionalCorrectnessChecksEnabled => false;

        /// <summary>   Asserts. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="length">   The length. </param>
        [DebuggerStepThrough]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2208:Instantiate argument exceptions correctly", Justification = "<Pending>" )]
        internal void Assert( long index, long length )
        {
            if ( LmdbValue.AdditionalCorrectnessChecksEnabled )
            {
                if ( !this.IsValid )
                {
                    throw new InvalidOperationException( $"{nameof( LmdbValue )} is invalid." );
                }

                unchecked
                {
                    if ( ( ulong ) index + ( ulong ) length > ( ulong ) this.Size )
                    {
                        throw new ArgumentNullException( $"Not enough space in LmdbValue or bad {nameof( index )}/{nameof( length )}." );
                    }
                }
            }
        }

        /// <summary>   Copies to described by destination. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <param name="destination">  [in,out] Destination for the. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CopyTo( ref Memory<byte> destination )
        {
            this.ToSpan().CopyTo( destination.Span );
        }

        /// <summary>   Copies to described by destination. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <param name="destination">  Destination for the. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CopyTo( in LmdbValue destination )
        {
            if ( AdditionalCorrectnessChecksEnabled )
            {
                destination.Assert( 0, ( long ) this.Size );
            }
            this.CopyTo( 0, destination.Data, ( int ) this.Size );
        }

        /// <summary>   Copies to described by destination. </summary>
        /// <remarks>   Remarked by David, 2020-12-19. </remarks>
        /// <param name="index">        Zero-based index of the. </param>
        /// <param name="destination">  [in,out] Destination for the. </param>
        /// <param name="length">       The length. </param>
        [CLSCompliant( false )]
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public void CopyTo( long index, void* destination, int length )
        {
            if ( AdditionalCorrectnessChecksEnabled )
            { this.Assert( index, length ); }
            Unsafe.CopyBlockUnaligned( destination, ( void* ) (( long ) this.Data + index), checked(( uint ) length) );
        }

        #endregion

        #region " EQUALITY "

        /// <summary>   Equality comparison. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="x">    Database value to be compared. </param>
        /// <param name="y">    Database value to be compared. </param>
        /// <returns>
        /// true if <paramref name="x">left hand side</paramref> equals the <paramref name="y">right hand
        /// side</paramref>; otherwise, false.
        /// </returns>
        public static bool Equals( LmdbValue x, LmdbValue y )
        {
            return x.Data == y.Data && x.Size == y.Size;
        }

        /// <summary>   Equality operator. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator ==( LmdbValue left, LmdbValue right )
        {
            return Equals( left, right );
        }

        /// <summary>   Inequality operator. </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="left">     The first instance to compare. </param>
        /// <param name="right">    The second instance to compare. </param>
        /// <returns>   The result of the operation. </returns>
        public static bool operator !=( LmdbValue left, LmdbValue right )
        {
            return !(left == right);
        }

        /// <summary>
        /// This method is not supported as DbValue cannot be boxed. To compare two DbValues, use
        /// operator==.
        /// </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <param name="obj">  The object to compare with the current instance. </param>
        /// <returns>
        /// true if <paramref name="obj">obj</paramref> and this instance are the same type and represent
        /// the same value; otherwise, false.
        /// </returns>
        ///
        /// <exception cref="System.NotSupportedException"> Always thrown by this method. </exception>
        [Obsolete( "Equals() on DbValue will always throw an exception. Use == instead." )]
        [EditorBrowsable( EditorBrowsableState.Never )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override bool Equals( object obj ) => throw new NotSupportedException( "Not supported. Cannot call Equals() on DbValue" );
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary>
        /// This method is not supported as DbValue cannot be boxed. To compare two DbValues, use
        /// operator==.
        /// </summary>
        /// <remarks> Remark added by David, 2020-12-10. </remarks>
        /// <returns>   A 32-bit signed integer that is the hash code for this instance. </returns>
        ///
        /// <exception cref="System.NotSupportedException"> Always thrown by this method. </exception>
        [Obsolete( "GetHashCode() on DbValue will always throw an exception." )]
        [EditorBrowsable( EditorBrowsableState.Never )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        public override int GetHashCode() => throw new NotSupportedException( "Not supported. Cannot call GetHashCode() on DbValue" );
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion " EQUALITY "

        #region " DEBUGGER PROXY CLASS "

        /// <summary>   The LMDB Value debug view. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        internal ref struct LmdbValueDebugView
        {
            /// <summary>   The LMDB Value. </summary>
            private readonly LmdbValue _Buffer;

            /// <summary>   Constructor. </summary>
            /// <remarks>   Remarked by David, 2020-12-20. </remarks>
            /// <param name="buffer">   The LMDB Value. </param>
            public LmdbValueDebugView( LmdbValue buffer )
            {
                this._Buffer = buffer;
            }

            /// <summary>   Gets the data. </summary>
            /// <value> The data. </value>
            public void* Data => this._Buffer.Data;

            /// <summary>   Gets the length. </summary>
            /// <value> The length. </value>
            public long Length => this._Buffer.Length;

            /// <summary>   Gets a value indicating whether this object is valid. </summary>
            /// <value> True if this object is valid; otherwise, false. </value>
            public bool IsValid => this._Buffer.IsValid;

            /// <summary>   Gets the span. </summary>
            /// <value> The span. </value>
            public Span<byte> Span => this._Buffer.IsValid ? this._Buffer.Span : default;
        }

        #endregion  " DEBUGGER PROXY CLASS "

    }

    /// <summary>   A LMDB value extensions. </summary>
    /// <remarks>   Remarked by David, 2020-12-19. </remarks>
    internal static unsafe class LmdbValueExtensions
    {

        /// <summary>   A  <see cref="System.ReadOnlySpan{Byte}"/> extension method that copies to <see cref="LmdbValue"/>. </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="source">       The source to act on. </param>
        /// <param name="destination">  Destination for the copy. </param>
        public static void CopyTo( this ReadOnlySpan<byte> source, LmdbValue destination )
        {
            fixed ( byte* ptr = source )
            {
                var sourceDb = new LmdbValue( ptr, source.Length );
                sourceDb.CopyTo( destination );
            }
        }

        /// <summary>
        /// A  <see cref="System.Span{Byte}"/> extension method that copies to <see cref="LmdbValue"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-20. </remarks>
        /// <param name="source">       The source to act on. </param>
        /// <param name="destination">  Destination for the copy. </param>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static void CopyTo( this Span<byte> source, LmdbValue destination )
        {
            fixed ( void* ptr = source )
            {
                var sourceDb = new LmdbValue( ptr, source.Length );
                sourceDb.CopyTo( destination );
            }
        }
    }

}



