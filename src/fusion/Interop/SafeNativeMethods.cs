using System;
using System.Runtime.InteropServices;

namespace isr.Lmdb.Fusion.Interop
{

    //TODO Annotate eligible methods with [SuppressGCTransition]
    /* see https://devblogs.microsoft.com/dotnet/improvements-in-native-code-interop-in-net-5-0/
     * Eligible methods must:
     *  - Always execute for a trivial amount of time (less than 1 microsecond)
     *  - Not perform a blocking syscall (e.g. any type of I/O)
     *  - Not call back into the runtime (e.g. Reverse P/Invoke)
     *  - Not throw exceptions
     *  - Not manipulate locks or other concurrency primitives
     */

    /// <summary>   Interface to the LMDB library. </summary>
    /// <remarks> Remark added by David, 2020-12-10. <para>
    /// (c) 2020 Karl Waclawek. All rights reserved. </para><para>
    /// (c) 2018 Victor Baybecov. All rights reserved. </para>
    /// </remarks>
    [System.Security.SuppressUnmanagedCodeSecurity]
    internal static unsafe class SafeNativeMethods
    {
#pragma warning disable IDE1006 // Naming Styles

        /// <summary>   Name of the library. </summary>
        /// <remarks>
        /// we expect DllImport to translate this to a platform-typical library name.
        /// </remarks>
        private const string _LibName = "libLmdbPlus"; // libLmdbPlus libspreads_lmdb

        #region " LIBRARY INFO "

        /// <summary>   Returns the LMDB library version information. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="major">    [out] The major. </param>
        /// <param name="minor">    [out] The minor. </param>
        /// <param name="patch">    [out] The patch. </param>
        /// <returns>   An IntPtr. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_version( out int major, out int minor, out int patch );

        /// <summary>   Returns the LMDB library version information. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="major">    [out] The major. </param>
        /// <param name="minor">    [out] The minor. </param>
        /// <param name="patch">    [out] The patch. </param>
        /// <returns>   An IntPtr. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_version( out IntPtr major, out IntPtr minor, out IntPtr patch );

        /// <summary>   Return a string describing a given error code. </summary>
        /// <remarks>
        /// This function is a superset of the ANSI C X3.159-1989 (ANSI C) strerror(3)
        /// function. If the error code is greater than or equal to 0, then the string returned by the
        /// system function strerror(3) is returned. If the error code is less than 0, an error string
        /// corresponding to the LMDB library error is returned. See <see cref="NativeResultCode"/> for a
        /// list of LMDB-specific error codes.
        /// </remarks>
        /// <param name="err">  The error code. </param>
        /// <returns>   "error message" The description of the error. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_strerror( int err );

        #endregion " LIBRARY INFO "

        #region " LMDB ENVIRONMENT METHODS "

        /// <summary>   Create an LMDB environment handle. </summary>
        /// <remarks>
        /// This function allocates memory for a #MDB_env structure. To release the allocated memory and
        /// discard the handle, call #mdb_env_close(). Before the handle may be used, it must be opened
        /// using #mdb_env_open(). Various other options may also need to be set before opening the
        /// handle, e.g. #mdb_env_set_mapsize(), #mdb_env_set_maxreaders(), #mdb_env_set_maxdbs(),
        /// depending on usage requirements.
        /// </remarks>
        /// <param name="env">  [out] The environment: The address where the new handle will be stored. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_create( out IntPtr env );

        /// <summary>   Open an environment handle. </summary>
        /// <remarks>
        /// If this function fails, #mdb_env_close() must be called to discard the #MDB_env handle.
        /// <list type="bullet"> <item>
        /// #MDB_FIXEDMAP
        ///     use a fixed address for the mmap region. This flag must be specified when creating the
        ///     environment, and is stored persistently in the environment.
        ///       If successful, the memory map will always reside at the same virtual address and
        ///       pointers used to reference data items in the database will be constant across multiple
        ///       invocations. This option may not always work, depending on how the operating system has
        ///       allocated memory to shared libraries and other uses. The feature is highly
        ///       experimental. </item><item>
        /// #MDB_NOSUBDIR  
        ///        By default, LMDB creates its environment in a directory whose pathname is given in
        ///        <b>path</b>, and creates its data and lock files under that directory. With this
        ///        option,
        ///        <b>path</b> is used as-is for the database main data file. The database lock file is
        ///        the
        ///        <b>path</b>
        ///        with "-lock" appended. </item><item>
        /// #MDB_RDONLY  
        ///        Open the environment in read-only mode. No write operations will be allowed. LMDB will
        ///        still modify the lock file - except on read-only file systems, where LMDB does not use
        ///        locks. </item><item>
        /// #MDB_WRITEMAP  
        ///        Use a writable memory map unless MDB_RDONLY is set. This uses fewer mallocs but loses
        ///        protection from application bugs like wild pointer writes and other bad updates into
        ///        the database. This may be slightly faster for DBs that fit entirely in RAM, but is
        ///        slower for DBs larger than RAM. Incompatible with nested transactions. Do not mix
        ///        processes with and without MDB_WRITEMAP on the same environment.  This can defeat
        ///        durability (#mdb_env_sync etc). </item><item>
        /// #MDB_NOMETASYNC  
        ///        Flush system buffers to disk only once per transaction, omit the metadata flush. Defer
        ///        that until the system flushes files to disk, or next non-MDB_RDONLY commit or
        ///        <see cref="SafeNativeMethods.mdb_env_sync(System.IntPtr, bool)"/>. This optimization maintains
        ///        database integrity, but a system crash may undo the last committed transaction. I.e.
        ///        it preserves the ACI (atomicity, consistency, isolation) but not D (durability)
        ///        database property. This flag may be changed at any time using
        ///        #mdb_env_set_flags(). </item><item>
        /// #MDB_NOSYNC
        ///        Don't flush system buffers to disk when committing a transaction. This optimization
        ///        means a system crash can corrupt the database or lose the last transactions if buffers
        ///        are not yet flushed to disk. The risk is governed by how often the system flushes
        ///        dirty buffers to disk and how often
        ///        <see cref="SafeNativeMethods.mdb_env_sync(System.IntPtr, bool)"/>
        ///        is called.  However, if the  file system preserves write order and the #MDB_WRITEMAP
        ///        flag is not used, transactions exhibit ACI (atomicity, consistency, isolation)
        ///        properties and only lose D (durability), i.e. database integrity is maintained, but a
        ///        system crash may undo the final transactions. Note that (#MDB_NOSYNC | #MDB_WRITEMAP)
        ///        leaves the system with no hint for when to write transactions to disk, unless
        ///        <see cref="SafeNativeMethods.mdb_env_sync(System.IntPtr, bool)"/> is called. (#MDB_MAPASYNC |
        ///        #MDB_WRITEMAP) may be preferable. This flag may be changed
        ///        at any time using #mdb_env_set_flags(). </item><item>
        /// #MDB_MAPASYNC
        ///        When using #MDB_WRITEMAP, use asynchronous flushes to disk. As with #MDB_NOSYNC, a
        ///        system crash can then corrupt the database or lose the last transactions. Calling
        ///        <see cref="SafeNativeMethods.mdb_env_sync(System.IntPtr, bool)"/> ensures on-disk database
        ///        integrity until next commit. This flag may be changed at any
        ///        time using #mdb_env_set_flags(). </item><item>
        /// #MDB_NOTLS
        ///        Don't use Thread-Local Storage. Tie reader locktable slots to
        ///        #MDB_txn objects instead of to threads. I.e. #mdb_txn_reset() keeps
        ///        the slot reseved for the #MDB_txn object. A thread may use parallel read-only
        ///        transactions. A read-only transaction may span threads if the user synchronizes its
        ///        use. Applications that multiplex many user threads over individual OS threads need
        ///        this option. Such an application must also serialize the write transactions in an OS
        ///        thread, since LMDB's write locking is unaware of the user threads. </item><item>
        /// #MDB_NOLOCK
        ///        Don't do any locking. If concurrent access is anticipated, the caller must manage all
        ///        concurrency itself. For proper operation the caller must enforce single-writer
        ///        semantics, and must ensure that no readers are using old transactions while a writer
        ///        is active. The simplest approach is to use an exclusive lock so that no readers may be
        ///        active at all when a writer begins. </item><item>
        /// #MDB_NORDAHEAD
        ///        Turn off read-ahead. Most operating systems perform read-ahead on read requests by
        ///        default. This option turns it off if the OS supports it. Turning it off may help
        ///        random read performance when the DB is larger than RAM and system RAM is full. The
        ///        option is not implemented on Windows. </item><item>
        /// #MDB_NOMEMINIT
        ///        Don't initialize malloc'd memory before writing to unused spaces in the data file. By
        ///        default, memory for pages written to the data file is obtained using malloc. While
        ///        these pages may be reused in subsequent transactions, freshly malloc'd pages will be
        ///        initialized to zeros before use. This avoids persisting leftover data from other code
        ///        (that used the heap and subsequently freed the memory) into the data file. Note that
        ///        many other system libraries may allocate and free memory from the heap for arbitrary
        ///        uses. E.g., stdio may use the heap for file I/O buffers. This initialization step has
        ///        a modest performance cost so some applications may want to disable it using this flag.
        ///        This option can be a problem for applications which handle sensitive data like
        ///        passwords, and it makes memory checkers like Valgrind noisy. This flag is not needed
        ///        with #MDB_WRITEMAP, which writes directly to the mmap instead of using malloc for
        ///        pages. The initialization is also skipped if
        ///        #MDB_RESERVE is used; the caller is expected to overwrite all of the memory that was
        ///        reserved in that case. This flag may be changed at any time using
        ///        #mdb_env_set_flags(). </item><item>
        /// #MDB_PREVSNAPSHOT
        ///        Open the environment with the previous snapshot rather than the latest one. This loses
        ///        the latest transaction, but may help work around some types of corruption. If opened
        ///        with write access, this must be the only process using the environment. This flag is
        ///        automatically reset after a write transaction is successfully committed. </item>
        ///        </list>
        /// Some possible errors are:
        /// <list type="bullet"> <item>
        ///  #MDB_VERSION_MISMATCH - the version of the LMDB library doesn't match the
        ///    version that created the database environment. </item><item>
        ///  #MDB_INVALID - the environment file headers are corrupted. </item><item>
        ///  ENOENT - the directory specified by the path  parameter doesn't exist. </item><item>
        ///  EACCES - the user didn't have permission to access the environment files. </item><item>
        ///  EAGAIN - the environment was locked by another process. </item> </list>
        /// </remarks>
        /// <param name="env">      [out] The environment: The address where the new handle will be
        ///                         stored. returned by #mdb_env_create. </param>
        /// <param name="path">     The directory in which the database files reside. This directory must
        ///                         already exist and be writable. </param>
        /// <param name="flags">    Special options for this environment. This parameter must be set to 0
        ///                         or by bitwise OR'ing together one or more of the values described here.
        ///                         Flags set by mdb_env_set_flags() are also used. </param>
        /// <param name="mode">     The UNIX permissions to set on created files and semaphores. This
        ///                         parameter is ignored on Windows. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "Already done" )]
        public static extern int mdb_env_open( IntPtr env, string path, EnvironmentOpenOptions flags, UnixAccessModes mode );

        /// <summary>   Open an environment handle.. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="env">      The environment. </param>
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="flags">    The flags. </param>
        /// <param name="mode">     The mode. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_open( IntPtr env, IntPtr path, EnvironmentOpenOptions flags, UnixAccessModes mode );

        /// <summary>   Copy an LMDB environment to the specified path. </summary>
        /// <remarks>
        /// This function may be used to make a backup of an existing environment. No lock file is
        /// created, since it gets recreated at need. Note: This call can trigger significant file size
        /// growth if run in parallel with write transactions, because it employs a read-only
        /// transaction. See long-lived transactions under @ref caveats_sec.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). It  must have
        ///                     already been opened successfully. </param>
        /// <param name="path"> The directory in which the copy will reside. This directory must already
        ///                     exist and be writable but must otherwise be empty. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "Already done" )]
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi )]
        public static extern int mdb_env_copy( IntPtr env, string path );

        /// <summary>   Copy an LMDB environment to the specified path. </summary>
        /// <remarks>
        /// This function may be used to make a backup of an existing environment. No lock file is
        /// created, since it gets recreated at need. Note: This call can trigger significant file size
        /// growth if run in parallel with write transactions, because it employs a read-only
        /// transaction. See long-lived transactions under @ref caveats_sec.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). It  must have
        ///                     already been opened successfully. </param>
        /// <param name="path"> Address of the full pathname of the file. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_copy( IntPtr env, IntPtr path );

        /// <summary>   Copy an LMDB environment to the specified file descriptor. </summary>
        /// <remarks>
        ///  This function may be used to make a backup of an existing environment. No lock file is
        ///  created, since it gets recreated at need.
        /// Note: This call can trigger significant file size growth if run in
        ///  parallel with write transactions, because it employs a read-only transaction. See long-lived
        ///  transactions under @ref caveats_sec.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). It must have already
        ///                     been opened successfully. </param>
        /// <param name="fd">   The file descriptor to write the copy to. It must have already been
        ///                     opened for Write access. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_copyfd( IntPtr env, IntPtr fd );

        /// <summary>   Copy an LMDB environment to the specified path, with options. </summary>
        /// <remarks>
        ///  This function may be used to make a backup of an existing environment. No lock file is
        ///  created, since it gets recreated at need.
        /// Note: This call can trigger significant file size growth if run in
        ///  parallel with write transactions, because it employs a read-only transaction. See long-lived
        ///  transactions under @ref caveats_sec.
        /// </remarks>
        /// <param name="env">          An environment handle returned by #mdb_env_create(). It must have
        ///                             already been opened successfully. </param>
        /// <param name="path">         The directory in which the copy will reside. This directory
        ///                             must already exist and be writable but must otherwise be empty. </param>
        /// <param name="copyFlags">    Special options for this operation. This parameter must be set to
        ///                             0 or by bitwise OR'ing together one or more of the values described here.
        ///                             <list type="bullet"> <item>
        ///                             #MDB_CP_COMPACT - Perform compaction while copying:
        ///                                omit free pages and sequentially renumber all pages in output. This
        ///                                option consumes more CPU and runs more slowly than the default. Currently it
        ///                                fails if the environment has suffered a page leak.
        ///                             </item> </list>. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "Already done" )]
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi )]
        public static extern int mdb_env_copy2( IntPtr env, string path, EnvironmentCopyOptions copyFlags );

        /// <summary>   Copy an LMDB environment to the specified path, with options. </summary>
        /// <remarks>
        ///  This function may be used to make a backup of an existing environment. No lock file is
        ///  created, since it gets recreated at need.
        /// Note: This call can trigger significant file size growth if run in
        ///  parallel with write transactions, because it employs a read-only transaction. See long-lived
        ///  transactions under @ref caveats_sec.
        /// </remarks>
        /// <param name="env">          An environment handle returned by #mdb_env_create(). It must have
        ///                             already been opened successfully. </param>
        /// <param name="path">         Points to the directory in which the copy will reside. This directory
        ///                             must already exist and be writable but must otherwise be empty. </param>
        /// <param name="copyFlags">    Special options for this operation. This parameter must be set to
        ///                             0 or by bitwise OR'ing together one or more of the values described
        ///                             here.
        ///                             <list type="bullet"> <item>
        ///                             #MDB_CP_COMPACT - Perform compaction while copying:
        ///                                omit free pages and sequentially renumber all pages in output.
        ///                                This option consumes more CPU and runs more slowly than the
        ///                                default. Currently it fails if the environment has suffered a page
        ///                                leak.
        ///                             </item> </list>. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_copy2( IntPtr env, IntPtr path, EnvironmentCopyOptions copyFlags );

        /// <summary>   Copy an LMDB environment to the specified file descriptor, with options. </summary>
        /// <remarks>
        ///  This function may be used to make a backup of an existing environment. No lock file is
        ///  created, since it gets recreated at need. See
        ///  #mdb_env_copy2() for further details.
        /// Note: This call can trigger significant file size growth if run in
        ///  parallel with write transactions, because it employs a read-only transaction. See long-lived
        ///  transactions under @ref caveats_sec.
        /// </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create(). It must have
        ///                         already been opened successfully. </param>
        /// <param name="fd">       file descriptor to write the copy to. It must have already been
        ///                         opened for Write access. </param>
        /// <param name="flags">    Special options for this operation. See #mdb_env_copy2() for options. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_copyfd2( IntPtr env, IntPtr fd, EnvironmentCopyOptions flags );

        /// <summary>   Return statistics about the LMDB environment. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="stat"> [out] The address of an #MDB_stat structure where the statistics will be
        ///                     copied. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_stat( IntPtr env, out LmdbStat stat );

        /// <summary>   Return information about the LMDB environment. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">  [An environment handle returned by #mdb_env_create(). </param>
        /// <param name="info"> [out] The address of an #MDB_envinfo structure where the information will
        ///                     be copied. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_info( IntPtr env, out LmdbEnvInfo info );

        /// <summary>   Flush the data buffers to disk. </summary>
        /// <remarks>
        /// Data is always written to disk when #mdb_txn_commit() is called, but the operating system may
        /// keep it buffered. LMDB always flushes the OS buffers upon commit as well, unless the
        /// environment was opened with #MDB_NOSYNC or in part #MDB_NOMETASYNC. This call is not valid if
        /// the environment was opened with #MDB_RDONLY.
        /// </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create() </param>
        /// <param name="force">    If non-zero, force a synchronous flush.  Otherwise if the environment
        ///                         has the #MDB_NOSYNC flag set the flushes will be omitted, and with
        ///                         #MDB_MAPASYNC they will be asynchronous. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// </item><item>
        ///    EACCES - the environment is read-only. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item><item>EIO - an error occurred during synchronization. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_sync( IntPtr env, bool force );

        /// <summary>   Close the environment and release the memory map. </summary>
        /// <remarks>
        /// Only a single thread may call this function. All transactions, databases, and cursors must
        /// already be closed before calling this function. Attempts to use any such handles after
        /// calling this function will cause a SIGSEGV (invalid memory access (segmentation fault)). The
        /// environment handle will be freed and must not be used again after this call.
        /// </remarks>
        /// <param name="env">  [out] The environment: The address where the new handle will be stored. </param>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern void mdb_env_close( IntPtr env );

        /// <summary>   Set environment flags. </summary>
        /// <remarks>
        /// This may be used to set some flags in addition to those from
        /// #mdb_env_open(), or to unset these flags.  If several threads
        /// change the flags at the same time, the result is undefined.
        /// </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create() </param>
        /// <param name="flags">    The flags to change, bitwise OR'ed together. </param>
        /// <param name="onoff">    A non-zero value sets the flags, zero clears them. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_set_flags( IntPtr env, EnvironmentOpenOptions flags, bool onoff );

        /// <summary>   Get environment flags. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create(). </param>
        /// <param name="flags">    [out] The address of an integer to store the flags. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_get_flags( IntPtr env, out EnvironmentOpenOptions flags );

        /// <summary>   Return the path that was used in #mdb_env_open(). </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="path"> [out] Address of a string pointer to contain the path. This is the actual
        ///                     string in the environment, not a copy. It should not be altered in any
        ///                     way. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi )]
        public static extern int mdb_env_get_path( IntPtr env, out IntPtr path );

        // crashes: public static extern int mdb_env_get_path( IntPtr env, [MarshalAs( UnmanagedType.LPStr )] out string path );

        /// <summary>   Return the file descriptor for the given environment. </summary>
        /// <remarks>
        /// This function may be called after fork(), so the descriptor can be closed before exec*().
        /// Other LMDB file descriptors have FD_CLOEXEC. (Until LMDB 0.9.18, only the lockfile had that.)
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="fd">   [out] Address of a mdb_filehandle_t to contain the descriptor. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_get_fd( IntPtr env, out IntPtr fd );

        /// <summary>   Set the size of the memory map to use for this environment. </summary>
        /// <remarks>
        /// The size should be a multiple of the OS page size. The default is
        /// 10485760 bytes. The size of the memory map is also the maximum size
        /// of the database. The value should be chosen as large as possible, to accommodate future
        /// growth of the database. This function should be called after #mdb_env_create() and before
        /// #mdb_env_open(). It may be called at later times if no transactions are active in this
        /// process. Note that the library does not check for this condition, the caller must ensure it
        /// explicitly.
        /// 
        /// The new size takes effect immediately for the current process but will not be persisted to
        /// any others until a write transaction has been committed by the current process. Also, only
        /// mapsize increases are persisted into the environment.
        /// 
        /// If the mapsize is increased by another process, and data has grown beyond the range of the
        /// current mapsize, #mdb_txn_begin() will return #MDB_MAP_RESIZED. This function may be called
        /// with a size of zero to adopt the new size.
        /// 
        /// Any attempt to set a size smaller than the space already consumed by the environment will be
        /// silently changed to the current size of the used space.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="size"> The size in bytes. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified, or the environment has an active  write
        ///    transaction.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_set_mapsize( IntPtr env, IntPtr size );

        /// <summary>   Set the maximum number of threads/reader slots for the environment. </summary>
        /// <remarks>
        /// This defines the number of slots in the lock table that is used to track readers in the the
        /// environment. The default is 126. Starting a read-only transaction normally ties a lock table
        /// slot to the current thread until the environment closes or the thread exits. If MDB_NOTLS is
        /// in use, #mdb_txn_begin() instead ties the slot to the MDB_txn object until it or the
        /// #MDB_env object is destroyed. This function may only be called after #mdb_env_create() and
        /// before #mdb_env_open().
        /// </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create(). </param>
        /// <param name="readers">  The maximum number of reader lock table slots. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified, or the environment is already open.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_set_maxreaders( IntPtr env, uint readers );

        /// <summary>   Get the maximum number of threads/reader slots for the environment. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create(). </param>
        /// <param name="readers">  [out] Address of an integer to store the number of readers. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_get_maxreaders( IntPtr env, out uint readers );

        /// <summary>   Set the maximum number of named databases for the environment. </summary>
        /// <remarks>
        /// This function is only needed if multiple databases will be used in the environment. Simpler
        /// applications that use the environment as a single unnamed database can ignore this option.
        /// This function may only be called after #mdb_env_create() and before #mdb_env_open().
        /// 
        /// Currently a moderate number of slots are cheap but a huge number gets expensive: 7-120 words
        /// per transaction, and every #mdb_dbi_open()
        /// does a linear search of the opened slots.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create() </param>
        /// <param name="dbs">  The maximum number of databases. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified, or the environment is already open.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_set_maxdbs( IntPtr env, uint dbs );

        /// <summary>   Get the maximum size of keys and #MDB_DUPSORT data we can write. </summary>
        /// <remarks>
        /// Depends on the compile-time constant #MDB_MAXKEYSIZE. Default 511. See @ref MDB_val.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <returns>   The maximum size of a key we can write. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_get_maxkeysize( IntPtr env );

        /// <summary>   Set application information associated with the #MDB_env. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="ctx">  An arbitrary pointer for whatever the application needs. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_set_userctx( IntPtr env, IntPtr ctx );

        /// <summary>   Get the application information associated with the #MDB_env. </summary>
        /// <remarks>   Remark added by David, 2020-12-10. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <returns>   The pointer set by #mdb_env_set_userctx(). </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_env_get_userctx( IntPtr env );

        /// <summary>   LMDB Library assert function. </summary>
        /// <remarks>
        /// A callback function for most LMDB assert() failures, called before printing the message and
        /// aborting: MSB_assert_func.
        /// </remarks>
        /// <param name="env">  The environment. </param>
        /// <param name="msg">  The message. </param>
        [UnmanagedFunctionPointer( CallingConvention.Cdecl ), System.Security.SuppressUnmanagedCodeSecurity]
        public delegate void LmdbAssertFunction( IntPtr env, [MarshalAs( UnmanagedType.LPStr ), In] string msg );

        /// <summary>
        /// Set or reset the assert() callback of the environment. Disabled if liblmdb is buillt with
        /// NDEBUG.
        /// 
        /// Note: This hack should become obsolete as lmdb's error handling matures.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="func"> An #MDB_assert_func function, or 0. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_env_set_assert( IntPtr env, LmdbAssertFunction func );

        /// <summary>   A callback function used to print a message from the library. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="msg">  The string to be printed. </param>
        /// <param name="ctx">  An arbitrary context pointer for the callback. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [UnmanagedFunctionPointer( CallingConvention.Cdecl ), System.Security.SuppressUnmanagedCodeSecurity]
        public delegate int LmdbMessageFunction( string msg, IntPtr ctx );

        /// <summary>   Dump the entries in the reader lock table. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="func"> A <see cref="LmdbMessageFunction">#MDB_msg_func function</see>. </param>
        /// <param name="ctx">  Anything the message function needs. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_reader_list( IntPtr env, LmdbMessageFunction func, IntPtr ctx );

        /// <summary>   Check for stale entries in the reader lock table. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="dead"> [out] Number of stale slots that were cleared. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_reader_check( IntPtr env, out int dead );

        #endregion " LMDB ENVIRONMENT METHODS "

        #region " LMDB TRANSACTION METHODS "

        /// <summary>   Create a transaction for use with the environment. </summary>
        /// <remarks>
        /// The transaction handle may be discarded using #mdb_txn_abort() or #mdb_txn_commit().
        /// 
        /// Note: A transaction and its cursors must only be used by a single thread, and a thread may
        /// only have a single transaction at a time. If #MDB_NOTLS is in use, this does not apply to
        /// read-only transactions.
        /// 
        /// Note: Cursors may not span transactions.
        /// </remarks>
        /// <param name="env">      An environment handle returned by #mdb_env_create(). </param>
        /// <param name="parent">   If this parameter is non-NULL, the new transaction will be a nested
        ///                         transaction, with the transaction indicated by <b>parent</b>
        ///                         as its parent. Transactions may be nested to any level. A parent
        ///                         transaction and its cursors may not issue any other operations than
        ///                         mdb_txn_commit and mdb_txn_abort while it has active child
        ///                         transactions. </param>
        /// <param name="flags">    Special options for this transaction. This parameter must be set to 0
        ///                         or by bitwise OR'ing together one or more of the values described
        ///                         here.
        ///                         <list type="bullet"> <item>
        ///                            #MDB_RDONLY
        ///                                This transaction will not perform any write operations.
        ///                                </item><item>
        ///                            #MDB_NOSYNC
        ///                                Don't flush system buffers to disk when committing this
        ///                                transaction. </item><item>
        ///                            #MDB_NOMETASYNC
        ///                                Flush system buffers but omit metadata flush when committing
        ///                                this transaction. </item> </list> </param>
        /// <param name="txn">      [out] Address where the new #MDB_txn handle will be stored. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_PANIC - a fatal error occurred earlier and the environment
        ///        must be shut down. </item><item>
        ///    #MDB_MAP_RESIZED - another process wrote data beyond this MDB_env's
        ///        map size and this environment's map must be resized as well. See
        ///        #mdb_env_set_mapsize(). </item><item>
        ///    #MDB_READERS_FULL - a read-only transaction was requested and
        ///        the reader lock table is full. See #mdb_env_set_maxreaders(). </item><item>
        ///    ENOMEM - out of memory.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_txn_begin( IntPtr env, IntPtr parent, TransactionBeginOptions flags, out IntPtr txn );

        /// <summary>   Returns the transaction's #MDB_env. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <returns>   An IntPtr. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_txn_env( IntPtr txn );

        /// <summary>   Return the transaction's ID. </summary>
        /// <remarks>
        /// This returns the identifier associated with this transaction. For a read-only transaction,
        /// this corresponds to the snapshot being read;
        /// concurrent readers will frequently have the same transaction ID.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <returns>   A transaction ID, valid if input is an active transaction. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_txn_id( IntPtr txn );

        /// <summary>   Commit all the operations of a transaction into the database. </summary>
        /// <remarks>
        /// The transaction handle is freed. It and its cursors must not be used again after this call,
        /// except with #mdb_cursor_renew().
        /// 
        /// Note: Earlier documentation incorrectly said all cursors would be freed.
        /// 
        /// Only write-transactions free cursors.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by  #mdb_txn_begin() . </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// EINVAL - an invalid parameter was specified. </item><item>  
        /// ENOSPC - no more disk space </item><item>
        /// EIO - a low-level I/O error occurred while writing. </item><item>  
        /// ENOMEM - out of memory. </item></list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_txn_commit( IntPtr txn );

        /// <summary>   Abandon all the operations of the transaction instead of saving them. </summary>
        /// <remarks>
        /// The transaction handle is freed. It and its cursors must not be used again after this call,
        /// except with #mdb_cursor_renew().
        /// 
        /// Note: Earlier documentation incorrectly said all cursors would be freed. Only write-
        /// transactions free cursors.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern void mdb_txn_abort( IntPtr txn );

        /// <summary>   Reset a read-only transaction. </summary>
        /// <remarks>
        /// Abort the transaction like #mdb_txn_abort(), but keep the transaction handle.
        /// #mdb_txn_renew() may reuse the handle. This saves allocation overhead if the process will
        /// start a new read-only transaction soon, and also locking overhead if #MDB_NOTLS is in use.
        /// The reader table lock is released, but the table slot stays tied to its thread or
        /// #MDB_txn. Use mdb_txn_abort() to discard a reset handle, and to free
        /// its lock table slot if MDB_NOTLS is in use. Cursors opened within the transaction must not be
        /// used again after this call, except with #mdb_cursor_renew(). Reader locks generally don't
        /// interfere with writers, but they keep old versions of database pages allocated. Thus they
        /// prevent the old pages from being reused when writers commit new data, and so under heavy load
        /// the database size may grow much more rapidly than otherwise.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern void mdb_txn_reset( IntPtr txn );

        /// <summary>   Renew a read-only transaction. </summary>
        /// <remarks>
        /// This acquires a new reader lock for a transaction handle that had been released by
        /// #mdb_txn_reset(). It must be called before a reset transaction may be used again.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// #MDB_PANIC - a fatal error occurred earlier and the environment must be shut down.
        /// </item><item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_txn_renew( IntPtr txn );

        #endregion " LMDB TRANSACTION METHODS "

        #region " LMDB DATABASE METHODS "

        /// <summary>   Open a database in the environment. </summary>
        /// <remarks>
        /// A database handle denotes the name and parameters of a database, independently of whether
        /// such a database exists. The database handle may be discarded by calling #mdb_dbi_close(). The
        /// old database handle is returned if the database was already open. The handle may only be
        /// closed once.
        /// 
        /// The database handle will be private to the current transaction until the transaction is
        /// successfully committed. If the transaction is aborted the handle will be closed
        /// automatically. After a successful commit the handle will reside in the shared environment,
        /// and may be used by other transactions.
        /// 
        /// This function must not be called from multiple concurrent transactions in the same process. A
        /// transaction that uses this function must finish (either commit or abort) before any other
        /// transaction in the process may use this function.
        /// 
        /// To use named databases (with name != NULL), #mdb_env_set_maxdbs()
        /// must be called before opening the environment.  Database names are keys in the unnamed
        /// database, and may be read but not written.
        /// 
        /// The flag parameter must be set to 0 or by bitwise ORing together one or more of the values
        /// described here.
        /// <list type="bullet"> <item>
        ///    #MDB_REVERSEKEY
        ///        Keys are strings to be compared in reverse order, from the end of the strings to the
        ///        beginning. By default, Keys are treated as strings and compared from beginning to end.
        ///        </item><item>
        ///    #MDB_DUPSORT
        ///        Duplicate keys may be used in the database. (Or, from another perspective, keys may
        ///        have multiple data items, stored in sorted order.) By default keys must be unique and
        ///        may have only a single data item. </item><item>
        ///    #MDB_INTEGERKEY
        ///        Keys are binary integers in native byte order, either unsigned int or #mdb_size_t, and
        ///        will be sorted as such. (lmdb expects 32-bit int &lt;= size_t &lt;= 32/64-bit
        ///        mdb_size_t.)
        ///        The keys must all be of the same size. </item><item>
        ///    #MDB_DUPFIXED
        ///        This flag may only be used in combination with #MDB_DUPSORT. This option tells the
        ///        library that the data items for this database are all the same size, which allows
        ///        further optimizations in storage and retrieval. When all data items are the same size,
        ///        the #MDB_GET_MULTIPLE, #MDB_NEXT_MULTIPLE and #MDB_PREV_MULTIPLE cursor operations may
        ///        be used to retrieve multiple items at once. </item><item>
        ///    #MDB_INTEGERDUP
        ///        This option specifies that duplicate data items are binary integers, similar to
        ///        #MDB_INTEGERKEY keys. </item><item>
        ///    #MDB_REVERSEDUP
        ///        This option specifies that duplicate data items should be compared as strings in
        ///        reverse order. </item><item>
        ///    #MDB_CREATE
        ///        Create the named database if it doesn't exist. This option is not allowed in a read-
        ///        only transaction or a read-only environment. </item> </list>
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="name">     The name of the database to open. If only a single database is needed
        ///                         in the environment, this value may be NULL. </param>
        /// <param name="flags">    Special options for opening this database. </param>
        /// <param name="db">       [out] Address where the new #MDB_dbi handle will be stored. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_NOTFOUND - the specified database doesn't exist in the environment and #MDB_CREATE
        ///    was not specified. </item><item>
        ///    #MDB_DBS_FULL - too many databases have been opened. See #mdb_env_set_maxdbs().
        /// </item> </list>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "Already done" )]
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi )]
        public static extern int mdb_dbi_open( IntPtr txn, string name, uint flags, out uint db );


        /// <summary>   Open a database in the environment. </summary>
        /// <remarks>
        /// A database handle denotes the name and parameters of a database, independently of whether
        /// such a database exists. The database handle may be discarded by calling #mdb_dbi_close(). The
        /// old database handle is returned if the database was already open. The handle may only be
        /// closed once.
        /// 
        /// The database handle will be private to the current transaction until the transaction is
        /// successfully committed. If the transaction is aborted the handle will be closed
        /// automatically. After a successful commit the handle will reside in the shared environment,
        /// and may be used by other transactions.
        /// 
        /// This function must not be called from multiple concurrent transactions in the same process. A
        /// transaction that uses this function must finish (either commit or abort) before any other
        /// transaction in the process may use this function.
        /// 
        /// To use named databases (with name != NULL), #mdb_env_set_maxdbs()
        /// must be called before opening the environment.  Database names are keys in the unnamed
        /// database, and may be read but not written.
        /// 
        /// The flag parameter must be set to 0 or by bitwise ORing together one or more of the values
        /// described here.
        /// <list type="bullet"> <item>
        ///    #MDB_REVERSEKEY
        ///        Keys are strings to be compared in reverse order, from the end of the strings to the
        ///        beginning. By default, Keys are treated as strings and compared from beginning to end.
        ///        </item><item>
        ///    #MDB_DUPSORT
        ///        Duplicate keys may be used in the database. (Or, from another perspective, keys may
        ///        have multiple data items, stored in sorted order.) By default keys must be unique and
        ///        may have only a single data item. </item><item>
        ///    #MDB_INTEGERKEY
        ///        Keys are binary integers in native byte order, either unsigned int or #mdb_size_t, and
        ///        will be sorted as such. (lmdb expects 32-bit int &lt;= size_t &lt;= 32/64-bit
        ///        mdb_size_t.)
        ///        The keys must all be of the same size. </item><item>
        ///    #MDB_DUPFIXED
        ///        This flag may only be used in combination with #MDB_DUPSORT. This option tells the
        ///        library that the data items for this database are all the same size, which allows
        ///        further optimizations in storage and retrieval. When all data items are the same size,
        ///        the #MDB_GET_MULTIPLE, #MDB_NEXT_MULTIPLE and #MDB_PREV_MULTIPLE cursor operations may
        ///        be used to retrieve multiple items at once. </item><item>
        ///    #MDB_INTEGERDUP
        ///        This option specifies that duplicate data items are binary integers, similar to
        ///        #MDB_INTEGERKEY keys. </item><item>
        ///    #MDB_REVERSEDUP
        ///        This option specifies that duplicate data items should be compared as strings in
        ///        reverse order. </item><item>
        ///    #MDB_CREATE
        ///        Create the named database if it doesn't exist. This option is not allowed in a read-
        ///        only transaction or a read-only environment. </item> </list>
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="name">     The name of the database to open. If only a single database is needed
        ///                         in the environment, this value may be NULL. </param>
        /// <param name="flags">    Special options for opening the database. </param>
        /// <param name="db">       [out] Address where the new #MDB_dbi handle will be stored. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_NOTFOUND - the specified database doesn't exist in the environment and #MDB_CREATE
        ///    was not specified. </item><item>
        ///    #MDB_DBS_FULL - too many databases have been opened. See #mdb_env_set_maxdbs().
        /// </item> </list>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Globalization", "CA2101:Specify marshaling for P/Invoke string arguments", Justification = "Already done" )]
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi )]
        public static extern int mdb_dbi_open( IntPtr txn, string name, DatabaseOpenOptions flags, out uint db );

        /// <summary>   Retrieve statistics for a database. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="stat"> [out] The address of an #MDB_stat structure where the statistics will be
        ///                     copied. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_stat( IntPtr txn, uint dbi, out LmdbStat stat );

        /// <summary>   Retrieve the DB flags for a database handle. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="flags">    [out] Address where the flags will be returned. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_dbi_flags( IntPtr txn, uint dbi, out uint flags );

        /// <summary>   Close a database handle. Normally unnecessary. Use with care: </summary>
        /// <remarks>
        /// This call is not mutex protected. Handles should only be closed by a single thread, and only
        /// if no other threads are going to reference the database handle or one of its cursors any
        /// further. Do not close a handle if an existing transaction has modified its database. Doing so
        /// can cause misbehavior from database corruption to errors like MDB_BAD_VALSIZE (since the DB
        /// name is gone).
        /// 
        /// Closing a database handle is not necessary, but lets #mdb_dbi_open()
        /// reuse the handle value.  Usually it's better to set a bigger
        /// #mdb_env_set_maxdbs(), unless that value would be large.
        /// </remarks>
        /// <param name="env">  An environment handle returned by #mdb_env_create(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern void mdb_dbi_close( IntPtr env, uint dbi );

        /// <summary>   Empty or delete and close a database. </summary>
        /// <remarks>   See #mdb_dbi_close() for restrictions about closing the DB handle. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="del">  0 to empty the DB, 1 to delete it from the environment and close the DB
        ///                     handle. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_drop( IntPtr txn, uint dbi, bool del );

        #endregion " LMDB DATABASE METHODS "

        #region " LMDB DATABASE METHODS: LIGHTNING/SPREAD "

        /// <summary>   Get items from a database. </summary>
        /// <remarks>
        ///  This function retrieves key/data pairs from the database. The address and length of the data
        ///  associated with the specified <b>key</b> are returned in the structure to which <b>data</b>
        ///  refers. If the database supports duplicate keys (#MDB_DUPSORT) then the first data item for
        ///  the key will be returned. Retrieval of other items requires the use of #mdb_cursor_get().
        /// 
        /// Note: The memory pointed to by the returned values is owned by the database. The caller need
        ///  not dispose of the memory, and may not modify it in any way. For values returned in a read-
        ///  only transaction any modification attempts will cause a SIGSEGV (invalid memory access
        ///  (segmentation fault)).
        /// 
        /// Note: Values returned from the database are valid only until a
        ///  subsequent update operation, or the end of the transaction. A non-zero error value on
        ///  failure and 0 on success. Some possible errors are:
        ///  <list type="bullet"> <item>
        ///  #MDB_NOTFOUND - the key was not in the database. </item><item>
        ///  EINVAL - an invalid parameter was specified. </item> </list>
        /// </remarks>
        /// <param name="txn">  [in,out] A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  [in,out] The key to search for in the database. </param>
        /// <param name="data"> [out] The data corresponding to the key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_get( void* txn, uint dbi, void* key, void* data );

        /// <summary>   Store items into a database. </summary>
        /// <remarks>
        /// This function stores key/data pairs in the database. The default behavior is to enter the new
        /// key/data pair, replacing any previously existing key if duplicates are disallowed, or adding
        /// a duplicate data item if duplicates are allowed (#MDB_DUPSORT).
        /// <list type="bullet"><listheader> Special options for this operation. This parameter
        /// must be set to 0 or by bitwise ORing together one or more of the values described here.
        /// </listheader> <item>
        ///    #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///        already appear in the database. This flag may only be specified if the database was
        ///        opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///        already appears in the database. </item><item>
        ///   #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///        does not already appear in the database. The function will return
        ///        #MDB_KEYEXIST if the key already appears in the database, even if
        ///        the database supports duplicates (#MDB_DUPSORT). The <b>data</b>
        ///        parameter will be set to point to the existing item. </item><item>
        ///   #MDB_RESERVE - reserve space for data of the given size, but
        ///        don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///        caller can fill in later - before the next update operation or the transaction ends.
        ///        This saves an extra memcpy if the data is being generated later. LMDB does nothing
        ///        else with this memory, the caller is expected to modify all of the space requested.
        ///        This flag must not be specified if the database was opened with #MDB_DUPSORT.
        /// </item><item>
        ///   #MDB_APPEND - append the given key/data pair to the end of the
        ///        database. This option allows fast bulk loading when keys are already known to be in
        ///        the correct order. Loading unsorted keys with this flag will cause a #MDB_KEYEXIST
        ///        error. </item><item>
        ///    #MDB_APPENDDUP - as above, but for sorted dup data.
        /// </item> </list>
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">      The key to store in the database. </param>
        /// <param name="value">    [out] The value. </param>
        /// <param name="flags">    Special options for this operation. This parameter must be set to 0
        ///                         or by bitwise ORing together one or more of the values described above. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        /// #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        /// EACCES - an attempt was  made to write in a read-only transaction. </item><item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        public static int mdb_put( IntPtr txn, uint dbi, LmdbBuffer key, LmdbBuffer value, TransactionPutOptions flags )
        {
            return SafeNativeMethods.mdb_put( txn, dbi, ref key, ref value, flags );
        }

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        public static int mdb_del( IntPtr txn, uint dbi, LmdbBuffer key )
        {
            return SafeNativeMethods.mdb_del( txn, dbi, in key, IntPtr.Zero );
        }

        #endregion " LMDB DATABASE METHODS "

        #region " LMDB DATABASE METHODS: LMDB BUFFER "

        /// <summary>   Set a custom key comparison function for a database. </summary>
        /// <remarks>
        /// The comparison function is called whenever it is necessary to compare a key specified by the
        /// application with a key currently stored in the database. If no comparison function is
        /// specified, and no special key flags were specified with #mdb_dbi_open(), the keys are
        /// compared lexically, with shorter keys collating before longer keys.
        /// 
        /// Warning: This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_compare( IntPtr txn, uint dbi, [MarshalAs( UnmanagedType.FunctionPtr )] LmdbBufferCompareFunction cmp );

#if NET5_0_OR_GREATER
        /// <summary>   Set a custom key comparison function for a database. </summary>
        /// <remarks>
        /// The comparison function is called whenever it is necessary to compare a key specified by the
        /// application with a key currently stored in the database. If no comparison function is
        /// specified, and no special key flags were specified with #mdb_dbi_open(), the keys are
        /// compared lexically, with shorter keys collating before longer keys.
        /// 
        /// Warning: This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_compare( IntPtr txn, uint dbi, delegate* unmanaged< LmdbBuffer, LmdbBuffer, int > cmp );
#endif

        /// <summary>   Set a custom data comparison function for a #MDB_DUPSORT database. </summary>
        /// <remarks>
        /// This comparison function is called whenever it is necessary to compare a data item specified
        /// by the application with a data item currently stored in the database. This function only
        /// takes effect if the database was opened with the #MDB_DUPSORT flag. If no comparison function
        /// is specified, and no special key flags were specified with #mdb_dbi_open(), the data items
        /// are compared lexically, with shorter items collating before longer items.
        /// 
        /// Warning This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_dupsort( IntPtr txn, uint dbi, [MarshalAs( UnmanagedType.FunctionPtr )] LmdbBufferCompareFunction cmp );

        // Lightning: 
        // public static extern MDBResultCode mdb_set_compare(IntPtr txn, uint dbi, CompareFunction cmp);
        // public static extern MDBResultCode mdb_set_dupsort(IntPtr txn, uint dbi, CompareFunction cmp);

#if NET5_0_OR_GREATER
        /// <summary>   Set a custom data comparison function for a #MDB_DUPSORT database. </summary>
        /// <remarks>
        /// This comparison function is called whenever it is necessary to compare a data item specified
        /// by the application with a data item currently stored in the database. This function only
        /// takes effect if the database was opened with the #MDB_DUPSORT flag. If no comparison function
        /// is specified, and no special key flags were specified with #mdb_dbi_open(), the data items
        /// are compared lexically, with shorter items collating before longer items.
        /// 
        /// Warning This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_dupsort( IntPtr txn, uint dbi, delegate* unmanaged< LmdbBuffer, LmdbBuffer, int > cmp );
#endif

        /// <summary>   Get items from a database. </summary>
        /// <remarks>
        ///  This function retrieves key/data pairs from the database. The address and length of the data
        ///  associated with the specified <b>key</b> are returned in the structure to which <b>data</b>
        ///  refers. If the database supports duplicate keys (#MDB_DUPSORT) then the first data item for
        ///  the key will be returned. Retrieval of other items requires the use of #mdb_cursor_get().
        /// 
        /// Note: The memory pointed to by the returned values is owned by the database. The caller need
        ///  not dispose of the memory, and may not modify it in any way. For values returned in a read-
        ///  only transaction any modification attempts will cause a SIGSEGV (invalid memory access
        ///  (segmentation fault)).
        /// 
        /// Note: Values returned from the database are valid only until a
        ///  subsequent update operation, or the end of the transaction. A non-zero error value on
        ///  failure and 0 on success. Some possible errors are:
        ///  <list type="bullet"> <item>
        ///  #MDB_NOTFOUND - the key was not in the database. </item><item>
        ///  EINVAL - an invalid parameter was specified. </item> </list>
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to search for in the database. </param>
        /// <param name="data"> [out] The data corresponding to the key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_get( IntPtr txn, uint dbi, in LmdbBuffer key, out LmdbBuffer data );

        /// <summary>   Store items into a database. </summary>
        /// <remarks>
        /// This function stores key/data pairs in the database. The default behavior is to enter the new
        /// key/data pair, replacing any previously existing key if duplicates are disallowed, or adding
        /// a duplicate data item if duplicates are allowed (#MDB_DUPSORT).
        /// <list type="bullet"><listheader> Special options for this operation. This parameter
        /// must be set to 0 or by bitwise ORing together one or more of the values described here.
        /// </listheader> <item>
        ///    #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///        already appear in the database. This flag may only be specified if the database was
        ///        opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///        already appears in the database. </item><item>
        ///   #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///        does not already appear in the database. The function will return
        ///        #MDB_KEYEXIST if the key already appears in the database, even if
        ///        the database supports duplicates (#MDB_DUPSORT). The <b>data</b>
        ///        parameter will be set to point to the existing item. </item><item>
        ///   #MDB_RESERVE - reserve space for data of the given size, but
        ///        don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///        caller can fill in later - before the next update operation or the transaction ends.
        ///        This saves an extra memcpy if the data is being generated later. LMDB does nothing
        ///        else with this memory, the caller is expected to modify all of the space requested.
        ///        This flag must not be specified if the database was opened with #MDB_DUPSORT.
        /// </item><item>
        ///   #MDB_APPEND - append the given key/data pair to the end of the
        ///        database. This option allows fast bulk loading when keys are already known to be in
        ///        the correct order. Loading unsorted keys with this flag will cause a #MDB_KEYEXIST
        ///        error. </item><item>
        ///    #MDB_APPENDDUP - as above, but for sorted dup data.
        /// </item> </list>
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">      The key to store in the database. </param>
        /// <param name="data">     The data to store. </param>
        /// <param name="flags">    Special options for this operation. This parameter must be set to 0
        ///                         or by bitwise ORing together one or more of the values described above. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        /// #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        /// EACCES - an attempt was  made to write in a read-only transaction. </item><item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_put( IntPtr txn, uint dbi, ref LmdbBuffer key, ref LmdbBuffer data, TransactionPutOptions flags );

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <param name="data"> The data to delete. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_del( IntPtr txn, uint dbi, in LmdbBuffer key, in LmdbBuffer data );

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <param name="data"> The data to delete. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_del( IntPtr txn, uint dbi, in LmdbBuffer key, in IntPtr data );

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <param name="data"> The data to delete. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_del( IntPtr txn, uint dbi, in LmdbBuffer key, IntPtr data );

        /// <summary>   Compare two data items according to a particular database. </summary>
        /// <remarks>
        /// This returns a comparison as if the two data items were keys in the specified database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="x">    The first <see cref="LmdbBuffer"/> to compare. </param>
        /// <param name="y">    The Second <see cref="LmdbBuffer"/> to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cmp( IntPtr txn, uint dbi, in LmdbBuffer x, in LmdbBuffer y );

        /// <summary>   Compare two data items according to a particular database. </summary>
        /// <remarks>
        /// This returns a comparison as if the two items were data items of the specified database. The
        /// database must have the #MDB_DUPSORT flag.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin() </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="x">    The first <see cref="LmdbBuffer"/> to compare. </param>
        /// <param name="y">    The Second <see cref="LmdbBuffer"/> to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_dcmp( IntPtr txn, uint dbi, in LmdbBuffer x, in LmdbBuffer y );

        #endregion " LMDB DATABASE METHODS: LMDB BUFFER "

        #region " LMDB DATABASE METHODS: LMDB VALUE "

        /// <summary>   Set a custom key comparison function for a database. </summary>
        /// <remarks>
        /// The comparison function is called whenever it is necessary to compare a key specified by the
        /// application with a key currently stored in the database. If no comparison function is
        /// specified, and no special key flags were specified with #mdb_dbi_open(), the keys are
        /// compared lexically, with shorter keys collating before longer keys.
        /// 
        /// Warning: This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_compare( IntPtr txn, uint dbi, LmdbValueCompareFunction cmp );

#if NET5_0_OR_GREATER
        /// <summary>   Set a custom key comparison function for a database. </summary>
        /// <remarks>
        /// The comparison function is called whenever it is necessary to compare a key specified by the
        /// application with a key currently stored in the database. If no comparison function is
        /// specified, and no special key flags were specified with #mdb_dbi_open(), the keys are
        /// compared lexically, with shorter keys collating before longer keys.
        /// 
        /// Warning: This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_compare( IntPtr txn, uint dbi, delegate* unmanaged< LmdbValue, LmdbValue, int > cmp );
#endif

        /// <summary>   Set a custom data comparison function for a #MDB_DUPSORT database. </summary>
        /// <remarks>
        /// This comparison function is called whenever it is necessary to compare a data item specified
        /// by the application with a data item currently stored in the database. This function only
        /// takes effect if the database was opened with the #MDB_DUPSORT flag. If no comparison function
        /// is specified, and no special key flags were specified with #mdb_dbi_open(), the data items
        /// are compared lexically, with shorter items collating before longer items.
        /// 
        /// Warning This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_dupsort( IntPtr txn, uint dbi, LmdbValueCompareFunction cmp );

#if NET5_0_OR_GREATER
        /// <summary>   Set a custom data comparison function for a #MDB_DUPSORT database. </summary>
        /// <remarks>
        /// This comparison function is called whenever it is necessary to compare a data item specified
        /// by the application with a data item currently stored in the database. This function only
        /// takes effect if the database was opened with the #MDB_DUPSORT flag. If no comparison function
        /// is specified, and no special key flags were specified with #mdb_dbi_open(), the data items
        /// are compared lexically, with shorter items collating before longer items.
        /// 
        /// Warning This function must be called before any data access functions are used, otherwise
        /// data corruption may occur. The same comparison function must be used by every program
        /// accessing the database, every time the database is used.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cmp">  A #MDB_cmp_func function. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_set_dupsort( IntPtr txn, uint dbi, delegate* unmanaged< LmdbValue, LmdbValue, int > cmp );
#endif

        /// <summary>   Get items from a database. </summary>
        /// <remarks>
        ///  This function retrieves key/data pairs from the database. The address and length of the data
        ///  associated with the specified <b>key</b> are returned in the structure to which <b>data</b>
        ///  refers. If the database supports duplicate keys (#MDB_DUPSORT) then the first data item for
        ///  the key will be returned. Retrieval of other items requires the use of #mdb_cursor_get().
        /// 
        /// Note: The memory pointed to by the returned values is owned by the database. The caller need
        ///  not dispose of the memory, and may not modify it in any way. For values returned in a read-
        ///  only transaction any modification attempts will cause a SIGSEGV (invalid memory access
        ///  (segmentation fault)).
        /// 
        /// Note: Values returned from the database are valid only until a
        ///  subsequent update operation, or the end of the transaction. A non-zero error value on
        ///  failure and 0 on success. Some possible errors are:
        ///  <list type="bullet"> <item>
        ///  #MDB_NOTFOUND - the key was not in the database. </item><item>
        ///  EINVAL - an invalid parameter was specified. </item> </list>
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to search for in the database. </param>
        /// <param name="data"> [out] The data corresponding to the key. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_get( IntPtr txn, uint dbi, in LmdbValue key, out LmdbValue data );

        /// <summary>   Store items into a database. </summary>
        /// <remarks>
        /// This function stores key/data pairs in the database. The default behavior is to enter the new
        /// key/data pair, replacing any previously existing key if duplicates are disallowed, or adding
        /// a duplicate data item if duplicates are allowed (#MDB_DUPSORT).
        /// <list type="bullet"><listheader> Special options for this operation. This parameter
        /// must be set to 0 or by bitwise ORing together one or more of the values described here.
        /// </listheader> <item>
        ///    #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///        already appear in the database. This flag may only be specified if the database was
        ///        opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///        already appears in the database. </item><item>
        ///   #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///        does not already appear in the database. The function will return
        ///        #MDB_KEYEXIST if the key already appears in the database, even if
        ///        the database supports duplicates (#MDB_DUPSORT). The <b>data</b>
        ///        parameter will be set to point to the existing item. </item><item>
        ///   #MDB_RESERVE - reserve space for data of the given size, but
        ///        don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///        caller can fill in later - before the next update operation or the transaction ends.
        ///        This saves an extra memcpy if the data is being generated later. LMDB does nothing
        ///        else with this memory, the caller is expected to modify all of the space requested.
        ///        This flag must not be specified if the database was opened with #MDB_DUPSORT.
        /// </item><item>
        ///   #MDB_APPEND - append the given key/data pair to the end of the
        ///        database. This option allows fast bulk loading when keys are already known to be in
        ///        the correct order. Loading unsorted keys with this flag will cause a #MDB_KEYEXIST
        ///        error. </item><item>
        ///    #MDB_APPENDDUP - as above, but for sorted dup data.
        /// </item> </list>
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">      The key to store in the database. </param>
        /// <param name="data">     The data to store. </param>
        /// <param name="flags">    Special options for this operation. This parameter must be set to 0
        ///                         or by bitwise ORing together one or more of the values described above. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        /// #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        /// EACCES - an attempt was  made to write in a read-only transaction. </item><item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_put( IntPtr txn, uint dbi, in LmdbValue key, in LmdbValue data, uint flags );

        /// <summary>   Store items into a database. </summary>
        /// <remarks>
        /// This function stores key/data pairs in the database. The default behavior is to enter the new
        /// key/data pair, replacing any previously existing key if duplicates are disallowed, or adding
        /// a duplicate data item if duplicates are allowed (#MDB_DUPSORT).
        /// <list type="bullet"><listheader> Special options for this operation. This parameter
        /// must be set to 0 or by bitwise ORing together one or more of the values described here.
        /// </listheader> <item>
        ///    #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///        already appear in the database. This flag may only be specified if the database was
        ///        opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///        already appears in the database. </item><item>
        ///   #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///        does not already appear in the database. The function will return
        ///        #MDB_KEYEXIST if the key already appears in the database, even if
        ///        the database supports duplicates (#MDB_DUPSORT). The <b>data</b>
        ///        parameter will be set to point to the existing item. </item><item>
        ///   #MDB_RESERVE - reserve space for data of the given size, but
        ///        don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///        caller can fill in later - before the next update operation or the transaction ends.
        ///        This saves an extra memcpy if the data is being generated later. LMDB does nothing
        ///        else with this memory, the caller is expected to modify all of the space requested.
        ///        This flag must not be specified if the database was opened with #MDB_DUPSORT.
        /// </item><item>
        ///   #MDB_APPEND - append the given key/data pair to the end of the
        ///        database. This option allows fast bulk loading when keys are already known to be in
        ///        the correct order. Loading unsorted keys with this flag will cause a #MDB_KEYEXIST
        ///        error. </item><item>
        ///    #MDB_APPENDDUP - as above, but for sorted dup data.
        /// </item> </list>
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">      The key to store in the database. </param>
        /// <param name="data">     The data to store. </param>
        /// <param name="flags">    Special options for this operation. This parameter must be set to 0
        ///                         or by bitwise ORing together one or more of the values described above. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        /// #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        /// EACCES - an attempt was  made to write in a read-only transaction. </item><item>
        /// EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_put( IntPtr txn, uint dbi, in LmdbValue key, in LmdbValue data, TransactionPutOptions flags );

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <param name="data"> The data to delete. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_del( IntPtr txn, uint dbi, in LmdbValue key, in LmdbValue data );

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <param name="data"> The data to delete. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_del( IntPtr txn, uint dbi, in LmdbValue key, in IntPtr data );

        /// <summary>   Delete items from a database. </summary>
        /// <remarks>
        /// This function removes key/data pairs from the database. If the database does not support
        /// sorted duplicate data items (#MDB_DUPSORT) the data parameter is ignored. If the database
        /// supports sorted duplicates and the data parameter is NULL, all of the duplicate data items
        /// for the key will be deleted. Otherwise, if the data parameter is non-NULL only the matching
        /// data item will be deleted. This function will return #MDB_NOTFOUND if the specified key/data
        /// pair is not in the database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">  The key to delete from the database. </param>
        /// <param name="data"> The data to delete. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_del( IntPtr txn, uint dbi, in LmdbValue key, IntPtr data );

        /// <summary>   Compare two data items according to a particular database. </summary>
        /// <remarks>
        /// This returns a comparison as if the two data items were keys in the specified database.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="x">    The first <see cref="LmdbValue"/> to compare. </param>
        /// <param name="y">    The Second <see cref="LmdbValue"/> to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cmp( IntPtr txn, uint dbi, in LmdbValue x, in LmdbValue y );

        /// <summary>   Compare two data items according to a particular database. </summary>
        /// <remarks>
        /// This returns a comparison as if the two items were data items of the specified database. The
        /// database must have the #MDB_DUPSORT flag.
        /// </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin() </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="x">    The first <see cref="LmdbValue"/> to compare. </param>
        /// <param name="y">    The Second <see cref="LmdbValue"/> to compare. </param>
        /// <returns>   &lt; 0 if x &lt; y, 0 if x == y, &gt; 0 if x &gt; y. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_dcmp( IntPtr txn, uint dbi, in LmdbValue x, in LmdbValue y );

        #endregion " LMDB DATABASE METHODS: LMDB VALUE "

        #region " CURSOR METHODS "

        /// <summary>   Create a cursor handle. </summary>
        /// <remarks>
        ///  A cursor is associated with a specific transaction and database.
        /// 
        ///  A cursor cannot be used when its database handle is closed, nor when its transaction has
        ///  ended, except with
        ///  #mdb_cursor_renew().
        /// 
        ///  A cursor can be discarded with #mdb_cursor_close().
        /// 
        ///  A cursor in a write-transaction can be closed before its transaction ends, and will
        ///  otherwise be closed when its transaction ends.
        /// 
        ///  A cursor in a read-only transaction must be closed explicitly, before or after its
        ///  transaction ends. It can be reused with #mdb_cursor_renew() before finally closing it.
        /// 
        /// Note: Earlier documentation said that cursors in every transaction were closed when the
        /// transaction committed or aborted.
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="cursor">   [out] Address where the new #MDB_cursor handle will be stored. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_open( IntPtr txn, uint dbi, out IntPtr cursor );

        /// <summary>   Close a cursor handle. </summary>
        /// <remarks>
        /// The cursor handle will be freed and must not be used again after this call. Its transaction
        /// must still be live if it is a write-transaction.
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern void mdb_cursor_close( IntPtr cursor );

        /// <summary>   Renew a cursor handle. </summary>
        /// <remarks>
        /// A cursor is associated with a specific transaction and database. Cursors that are only used
        /// in read-only transactions may be re-used, to avoid unnecessary malloc/free overhead. The
        /// cursor may be associated with a new read-only transaction, and referencing the same database
        /// handle as it was created with. This may be done whether the previous transaction is live or
        /// dead.
        /// </remarks>
        /// <param name="txn">      A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_renew( IntPtr txn, IntPtr cursor );

        /// <summary>   Return the cursor's transaction handle. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <returns>   An IntPtr. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_cursor_txn( IntPtr cursor );

        /// <summary>   Return the cursor's database handle. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <returns>   An IntPtr. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern IntPtr mdb_cursor_dbi( IntPtr cursor );

        /// <summary>   Retrieve by cursor. </summary>
        /// <remarks>
        /// This function retrieves key/data pairs from the database. The address and length of the key
        /// are returned in the object to which <b>key</b> refers (except for the case of the #MDB_SET
        /// option, in which the <b>key</b> object is unchanged), and the address and length of the data
        /// are returned in the object to which <b>data</b>
        /// refers. See #mdb_get() for restrictions on using the output values.
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      The key for a retrieved item. </param>
        /// <param name="data">     [in,out] data The data of a retrieved item. </param>
        /// <param name="op">       A cursor operation #MDB_cursor_op. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_NOTFOUND - no matching key found. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_get( IntPtr cursor, in LmdbValue key, LmdbValue* data, CursorOperation op );

        /// <summary>   Retrieve by cursor. </summary>
        /// <remarks>
        /// This function retrieves key/data pairs from the database. The address and length of the key
        /// are returned in the object to which <b>key</b> refers (except for the case of the #MDB_SET
        /// option, in which the <b>key</b> object is unchanged), and the address and length of the data
        /// are returned in the object to which <b>data</b>
        /// refers. See #mdb_get() for restrictions on using the output values.
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key to search for in the database. </param>
        /// <param name="data">     [in,out] The data corresponding to the key. </param>
        /// <param name="op">       A cursor operation #MDB_cursor_op. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_NOTFOUND - no matching key found. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_get( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data, CursorOperation op );

        /// <summary>   Retrieve by cursor. </summary>
        /// <remarks>
        /// This function retrieves key/data pairs from the database. The address and length of the key
        /// are returned in the object to which <b>key</b> refers (except for the case of the #MDB_SET
        /// option, in which the <b>key</b> object is unchanged), and the address and length of the data
        /// are returned in the object to which <b>data</b>
        /// refers. See #mdb_get() for restrictions on using the output values.
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key to search for in the database. </param>
        /// <param name="data">     [in,out] The data corresponding to the key. </param>
        /// <param name="op">       A cursor operation #MDB_cursor_op. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_NOTFOUND - no matching key found. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_get( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data, CursorGetOption op );

        /// <summary>   Store by cursor. </summary>
        /// <remarks>
        /// store multiple contiguous data elements in a single request. May only be used with
        /// MDB_DUPFIXED.
        /// 
        ///  This function stores key/data pairs into the database. The cursor is positioned at the new
        ///  item, or on failure usually near it.
        /// Note: Earlier documentation incorrectly said errors would leave the
        ///  state of the cursor unchanged. The Flags parameter must be set to 0 or one of the values
        ///  described here.
        ///  <list type="bullet"> <item>
        ///     #MDB_CURRENT - replace the item at the current cursor position.
        ///         The <b>key</b> parameter must still be provided, and must match it. If using sorted
        ///         duplicates (#MDB_DUPSORT) the data item must still sort into the same place. This is
        ///         intended to be used when the new data is the same size as the old. Otherwise it will
        ///         simply perform a delete of the old record followed by an insert. </item><item>
        ///     #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///         already appear in the database. This flag may only be specified if the database was
        ///         opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///         already appears in the database. </item><item>
        ///     #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///         does not already appear in the database. The function will return
        ///         #MDB_KEYEXIST if the key already appears in the database, even if
        ///         the database supports duplicates (#MDB_DUPSORT). </item><item>
        ///     #MDB_RESERVE - reserve space for data of the given size, but
        ///         don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///         caller can fill in later - before the next update operation or the transaction ends.
        ///         This saves an extra memcpy if the data is being generated later. This flag must not
        ///         be specified if the database was opened with #MDB_DUPSORT. </item><item>
        ///     #MDB_APPEND - append the given key/data pair to the end of the
        ///         database. No key comparisons are performed. This option allows fast bulk loading when
        ///         keys are already known to be in the correct order. Loading unsorted keys with this
        ///         flag will cause a #MDB_KEYEXIST error. </item><item>
        ///     #MDB_APPENDDUP - as above, but for sorted dup data. </item><item>
        ///     #MDB_MULTIPLE - store multiple contiguous data elements in a
        ///         single request. This flag may only be specified if the database was opened with
        ///         #MDB_DUPFIXED. The <b>data</b> argument must be an array of two MDB_vals. The mv_size
        ///         of the first MDB_val must be the size of a single data element. The mv_data of the
        ///         first MDB_val must point to the beginning of the array of contiguous data elements.
        ///         The mv_size of the second MDB_val must be the count of the number of data elements to
        ///         store. On return this field will be set to the count of the number of elements
        ///         actually written. The mv_data of the second MDB_val is unused. </item> </list>
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      The key operated on. </param>
        /// <param name="data">     [in,out] The data operated on. </param>
        /// <param name="flags">    Options for this operation. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        ///    #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_put( IntPtr cursor, in LmdbValue key, LmdbValue* data, CursorPutOptions flags );

        /// <summary>   Store by cursor. </summary>
        /// <remarks>
        /// store multiple contiguous data elements in a single request. May only be used with
        /// MDB_DUPFIXED.
        /// 
        /// This function stores key/data pairs into the database. The cursor is positioned at the new
        /// item, or on failure usually near it.
        /// 
        /// Note: Earlier documentation incorrectly said errors would leave the state of the cursor
        /// unchanged. The Flags parameter must be set to 0 or one of the values described here:
        /// <list type="bullet"> <item>
        /// #MDB_CURRENT - <see cref="CursorPutOptions.Current"/> replace the item at the current cursor
        ///     position. The <b>key</b> parameter must still be provided, and must match it. If using
        ///     sorted duplicates (#MDB_DUPSORT) the data item must still sort into the same place. This
        ///     is intended to be used when the new data is the same size as the old. Otherwise it will
        ///     simply perform a delete of the old record followed by an insert. </item><item>
        /// #MDB_NODUPDATA - <see cref="CursorPutOptions.NoDuplicateData"/> enter the new key/data pair
        ///     only if it does not already appear in the database. This flag may only be specified if
        ///     the database was opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the
        ///     key/data pair already appears in the database. </item><item>
        /// #MDB_NOOVERWRITE - <see cref="CursorPutOptions.NoOverwrite"/> enter the new key/data pair
        ///     only if the key does not already appear in the database. The function will return
        ///     #MDB_KEYEXIST if the key already appears in the database, even if
        ///     the database supports duplicates (#MDB_DUPSORT). </item><item>
        /// #MDB_RESERVE - <see cref="CursorPutOptions.ReserveSpace"/> reserve space for data of the
        ///     given size, but don't copy the given data. Instead, return a pointer to the reserved
        ///     space, which the caller can fill in later - before the next update operation or the
        ///     transaction ends. This saves an extra memcpy if the data is being generated later. This
        ///     flag must not be specified if the database was opened with #MDB_DUPSORT. </item><item>
        /// #MDB_APPEND - <see cref="CursorPutOptions.AppendData"/> append the given key/data pair to the
        ///     end of the database. No key comparisons are performed. This option allows fast bulk
        ///     loading when keys are already known to be in the correct order. Loading unsorted keys
        ///     with this flag will cause a #MDB_KEYEXIST error. </item><item>
        /// #MDB_APPENDDUP - <see cref="CursorPutOptions.AppendDuplicateData"/> as above, but for sorted
        ///     dup data. </item><item>
        /// #MDB_MULTIPLE - <see cref="CursorPutOptions.MultipleData"/> store multiple contiguous data
        ///     elements in a single request. This flag may only be specified if the database was opened with
        ///     #MDB_DUPFIXED. The <b>data</b> argument must be an array of two MDB_vals. The mv_size
        ///     of the first MDB_val must be the size of a single data element. The mv_data of the first
        ///     MDB_val must point to the beginning of the array of contiguous data elements. The mv_size
        ///     of the second MDB_val must be the count of the number of data elements to store. On
        ///     return this field will be set to the count of the number of elements actually written.
        ///     The mv_data of the second MDB_val is unused. </item> </list>
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key to search for in the database. </param>
        /// <param name="data">     [in,out] The data corresponding to the key. </param>
        /// <param name="flags">    Options for this operation. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        /// #MDB_MAP_FULL - <see cref="NativeResultCode.MapFull"/> the database is full, see #mdb_env_set_mapsize(). </item><item>
        /// #MDB_TXN_FULL - <see cref="NativeResultCode.TransactionFull"/>the transaction has too many dirty pages. </item><item>
        /// EACCES -  <see cref="NativeResultCode.AccessDenied"/>an attempt was made to write in a read-only transaction. </item><item>
        /// EINVAL - <see cref="NativeResultCode.Invalid"/> an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_put( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data, CursorPutOptions flags );

        /// <summary>   Store by cursor. </summary>
        /// <remarks>
        /// store multiple contiguous data elements in a single request. May only be used with
        /// MDB_DUPFIXED.
        /// 
        ///  This function stores key/data pairs into the database. The cursor is positioned at the new
        ///  item, or on failure usually near it.
        /// Note: Earlier documentation incorrectly said errors would leave the
        ///  state of the cursor unchanged. The Flags parameter must be set to 0 or one of the values
        ///  described here.
        ///  <list type="bullet"> <item>
        ///     #MDB_CURRENT - replace the item at the current cursor position.
        ///         The <b>key</b> parameter must still be provided, and must match it. If using sorted
        ///         duplicates (#MDB_DUPSORT) the data item must still sort into the same place. This is
        ///         intended to be used when the new data is the same size as the old. Otherwise it will
        ///         simply perform a delete of the old record followed by an insert. </item><item>
        ///     #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///         already appear in the database. This flag may only be specified if the database was
        ///         opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///         already appears in the database. </item><item>
        ///     #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///         does not already appear in the database. The function will return
        ///         #MDB_KEYEXIST if the key already appears in the database, even if
        ///         the database supports duplicates (#MDB_DUPSORT). </item><item>
        ///     #MDB_RESERVE - reserve space for data of the given size, but
        ///         don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///         caller can fill in later - before the next update operation or the transaction ends.
        ///         This saves an extra memcpy if the data is being generated later. This flag must not
        ///         be specified if the database was opened with #MDB_DUPSORT. </item><item>
        ///     #MDB_APPEND - append the given key/data pair to the end of the
        ///         database. No key comparisons are performed. This option allows fast bulk loading when
        ///         keys are already known to be in the correct order. Loading unsorted keys with this
        ///         flag will cause a #MDB_KEYEXIST error. </item><item>
        ///     #MDB_APPENDDUP - as above, but for sorted dup data. </item><item>
        ///     #MDB_MULTIPLE - store multiple contiguous data elements in a
        ///         single request. This flag may only be specified if the database was opened with
        ///         #MDB_DUPFIXED. The <b>data</b> argument must be an array of two MDB_vals. The mv_size
        ///         of the first MDB_val must be the size of a single data element. The mv_data of the
        ///         first MDB_val must point to the beginning of the array of contiguous data elements.
        ///         The mv_size of the second MDB_val must be the count of the number of data elements to
        ///         store. On return this field will be set to the count of the number of elements
        ///         actually written. The mv_data of the second MDB_val is unused. </item> </list>
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key to search for in the database. </param>
        /// <param name="data">     The data operated on. </param>
        /// <param name="flags">    Options for this operation. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        ///    #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_put( IntPtr cursor, ref LmdbBuffer key, LmdbBuffer[] data, CursorPutOptions flags );

        /// <summary>
        /// store multiple contiguous data elements in a single request. May only be used with
        /// MDB_DUPFIXED.
        /// </summary>
        /// <remarks>   David, 2021-06-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key to search for in the database. </param>
        /// <param name="data">     [in,out] This span must be pinned or stackalloc memory. </param>
        /// <param name="flags">    Options for this operation. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        ///    #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        public static int mdb_cursor_put( IntPtr cursor, ref LmdbBuffer key, ref Span<LmdbBuffer> data, CursorPutOptions flags )
        {
            ref var dataRef = ref MemoryMarshal.GetReference( data );
            return mdb_cursor_put( cursor, ref key, ref dataRef, flags );
        }

        /// <summary>   Store by cursor. </summary>
        /// <remarks>
        /// store multiple contiguous data elements in a single request. May only be used with
        /// MDB_DUPFIXED.
        /// 
        ///  This function stores key/data pairs into the database. The cursor is positioned at the new
        ///  item, or on failure usually near it.
        /// Note: Earlier documentation incorrectly said errors would leave the
        ///  state of the cursor unchanged. The Flags parameter must be set to 0 or one of the values
        ///  described here.
        ///  <list type="bullet"> <item>
        ///     #MDB_CURRENT - replace the item at the current cursor position.
        ///         The <b>key</b> parameter must still be provided, and must match it. If using sorted
        ///         duplicates (#MDB_DUPSORT) the data item must still sort into the same place. This is
        ///         intended to be used when the new data is the same size as the old. Otherwise it will
        ///         simply perform a delete of the old record followed by an insert. </item><item>
        ///     #MDB_NODUPDATA - enter the new key/data pair only if it does not
        ///         already appear in the database. This flag may only be specified if the database was
        ///         opened with #MDB_DUPSORT. The function will return #MDB_KEYEXIST if the key/data pair
        ///         already appears in the database. </item><item>
        ///     #MDB_NOOVERWRITE - enter the new key/data pair only if the key
        ///         does not already appear in the database. The function will return
        ///         #MDB_KEYEXIST if the key already appears in the database, even if
        ///         the database supports duplicates (#MDB_DUPSORT). </item><item>
        ///     #MDB_RESERVE - reserve space for data of the given size, but
        ///         don't copy the given data. Instead, return a pointer to the reserved space, which the
        ///         caller can fill in later - before the next update operation or the transaction ends.
        ///         This saves an extra memcpy if the data is being generated later. This flag must not
        ///         be specified if the database was opened with #MDB_DUPSORT. </item><item>
        ///     #MDB_APPEND - append the given key/data pair to the end of the
        ///         database. No key comparisons are performed. This option allows fast bulk loading when
        ///         keys are already known to be in the correct order. Loading unsorted keys with this
        ///         flag will cause a #MDB_KEYEXIST error. </item><item>
        ///     #MDB_APPENDDUP - as above, but for sorted dup data. </item><item>
        ///     #MDB_MULTIPLE - store multiple contiguous data elements in a
        ///         single request. This flag may only be specified if the database was opened with
        ///         #MDB_DUPFIXED. The <b>data</b> argument must be an array of two MDB_vals. The mv_size
        ///         of the first MDB_val must be the size of a single data element. The mv_data of the
        ///         first MDB_val must point to the beginning of the array of contiguous data elements.
        ///         The mv_size of the second MDB_val must be the count of the number of data elements to
        ///         store. On return this field will be set to the count of the number of elements
        ///         actually written. The mv_data of the second MDB_val is unused. </item> </list>
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      The key operated on. </param>
        /// <param name="data">     [in,out] The data operated on. </param>
        /// <param name="flags">    Special options for opening this database. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    #MDB_MAP_FULL - the database is full, see #mdb_env_set_mapsize(). </item><item>
        ///    #MDB_TXN_FULL - the transaction has too many dirty pages. </item><item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_put( IntPtr cursor, in LmdbValue key, LmdbValue* data, uint flags );

        /// <summary>   Delete current key/data pair. </summary>
        /// <remarks>
        /// This function deletes the key/data pair to which the cursor refers. This does not invalidate
        /// the cursor, so operations such as MDB_NEXT can still be used on it. Both MDB_NEXT and
        /// MDB_GET_CURRENT will return the same record after this operation.\
        /// 
        /// The flags parameter must be set to 0 or one of the values described here.
        /// <list type="bullet"> <item>
        ///    #MDB_NODUPDATA - delete all of the data items for the current key. This flag may only be
        ///    specified if the database was opened with #MDB_DUPSORT. </item> </list>
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="flags">    Options for this operation. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_del( IntPtr cursor, CursorDeleteOption flags );

        /// <summary>   Delete current key/data pair. </summary>
        /// <remarks>
        /// This function deletes the key/data pair to which the cursor refers. This does not invalidate
        /// the cursor, so operations such as MDB_NEXT can still be used on it. Both MDB_NEXT and
        /// MDB_GET_CURRENT will return the same record after this operation.\
        /// 
        /// The flags parameter must be set to 0 or one of the values described here.
        /// <list type="bullet"> <item>
        ///    #MDB_NODUPDATA - delete all of the data items for the current key. This flag may only be
        ///    specified if the database was opened with #MDB_DUPSORT. </item> </list>
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="flags">    Options for this operation. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible errors are:
        /// <list type="bullet"> <item>
        ///    EACCES - an attempt was made to write in a read-only transaction. </item><item>
        ///    EINVAL - an invalid parameter was specified. </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_del( IntPtr cursor, int flags );

        /// <summary>   Return count of duplicates for current key. </summary>
        /// <remarks>
        /// This call is only valid on databases that support sorted duplicate data items #MDB_DUPSORT.
        /// </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="count">    [out] Address where the count will be stored. </param>
        /// <returns>
        /// A non-zero error value on failure and 0 on success. Some possible
        ///  errors are:
        ///  <list type="bullet"> <item>
        ///     EINVAL - cursor is not initialized, or an invalid parameter was specified.
        /// </item> </list>.
        /// </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int mdb_cursor_count( IntPtr cursor, out UIntPtr count );

        #endregion

        #region " LOOKUP EXTENSIONS TO LMDB LIBRARY BY Victor Baybecov "

        /// <summary> Lookup the first key greater than or equal to specified key.
        /// If found, return the previous value; otherwise, return the last value.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>

        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_lt( IntPtr cursor, ref LmdbBuffer key, out LmdbBuffer data );

        /// <summary> Lookup the first key greater than or equal to specified key.
        /// If found, lookup for the first key greater than the search key. 
        /// If found, return the previous value; otherwise, return the last value.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_le( IntPtr cursor, ref LmdbBuffer key, out LmdbBuffer data );

        /// <summary>
        /// Lookup the first key the is equal to the lookup key and return the value or error.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_eq( IntPtr cursor, ref LmdbBuffer key, out LmdbBuffer data );

        /// <summary> Lookup the first key greater than or equal to specified key.
        /// If found, return the value; otherwise, return an error code.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_ge( IntPtr cursor, ref LmdbBuffer key, out LmdbBuffer data );

        /// <summary>
        /// Lookup the first key greater than or equal to specified key. If found, lookup the first equal
        /// key and return the next value; otherwise, return <see cref="NativeResultCode.NotFound"/>.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_gt( IntPtr cursor, ref LmdbBuffer key, out LmdbBuffer data );

        /// <summary>
        /// Lookup the first key greater than or equal to specified key. If found,
        /// Compare the data to the search data. If smaller or equal, return the previous value.
        /// Otherwise, get the last value of the found key.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [in,out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_lt_dup( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data );

        /// <summary>
        /// Position the key at the nearest data.  If success and if the value is 
        /// smaller than the reference value, return the previous value. Otherwise,
        /// return the last value.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [in,out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_le_dup( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data );

        /// <summary>
        /// Position the cursor at the matching key and data values and return these values. 
        /// Otherwise, return an error.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [in,out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_eq_dup( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data );

        /// <summary>
        /// Position the key at the nearest data and return the data or error.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [in,out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_ge_dup( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data );

        /// <summary>
        /// Position the key at the nearest data. Return the next value 
        /// if the search value is greater than or equal to the cursor value. Otherwise, 
        /// return the cursor data value or error.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="cursor">   A cursor handle returned by #mdb_cursor_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [in,out] The data. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_cursor_get_gt_dup( IntPtr cursor, ref LmdbBuffer key, ref LmdbBuffer data );

        /// <summary>   Set custom comparison function for unsigned 128 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint128( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 96 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint96( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 80 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint80( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 64 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint64( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 64 bit integer values.
        /// If first 64 bits are not zero then compare by them only, else ignore them and compare and use next 64 bits. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint64x64( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 48 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint48( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 32 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint32( IntPtr txn, uint dbi );

        /// <summary>   Set custom comparison function for unsigned 16 bit integer values. </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="txn">  A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">  A database handle returned by #mdb_dbi_open(). </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_set_dupsort_as_uint16( IntPtr txn, uint dbi );

        /// <summary>
        /// Runs a transaction and stores a <see cref="LmdbBuffer"/> Key value pair into a database .
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-16. </remarks>
        /// <param name="env">      The environment. </param>
        /// <param name="dbi">      A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="key">      [in,out] The key. </param>
        /// <param name="data">     [in,out] The data. </param>
        /// <param name="flags">    The flags. </param>
        /// <returns>   Result Code: A non-zero error value on failure and 0 on success. </returns>
        [DllImport( _LibName, CallingConvention = CallingConvention.Cdecl )]
        public static extern int sdb_put( IntPtr env, uint dbi, ref LmdbBuffer key, ref LmdbBuffer data, TransactionPutOptions flags );

        /// <summary>   Sets custom comparison function. </summary>
        /// <remarks>   David, 2020-12-24. </remarks>
        /// <exception cref="NotSupportedException">    Thrown when the requested operation is not
        ///                                             supported. </exception>
        /// <param name="txn">              A transaction handle returned by #mdb_txn_begin(). </param>
        /// <param name="dbi">              A database handle returned by #mdb_dbi_open(). </param>
        /// <param name="dupSortPrefix">    The duplicate sort prefix for identifying the comparison
        ///                                 function. </param>
        /// <returns>   An int. </returns>
        public static int SetCustomComparisonFunction( IntPtr txn, uint dbi, int dupSortPrefix )
        {
            int ret = 0;
            if ( dupSortPrefix > 0 )
            {
                ret = dupSortPrefix == 64 * 64
                    ? SafeNativeMethods.sdb_set_dupsort_as_uint64x64( txn, dbi )
                    : dupSortPrefix == 128
                        ? SafeNativeMethods.sdb_set_dupsort_as_uint128( txn, dbi )
                        : dupSortPrefix == 96
                            ? SafeNativeMethods.sdb_set_dupsort_as_uint96( txn, dbi )
                            : dupSortPrefix == 80
                                ? SafeNativeMethods.sdb_set_dupsort_as_uint80( txn, dbi )
                                : dupSortPrefix == 64
                                    ? SafeNativeMethods.sdb_set_dupsort_as_uint64( txn, dbi )
                                    : dupSortPrefix == 48
                                        ? SafeNativeMethods.sdb_set_dupsort_as_uint48( txn, dbi )
                                        : dupSortPrefix == 32
                                            ? SafeNativeMethods.sdb_set_dupsort_as_uint32( txn, dbi )
                                            : dupSortPrefix == 16
                                                ? SafeNativeMethods.sdb_set_dupsort_as_uint16( txn, dbi )
                                                : throw new NotSupportedException( $"Unhandled {nameof( dupSortPrefix )} of {dupSortPrefix}; Rethink your design if you need this!" );
            }
            return ret;
        }

        #endregion " LOOKUP EXTENSIONS TO LMDB LIBRARY BY Victor Baybecov "

#pragma warning restore IDE1006 // Naming Styles

    }
}
