using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests.Framework
{
    /// <summary>   LMDB Exception unit tests. </summary>
    /// <remarks>   David, 2020-12-14. </remarks>
    [Obsolete("Exception no longer serializable")]
    public class LmdbExceptionTests
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="output">   The output. </param>
        public LmdbExceptionTests( ITestOutputHelper output )
        {
            this._Output = output;
        }

        private const string _Message = "Access denied.";
        private const int _ResultCode = 5;
        private const NativeResultCode _NativeResultCode = NativeResultCode.AccessDenied;

        /// <summary>   ( unit test method ) Test Serializing LMDB exception. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        [Fact]
        public void SerializeLmdbException()
        {

            // construct a new LMDB exception 
            LmdbException ex = new LmdbException( LmdbExceptionTests._ResultCode, LmdbExceptionTests._Message );

            // Sanity check: Make sure custom properties are set before serialization
            Assert.Equal( LmdbExceptionTests._Message, ex.Message );
            Assert.Equal( LmdbExceptionTests._ResultCode, ex.ErrorCode );
            Assert.Equal( ( int )LmdbExceptionTests._NativeResultCode, ex.ErrorCode );

            // Save the full ToString() value, including the exception message and stack trace.
            string exceptionToString = ex.ToString();

            // Round-trip the exception: Serialize and deserialize with a BinaryFormatter
            BinaryFormatter bf = new BinaryFormatter();
            using ( MemoryStream ms = new MemoryStream() )
            {
#if false
                // Write object state: This works.
                string exString = System.Text.Json.JsonSerializer.Serialize( ex );
                BinaryWriter bw = new BinaryWriter( ms );
                bw.Write( exString );
#else
#pragma warning disable SYSLIB0011 // Type or member is obsolete
                bf.Serialize( ms, ex );
#pragma warning restore SYSLIB0011 // Type or member is obsolete
#endif

                // use the same stream to read the exception
                ms.Seek( 0, 0 );
#if false
                // Read object state
                BinaryReader br = new BinaryReader( ms );
                exString = br.ReadString();
                // This does not handle the message property. In fact, it seems that the class
                // deserialization constructor is not called. 
                ex = System.Text.Json.JsonSerializer.Deserialize<LmdbException>( exString );
#else
#pragma warning disable SYSLIB0011 // Type or member is obsolete
                ex = ( LmdbException ) bf.Deserialize( ms );
#pragma warning restore SYSLIB0011 // Type or member is obsolete
#endif
            }

            // Make sure custom properties are preserved after serialization
            Assert.Equal( LmdbExceptionTests._Message, ex.Message );
            Assert.Equal( LmdbExceptionTests._ResultCode, ex.ErrorCode );
            Assert.Equal( ( int )LmdbExceptionTests._NativeResultCode, ex.ErrorCode );

            // Double-check that the exception message and stack trace (owned by the base Exception) are preserved
            Assert.Equal( exceptionToString, ex.ToString() );

        }

    }
}
