using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests.Framework
{
    /// <summary>   LMDB Assembly unit tests. </summary>
    /// <remarks>   David, 2020-12-14. </remarks>
    public class LmdbAssemblyTests
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="output">   The output. </param>
        public LmdbAssemblyTests( ITestOutputHelper output )
        {
            this._Output = output;
        }

        /// <summary>   ( unit test method ) Reads LMDB version information. </summary>
        /// <remarks>   David, 2020-12-19. </remarks>
        [Fact]
        public void ReadLmdbVersionInformation()
        {
            this._Output.WriteLine( $"Version.{nameof(LmdbVersionInfo.Major)} {LmdbVersionInfo.Major}" );
            Assert.Equal( 0, LmdbVersionInfo.Major );
            this._Output.WriteLine( $"Version.{nameof( LmdbVersionInfo.Minor )} {LmdbVersionInfo.Minor}" );
            Assert.Equal( 9, LmdbVersionInfo.Minor );
            this._Output.WriteLine( $"Version.{nameof( LmdbVersionInfo.Patch )} {LmdbVersionInfo.Patch}" );
            Assert.Equal( 70, LmdbVersionInfo.Patch );
            this._Output.WriteLine( $"Version.{nameof( LmdbVersionInfo.Patch )} {LmdbVersionInfo.Patch}" );
            Assert.Equal( "0.9.70", LmdbVersionInfo.Version.ToString() );
        }
    }
}
