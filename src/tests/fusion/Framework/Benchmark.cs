using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A utility to benchmark code snippets inside a using block. </summary>
    /// <remarks>   
    /// Remark added by David, 2020-12-30. <para>
    /// Adapted for XUnit testing. </para><para>
    /// (c) 2018 Victor Baybecov. All rights reserved. </para><para>
    /// </remarks>
    public static class Benchmark
    {

        /// <summary>
        /// Returns an <see cref="IDisposable"/> structure that starts benchmarking and stops it when its
        /// Dispose method is called. Prints benchmark results on disposal unless the silent parameter is
        /// not set to true.
        /// </summary>
        /// <remarks>   Remarked by David, 2020-12-30. </remarks>
        /// <param name="writer">           The writer. </param>
        /// <param name="caseName">         Benchmark case. </param>
        /// <param name="innerLoopCount">   (Optional) Number of iterations to calculate performance. </param>
        /// <param name="silent">           (Optional) True to mute output during disposal. </param>
        /// <returns>
        /// A disposable structure that measures time and memory allocations until disposed.
        /// </returns>
        public static Stat Run( Xunit.Abstractions.ITestOutputHelper writer, string caseName, long innerLoopCount = 1, bool silent = false )
        {
            var sw = Interlocked.Exchange( ref Benchmark._Sw, null );
            sw ??= new Stopwatch();

            var stat = new Stat( writer, caseName, sw, innerLoopCount, silent );

            return stat;
        }

        #region " PRINT "

        /// <summary>   A <see cref="ConcurrentDictionary{TKey, TValue}"/> holding the statistics for the . </summary>
        private static readonly ConcurrentDictionary<string, List<Stat>> Stats = new ConcurrentDictionary<string, List<Stat>>();

        /// <summary>   Print header. </summary>
        /// <remarks>   Remarked by David, 2020-12-30. </remarks>
        /// <param name="summary">      . </param>
        /// <param name="caller">       A description of the benchmark that is printed above the table. </param>
        /// <param name="caseLength">   (Optional) Length of the case. </param>
        /// <param name="unit">         (Optional) Overwrite default MOPS unit of measure. </param>
        private static void PrintHeader( Xunit.Abstractions.ITestOutputHelper writer, string summary, string caller, int? caseLength = null, string unit = null )
        {
            if ( !string.IsNullOrWhiteSpace( caller ) ) { writer.WriteLine( $"**{caller}**" ); }
            if ( !string.IsNullOrWhiteSpace( summary ) ) { writer.WriteLine( $"*{summary}*" ); }
            writer.WriteLine( GetHeader( caseLength, unit ) );
        }

        /// <summary>   Gets a header. </summary>
        /// <remarks>   Remarked by David, 2020-12-30. </remarks>
        /// <param name="caseLength">   (Optional) Length of the case. </param>
        /// <param name="unit">         (Optional) Overwrite default MOPS unit of measure. </param>
        /// <returns>   The header. </returns>
        internal static string GetHeader( int? caseLength = null, string unit = null )
        {
            unit ??= "MOPS";
            var len = caseLength ?? 20;
            var caseHeader = "Case".PadRight( len );
            return $" {caseHeader,-20} | {"Count",8} | {unit,7} | {"Elapsed, us",11} | {"GC0",5} | {"GC1",5} | {"GC2",5} | {"Memory, MB",7} |";
        }

        /// <summary>   Print a table with average benchmark results for all cases. </summary>
        /// <remarks>   Remarked by David, 2020-12-30. </remarks>
        /// <exception cref="ArgumentException">            Thrown when one or more arguments have
        ///                                                 unsupported or illegal values. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="writer">   The writer. </param>
        /// <param name="summary">  (Optional) </param>
        /// <param name="caller">   (Optional) A description of the benchmark that is printed above the
        ///                         table. </param>
        /// <param name="unit">     (Optional) Overwrite default MOPS unit of measure. </param>
        public static void Dump( Xunit.Abstractions.ITestOutputHelper writer, string summary = "", [CallerMemberName] string caller = "", string unit = null )
        {
            var maxLength = Stats.Keys.Select( k => k.Length ).Max();

            PrintHeader( writer, summary, caller, maxLength, unit );

            var stats = Stats.Select( GetAverages ).OrderByDescending( s => s.MOPS );

            foreach ( var stat in stats )
            {
                writer.WriteLine( stat.ToString( maxLength ) );
            }

            Stats.Clear();

            static Stat GetAverages( KeyValuePair<string, List<Stat>> kvp )
            {
                if ( kvp.Value == null ) throw new ArgumentException( $"Null stat list for the case: {kvp.Key}" );
                if ( kvp.Value.Count == 0 ) throw new InvalidOperationException( $"Empty stat list for the case: {kvp.Key}" );
                var skip = kvp.Value.Count > 1
                    ? (kvp.Value.Count >= 10 ? 3 : 1)
                    : 0;
                var values = kvp.Value.Skip( skip ).ToList();

                var elapsed = values.Select( l => ( double ) l.StatSnapshot.Elapsed ).Average();
                var gc0 = values.Select( l => l.StatSnapshot.Gc0 ).Average();
                var gc1 = values.Select( l => l.StatSnapshot.Gc1 ).Average();
                var gc2 = values.Select( l => l.StatSnapshot.Gc2 ).Average();
                var memory = values.Select( l => l.StatSnapshot.Memory ).Average();

                var result = kvp.Value.First();

                result.StatSnapshot.Elapsed = ( long ) elapsed;
                result.StatSnapshot.Gc0 = gc0;
                result.StatSnapshot.Gc1 = gc1;
                result.StatSnapshot.Gc2 = gc2;
                result.StatSnapshot.Memory = memory;

                return result;
            }
        }

        #endregion

        /// <summary>
        /// Disable output regardless of individual <see cref="Run"/> method parameters.
        /// </summary>
        public static bool ForceSilence { get; set; }

        /// <summary>   The stopwatch timer timing the operations. </summary>
        private static Stopwatch _Sw;

        /// <summary>
        /// Benchmark run statistics.
        /// </summary>
        public struct Stat : IDisposable
        {

            /// <summary>   True if header is printed. </summary>
            private bool _HeaderIsPrinted;

            public const int TicksPerMicrosecond = 10;

            public const int TicksPerMillisecond = 10000;

            public Xunit.Abstractions.ITestOutputHelper Writer { get; }

            public string CaseName { get; }

            public Stopwatch Stopwatch { get; }

            public long InnerLoopCount { get; }

            private readonly bool _Silent;

            private readonly string _Unit;

            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2020-12-30. </remarks>
            /// <param name="writer">           The writer. </param>
            /// <param name="caseName">         Name of the case. </param>
            /// <param name="sw">               The software. </param>
            /// <param name="innerLoopCount">   Number of inner loops. </param>
            /// <param name="silent">           (Optional) True to silent. </param>
            /// <param name="unit">             (Optional) The unit. </param>
            internal Stat( Xunit.Abstractions.ITestOutputHelper writer, string caseName, Stopwatch sw, long innerLoopCount, bool silent = false, string unit = null )
            {
                this.Writer = writer;
                this.CaseName = caseName;
                this.Stopwatch = sw;
                this.InnerLoopCount = innerLoopCount;
                this._Silent = silent;
                this._Unit = unit;
                this.StatSnapshot = new StatSnapshot( this.Stopwatch, true );
                this._HeaderIsPrinted = false;
            }

            /// <inheritdoc/>
            public void Dispose()
            {
                var statEntry = new StatSnapshot( this.Stopwatch, false );
                _ = Interlocked.Exchange( ref _Sw, this.Stopwatch );

                this.StatSnapshot.Elapsed = statEntry.Elapsed;
                this.StatSnapshot.Gc0 = statEntry.Gc0 - this.StatSnapshot.Gc0;
                this.StatSnapshot.Gc1 = statEntry.Gc1 - this.StatSnapshot.Gc1;
                this.StatSnapshot.Gc2 = statEntry.Gc2 - this.StatSnapshot.Gc2;
                this.StatSnapshot.Memory = statEntry.Memory - this.StatSnapshot.Memory;

                var list = Stats.GetOrAdd( this.CaseName, ( s1 ) => new List<Stat>() );
                list.Add( this );

                if ( !this._Silent && !ForceSilence )
                {
                    if ( !this._HeaderIsPrinted )
                    {
                        Benchmark.PrintHeader( this.Writer, null, null, unit: this._Unit );
                        this._HeaderIsPrinted = true;
                    }
                    this.Writer.WriteLine( this.ToString() );
                }
            }

            /// <summary>
            /// Million operations per second.
            /// </summary>
            public double MOPS => Math.Round( (this.InnerLoopCount * 0.001) / (this.StatSnapshot.Elapsed / 10000.0), 3 );

            internal StatSnapshot StatSnapshot { get; set; }

            /// <inheritdoc />
            public override string ToString()
            {
                var trimmedCaseName = this.CaseName.Length > 20 ? this.CaseName.Substring( 0, 17 ) + "..." : this.CaseName;
                return $"{trimmedCaseName,-20} | {this.InnerLoopCount,8:N0} | {this.MOPS,7:f3} | {this.StatSnapshot.Elapsed / TicksPerMicrosecond,11:N0} | {this.StatSnapshot.Gc0,5:f1} | {this.StatSnapshot.Gc1,5:f1} | {this.StatSnapshot.Gc2,5:f1} | {this.StatSnapshot.Memory / (1024 * 1024.0),10:f3} |";
            }

            /// <summary>   Convert this object into a string representation. </summary>
            /// <remarks>   David, 2020-12-30. </remarks>
            /// <param name="caseAlignmentLength">  Length of the case alignment. </param>
            /// <returns>   A string that represents this object. </returns>
            internal string ToString( int caseAlignmentLength )
            {
                var paddedCaseName = this.CaseName.PadRight( caseAlignmentLength );
                return $"{paddedCaseName,-20} | {this.InnerLoopCount,8:N0} | {this.MOPS,7:f3} | {this.StatSnapshot.Elapsed / TicksPerMicrosecond,11:N0} | {this.StatSnapshot.Gc0,5:f1} | {this.StatSnapshot.Gc1,5:f1} | {this.StatSnapshot.Gc2,5:f1} | {this.StatSnapshot.Memory / (1024 * 1024.0),10:f3} |";
            }
        }

        /// <summary>   A stat snapshot. </summary>
        /// <remarks>   Remarked by David, 2020-12-30. </remarks>
        public class StatSnapshot
        {
            public StatSnapshot( Stopwatch sw, bool start )
            {

                if ( !start )
                {
                    // end of measurement, first stop timer then collect/count
                    sw.Stop();
                    this.Elapsed = sw.Elapsed.Ticks;

                    // NB we exclude forced GC from counters,
                    // by measuring memory before forced GC we could
                    // calculate uncollected garbage
                    this.Memory = GC.GetTotalMemory( false );
                }

                //GC.Collect(2, GCCollectionMode.Forced, true);
                //GC.WaitForPendingFinalizers();
                //GC.Collect(2, GCCollectionMode.Forced, true);
                //GC.WaitForPendingFinalizers();


                this.Gc0 = GC.CollectionCount( 0 );
                this.Gc1 = GC.CollectionCount( 1 );
                this.Gc2 = GC.CollectionCount( 2 );

                if ( start )
                {
                    this.Memory = GC.GetTotalMemory( false );
                    // start timer after collecting GC stat
                    sw.Restart();
                }
            }

            /// <summary>   Gets or sets the elapsed time. </summary>
            /// <value> The elapsed. </value>
            public long Elapsed { get; set; }

            /// <summary>   Gets or sets the number of times garbage collection has occurred for the '0' generation of objects. </summary>
            /// <value> The number of times garbage collection has occurred for the '0' generation of objects. </value>
            public double Gc0 { get; set; }

            /// <summary>   Gets or sets the number of times garbage collection has occurred for the '1' generation of objects. </summary>
            /// <value> The number of times garbage collection has occurred for the '1' generation of objects. </value>
            public double Gc1 { get; set; }

            /// <summary>   Gets or sets the number of times garbage collection has occurred for the '2' generation of objects. </summary>
            /// <value> The number of times garbage collection has occurred for the '2' generation of objects. </value>
            public double Gc2 { get; set; }

            /// <summary>   Gets or sets the number of bytes currently thought to be allocated. </summary>
            /// <value> The number of bytes currently thought to be allocated. </value>
            public double Memory { get; set; }
        }

        /// <summary>   A console writer. </summary>
        /// <remarks>   David, 2020-12-30. </remarks>
        public class ConsoleWriter : Xunit.Abstractions.ITestOutputHelper
        {

            public void WriteLine( string message )
            {
                Console.WriteLine( message );
            }

            public void WriteLine( string format, params object[] args )
            {
                Console.WriteLine( format, args );
            }
        }

    }

}
