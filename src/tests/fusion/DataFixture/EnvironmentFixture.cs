using System;
using System.IO;

using Xunit;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   An environment fixture. </summary>
    /// <remarks>
    /// Remark added by David, 2020-12-11. A single test context to be shared among tests in several
    /// test classes, and cleaned up after all the tests in the test classes have finished.
    /// </remarks>
    public class EnvironmentFixture : IDisposable
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public EnvironmentFixture()
        {

            var env = new LmdbEnvironment( );
            env.SetMapSize( 1000000000 );
            env.MaxDatabases = 10;

            // TO_DO: Check how to reduce the number of readers on the environment. 
            // Iterate over key range test failed on 10 readers whereas before fusing the Lightning
            // code, it would not fail. Possibly, transactions are not releasing the cursor fast enough.
            env.MaxReaders = 100;


            // create the default directory and open the environment for databases to that path.
            env.Open( FixtureInfo.CreateFixtureDirectory( FixtureInfo.DatabaseDirName ), EnvironmentOpenOptions.NoThreadLocalStorage );

            this.Env = env;
            this.Buffers = new BufferPool();
        }


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.Env?.Dispose();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>   Gets the environment. </summary>
        /// <value> The environment. </value>
        public LmdbEnvironment Env { get; }

        /// <summary>   Gets the buffers. </summary>
        /// <value> The buffers. </value>
        public BufferPool Buffers { get; }

    }

    /// <summary>   Defines the <see cref="EnvironmentFixture"/> Collections. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [CollectionDefinition( nameof( EnvironmentCollection ) )]
    public class EnvironmentCollection : ICollectionFixture<EnvironmentFixture>
    {
        // This class has no code, and is never created. Its purpose is simply to be
        // the place to apply [CollectionDefinition] and all the ICollectionFixture<> interfaces. 
    }
}
