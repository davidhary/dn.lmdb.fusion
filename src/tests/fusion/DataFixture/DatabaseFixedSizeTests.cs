using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A fixed size database tests. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [Collection( nameof( FixedSizeDatabaseCollection ) )]
    public class DatabaseFixedSizeTests
    {
        /// <summary>   The fixture. </summary>
        private readonly FixedSizeDatabaseFixture _Fixture;

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="fixture">  The fixture. </param>
        /// <param name="output">   The output. </param>
        public DatabaseFixedSizeTests( FixedSizeDatabaseFixture fixture, ITestOutputHelper output )
        {
            this._Fixture = fixture;
            this._Output = output;
        }

        /// <summary>   Puts the multiple. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void PutMultiple()
        {
            var putBuffer = ( Span<byte> ) new byte[4096];

            using ( var txn = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
            {
                using ( var cur = Cursor.OpenCursor( this._Fixture.Db, txn ) ) //  this._Fixture.Db.OpenFixedMultiValueCursor( tx ) )
                {
                    foreach ( var testEntry in this._Fixture.TestData )
                    {
                        var key = testEntry.Key;
                        var keyBytes = BitConverter.GetBytes( key );
                        var putData = testEntry.Value;

                        for ( int indx = 0; indx < putData.Count; indx++ )
                        {
                            var putDataItem = MemoryMarshal.AsBytes( ( ReadOnlySpan<char> ) putData[indx] );
                            // var putSpan = putBuffer.Slice( indx * putDataItem.Length );
                            var putSpan = putBuffer[(indx * putDataItem.Length)..];
                            putDataItem.CopyTo( putSpan );
                        }

                        var itemCount = putData.Count;
                        if ( !cur.TryPutMultiple( keyBytes, putBuffer, ref itemCount ) )
                            break;
                    }
                }
                txn.Commit();
            }

            var testGetData = new Dictionary<int, List<string>>();
            int recordSize = this._Fixture.Db.Config.FixedDataSize;

            using ( var txn = this._Fixture.Env.BeginReadOnlyTransaction( TransactionBeginOptions.None ) )
            {
                using ( var cur = Cursor.OpenCursor( this._Fixture.Db, txn ) ) //  this._Fixture.Db.OpenFixedMultiValueCursor( tx ) )
                {
                    foreach ( var entry in cur.ForwardByKey )
                    {
                        int key = BitConverter.ToInt32( entry.Key );
                        var tempList = new List<string>();
                        testGetData[key] = tempList;

                        foreach ( var dupData in cur.ForwardMultiple )
                        {
                            int count = dupData.Length / recordSize;
                            for ( int indx = 0; indx < count; indx++ )
                            {
                                var dupDataItem = dupData.Slice( indx * recordSize, recordSize );
                                var dupDataChars = MemoryMarshal.Cast<byte, char>( dupDataItem );
                                tempList.Add( new string( dupDataChars ) );
                            }
                        }
                    }
                }
                txn.Commit();
            }

            // the DB sorts the duplicate records, so we need to sort the input data for comparison
            var testCompData = new Dictionary<int, List<string>>( this._Fixture.TestData );
            foreach ( var entry in testCompData )
            {
                entry.Value.Sort();
            }

            Assert.Equal( testCompData, testGetData );
        }
    }
}
