using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using isr.Lmdb.Fusion.Interop;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A database tests. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [Collection( nameof( EnvironmentCollection ) )]
    public class DatabaseDataFixtureTests
    {
        /// <summary>   The fixture. </summary>
        private readonly EnvironmentFixture _Fixture;

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="fixture">  The fixture. </param>
        /// <param name="output">   The output. </param>
        public DatabaseDataFixtureTests( EnvironmentFixture fixture, ITestOutputHelper output )
        {
            this._Fixture = fixture;
            this._Output = output;
        }

        /// <summary>   Opens the database. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void OpenDatabase()
        {
            string expectedVersion = "0.9.70";
            string actualVersion = isr.Lmdb.Fusion.LmdbVersionInfo.Version.ToString();
            AssertX.Equal( expectedVersion, actualVersion , $"LMDB Library version {actualVersion} should be {expectedVersion}" );

            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            using ( var tx = this._Fixture.Env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb1", config );
                tx.Commit();
            }

            using ( var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
            {
                dbase.DropThrow( tx );
                tx.Commit();
            }

            // release the database handle and remove the database dispose action from the Environment disposing event invocation list.
            dbase.Dispose();

        }

        /// <summary>   Information describing the test. </summary>
        private const string _TestData = "Test Data";

        /// <summary>   Basic store retrieve. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void BasicStoreRetrieve()
        {
            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            using ( var tx = this._Fixture.Env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "SimpleStoreRetrieve", config );
                tx.Commit();
            }

            int key = 234;
            var keyBuf = BitConverter.GetBytes( key );
            string putData = _TestData;
            try
            {
                using ( var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    _ = dbase.Put( tx, keyBuf, Encoding.UTF8.GetBytes( putData ), DatabasePutOptions.None );
                    tx.Commit();
                }

                ReadOnlySpan<byte> getData;
                using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction( TransactionBeginOptions.None ) )
                {
                    Assert.True( dbase.Get( tx, keyBuf, out getData ) );
                    tx.Commit();
                }

                Assert.Equal( putData, Encoding.UTF8.GetString( getData ) );
            }
            finally
            {
                using var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None );
                dbase.DropThrow( tx );
                tx.Commit();
            }
        }

        /// <summary>   Simple store retrieve. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void SimpleStoreRetrieve()
        {
            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            Statistics stats;
            using ( var tx = this._Fixture.Env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb2", config );
                stats = dbase.GetStats( tx );
                tx.Commit();
            }

            this._Output.WriteLine( $"Entries: {stats.Entries}" );
            this._Output.WriteLine( $"Depth: {stats.BTreeDepth}" );
            this._Output.WriteLine( $"PageSize: {stats.PageSize}" );
            this._Output.WriteLine( $"BranchPages: {stats.BranchPages}" );
            this._Output.WriteLine( $"LeafPages: {stats.LeafPages}" );
            this._Output.WriteLine( $"OverflowPages: {stats.OverflowPages}" );

            var buffer = this._Fixture.Buffers.Acquire( 1024 );  // re-usable buffer

            var key1 = Guid.NewGuid();
            var key2 = Guid.NewGuid();
            try
            {
                // use the same buffer for some keys and data
                int bufPos = 0;
                var keySpan1 = new Span<byte>( buffer, bufPos, 16 );
                _ = key1.TryWriteBytes( keySpan1 );
                bufPos += 16;
                var keySpan2 = new Span<byte>( buffer, bufPos, 16 );
                _ = key2.TryWriteBytes( keySpan2 );
                bufPos += 16;
                // this one encoded as UTF-8
                int byteCount = Encoding.UTF8.GetBytes( _TestData, 0, _TestData.Length, buffer, bufPos );
                var putData2 = new ReadOnlySpan<byte>( buffer, bufPos, byteCount );
                // this one encoded as UTF-16, we can access the memory directly
                var putData1 = MemoryMarshal.AsBytes( _TestData.AsSpan() );

                using ( var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    _ = dbase.Put( tx, keySpan1, putData1, DatabasePutOptions.None );
                    _ = dbase.Put( tx, keySpan2, putData2, DatabasePutOptions.None );
                    tx.Commit();
                }

                ReadOnlySpan<byte> getData1;
                ReadOnlySpan<byte> getData2;
                using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction( TransactionBeginOptions.None ) )
                {
                    Assert.True( dbase.Get( tx, key1.ToByteArray(), out getData1 ) );
                    Assert.True( dbase.Get( tx, key2.ToByteArray(), out getData2 ) );
                    tx.Commit();
                }

                Assert.True( putData1.SequenceEqual( getData1 ) );
                Assert.Equal( _TestData, Encoding.UTF8.GetString( getData2 ) );
            }
            finally
            {
                this._Fixture.Buffers.Return( buffer );
                using var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None );
                dbase.DropThrow( tx );
                tx.Commit();
            }
        }

        /// <summary>   Int key compare. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A LmdbValue to process. </param>
        /// <param name="y">    A LmdbValue to process. </param>
        /// <returns>   An int. </returns>
        private static int IntKeyCompare( in ReadOnlySpan<byte> x, in ReadOnlySpan<byte> y )
        {
            var xInt = BitConverter.ToInt32( x.ToArray(), 0 );
            var yInt = BitConverter.ToInt32( y.ToArray(), 0 );
            return Comparer<int>.Default.Compare( xInt, yInt );
        }

        /// <summary>   Use compare function. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public void UseCompareFunction()
        {
            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create, new LmdbValue(), IntKeyCompare );
            Database dbase;
            using ( var tx = this._Fixture.Env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb3", config );
                tx.Commit();
            }

            var buffer = this._Fixture.Buffers.Acquire( 1024 );
            try
            {
                using ( var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    for ( int key = 0; key < 10; key++ )
                    {
                        var putData = $"Test Data {key}";
                        int byteCount = Encoding.UTF8.GetBytes( putData, 0, putData.Length, buffer, 0 );
                        _ = dbase.Put( tx, BitConverter.GetBytes( key ), new ReadOnlySpan<byte>( buffer, 0, byteCount ), DatabasePutOptions.None );
                    }
                    tx.Commit();
                }

                using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction( TransactionBeginOptions.None ) )
                {
                    for ( int key = 0; key < 10; key++ )
                    {
                        var compareData = $"Test Data {key}";
                        _ = dbase.Get( tx, BitConverter.GetBytes( key ), out ReadOnlySpan<byte> getData );
                        Assert.Equal( compareData, Encoding.UTF8.GetString( getData ) );

                        // check if two adjacent keys are comparing correctly when using the database's compare function
                        if ( key > 0 )
                        {
                            int compResult = dbase.Compare( tx, BitConverter.GetBytes( key ), BitConverter.GetBytes( key - 1 ) );
                            Assert.True( compResult > 0 );
                        }
                    }
                    tx.Commit();
                }
            }
            finally
            {
                this._Fixture.Buffers.Return( buffer );
                using var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None );
                dbase.DropThrow( tx );
                tx.Commit();
            }
        }

        //[UnmanagedCallersOnly(CallConvs = new[] { typeof(CallConvCdecl) })]
         
        
        /// <summary>   Int key compare fast. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="x">    A LmdbValue to process. </param>
        /// <param name="y">    A LmdbValue to process. </param>
        /// <returns>   An int. </returns>
        [UnmanagedCallersOnly]
        private static int IntKeyCompareFast( LmdbValue x, LmdbValue y )
        {
            var xInt = BitConverter.ToInt32( x.ToReadOnlySpan() );
            var yInt = BitConverter.ToInt32( y.ToReadOnlySpan() );
            return Comparer<int>.Default.Compare( xInt, yInt );
        }

#if NET5_0_OR_GREATER
        /// <summary>   Use compare function fast. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        [Fact]
        public unsafe void UseCompareFunctionFast()
        {
            delegate* unmanaged< LmdbValue, LmdbValue, int > unmanagedPtr = &IntKeyCompareFast;
            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create, unmanagedPtr );
            Database dbase;
            using ( var tx = this._Fixture.Env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb3", config );
                tx.Commit();
            }

            var buffer = this._Fixture.Buffers.Acquire( 1024 );
            try
            {
                using ( var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None ) )
                {
                    for ( int key = 0; key < 10; key++ )
                    {
                        var putData = $"Test Data {key}";
                        int byteCount = Encoding.UTF8.GetBytes( putData, 0, putData.Length, buffer, 0 );
                        _ = dbase.Put( tx, BitConverter.GetBytes( key ), new ReadOnlySpan<byte>( buffer, 0, byteCount ), DatabasePutOptions.None );
                    }
                    tx.Commit();
                }

                using ( var tx = this._Fixture.Env.BeginReadOnlyTransaction( TransactionBeginOptions.None ) )
                {
                    for ( int key = 0; key < 10; key++ )
                    {
                        var compareData = $"Test Data {key}";
                        _ = dbase.Get( tx, BitConverter.GetBytes( key ), out ReadOnlySpan<byte> getData );
                        Assert.Equal( compareData, Encoding.UTF8.GetString( getData ) );

                        // check if two adjacent keys are comparing correctly when using the database's compare function
                        if ( key > 0 )
                        {
                            int compResult = dbase.Compare( tx, BitConverter.GetBytes( key ), BitConverter.GetBytes( key - 1 ) );
                            Assert.True( compResult > 0 );
                        }
                    }
                    tx.Commit();
                }
            }
            finally
            {
                this._Fixture.Buffers.Return( buffer );
                using var tx = this._Fixture.Env.BeginTransaction( TransactionBeginOptions.None );
                dbase.DropThrow( tx );
                tx.Commit();
            }
        }
#endif
    }
}
