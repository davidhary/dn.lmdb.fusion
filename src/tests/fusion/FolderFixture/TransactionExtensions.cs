namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A transaction extensions. </summary>
    /// <remarks>   David, 2021-01-12. </remarks>
    public static class TransactionExtensions
    {

        /// <summary>   A Transaction extension method that puts. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="tx">       The transaction. </param>
        /// <param name="db">       The database. </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    [out] The value. </param>
        /// <returns>   An int. </returns>
        public static int Put( this Transaction tx, Database db, string key, string value )
        {
            var enc = System.Text.Encoding.UTF8;
            return tx.Put( db, enc.GetBytes( key ), enc.GetBytes( value ) );
        }

        /// <summary>   A Transaction extension method that gets. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="tx">   The transaction. </param>
        /// <param name="db">   The database. </param>
        /// <param name="key">  The key. </param>
        /// <returns>   A string. </returns>
        public static string Get( this Transaction tx, Database db, string key )
        {
            var enc = System.Text.Encoding.UTF8;
            var result = tx.Get( db, enc.GetBytes( key ) );
            return enc.GetString( result.value.CopyToNewArray() );
        }

        /// <summary>   A Transaction extension method that deletes this object. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="tx">   The transaction. </param>
        /// <param name="db">   The database. </param>
        /// <param name="key">  The key. </param>
        public static void Delete( this Transaction tx, Database db, string key )
        {
            var enc = System.Text.Encoding.UTF8;
            _ = tx.Delete( db, enc.GetBytes( key ) );
        }

        /// <summary>   A Transaction extension method that query if 'tx' contains key. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="tx">   The transaction. </param>
        /// <param name="db">   The database. </param>
        /// <param name="key">  The key. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool ContainsKey( this Transaction tx, Database db, string key )
        {
            var enc = System.Text.Encoding.UTF8;
            return tx.ContainsKey( db, enc.GetBytes( key ) );
        }

        /// <summary>   A Transaction extension method that attempts to get. </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="tx">       The transaction. </param>
        /// <param name="db">       The database. </param>
        /// <param name="key">      The key. </param>
        /// <param name="value">    [out] The value. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        public static bool TryGet( this Transaction tx, Database db, string key, out string value )
        {
            var enc = System.Text.Encoding.UTF8;
            var found = tx.TryGet( db, enc.GetBytes( key ), out byte[] result );
            value = enc.GetString( result );
            return found;
        }


    }
}
