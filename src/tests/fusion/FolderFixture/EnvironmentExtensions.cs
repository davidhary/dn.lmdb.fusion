using System;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   An environment extensions. </summary>
    /// <remarks>   David, 2021-01-12. </remarks>
    public static class EnvironmentExtensions
    {

        /// <summary>
        /// A LmdbEnvironment extension method that executes the 'cursor scenario' operation.
        /// </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="env">          The environment instance. </param>
        /// <param name="scenario">     The scenario. </param>
        /// <param name="openOptions">  (Optional) Options for opening the database. </param>
        /// <param name="beginOptions"> (Optional) Options for beginning the transaction. </param>
        public static void RunCursorScenario( this LmdbEnvironment env, Action<Transaction, Database, Cursor> scenario,
                                              DatabaseOpenOptions openOptions = DatabaseOpenOptions.Create,
                                              TransactionBeginOptions beginOptions = TransactionBeginOptions.None )
        {
            using var tx = env.BeginTransaction( beginOptions );
            using var db = tx.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = openOptions } );
            using var cursor = tx.CreateCursor( db );
            scenario( tx, db, cursor );
        }

        /// <summary>
        /// A LmdbEnvironment extension method that executes the 'transaction scenario' operation.
        /// </summary>
        /// <remarks>   David, 2021-01-12. </remarks>
        /// <param name="env">          The environment instance. </param>
        /// <param name="scenario">     The scenario. </param>
        /// <param name="openOptions">  (Optional) Options for opening the database. </param>
        /// <param name="beginOptions"> (Optional) Options for beginning the transaction. </param>
        public static void RunTransactionScenario( this LmdbEnvironment env, Action<Transaction, Database> scenario,
                                                   DatabaseOpenOptions openOptions = DatabaseOpenOptions.Create,
                                                   TransactionBeginOptions beginOptions = TransactionBeginOptions.None )
        {
            using var tx = env.BeginTransaction( beginOptions );
            using var db = tx.OpenDatabase( config: new DatabaseConfiguration { OpenOptions = openOptions } );
            scenario( tx, db );
        }

        /// <summary>
        /// A LmdbEnvironment extension method that executes the 'transaction scenario' operation.
        /// </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        /// <param name="env">          The environment instance. </param>
        /// <param name="scenario">     The scenario. </param>
        /// <param name="beginOptions"> (Optional) Options for beginning the transaction. </param>
        public static void RunTransactionScenario( this LmdbEnvironment env, Action<Transaction> scenario,
                                                   TransactionBeginOptions beginOptions = TransactionBeginOptions.None )
        {
            using var tx = env.BeginTransaction( beginOptions );
            scenario( tx );
        }

    }
}
