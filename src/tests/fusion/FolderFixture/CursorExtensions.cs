using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion.Tests
{
    /// <summary>   A cursor extensions. </summary>
    /// <remarks>   David, 2021-01-12. </remarks>
    public static class CursorExtensions
    {

        /// <summary>
        /// Enumerates the key/value pairs of the <see cref="Cursor"/> starting at the current position.
        /// </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        /// <param name="cursor">   <see cref="Cursor"/> </param>
        /// <returns>   <see cref="ValueTuple"/> key/value pairs of <see cref="LmdbBuffer"/> </returns>
        [MethodImpl( MethodImplOptions.AggressiveInlining )]
        public static IEnumerable<ValueTuple<LmdbBuffer, LmdbBuffer>> AsEnumerable( this Cursor cursor )
        {
            do
            {
                var (resultCode, key, value) = cursor.GetCurrent();
                if ( resultCode == ( int ) NativeResultCode.Success )
                {
                    yield return (key, value);
                }
            } while ( cursor.Next() == ( int )NativeResultCode.Success );
        }


    }
}
