using System;
using System.Linq;
using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{
    [Collection( nameof( FolderCollection ) )]
    public class IntegerKeyPrefixedValuesTests
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public IntegerKeyPrefixedValuesTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>   Stores key and prefixed-byte-array pairs. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        /// <param name="env">      The environment. </param>
        /// <param name="db">       The database. </param>
        /// <param name="key">      The key. </param>
        /// <param name="count">    Number of key value pairs </param>
        private static void StoreKeyPrefixedByteArrayPairs( LmdbEnvironment env, Database db, long key, int count )
        {
            var keyBytes = BitConverter.GetBytes( key );
            var KeyBytesHandle = GCHandle.Alloc( keyBytes, GCHandleType.Pinned );
            try
            {
                var kdb = new LmdbBuffer( BitConverter.GetBytes( key ) );
                var r = new Random();
                var buffer = new byte[64];
                using var tx = env.BeginTransaction();
                for ( int i = 0; i < count; i++ )
                {
                    long prefix = i + 1;
                    r.NextBytes( buffer );
                    var bytes = BitConverter.GetBytes( prefix ).Concat( buffer ).ToArray();
                    var valdb = new LmdbBuffer( bytes );
                    int res = db.Put( tx, ref kdb, ref valdb, TransactionPutOptions.AppendDuplicateData );
                    AssertX.Equal( 0, res, $"Result code should be zero" );
                }
                tx.Commit();
                tx.Dispose();
            }
            catch
            {
                throw;
            }
            finally
            {
                KeyBytesHandle.Free();
            }

        }

        /// <summary>   (Unit Test Method)  Could store key and prefixed-byte-array pairs. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void CouldStoreKeyPrefixedByteArrayPairs()
        {

            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 100 * 1024 * 1024 };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            using var db = env.OpenDatabase( "KeyByteArrayPairs",
                                             new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.DuplicatesFixed ) { DupSortPrefix = 64, FixedDataSize = 72 } );
            db.TruncateThrow();

            int count = 20;
            var key = 42L;
            StoreKeyPrefixedByteArrayPairs( env, db, key, count );
        }

        /// <summary>   (Unit Test Method)  Could lookup prefixed key byte array pairs. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void CouldLookupPrefixedKeyByteArrayPairs()
        {
            string testItem;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 100 * 1024 * 1024 };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            using var db = env.OpenDatabase( "KeyByteArrayPairs",
                                             new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.DuplicatesFixed ) { DupSortPrefix = 64, FixedDataSize = 72 } );
            db.TruncateThrow();

            int count = 20;
            var key = 42L;
            StoreKeyPrefixedByteArrayPairs( env, db, key, count );

            using var tx = env.BeginTransaction();
            using var c = Cursor.OpenCursor( db, tx );
            long expectedPrefix = 1L;
            long prefix = 1L;
            testItem = $"{nameof( Cursor.TryFindDup )}( {Lookup.EQ}, key = {key}, prefix )";
            Assert.True( c.TryFindDup( Lookup.EQ, ref key, ref prefix ), $"{testItem} should succeed" );
            AssertX.Equal( expectedPrefix, prefix, $"{testItem} prefix should match" );

            // c is now at prefix 1
            testItem = $"{nameof( Cursor.Get )}( key = {key}, prefix, {CursorGetOption.GetBothRange} )";
            while ( 0 == c.Delete( false ) )
            {
                // One way to detect when all values are deleted is to call the method with CursorGetOption.Set option.
                // We need to check is kdb exists, and it is deleted when the last dupsorted value is deleted.
                //if (!c.TryGet(ref key, ref prefix, CursorGetOption.Set))
                //{
                //    break;
                //}

                // We cannot call GetCurrent after deleting the last dupsorted value,
                // because GetCurrent does not move the cursor. It is the only cursor
                // operation (other then multi ops which are not supported by this lib so far)
                // that does not move the cursors, but depends on previous moves.
                // http://www.lmdb.tech/doc/group__mdb.html#ga1206b2af8b95e7f6b0ef6b28708c9127
                // If you want to move cursor to both key and dubsorted value then use
                // CursorGetOption.GetBoth for exact match, or CursorGetOption.GetBothRange, which
                // "positions at key, nearest data.", but does not specify nearest to which direction.
                // It's better to call Spreads's extension TryFindDup with Lookup option,
                // it behaves much more intuitively and does all required work on C side, saving P/Invoke calls.
                expectedPrefix += 1;
                if ( 0 == c.Get( ref key, ref prefix, CursorGetOption.GetBothRange ) )
                {
                    AssertX.Equal( expectedPrefix, prefix, $"{testItem} prefix should match" );
                }
                else
                {
                    // need to exit the loop if there are no more values, delete works on current value
                    // which is invalid after deleting the last one.
                    break;
                }
            }

        }
    }
}
