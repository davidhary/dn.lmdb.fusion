using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A multi process tests. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class MultiProcessTests
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public MultiProcessTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
            this.FolderFixture= folderFixture;
        }

        /// <summary>   Define process. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="arguments">    The arguments. </param>
        /// <returns>   The Process. </returns>
        private Process DefineProcess( string arguments )
        {
            var process = new Process {
                StartInfo = new ProcessStartInfo {
                    FileName = "dotnet",
                    Arguments = arguments,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = Directory.GetCurrentDirectory()
                }
            };

            process.EnableRaisingEvents = true;
            process.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler( this.Process_OutputDataReceived );
            process.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler( this.Process_ErrorDataReceived );
            process.Exited += new System.EventHandler( this.Process_Exited );

            return process;
        }

        /// <summary>   Saves the given file. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="path"> The path to save. </param>
        /// <returns>   A string. </returns>
        private static string Save( string path )
        {
            using var env = new LmdbEnvironment() { DirectoryPath = path };
            env.Open();
            var expected = "world";
            using var tx = env.BeginTransaction();
            using var db = tx.OpenDatabase();
            _ = tx.Put( db, Encoding.UTF8.GetBytes( "hello" ), Encoding.UTF8.GetBytes( expected ) );
            tx.Commit();
            return expected;
        }

        /// <summary>   Assert process standard output. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="process">  The process. </param>
        /// <param name="expected"> The expected. </param>
        private void AssertProcessStandardOutput( Process process, string expected )
        {
            var current = Process.GetCurrentProcess();
            _ = process.Start();
            Assert.NotEqual( current.Id, process.Id );

            var actual = process.StandardOutput.ReadLine();
            process.WaitForExit();
            this._Output.WriteLine( $"Expecting: {expected}" );
            Assert.Equal( expected, actual );
        }

        /// <summary>   Gets the full pathname of the process file. </summary>
        /// <value> The full pathname of the process file. </value>
        private static string ProcessPath => Path.GetFullPath( "../../../../Process/bin/Debug/net5.0/isr.Lmdb.Fusion.Tests.Process.dll" );

        /// <summary>   Can read process output. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [Fact]
        public void CanReadProcessOutput()
        {
            string outputOption = "/out:";
            var expected = "Hello-World!";
            string arguments = $"{MultiProcessTests.ProcessPath} {outputOption}{expected}";
            using var process = this.DefineProcess( arguments );
            this.AssertProcessStandardOutput( process, expected );
        }


        /// <summary>   Can load environment from multiple processes. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [Fact]
        public void CanLoadEnvironmentFromMultipleProcesses()
        {
            string pathOption = "/path:";
            var path = this.FolderFixture.CreateDirectoryForTest();
            string arguments = $"{MultiProcessTests.ProcessPath} {pathOption}{path}";
            using var process = this.DefineProcess( arguments );
            var expected = MultiProcessTests.Save( path );
            this.AssertProcessStandardOutput( process, expected );
        }


        /// <summary>   Event handler. Called by Process for exited events. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        private void Process_Exited( object sender, EventArgs e )
        {
            var p = ( Process ) sender;
            if ( p is object )
                this._Output.WriteLine( $"process (id = {p.Id}) exited with code {p.ExitCode}\n" );
        }

        /// <summary>   Event handler. Called by Process for error data received events. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Data received event information. </param>
        private void Process_ErrorDataReceived( object sender, DataReceivedEventArgs e )
        {
            this._Output.WriteLine( e.Data + "\n" );
        }

        /// <summary>   Event handler. Called by Process for output data received events. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Data received event information. </param>
        private void Process_OutputDataReceived( object sender, DataReceivedEventArgs e )
        {
            this._Output.WriteLine( e.Data + "\n" );
        }

    }
}

