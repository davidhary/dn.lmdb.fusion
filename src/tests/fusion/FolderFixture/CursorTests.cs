using System;
using static System.Text.Encoding;

using Xunit;
using Xunit.Abstractions;
using System.Linq;
using isr.Lmdb.Fusion.Interop;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A cursor tests. </summary>
    /// <remarks>   David, 2021-01-13. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class CursorTests : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; set;  }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public CursorTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.FolderFixture = null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #region " POPULATE CUROSR VALUES "

        /// <summary>   Populates a cursor values. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        /// <param name="cursor">       The cursor. </param>
        /// <param name="count">        (Optional) Number of values. </param>
        /// <param name="keyPrefix">    (Optional) The key prefix. </param>
        /// <returns>   A byte[][]. </returns>
        private static byte[][] PopulateCursorValues( Cursor cursor, int count = 5, string keyPrefix = "key" )
        {
            var keys = Enumerable.Range( 1, count )
                .Select( i => UTF8.GetBytes( keyPrefix + i ) )
                .ToArray();

            foreach ( var k in keys )
            {
                var result = cursor.Put( k, k, CursorPutOptions.None );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
            }

            return keys;
        }

        /// <summary>   Populates a multiple cursor values. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        /// <param name="cursor">   The cursor. </param>
        /// <param name="key">      (Optional) The key. </param>
        /// <returns>   A byte[][]. </returns>
        private static byte[][] PopulateMultipleCursorValues( Cursor cursor, string key = "TestKey" )
        {
            var values = Enumerable.Range( 1, 5 ).Select( BitConverter.GetBytes ).ToArray();
            var result = cursor.Put( UTF8.GetBytes( key ), values );
            Assert.Equal( ( int ) NativeResultCode.Success, result );

            var notDuplicate = values[0];
            result = cursor.Put( notDuplicate, notDuplicate, CursorPutOptions.NoDuplicateData );
            Assert.Equal( ( int ) NativeResultCode.Success, result );
            return values;
        }

        #endregion

        /// <summary>   (Unit Test Method) could open read-only cursor from write transaction. </summary>
        /// <remarks>   David, 2020-12-16. </remarks>
        [Fact]
        public void CouldOpenReadOnlyCursorFromWriteTransaction()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() ); 
            using var txn = env.BeginOpenDatabaseTransaction();
            using var db = txn.OpenDatabase( "first_db", new DatabaseConfiguration( DatabaseOpenOptions.Create ) );
            using var cursor = new Cursor( db, txn, true, db.Config.IsMultiValue, db.Config.FixedDataSize );
            using var cursor2 = Cursor.OpenCursor( db, txn );
        }

        /// <summary>   (Unit Test Method) Cursor should be created. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CursorShouldBeCreated()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => Assert.NotNull( c ) );
        }

        /// <summary>   (Unit Test Method) Cursor should put values. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CursorShouldPutValues()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                _ = PopulateCursorValues( c );
                tx.Commit();
            } );
        }

        /// <summary>   (Unit Test Method) Cursor should set span key. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CursorShouldSetSpanKey()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var keys = PopulateCursorValues( c );
                var firstKey = keys.First();
                var result = c.Set( firstKey.AsSpan() );
                Assert.Equal( ( int ) NativeResultCode.Success, result );

                var (resultCode, key, value) = c.GetCurrent();
                Assert.Equal( firstKey, key.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Cursor should move to last. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CursorShouldMoveToLast()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var keys = PopulateCursorValues( c );
                var lastKey = keys.Last();
                var result = c.Last();
                Assert.Equal( ( int ) NativeResultCode.Success, result );

                var (resultCode, key, value) = c.GetCurrent();
                Assert.Equal( lastKey, key.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Cursor should move to first. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CursorShouldMoveToFirst()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var keys = PopulateCursorValues( c );
                var firstKey = keys.First();
                var result = c.First();
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                var (resultCode, key, value) = c.GetCurrent();
                Assert.Equal( firstKey, key.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Should iterate through cursor. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldIterateThroughCursor()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var keys = PopulateCursorValues( c );
                using var c2 = tx.CreateCursor( db );
                _ = c2.First();
                var items = c2.AsEnumerable().Select( ( x, i ) => (x, i) ).ToList();
                foreach ( var (x, i) in items )
                {
                    Assert.Equal( keys[i], x.Item1.CopyToNewArray() );
                }

                Assert.Equal( keys.Length, items.Count );
            } );
        }

        /// <summary>   (Unit Test Method) Cursor should delete elements. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void CursorShouldDeleteElements()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var keys = PopulateCursorValues( c ).Take( 2 ).ToArray();
                _ = c.First();
                for ( var i = 0; i < 2; ++i )
                {

                    _ = c.Next();
                    _ = c.Delete();
                }

                using var c2 = tx.CreateCursor( db );
                _ = c2.First();
                Assert.DoesNotContain( c2.AsEnumerable(), x =>
                     keys.Any( k => x.Item1.CopyToNewArray() == k ) );
            } );
        }

        /// <summary>   (Unit Test Method) Should put multiple. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldPutMultiple()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => { _ = PopulateMultipleCursorValues( c ); },
                DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should get multiple. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldGetMultiple()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" );
                var keys = PopulateMultipleCursorValues( c );
                _ = c.Set( key );
                _ = c.NextDuplicate();
                var (resultCode, _, value) = c.GetMultiple();
                Assert.Equal( ( int ) NativeResultCode.Success, resultCode );
                Assert.Equal( keys, value.CopyToNewArray().Split( sizeof( int ) ).ToArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should get next multiple. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldGetNextMultiple()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" );
                var keys = PopulateMultipleCursorValues( c );
                _ = c.Set( key );
                var (resultCode, _, value) = c.NextMultiple();
                Assert.Equal( ( int ) NativeResultCode.Success, resultCode );
                Assert.Equal( keys, value.CopyToNewArray().Split( sizeof( int ) ).ToArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should advance key to closest when key not found. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldAdvanceKeyToClosestWhenKeyNotFound()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var expected = PopulateCursorValues( c ).First();
                var result = c.Set( UTF8.GetBytes( "key" ) );
                Assert.Equal( ( int ) NativeResultCode.NotFound, result );
                var (_, key, _) = c.GetCurrent();
                Assert.Equal( expected, key.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Should set key and get. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldSetKeyAndGet()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var expected = PopulateCursorValues( c ).ElementAt( 2 );
                var (resultCode, key, value) = c.SetKey( expected );
                Assert.Equal( ( int ) NativeResultCode.Success, resultCode );
                Assert.Equal( expected, key.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Should set key and get with span. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldSetKeyAndGetWithSpan()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var expected = PopulateCursorValues( c ).ElementAt( 2 );
                var (resultCode, key, value) = c.SetKey( expected.AsSpan() );
                Assert.Equal( ( int ) NativeResultCode.Success, resultCode );
                Assert.Equal( expected, key.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Should get both. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldGetBoth()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var expected = PopulateCursorValues( c ).ElementAt( 2 );
                var result = c.GetBoth( expected, expected );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should get both with span. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldGetBothWithSpan()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var expected = PopulateCursorValues( c ).ElementAt( 2 );
                var expectedSpan = expected.AsSpan();
                var result = c.GetBoth( expectedSpan, expectedSpan );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should move to previous. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldMoveToPrevious()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var expected = PopulateCursorValues( c ).ElementAt( 2 );
                var expectedSpan = expected.AsSpan();
                _ = c.GetBoth( expectedSpan, expectedSpan );
                var result = c.Previous();
                Assert.Equal( ( int ) NativeResultCode.Success, result );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should set range with span. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldSetRangeWithSpan()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var values = PopulateCursorValues( c );
                var firstAfter = values[0].AsSpan();
                var result = c.SetRange( firstAfter );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                var (resultCode, key, value) = c.GetCurrent();
                Assert.Equal( values[0], value.CopyToNewArray() );
            } );
        }

        /// <summary>   (Unit Test Method) Should get both range. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldGetBothRange()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" );
                var values = PopulateMultipleCursorValues( c );
                var result = c.GetBothRange( key, values[1] );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                var current = c.GetCurrent();
                Assert.Equal( values[1], current.value.CopyToNewArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should get both range with span. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldGetBothRangeWithSpan()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" ).AsSpan();
                var values = PopulateMultipleCursorValues( c );
                var result = c.GetBothRange( key, values[1].AsSpan() );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                var current = c.GetCurrent();
                Assert.Equal( values[1], current.value.CopyToNewArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should move to first duplicate. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldMoveToFirstDuplicate()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" );
                var values = PopulateMultipleCursorValues( c );
                var result = c.GetBothRange( key, values[1] );
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                result = c.FirstDuplicate();
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                var current = c.GetCurrent();
                Assert.Equal( values[0], current.value.CopyToNewArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should move to last duplicate. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldMoveToLastDuplicate()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" );
                var values = PopulateMultipleCursorValues( c );
                _ = c.Set( key );
                var result = c.LastDuplicate();
                Assert.Equal( ( int) NativeResultCode.Success, result );
                var current = c.GetCurrent();
                Assert.Equal( values[4], current.value.CopyToNewArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should move to next no duplicate. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldMoveToNextNoDuplicate()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var values = PopulateMultipleCursorValues( c );
                var result = c.NextNoDuplicate();
                Assert.Equal( ( int ) NativeResultCode.Success, result );
                var (resultCode, key, value) = c.GetCurrent();
                Assert.Equal( values[0], value.CopyToNewArray() );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) Should renew same transaction. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldRenewSameTransaction()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var result = c.Renew();
                Assert.Equal( ( int ) NativeResultCode.Success, result );
            }, DatabaseOpenOptions.DuplicatesFixed, TransactionBeginOptions.ReadOnly );
        }

        /// <summary>   (Unit Test Method) Should delete duplicates. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ShouldDeleteDuplicates()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            env.RunCursorScenario( ( tx, db, c ) => {
                var key = UTF8.GetBytes( "TestKey" );
                _ = PopulateMultipleCursorValues( c );
                _ = c.Set( key );
                _ = c.DeleteDuplicateData();
                var result = c.Set( key );
                Assert.Equal( ( int ) NativeResultCode.NotFound, result );
            }, DatabaseOpenOptions.DuplicatesFixed | DatabaseOpenOptions.Create );
        }

        /// <summary>   (Unit Test Method) could write <see cref="LmdbBuffer"/> byte array. </summary>
        /// <remarks>   David, 2020-12-16.<para>
        /// read: 1..4; written: 1..4</para> </remarks>
        [Fact]
        public void CouldWriteLmdbBufferByteArray()
        {

            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( CursorTests.CouldWriteLmdbBufferByteArray ), new DatabaseConfiguration( DatabaseOpenOptions.Create ) );

            var values = new byte[] { 1, 2, 3, 4 };

            env.Write( txn => {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                string written = $"written: {value.Span[0]}..{value.Span[3]}";
                LmdbBuffer value2 = default;

                testItemMessage = $"Writing {nameof( LmdbBuffer )} key value pair to {db.Name}";
                using ( var cursor = Cursor.OpenCursor( db, txn ) )
                {
                    Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ), testItemMessage );
                }

                testItemMessage = $"Reading {nameof( LmdbBuffer )} key value pair from {db.Name}";
                using ( var cursor = Cursor.OpenCursor( db, txn ) )
                {
                    Assert.True( 0 == cursor.Get( ref key, ref value2, CursorGetOption.SetKey ), testItemMessage );
                }

                // at times test when setting this value after executing the Put function.
                // string written = $"written: {value.Span[0]}..{value.Span[3]}";
                testItemMessage = $"read: {value2.Span[0]}..{value2.Span[3]}; {written}";
                Assert.True( values.SequenceEqual( value2.Span.ToArray() ), testItemMessage );

                txn.Commit();
            } );
        }

        /// <summary>   (Unit Test Method) Could write <see cref="LmdbBuffer"/> using string key. </summary>
        /// <remarks>
        /// David, 2021-01-04. <para>
        /// Executing Cursor.TryPut </para><para>
        /// Executing Cursor.TryGet </para><para>
        /// Count: 4; written: 4 </para><para>
        /// read: 1..4; written: 1..4  </para><para>
        /// </para>
        /// </remarks>
        [Fact]
        public void CouldWriteLmdbBufferStringKey()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( CursorTests.CouldWriteLmdbBufferStringKey ), new DatabaseConfiguration( DatabaseOpenOptions.Create ) );

            var keyString = "my_string_key";
            var values = new byte[] { 1, 2, 3, 4 };

            /*
            The fixed statement prevents the garbage collector from relocating a movable variable.
            The fixed statement is only permitted in an unsafe context. You can also use the fixed keyword to create fixed size buffers.
            The fixed statement sets a pointer to a managed variable and "pins" that variable during the execution of the statement.
            Pointers to movable managed variables are useful only in a fixed context.
            Without a fixed context, garbage collection could relocate the variables unpredictably.
            The C# compiler only lets you assign a pointer to a managed variable in a fixed statement.
            */
#if ( NETSTANDARD2_1_OR_GREATER || NET5_0_OR_GREATER )
            env.Write( txn => {
                var key = new LmdbBuffer( keyString );
                var value = new LmdbBuffer( values );
                LmdbBuffer value2 = default;

                testItemMessage = $"Executing {nameof( Fusion.Cursor )}.{nameof( Cursor.Put )}";
                this._Output.WriteLine( testItemMessage );
                using ( var cursor = Cursor.OpenCursor( db, txn ) )
                {
                    Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ), testItemMessage );
                }

                testItemMessage = $"Executing {nameof( Fusion.Cursor )}.{nameof( Cursor.Get )}";
                this._Output.WriteLine( testItemMessage );
                using ( var cursor = Cursor.OpenCursor( db, txn ) )
                {
                    Assert.True( 0 == cursor.Get( ref key, ref value2, CursorGetOption.SetKey ), testItemMessage );
                }

                testItemMessage = $"Count: {value2.Span.Length}; written: {value.Span.Length}";
                this._Output.WriteLine( testItemMessage );
                Assert.True( value2.Span.Length.Equals( value.Span.Length ), testItemMessage );

                testItemMessage = $"read: {value2.Span[0]}..{value2.Span[3]}; written: {value.Span[0]}..{value.Span[3]}";
                this._Output.WriteLine( testItemMessage );
                Assert.True( values.SequenceEqual( value2.Span.ToArray() ), testItemMessage );

                txn.Commit();
            } );
#endif

        }

        /// <summary>   (Unit Test Method) could write and read profile read path. </summary>
        /// <remarks>   David, 2020-12-16. </remarks>
        [Fact]
        public void CouldWriteLmdbBufferUsingEnvironmentFunctions()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path );
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            using var db = env.OpenDatabase( nameof( CursorTests.CouldWriteLmdbBufferUsingEnvironmentFunctions ), new DatabaseConfiguration( DatabaseOpenOptions.Create ) );

            var values = new byte[] { 1, 2, 3, 4 };

            env.Write( txn => {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                using ( var cursor = Cursor.OpenCursor( db, txn ) )
                {
                    Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ) );
                }
                txn.Commit();
            } );

            _ = env.Read( txn => {
                var key = new LmdbBuffer( values );
                var value = new LmdbBuffer( values );
                LmdbBuffer value2 = default;
                using var cursor = new Cursor( db, txn, true, db.Config.IsMultiValue, db.Config.FixedDataSize );
                _ = cursor.Get( ref key, ref value2, CursorGetOption.SetKey );
                Assert.True( values.SequenceEqual( value2.Span.ToArray() ) );
                return true;
            } );
        }

    }
}
