using System;

using isr.Lmdb.Fusion.Interop;

using Xunit;
using Xunit.Abstractions;


namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A database tests. </summary>
    /// <remarks>   David, 2021-01-13. </remarks>
    [Collection( nameof( FolderCollection ) )]
    public class DatabaseTests : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; set;  }

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="output">   The output. </param>
        public DatabaseTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                this.FolderFixture = null;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        /// <summary>   (Unit Test Method) Queries if a given database should open. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void DatabaseShouldOpen()
        {
            var config = new DatabaseConfiguration( DatabaseOpenOptions.Create );
            Database dbase;
            var path = this.FolderFixture.CreateDirectoryForTest();
            using var env = LmdbEnvironment.OpenNew( path ); //, EnvironmentOpenOptions.WriteMap );

            using ( var tx = env.BeginOpenDatabaseTransaction( TransactionBeginOptions.None ) )
            {
                dbase = tx.OpenDatabase( "TestDb1", config );
                Assert.True( dbase.IsOpen, $"{nameof( Database.IsOpen )}" );
                tx.Commit();
            }

            using ( var tx = env.BeginTransaction( TransactionBeginOptions.None ) )
            {
                dbase.DropThrow( tx );
                Assert.False( dbase.IsOpen, $"{nameof( Database.IsOpen )}" );
                Assert.False( dbase.SafeHandle.IsAllocated, $"{nameof( dbase.SafeHandle.IsAllocated )}" );
                tx.Commit();
            }
            // release the database handle the remove the database dispose function from the Environment disposing events.
            dbase.Dispose();
            Assert.False( env.HasDisposingEvents, $"{nameof( LmdbEnvironment.HasDisposingEvents )}" );
        }


        /// <summary>   (Unit Test Method) Database should be created. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseShouldBeCreated()
        {
            var dbName = "test";
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            using ( var txn = env.BeginTransaction() )
            using ( txn.OpenDatabase( dbName, new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } ) )
            {
                txn.Commit();
            }
            using ( var txn = env.BeginTransaction() )
            using ( var db = txn.OpenDatabase( dbName, new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.None } ) )
            {
                Assert.False( db.SafeHandle.Freed );
                txn.Commit();
            }
        }

        /// <summary>   (Unit Test Method) Database should be closed. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseShouldBeClosed()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            using var txn = env.BeginTransaction();
            using Database db = txn.OpenDatabase();
            db.Dispose();
            Assert.False( db.IsOpen );
        }

        /// <summary>   (Unit Test Method) Database from committed transaction should be accessable. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseFromCommittedTransactionShouldBeAccessable()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            Database db;
            using ( var committed = env.BeginTransaction() )
            {
                db = committed.OpenDatabase();
                committed.Commit();
            }

            using ( db )
            using ( var txn = env.BeginTransaction() )
            {
                _ = txn.Put( db, "key", 1.ToString() );
                txn.Commit();
            }
        }

        /// <summary>   (Unit Test Method) Named database name exists in master. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void NamedDatabaseNameExistsInMaster()
        {
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() ) ;

            string namedDatabase = "customdb";
            using ( var tx = env.BeginTransaction() )
            {
                var db = tx.OpenDatabase( namedDatabase, new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
                tx.Commit();
            }
            using ( var tx = env.BeginTransaction() )
            {
                var db = tx.OpenDatabase();
                using var cursor = tx.CreateCursor( db );
                _ = cursor.Next();
                Assert.Equal( namedDatabase, System.Text.Encoding.UTF8.GetString( cursor.GetCurrent().key.CopyToNewArray() ) );
            }
        }

        /// <summary>
        /// (Unit Test Method) Read Only transaction opened databases do not get reused.
        /// </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void ReadOnlyTransactionOpenedDatabasesDoNotGetReused()
        {
            //This is here to assert that previous issues with the way manager
            //classes (since removed) worked don't happen anymore.
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );

            using ( var txn = env.BeginTransaction() )
            using ( var db = txn.OpenDatabase( "custom", new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } ) )
            {
                _ = txn.Put( db, "hello", "world" );
                txn.Commit();
            }
            using ( var txn = env.BeginTransaction( TransactionBeginOptions.ReadOnly ) )
            {
                var db = txn.OpenDatabase( "custom" );
                var result = txn.Get( db, "hello" );
                Assert.Equal( "world", result );
            }
            using ( var txn = env.BeginTransaction( TransactionBeginOptions.ReadOnly ) )
            {
                var db = txn.OpenDatabase( "custom" );
                var result = txn.Get( db, "hello" );
                Assert.Equal( "world", result );
            }
        }

        /// <summary>   (Unit Test Method) Database should be dropped. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseShouldBeDropped()
        {
            Database db;
            string dbName = "notmaster";
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            using ( var txn = env.BeginTransaction() )
            {
                db = txn.OpenDatabase( dbName, new DatabaseConfiguration { OpenOptions = DatabaseOpenOptions.Create } );
                txn.Commit();
                txn.Dispose();
                db.Dispose();
            }

            using ( var txn = env.BeginTransaction() )
            {
                db = txn.OpenDatabase( dbName );
                db.DropThrow( txn );
                txn.Commit();
                txn.Dispose();
            }

            using ( var txn = env.BeginTransaction() )
            {
                var ex = Assert.Throws<LmdbException>( () => txn.OpenDatabase( dbName ) );
                Assert.Equal( ( int ) NativeResultCode.NotFound, ex.ErrorCode );
            }

        }

        /// <summary>   (Unit Test Method) Database should truncate. </summary>
        /// <remarks>   David, 2021-01-13. </remarks>
        [Fact]
        public void DatabaseShouldTruncate()
        {
            Database db;
            using var env = LmdbEnvironment.OpenNew( this.FolderFixture.CreateDirectoryForTest() );
            using ( var txn = env.BeginTransaction() )
            {
                db = txn.OpenDatabase();
                _ = txn.Put( db, "hello", "world" );
                txn.Commit();
                txn.Dispose();
            }

            using ( var txn = env.BeginTransaction() )
            {
                db = txn.OpenDatabase();
                _ = db.Truncate( txn );
                txn.Commit();
                txn.Dispose();
            }

            using ( var txn = env.BeginTransaction() )
            {
                db = txn.OpenDatabase();
                var (resultCode, _, _) = txn.Get( db, System.Text.Encoding.UTF8.GetBytes( "hello" ) );

                Assert.Equal( ( int ) NativeResultCode.NotFound, resultCode );
                txn.Commit();
                txn.Dispose();
            }
        }

        /// <summary>   (Unit Test Method) could store into a huge environment. </summary>
        /// <remarks>   David, 2020-12-16.  <para>
        /// LMDB 0.9.70: (December 19, 2015) </para><para>
        /// LmdbEnvironment.Statistics.Entries: 0  </para><para>
        /// Writing 100 of LmdbBuffer four byte values for key of four byte value
        /// LmdbEnvironment.Statistics.Entries: 1 </para><para>
        /// Database( db_reserve).Statistics.Entries: 1 </para><para>
        /// Time: 45-50 ms; was marked as Explicit long running. 
        /// </para> </remarks>
        [Fact]
        public void CouldStoreIntoEnvironment()
        {
            string testItemMessage;
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            long mapSize = 2 * 1024L * 1024 * 1024 * 1024L;
            using var env = new LmdbEnvironment() { MapSize = mapSize };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( LmdbEnvironment.IsOpen )}";
            testItemMessage = $"{nameof( LmdbEnvironment )} should be open ({testItemName}) after {nameof( LmdbEnvironment.Open )}";
            Assert.True( env.IsOpen, testItemMessage );

            var stat = env.GetStats();
            testItemName = $"{nameof( LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.Statistics )}.{nameof( isr.Lmdb.Fusion.Statistics.Entries )}";
            this._Output.WriteLine( $"{testItemName}: {stat.Entries}" );
            AssertX.Equal( 0, stat.Entries, testItemName );

            testItemName = $"{nameof( isr.Lmdb.Fusion.LmdbEnvironment )}.{nameof( isr.Lmdb.Fusion.LmdbEnvironment.MapSize )}";
            this._Output.WriteLine( $"{testItemName}: {env.MapSize }" );
            AssertX.Equal( mapSize, env.MapSize, testItemName );

            using var db = env.OpenDatabase( "db_reserve", new DatabaseConfiguration( DatabaseOpenOptions.Create ) );
            var keyBytes = new byte[] { 1, 2, 3, 4 };
            var values = new byte[] { 1, 2, 3, 4 };
            var count = 100;

            this._Output.WriteLine( $"Writing {count} of {nameof( LmdbBuffer )} four byte values for key of four byte value" );
            env.Write( txn => {
                var key = new LmdbBuffer( keyBytes );
                var value = new LmdbBuffer( values );
                for ( int i = 0; i < count; i++ )
                    using ( var cursor = Cursor.OpenCursor( db, txn ) )
                    {
                        Assert.True( 0 == cursor.Put( ref key, ref value, CursorPutOptions.None ), $"{nameof( Fusion.Cursor.Put )} #{i}" );
                    }
                txn.Commit();
            } );


            stat = db.GetStats();
            testItemName = $"{nameof( Database )}({db.Name}).{nameof( isr.Lmdb.Fusion.Statistics )}.{nameof( isr.Lmdb.Fusion.Statistics.Entries )}";
            this._Output.WriteLine( $"{testItemName}: {stat.Entries}" );
            AssertX.Equal( 1, stat.Entries, testItemName );
        }


    }
}
