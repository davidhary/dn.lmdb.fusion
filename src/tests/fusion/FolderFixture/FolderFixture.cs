using System;
using System.IO;
using System.Runtime.CompilerServices;

using Xunit;

namespace isr.Lmdb.Fusion.Tests
{

    /// <summary>   A folder fixture for defining the environment test folders. </summary>
    /// <remarks>   David, 2021-01-11. </remarks>
    public class FolderFixture : IDisposable
    {

        private const string _RootDirectory = "r:\\fusion";

        private readonly string _TempRootDir;

        private const string _TempRootDirName = "tests";

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-01-19. </remarks>
        public FolderFixture()
        {
            this._TempRootDir = Path.Combine( FolderFixture._RootDirectory, FolderFixture._TempRootDirName );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="disposing">    True to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( disposing )
                if ( Directory.Exists( this._TempRootDir ) )
                {
                    Directory.Delete( this._TempRootDir, true );
                }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>   Creates a directory for test using GUID. </summary>
        /// <remarks>   David, 2021-01-11. </remarks>
        /// <returns>   The new directory for test. </returns>
        public string CreateGuidDirectoryForTest()
        {
            var path = Path.Combine( this._TempRootDir, "TestDb", Guid.NewGuid().ToString() );
            _ = Directory.CreateDirectory( path );
            return path;
        }


        /// <summary>   Creates a directory for tests. </summary>
        /// <remarks>   David, 2021-01-19. </remarks>
        /// <param name="path">     The first path. </param>
        /// <param name="suffix">   The suffix. </param>
        /// <returns>   The path. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string CreateDirectoryForTest(string path, string suffix)
        {
            var newPath = $"{path}{suffix}";
            if ( Directory.Exists( newPath ) )
            {
                Directory.Delete( newPath, true );
            }
            if ( !Directory.Exists( newPath ) )
            {
                newPath = Directory.CreateDirectory( newPath ).FullName;
            }
            return newPath;
        }

        /// <summary>   Creates a directory for tests using the calling function folder and name. </summary>
        /// <remarks>   David, 2020-12-16. </remarks>
        /// <param name="groupPath">        (Optional) Full pathname of the calling function directory. </param>
        /// <param name="directoryTitle">   (Optional) The title of the test directory. </param>
        /// <param name="clear">            (Optional) True to clear. </param>
        /// <returns>   The path. </returns>
        public string CreateDirectoryForTest( [CallerFilePath] string groupPath = null, [CallerMemberName] string directoryTitle = null, bool clear = true )
        {
            var parentDirectoryTitle = Path.GetFileName( Path.GetFileNameWithoutExtension( groupPath ) );
            var path = Path.Combine( this._TempRootDir, parentDirectoryTitle, directoryTitle );
            if ( clear && Directory.Exists( path ) )
            {
                Directory.Delete( path, true );
            }
            if ( !Directory.Exists( path ) )
            {
                path = Directory.CreateDirectory( path ).FullName;
            }
            return path;
        }
    }

    /// <summary>   Defines the <see cref="FolderFixture"/> Collections. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    [CollectionDefinition( nameof( FolderCollection ), DisableParallelization = true )]
    public class FolderCollection : ICollectionFixture<FolderFixture>
    {
        // This class has no code, and is never created. Its purpose is simply to be
        // the place to apply [CollectionDefinition] and all the ICollectionFixture<> interfaces. 
    }
}
