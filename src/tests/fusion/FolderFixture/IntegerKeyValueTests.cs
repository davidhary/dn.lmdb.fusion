using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using isr.Lmdb.Fusion.Interop;

using Xunit;
using Xunit.Abstractions;

namespace isr.Lmdb.Fusion.Tests
{
    [Collection( nameof( FolderCollection ) )]
    public class IntegerKeyValueTests 
    {

        /// <summary>   The output. </summary>
        private readonly ITestOutputHelper _Output;

        /// <summary>   Gets the folder fixture. </summary>
        /// <value> The folder fixture. </value>
        private FolderFixture FolderFixture { get; }

        /// <summary>   Constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        /// <param name="folderFixture">    The folder fixture. </param>
        /// <param name="output">           The output. </param>
        public IntegerKeyValueTests( FolderFixture folderFixture, ITestOutputHelper output )
        {
            this.FolderFixture = folderFixture;
            this._Output = output;
        }

        /// <summary>   Writes the integers. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        /// <param name="db">       The database. </param>
        /// <param name="count">    Number of. </param>
        /// <param name="key">      The key. </param>
        private IList<int> WriteIntegers( Database db, int count, int key )
        {
            List<int> values = new List<int>();
            for ( var i = 1; i <= count; i++ )
            {
                try
                {
                    values.Add( i );
                    _ = db.Put( key, i, TransactionPutOptions.AppendDuplicateData );
                }
                catch ( Exception e )
                {
                    this._Output.WriteLine( e.ToString() );
                }
            }
            return values;
        }


        /// <summary>  (Unit Test Method)  Could store integer key multi-value pairs. </summary>
        /// <remarks>   David, 2021-01-02. </remarks>
        [Fact]
        public void CouldStoreIntegerKeyMultiValuePairs()
        {
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 100 * 1024 * 1024 };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            using var db = env.OpenDatabase( "IntegerKeyValuePairs", TransactionBeginOptions.None,
                                            new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) );
            db.TruncateThrow();
            int count = 10;
            int key = 0;
            _ = this.WriteIntegers( db, count, key );

        }

        /// <summary>  (Unit Test Method)  Could enumerate integer key multi-value pairs. </summary>
        /// <remarks>   David, 2021-01-02. </remarks>
        [Fact]
        public void CouldEnumerateIntegerKeyMultiValuePairs()
        {
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 100 * 1024 * 1024 };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            using var db = env.OpenDatabase( "IntegerKeyValuePairs", TransactionBeginOptions.None,
                                            new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) );
            db.TruncateThrow();
            int count = 10;
            int key = 0;
            IList<int> values = this.WriteIntegers( db, count, key );

            int expectedKeyCount = 1;
            int expectedValueCount = count;
            int value = 0;
            int index = 0;
            using var txn = env.BeginReadOnlyTransaction();

            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Key Values Pairs Count";
            int actualKeyValuePairsCount = db.AsEnumerable<int, int>( txn ).Count();
            AssertX.Equal( expectedKeyCount, actualKeyValuePairsCount, testItemName );
            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Key value pairs";
            foreach ( var kvp in db.AsEnumerable<int, int>( txn ) )
            {
                value = values[index++];
                AssertX.Equal( $"[{key}, {value}]", $"[{ kvp.Key}, { kvp.Value}]", $"Key, value at {index}" );
            }

            index = 0;
            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Value Count for key = {key}";
            int actualKeyValuesCount = db.AsEnumerable<int, int>( txn, key ).Count();
            AssertX.Equal( expectedValueCount, actualKeyValuesCount, testItemName );
            foreach ( var actualValue in db.AsEnumerable<int, int>( txn, key ) )
            {
                value = values[index++];
                AssertX.Equal( value, actualValue, $"value at {index}" );
            }
        }

        /// <summary>   (Unit Test Method)  Could delete single integer key value pair. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void CouldDeleteSingleIntegerKeyValuePair()
        {
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 100 * 1024 * 1024 };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            using var db = env.OpenDatabase( "IntegerKeyValuePairs", TransactionBeginOptions.None,
                                            new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) );
            db.TruncateThrow();
            int count = 10;
            int key = 0;
            IList<int> values = this.WriteIntegers( db, count, key );

            int expectedKeyCount = 1;
            int expectedValueCount = count;

            // delete a single value
            int v = 5;
            env.Write( txn => {
                _ = db.Delete( txn, key, v );
                txn.Commit();
            } );
            expectedValueCount -= 1;
            Assert.True( values.Remove( v ) , $"Should remove value {v} from the list of values");

            int value = 0;
            int index = 0;
            using var txn = env.BeginReadOnlyTransaction();

            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Key Value Pairs Count";
            int actualKeyValuePairsCount = db.AsEnumerable<int, int>( txn ).Count();
            AssertX.Equal( expectedKeyCount, actualKeyValuePairsCount, testItemName );
            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Key value pairs";
            foreach ( var kvp in db.AsEnumerable<int, int>( txn ) )
            {
                value = values[index++];
                AssertX.Equal( $"[{key}, {value}]", $"[{ kvp.Key}, { kvp.Value}]", $"Key, value at {index}" );
            }

            index = 0;
            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Value Count for key = {key}";
            int actualKeyValuesCount = db.AsEnumerable<int, int>( txn, key ).Count();
            AssertX.Equal( expectedValueCount, actualKeyValuesCount, testItemName );
            foreach ( var actualValue in db.AsEnumerable<int, int>( txn, key ) )
            {
                value = values[index++];
                AssertX.Equal( value, actualValue, $"value at {index}" );
            }



        }

        /// <summary>   (Unit Test Method)  Could delete all integer key value pairs. </summary>
        /// <remarks>   David, 2021-01-20. </remarks>
        [Fact]
        public void CouldDeleteAllIntegerKeyValuePairs()
        {
            string testItemName;
            this._Output.WriteLine( LmdbVersionInfo.LmdbVersionMessage );
            var path = this.FolderFixture.CreateDirectoryForTest();
            using LmdbEnvironment env = new LmdbEnvironment() { MapSize = 100 * 1024 * 1024 };
            env.Open( path, EnvironmentOpenOptions.WriteMap | EnvironmentOpenOptions.NoSync );

            using var db = env.OpenDatabase( "IntegerKeyValuePairs", TransactionBeginOptions.None,
                                            new DatabaseConfiguration( DatabaseOpenOptions.Create | DatabaseOpenOptions.IntegerDuplicates ) );
            db.TruncateThrow();
            int count = 10;
            int key = 0;
            IList<int> values = this.WriteIntegers( db, count, key );

            int expectedKeyCount = 1;
            int expectedValueCount = count;

            // delete all values.
            env.Write( txn => {
                _ = db.Delete( txn, 0 );
                txn.Commit();
            } );
            values.Clear();
            expectedValueCount = 0;
            expectedKeyCount = 0;

            int value = 0;
            int index = 0;
            using var txn = env.BeginReadOnlyTransaction();

            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Key Value Pairs Count";
            int actualKeyValuePairsCount = db.AsEnumerable<int, int>( txn ).Count();
            AssertX.Equal( expectedKeyCount, actualKeyValuePairsCount, testItemName );
            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Key value pairs";
            foreach ( var kvp in db.AsEnumerable<int, int>( txn ) )
            {
                value = values[index++];
                AssertX.Equal( $"[{key}, {value}]", $"[{ kvp.Key}, { kvp.Value}]", $"Key, value at {index}" );
            }

            index = 0;
            testItemName = $"{nameof( Fusion.Database )}.{nameof( Fusion.Database.AsEnumerable )} Value Count for key = {key}";
            int actualKeyValuesCount = db.AsEnumerable<int, int>( txn, key ).Count();
            AssertX.Equal( expectedValueCount, actualKeyValuesCount, testItemName );
            foreach ( var actualValue in db.AsEnumerable<int, int>( txn, key ) )
            {
                value = values[index++];
                AssertX.Equal( value, actualValue, $"value at {index}" );
            }
        }

    }
}
