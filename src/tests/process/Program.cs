using System;
using System.Linq;
using System.Text;

using isr.Lmdb.Fusion;

namespace isr.Lmdb.Fusion.Tests.Process
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2020-12-26. </remarks>
    internal class Program
    {
        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2020-12-26. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        internal static void Main( string[] args )
        {
            string pathOption = "/path:";
            string outputOption = "/out:";
            var firstArgument = args.First();
            string path = firstArgument;
            string output = string.Empty;
            if ( firstArgument.StartsWith( pathOption ) )
                path = firstArgument[pathOption.Length..];
            else if ( firstArgument.StartsWith( outputOption ) )
                output = firstArgument[outputOption.Length..];

            if ( string.IsNullOrEmpty( output ) )
            {
                Console.WriteLine( ReadData( path ) );
            }
            else
            {
                Console.WriteLine( output );
            }
        }

        /// <summary>   Reads a data. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="path"> Full pathname of the file. </param>
        /// <returns>   The data. </returns>
        private static string ReadData( string path )
        {
            using var env = new LmdbEnvironment() { DirectoryPath = path };
            env.Open( EnvironmentOpenOptions.ReadOnly );
            byte[] results;
            using ( var tx = env.BeginTransaction( TransactionBeginOptions.ReadOnly ) )
            {
                using var db = tx.OpenDatabase();
                var (resultCode, key, value) = tx.Get( db, Encoding.UTF8.GetBytes( "hello" ) );
                results = value.CopyToNewArray();
                tx.Commit();
            }
            return Encoding.UTF8.GetString( results );
        }
    }
}

