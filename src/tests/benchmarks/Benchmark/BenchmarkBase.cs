using System;
using System.IO;

using BenchmarkDotNet.Attributes;

namespace isr.Lmdb.Fusion.Benchmarks
{
    /// <summary>   The benchmarks base. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    public abstract class BenchmarksBase
    {
        /// <summary>   Gets or sets the environment. </summary>
        /// <value> The environment. </value>
        public LmdbEnvironment Env { get; set; }
        /// <summary>   Gets or sets the database. </summary>
        /// <value> The database. </value>
        public Database DB { get; set; }

        /// <summary>   Pathname of the root directory. </summary>
        private const string _RootDirectory = "r:\\fusion";

        /// <summary>   Global setup. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [GlobalSetup]
        public void GlobalSetup()
        {
            Console.WriteLine( "Global Setup Begin" );

            string path = System.IO.Path.Combine( _RootDirectory, "Benchmark" );

            if ( Directory.Exists( path ) )
                Directory.Delete( path, true );

            this.Env = new LmdbEnvironment() { DirectoryPath = path , MaxDatabases = 1 };

            this.Env.Open();

            using ( var tx = this.Env.BeginTransaction() )
            {
                this.DB = tx.OpenDatabase();
                tx.Commit();
            }

            this.RunSetup();

            Console.WriteLine( "Global Setup End" );
        }

        /// <summary>   Executes the 'setup' operation. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        public abstract void RunSetup();

        /// <summary>   Global cleanup. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        [GlobalCleanup]
        public void GlobalCleanup()
        {
            Console.WriteLine( "Global Cleanup Begin" );

            try
            {
                this.DB.Dispose();
                this.Env.Dispose();
            }
            catch ( Exception ex )
            {
                Console.WriteLine( ex.ToString() );
            }
            Console.WriteLine( "Global Cleanup End" );
        }
    }

    /// <summary>   The benchmarks base. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    public abstract class RWBenchmarksBase : BenchmarksBase
    {
        //***** Argument Matrix Start *****//
        /// <summary>   Gets or sets the ops per transaction. </summary>
        /// <value> The ops per transaction. </value>
        [Params( 1, 100, 1000 )]
        public int OpsPerTransaction { get; set; }

        /// <summary>   Gets or sets the value size. </summary>
        /// <value> The size of the value. </value>
        [Params( 8, 64, 256 )]
        public int ValueSize { get; set; }

        /// <summary>   Gets or sets the key order. </summary>
        /// <value> The key order. </value>
        [Params( KeyOrdering.Sequential )]
        public KeyOrdering KeyOrder { get; set; }

        //***** Argument Matrix End *****//

        //***** Test Values Begin *****//

        /// <summary>   Gets or sets the buffer for value data. </summary>
        /// <value> A buffer for value data. </value>
        protected byte[] ValueBuffer { get; private set; }
        /// <summary>   Gets or sets the key buffers. </summary>
        /// <value> The key buffers. </value>
        protected KeyBatch KeyBuffers { get; private set; }

        //***** Test Values End *****//

        /// <summary>   Executes the 'setup' operation. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        public override void RunSetup()
        {
            this.ValueBuffer = new byte[this.ValueSize];
            this.KeyBuffers = KeyBatch.Generate( this.OpsPerTransaction, this.KeyOrder );
        }
    }
}
