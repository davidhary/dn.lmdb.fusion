using System.Collections.Generic;

using BenchmarkDotNet.Running;
using BenchmarkDotNet.Configs;

namespace isr.Lmdb.Fusion.Benchmarks
{
    /// <summary>   A program. </summary>
    /// <remarks>   David, 2021-01-14. </remarks>
    internal class Program
    {
        /// <summary>   Main entry-point for this application. </summary>
        /// <remarks>   David, 2021-01-14. </remarks>
        /// <param name="args"> An array of command-line argument strings. </param>
        private static void Main(string[] args)
        {
            var config = DefaultConfig.Instance.WithOptions( ConfigOptions.DisableOptimizationsValidator );
            if ( new List<string>( args ).Contains( "read" ) )
                _ = BenchmarkRunner.Run<ReadBenchmarks>( config );

            if ( new List<string>( args ).Contains( "write" ) )
                _ = BenchmarkRunner.Run<WriteBenchmarks>( config );
        }
    }
}
