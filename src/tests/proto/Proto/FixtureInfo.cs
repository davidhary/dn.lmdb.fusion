namespace isr.Lmdb.Fusion.Proto.Tests.Proto
{
    /// <summary>   Information about the fixture. </summary>
    /// <remarks>   David, 2020-12-14. </remarks>
    internal static class FixtureInfo
    {

        /// <summary>   Pathname of the environment test directory. </summary>
        public const string EnvDirName = @"env";

        /// <summary>   Name of the environment data directory. </summary>
        public const string DatabaseDirName = @"db";

        /// <summary>   The environment parent directory. </summary>
        /// <remarks> The Configuration manager takes some 100 ms to load. It seems that this issue was reported to the Visual Studio team. </remarks>
        public const string EnvParentDir = "r:\\fusion\\proto";

        /// <summary>   Creates fixture directory. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        /// <param name="subDirectory"> Pathname of the sub directory. </param>
        /// <returns>   The new fixture directory. </returns>
        public static string CreateFixtureDirectory( string subDirectory )
        {
            string path = System.IO.Path.Combine( FixtureInfo.EnvParentDir, subDirectory ); ;
            _ = System.IO.Directory.CreateDirectory( path );
            return path;
        }

    }
}
