using System;
using System.Collections.Generic;

namespace isr.Lmdb.Fusion.Proto.Tests
{
    /// <summary>   An order. </summary>
    /// <remarks>   Remark added by David, 2020-12-11. </remarks>
    public class Order
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   Remark added by David, 2020-12-11. </remarks>
        public Order()
        {
            this.LineItems = new List<LineItem>();
        }

        /// <summary>   Gets or sets the identifier. </summary>
        /// <value> The identifier. </value>
        public int Id { get; set; }

        /// <summary>   Gets or sets the identifier of the customer. </summary>
        /// <value> The identifier of the customer. </value>
        public int CustomerId { get; set; }

        /// <summary>   Gets the line items. </summary>
        /// <value> The line items. </value>
        public List<LineItem> LineItems { get; }
    }

}
