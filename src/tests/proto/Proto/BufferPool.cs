using System.Collections.Concurrent;

namespace isr.Lmdb.Fusion.Proto.Tests.Proto
{

    /// <summary>   A thread-safe <see cref="ConcurrentBag{byte[]}"/>. </summary>
    /// <remarks>   David, 2020-12-14. </remarks>
    public class BufferPool
    {

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        public BufferPool()
        {
            this._Pool = new ConcurrentBag<byte[]>();
        }

        /// <summary>   The pool of <see cref="ConcurrentBag{Byte[]}">buffers</see>. </summary>
        private readonly ConcurrentBag<byte[]> _Pool;

        /// <summary>
        /// Acquire a new or available <see cref="byte[]">buffer</see>
        /// matching the range of buffer sizes of <paramref name="minSize"/> and twice this size.
        /// </summary>
        /// <remarks>
        /// David, 2020-12-14. <para>
        /// Non-matching buffers are discarded, keeping the pool's buffers close to the desired buffer
        /// sizes;
        /// this works well when consecutive requests require similar buffer sizes. </para>
        /// </remarks>
        /// <param name="minSize">  The minimum size of the. </param>
        /// <returns>   A byte[]. </returns>
        public byte[] Acquire( int minSize )
        {
            return this.Acquire( minSize, 2 * minSize );
        }

        /// <summary>   Acquire a new or available <see cref="byte[]">buffer</see>
        /// matching the specified range of buffer sizes. </summary>
        /// <remarks>
        /// David, 2020-12-14. <para>
        /// Non-matching buffers are discarded, keeping the pool's buffers close to the desired buffer sizes;
        /// this works well when consecutive requests require similar buffer sizes. </para>
        /// </remarks>
        /// <param name="minSize">  The minimum size of the. </param>
        /// <param name="maxSize">  The maximum size of the. </param>
        /// <returns>   A byte[]. </returns>
        public byte[] Acquire( int minSize, int maxSize )
        {
            if ( !this._Pool.TryTake( out byte[] buffer ) || (buffer.Length < minSize || buffer.Length > maxSize) )
            {
                buffer = new byte[minSize];
            }
            return buffer;
        }

        /// <summary>
        /// Acquire a new or available <see cref="byte[]">buffer</see>
        /// matching the specified buffer size.
        /// </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        /// <param name="size"> The size. </param>
        /// <returns>   A byte[]. </returns>
        public byte[] AcquireExact( int size )
        {
            if ( !this._Pool.TryTake( out byte[] buffer ) || (buffer.Length != size) )
            {
                buffer = new byte[size];
            }
            return buffer;
        }

        /// <summary>   Adds the given buffer to the poll. </summary>
        /// <remarks>   David, 2020-12-14. </remarks>
        /// <param name="buffer">   The buffer to return. </param>
        public void Return( byte[] buffer )
        {
            this._Pool.Add( buffer );
        }

        /// <summary>   Gets the number of buffers in the pool. </summary>
        /// <value> The number of buffers in the pool. </value>
        public int Count => this._Pool.Count;
    }
}
